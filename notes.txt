Schriftstile
=============
text-header     Seitenüberschriften
				Weight: 200  (Light)
                Medium: 42pt
                
text-subheader  Überschriften innerhalb Seite
				Weight: 400  (normal)
				Medium: 20pt
				                
text-small-subheader  Unterüberschriften innerhalb Seite
				Weight: 600  (bold)
				Medium: 11pt   (wie text-body)

text-body	    Längere Inhalte
				Weight: 300
                Medium: 11pt
                
Farben
=======================
Zwei grundsätzliche Farbschemata: 
  lightbg - für hellen Hintergrund mit dunkler Schrift
  darkbg  - für dunklen Hintergrund mit heller Schrift

lightbg   #f3f3f3
darkbg    #000000

lightbg-content   #ffffff
darkbg-content    #0c0c0c


Schriftfarben
==============
text-normal		Standardfarbe für Text und Überschrift
text-highlight	Hervorhebungen, ggf. auch Überschriften
text-tertiary   Alpha 80% von text-normal
text-link       Linkfarbe


screenManager.show(page) -->  page.setScreenManager(screenManager)

screenManager.show(page, ScreenSkin)



ScreenManager - dafür da, verschiedene Overlays anzuzeigen (Screens und Dialoge)
NavigationView - ein Screen, in dem NavigableContent ausgetauscht wird
