package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.NavigationView;
import org.prelle.javafx.NodeWithCommandBar;

import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SkinBase;

/**
 * @author Stefan Prelle
 *
 */
public class ManagedScreenSkin extends SkinBase<ManagedScreen> {

	private final static Logger logger = LogManager.getLogger("prelle.jfx");

	private NavigationView navView;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ManagedScreenSkin(ManagedScreen screen) {
		super(screen);

		/*
		 * Create a new NavigationView for the screen
		 */
		navView = new NavigationView();
		navView.getItems().addAll(screen.getNavigationItems());
		navView.setContent(screen.getLandingPage().getContent());
		navView.setHeader(screen.getLandingPage().getTitle());
		navView.selectedItemProperty().addListener( (ov,o,n) -> screen.navigationItemChanged(o, n));
		navView.setOnBackAction(ev -> screen.getManager().impl_onBackRequested(screen));

		getChildren().add(navView);

		// Wait for updates
		screen.landingPageProperty().addListener( (ov,o,n) -> {
			logger.debug("landing page changed to "+screen.getLandingPage());
			if (getSkinnable().getVisiblePage()==null) {
				n.setManager(getSkinnable().getManager());
				getSkinnable().setVisiblePage(n);
			}
		});

		// Manager updates to enable
		screen.managerProperty().addListener( (ov,o,n) ->  {
			if (n!=null) {
				navView.backButtonVisibleProperty().set(!getChildren().isEmpty());
				navView.backButtonEnabledProperty().set(!getChildren().isEmpty());
			}
		});

		screen.getNavigationItems().addListener(new ListChangeListener<MenuItem>() {
			public void onChanged(Change<? extends MenuItem> c) {
				logger.debug("onChanged "+c);
//				navView.getItems().clear();
				navView.getItems().addAll(screen.getNavigationItems());

			}});
		
		screen.visiblePageProperty().addListener( (ov,o,n) -> {
			logger.debug("visiblePage changed from "+o+" to "+n);
			if (n!=null) {
				n.setManager(getSkinnable().getManager());
				navView.setHeader(n.getTitle());
				navView.setContent(n);
				n.setStyle("-fx-border-width: 5px; -fx-border-color: green;");
				logger.debug("child of "+n+" is "+((NodeWithCommandBar)n).getContent());
				((NodeWithCommandBar)n).getContent().setStyle("-fx-border-width: 5px; -fx-border-color: blue;");
			}
		});
	}

	//-------------------------------------------------------------------
	public NavigationView getNavigationView() {
		return navView;
	}

}
