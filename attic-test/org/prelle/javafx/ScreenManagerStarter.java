package org.prelle.javafx;
/**
 *
 */


import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ModernUI;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.fluent.NavigableContentNode;
import org.prelle.javafx.fluent.skin.NavigationViewClassicSkin;

import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class ScreenManagerStarter extends Application {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		ScreenManager navView = new ScreenManager();
		
		NavigableContentNode page1 = new NavigableContentNode() {
			ObjectProperty<Node> content = new SimpleObjectProperty<Node>(new Label("Hallo Welt"));
			StringProperty title = new SimpleStringProperty("Startseite");
			public String getTitle() {return title.get();}
			public String[] getStyleSheets() {return null;}
			public ObservableList<MenuItem> getStaticButtons() {
				ObservableList<MenuItem> list = FXCollections.observableArrayList();
				list.add(new MenuItem("Option 1", new Label("\uE083")));
				list.add(new MenuItem("Option 2", new Label("\uE084")));
				list.add(new MenuItem("Option 3", new Label("\uE085")));
				return list;
			}
			public Node getContent() { return content.get(); }
			public ReadOnlyStringProperty titleProperty() { return title; }
			public ReadOnlyObjectProperty<Node> contentProperty() { return content; }
			public CloseType backSelected() {	return CloseType.APPLY; }
		};
		
		NavigableContentNode page2 = new NavigableContentNode() {
			ObjectProperty<Node> content = new SimpleObjectProperty<Node>(new Label("Hallo Unterwelt"));
			StringProperty title = new SimpleStringProperty("Unterseite 1");
			public String getTitle() {return title.get();}
			public String[] getStyleSheets() {return null;}
			public ObservableList<MenuItem> getStaticButtons() {
				ObservableList<MenuItem> list = FXCollections.observableArrayList();
				list.add(new MenuItem("Option 1", new Label("\uE086")));
				list.add(new MenuItem("Option 2", new Label("\uE087")));
				return list;
			}
			public Node getContent() { return content.get(); }
			public ReadOnlyStringProperty titleProperty() { return title; }
			public ReadOnlyObjectProperty<Node> contentProperty() { return content; }
			public CloseType backSelected() {	return CloseType.APPLY; }
		};

		Scene scene = new Scene(navView,1200,600);
		scene.widthProperty().addListener( (ov,o,n) -> {
			WindowMode newMode = WindowMode.MINIMAL;
			if (scene.getWidth()<=640) newMode = WindowMode.MINIMAL;
			else if (scene.getWidth()<=1007) newMode = WindowMode.COMPACT;
			else newMode = WindowMode.EXPANDED;
			
				navView.setResponsiveMode(newMode);
		});
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
		primaryStage.show();

		ModernUI.initialize(scene);

		int value = 2;

		switch (value) {
		case 0:
			navView.show(page1);
			System.out.println("---calling showAlertAndCall");
			navView.showAlertAndCall(AlertType.ERROR, "Heading", new Label("Content\nReally!"));
//			navView.setBackEnabled(false);
			break;
		case 1:
			navView.show(page1);
			navView.setSkin(new NavigationViewClassicSkin(navView));
//			navView.setBackEnabled(true);
			break;
		case 2:
			navView.show(page1);
			navView.show(page2);
			break;
		}
	}

}
