package org.prelle.javafx;
/**
 *
 */


import org.prelle.javafx.CloseType;
import org.prelle.javafx.ModernUI;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.fluent.NavigableContentNode;
import org.prelle.javafx.fluent.NavigationView;

import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class NavigationViewStarter extends Application {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@SuppressWarnings("unused")
	@Override
	public void start(Stage primaryStage) throws Exception {
		NavigationView navView = new NavigationView();
		
		NavigableContentNode page1 = new NavigableContentNode() {
			public String getTitle() {return "Startseite";}
			public String[] getStyleSheets() {return null;}
			public ObservableList<MenuItem> getStaticButtons() {
				ObservableList<MenuItem> list = FXCollections.observableArrayList();
				list.add(new MenuItem("Option 1", new Label("\uE083")));
				list.add(new MenuItem("Option 2", new Label("\uE084")));
				list.add(new MenuItem("Option 3", new Label("\uE085")));
				return list;
			}
			public Node getContent() {
				return new Label("Hallo Welt");
			}
			public ReadOnlyStringProperty titleProperty() {
				return new SimpleStringProperty("Startseite");
			}
			@Override
			public ReadOnlyObjectProperty<Node> contentProperty() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public CloseType backSelected() {
				return CloseType.APPLY;
			}
		};
		
		NavigableContentNode page2 = new NavigableContentNode() {
			public String getTitle() {return "Unterseite 1";}
			public String[] getStyleSheets() {return null;}
			public ObservableList<MenuItem> getStaticButtons() {
				ObservableList<MenuItem> list = FXCollections.observableArrayList();
				list.add(new MenuItem("Option 1", new Label("\uE086")));
				list.add(new MenuItem("Option 2", new Label("\uE087")));
				return list;
			}
			public Node getContent() {
				return new Label("Hallo Unterwelt");
			}
			public ReadOnlyStringProperty titleProperty() {
				return new SimpleStringProperty("Unterseite 1");
			}
			@Override
			public ReadOnlyObjectProperty<Node> contentProperty() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public CloseType backSelected() {
				return CloseType.APPLY;
			}
		};

		int value = 3;

		switch (value) {
//		case 0:
//			navView.navigateTo(page1);
////			navView.setBackEnabled(false);
//			break;
//		case 1:
//			navView.navigateTo(page1);
//			navView.setSkin(new NavigationViewClassicSkin(navView));
////			navView.setBackEnabled(true);
//			break;
//		case 2:
//			navView.navigateTo(page1);
//			navView.navigateTo(page2);
//			break;
		case 3:
			navView.getNavPane().getItems().addAll(
					new MenuItem("Option 1", new Label("\uE083")),
					new MenuItem("Option 2", new Label("\uE084"))
					);
			navView.getNavPane().setBackEnabled(true);
			navView.setHeader("Dies ist der Titel");
			navView.setContent(new Label("Ein Label als Content"));
			break;
		}

		Scene scene = new Scene(navView,600,600);
		scene.widthProperty().addListener( (ov,o,n) -> {
			WindowMode newMode = WindowMode.MINIMAL;
			if (scene.getWidth()<=640) newMode = WindowMode.MINIMAL;
			else if (scene.getWidth()<=1007) newMode = WindowMode.COMPACT;
			else newMode = WindowMode.EXPANDED;
			navView.setResponsiveMode(newMode);
		});
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
		primaryStage.show();

		ModernUI.initialize(scene);
	}

}
