package org.prelle.javafx;
/**
 *
 */


import org.prelle.javafx.fluent.NavigationView;
import org.prelle.javafx.skin.ManagedScreenDialogSkin;
import org.prelle.javafx.skin.ManagedScreenStructuredSkin;

import javafx.application.Application;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class MyFluentStarter extends Application {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
//		StyleManager.getInstance().addUserAgentStylesheet("css/light.css");
		// TODO Auto-generated method stub

//		Image img = new Image(ClassLoader.getSystemResourceAsStream("example/testbild.png"));
//		Image img2 = new Image(ClassLoader.getSystemResourceAsStream("example/testbild2.png"));
		Image img = null;
		Image img2 = null;

		int value = 8	;

		Parent content = null;

		switch (value) {
//		case 0:
//			TiledCell<String,String> cell = new TiledCell<String,String>(null);
//			content = cell;
//			break;
//		case 1:
//			TiledListView<StringBuffer,Character> view = new TiledListView<>();
//			view.setCellFactory(new Callback<TiledListView<StringBuffer,Character>, TiledCell<StringBuffer,Character>>() {
//				public TiledCell<StringBuffer,Character> call(TiledListView<StringBuffer,Character> param) {
//					return  new TiledCell<StringBuffer,Character>(param){
//						   protected void updateItem(StringBuffer item, boolean empty) {
//							   logger.debug("####updateItem("+item+")");
//							   textProperty().set(item.toString());
//						   }
//					};
//				}
//			});
//			view.setSectionFactory(new Callback<StringBuffer, Character>() {
//				public Character call(StringBuffer param) {
//					return param.charAt(0);
//				}
//			});
//			StringBuffer val1 = new StringBuffer("Hallo");
//			StringBuffer val2 = new StringBuffer("Welt");
//			view.getItems().addAll(val1, val2);
//			val2.insert(0, "Aber ");
//			view.updateItem(val2);
////			view.getItems().remove(0);
//			content = view;
//			break;
//		case 2:
//			TiledListView<String,Character> view2 = new TiledListView<>();
////			view.setCellFactory(new Callback<TiledListView<String>, TiledCell<String>>() {
////
////				@Override
////				public TiledCell<String> call(TiledListView<String> param) {
////					// TODO Auto-generated method stub
////					return new TiledCell<String>(param);
////				}
////			});
//			view2.getItems().addAll("Hallo","Welt");
//			content = view2;
//			break;
		case 3:
			FlipControl flip = new FlipControl(Orientation.HORIZONTAL, true);
			Label bigger = new Label("Hallo\nWelt\nwie\ngeht\nes\nDir?");
			flip.getItems().addAll(new Button("1111"), new Label("2222"), bigger);
			flip.setOnMouseClicked(event -> System.out.println("Click"));
//			flip.itemsProperty().setValue(FXCollections.observableArrayList(new Label("4444"), new Label("2222"), new Label("3333")));
			FlowPane flow = new FlowPane();
			flow.getChildren().addAll(new Label("Label before"), flip, new Label("Label after"));
			content = flow;
			flow.setPrefSize(400, 400);
			break;
		case 4:
			ManagedScreen screen = new ManagedScreen();
			screen.setTitle("Heading");
			screen.setImage(img);
			screen.setContent(new Label("Hallo Welt"));
			screen.getNavigButtons().addAll(
					CloseType.PREVIOUS,
					CloseType.CANCEL
					);
			screen.getStaticButtons().addAll(
					new MenuItem("Ich bin ein MenuItem")
					);
//			screen.setSkin(new ManagedScreenDialogSkin(screen));
			screen.setSkin(new ManagedScreenStructuredSkin(screen));
			content = screen;
			break;
		case 5:
			screen = new ManagedScreen();
			screen.setImage(img);
			screen.setTitle("Screen below");
			screen.setContent(new Label("Content of screen below"));
			screen.getNavigButtons().addAll(
					CloseType.PREVIOUS,
					CloseType.CANCEL
					);
			screen.getStaticButtons().addAll(
					new MenuItem("Ich bin ein MenuItem")
					);
			screen.setSkin(new ManagedScreenStructuredSkin(screen));

			ManagedScreen screen2 = new ManagedScreen();
			screen2.setTitle("Dialog header");
			screen2.setImage(img2);
			screen2.setContent(new Label("Dialog content"));
			screen2.getNavigButtons().addAll(
					CloseType.NEXT,
					CloseType.APPLY
					);
			screen2.getStaticButtons().addAll(
					new MenuItem("Sollte ich sein?")
					);
			screen2.setSkin(new ManagedScreenDialogSkin(screen2));
			ScreenManager manager = new ScreenManager();
			manager.show(screen);
			manager.show(screen2);
			content = manager;
			break;
		case 6:
			MetroRadioButton mrb = new MetroRadioButton();
			mrb.setStateFormatter(new StringConverter<Boolean>() {
				public String toString(Boolean val) {
					return val?"Sie haben JA gewählt":"Dann halt nicht";
				}
				public Boolean fromString(String val) {
					return null;
				}
			});
			content = mrb;
			break;
//		case 7:
//			ListView<String> list1 = new ListView<>(FXCollections.observableArrayList("Hallo","Welt","Wie","geht","es","dir","heute","Abend?"));
//			list1.setSkin(new GridListViewSkin<String>(list1));
//			list1.setStyle("-fx-pref-width: 800px; -fx-pref-height: 600px");
//			list1.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
//				public ListCell<String> call(ListView<String> param) {
//					// TODO Auto-generated method stub
//					return new ListCell<String>() {
//			            @Override public void updateItem(String item, boolean empty) {
//			                super.updateItem(item, empty);
//
//			                if (empty) {
//			                    setText(null);
//			                    setGraphic(null);
//			                } else {
//			                	String color = null;
//			                	switch (item.hashCode()%5) {
//			                	case 0: color="#FF4040"; break;
//			                	case 1: color="#40FF40"; break;
//			                	case 2: color="#4040FF"; break;
//			                	case 3: color="#FFFF40"; break;
//			                	case 4: color="#40FFFF"; break;
//			                	}
//			                	setStyle("-fx-background: "+color);
//			                    setText(item == null ? "null" : item.toString());
//			                    setGraphic(null);
//			                }
//			            }
//						
//					};
//				}
//			});
//			content = list1;
//			break;
		case 8:
			NavigationView navView = new NavigationView();
			navView.getNavPane().getItems().add(new MenuItem("Option 1", new Label("\uE07D")));
			navView.widthProperty().addListener( (ov,o,n) -> {
				WindowMode newMode = WindowMode.MINIMAL;
				if (navView.getWidth()<=640) newMode = WindowMode.MINIMAL;
				else if (navView.getWidth()<=1007) newMode = WindowMode.COMPACT;
				else newMode = WindowMode.EXPANDED;
				
					navView.setWindowMode(newMode);
			});

			content = navView;
			break;
		}

		Effect frostEffect =  new BoxBlur(10, 10, 3);
		
		System.out.println("Transparency = "+Platform.isSupported(ConditionalFeature.TRANSPARENT_WINDOW));
		
		Rectangle rect = new Rectangle(400,400);
		rect.setEffect(frostEffect);
		rect.setFill(Color.rgb(128, 128, 255, 0.6));
		
//		StackPane stack = new StackPane(rect, label);
		
		Scene scene = new Scene(content, 800, 600);
//		scene.setFill(Color.rgb(255, 128, 12, 0.5));
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
//		scene.setFill(Color.TRANSPARENT);
//		primaryStage.initStyle(StageStyle.TRANSPARENT);
		primaryStage.show();

//		primaryStage.setOpacity(0.6);
		scene.getStylesheets().add(ClassLoader.getSystemResource("css/fluent.css").toString());
		ModernUI.initialize(scene);

		System.out.println("Showing "+content);
	}

}
