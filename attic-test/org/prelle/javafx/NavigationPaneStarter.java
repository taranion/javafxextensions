package org.prelle.javafx;
/**
 *
 */


import org.prelle.javafx.ModernUI;
import org.prelle.javafx.fluent.NavigationPane;
import org.prelle.javafx.fluent.NavigationPane.FoldingMode;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class NavigationPaneStarter extends Application {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		NavigationPane navView = new NavigationPane();
		
		navView.itemsProperty().set( FXCollections.observableArrayList(
				new MenuItem("Option 1", new Label("\uE083")),
				new NavigationPane.SpacingMenuItem(),
				new MenuItem("Überschrift", null),
				new MenuItem("Option 2", new Label("\uE084")),
				new MenuItem("Option 3", new Label("\uE085"))
				));

		int value = 3; // 4 = OPEN; 2 = Settings, 1 = Back

		boolean open = (value&4)>0;
		boolean settings = (value&2)>0;
		boolean back = (value&1)>0;
		System.out.println("open="+open+"  settings="+settings+"   back="+back);

		navView.setBackEnabled(back);
		navView.setSettingsEnabled(settings);
		navView.setDisplayMode(open?FoldingMode.OPEN:FoldingMode.FOLDED);

		Scene scene = new Scene(navView,600,600);
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
		primaryStage.show();

		ModernUI.initialize(scene);
	}

}
