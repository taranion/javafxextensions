package org.prelle.javafx;
/**
 *
 */


import org.prelle.javafx.fluent.CommandBar;
import org.prelle.javafx.fluent.NavigationPane;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class CommandBarStarter extends Application {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		CommandBar control = new CommandBar();
		
		control.itemsProperty().set( FXCollections.observableArrayList(
				new MenuItem("Option 1", new Label("\uE083")),
				new NavigationPane.SpacingMenuItem(),
				new MenuItem("Überschrift", null),
				new MenuItem("Option 2", new Label("\uE084")),
				new MenuItem("Option 3", new Label("\uE085"))
				));

		Scene scene = new Scene(control,600,600);
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);
		primaryStage.show();

		ModernUI.initialize(scene);
	}

}
