package org.prelle.javafx;
/**
 *
 */


import org.prelle.javafx.fluent.NavigationPane;
import org.prelle.javafx.fluent.NavigationPane.FoldingMode;
import org.prelle.javafx.fluent.NavigationView;
import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.javafx.skin.ManagedScreenDialogSkin;
import org.prelle.javafx.skin.ManagedScreenStructuredSkin;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.util.Pair;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class MyJafaFXExtStarter extends Application {

	//--------------------------------------------------------------------
	public static void main(String[] args) {
		launch(args);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws Exception {
//		StyleManager.getInstance().addUserAgentStylesheet("css/light.css");
		// TODO Auto-generated method stub

//		Image img = new Image(ClassLoader.getSystemResourceAsStream("example/testbild.png"));
//		Image img2 = new Image(ClassLoader.getSystemResourceAsStream("example/testbild2.png"));
		Image img = null;
		Image img2 = null;

		int value = 10;

		Parent content = null;

		switch (value) {
//		case 0:
//			TiledCell<String,String> cell = new TiledCell<String,String>(null);
//			content = cell;
//			break;
//		case 1:
//			TiledListView<StringBuffer,Character> view = new TiledListView<>();
//			view.setCellFactory(new Callback<TiledListView<StringBuffer,Character>, TiledCell<StringBuffer,Character>>() {
//				public TiledCell<StringBuffer,Character> call(TiledListView<StringBuffer,Character> param) {
//					return  new TiledCell<StringBuffer,Character>(param){
//						   protected void updateItem(StringBuffer item, boolean empty) {
//							   logger.debug("####updateItem("+item+")");
//							   textProperty().set(item.toString());
//						   }
//					};
//				}
//			});
//			view.setSectionFactory(new Callback<StringBuffer, Character>() {
//				public Character call(StringBuffer param) {
//					return param.charAt(0);
//				}
//			});
//			StringBuffer val1 = new StringBuffer("Hallo");
//			StringBuffer val2 = new StringBuffer("Welt");
//			view.getItems().addAll(val1, val2);
//			val2.insert(0, "Aber ");
//			view.updateItem(val2);
////			view.getItems().remove(0);
//			content = view;
//			break;
//		case 2:
//			TiledListView<String,Character> view2 = new TiledListView<>();
////			view.setCellFactory(new Callback<TiledListView<String>, TiledCell<String>>() {
////
////				@Override
////				public TiledCell<String> call(TiledListView<String> param) {
////					// TODO Auto-generated method stub
////					return new TiledCell<String>(param);
////				}
////			});
//			view2.getItems().addAll("Hallo","Welt");
//			content = view2;
//			break;
		case 3:
			FlipControl flip = new FlipControl(Orientation.HORIZONTAL, true);
			Label bigger = new Label("Hallo\nWelt\nwie\ngeht\nes\nDir?");
			flip.getItems().addAll(new Button("1111"), new Label("2222"), bigger);
			flip.setOnMouseClicked(event -> System.out.println("Click"));
//			flip.itemsProperty().setValue(FXCollections.observableArrayList(new Label("4444"), new Label("2222"), new Label("3333")));
			FlowPane flow = new FlowPane();
			flow.getChildren().addAll(new Label("Label before"), flip, new Label("Label after"));
			content = flow;
			flow.setPrefSize(400, 400);
			break;
		case 4:
			ManagedScreen screen = new ManagedScreen();
			screen.setTitle("Heading");
			screen.setImage(img);
			screen.setContent(new Label("Hallo Welt"));
			screen.getNavigButtons().addAll(
					CloseType.PREVIOUS,
					CloseType.CANCEL
					);
			screen.getStaticButtons().addAll(
					new MenuItem("Ich bin ein MenuItem")
					);
//			screen.setSkin(new ManagedScreenDialogSkin(screen));
			screen.setSkin(new ManagedScreenStructuredSkin(screen));
			content = screen;
			break;
		case 5:
			screen = new ManagedScreen();
			screen.setImage(img);
			screen.setTitle("Screen below");
			screen.setContent(new Label("Content of screen below"));
			screen.getNavigButtons().addAll(
					CloseType.PREVIOUS,
					CloseType.CANCEL
					);
			screen.getStaticButtons().addAll(
					new MenuItem("Ich bin ein MenuItem")
					);
			screen.setSkin(new ManagedScreenStructuredSkin(screen));

			ManagedScreen screen2 = new ManagedScreen();
			screen2.setTitle("Dialog header");
			screen2.setImage(img2);
			screen2.setContent(new Label("Dialog content"));
			screen2.getNavigButtons().addAll(
					CloseType.NEXT,
					CloseType.APPLY
					);
			screen2.getStaticButtons().addAll(
					new MenuItem("Sollte ich sein?")
					);
			screen2.setSkin(new ManagedScreenDialogSkin(screen2));
			ScreenManager manager = new ScreenManager();
			manager.show(screen);
			manager.show(screen2);
			content = manager;
			break;
		case 6:
			MetroRadioButton mrb = new MetroRadioButton();
			mrb.setStateFormatter(new StringConverter<Boolean>() {
				public String toString(Boolean val) {
					return val?"Sie haben JA gewählt":"Dann halt nicht";
				}
				public Boolean fromString(String val) {
					return null;
				}
			});
			content = mrb;
			break;
		case 7:
			NavigationPane navPane1 = new NavigationPane();
			MenuItem switchMode = new MenuItem("Switch", new Label("\u2192"));
			navPane1.getItems().add(switchMode);
			navPane1.getItems().add(new MenuItem(null, new Label("\u2631")));
			navPane1.getItems().add(new MenuItem("Alarms", new Label("\u2611")));
			navPane1.getItems().add(new MenuItem("Devices", new Label("\u2618")));
			navPane1.getItems().add(new NavigationPane.SpacingMenuItem());
			navPane1.getItems().add(new MenuItem("Something", new Label("\u2617")));
			navPane1.setSettingsEnabled(true);
			navPane1.setBackEnabled(true);
			navPane1.setDisplayMode(FoldingMode.OPEN);
			switchMode.setOnAction(ev -> navPane1.setDisplayMode((navPane1.getDisplayMode()==FoldingMode.OPEN)?FoldingMode.FOLDED:FoldingMode.OPEN));

			Region buffer7 = new Region();
			buffer7.setMaxWidth(Double.MAX_VALUE);
			HBox box7 = new HBox(navPane1, buffer7);
			HBox.setHgrow(buffer7, Priority.ALWAYS);
			content = box7;
			break;
		case 8:
			navPane1 = new NavigationPane();
			navPane1.getItems().add(new MenuItem(null, new Label("\u2190")));
			navPane1.getItems().add(new MenuItem(null, new Label("\u2630")));
			navPane1.getItems().add(new MenuItem("Alarme", new Label("\u2612")));
			navPane1.getItems().add(new MenuItem("Geräte", new Label("\u2614")));
			navPane1.getItems().add(new NavigationPane.SpacingMenuItem());
			navPane1.getItems().add(new MenuItem("Settings", new Label("\u2616")));
			navPane1.setDisplayMode(FoldingMode.FOLDED);
			NavigationPane navPane2 = new NavigationPane();
			navPane2.getItems().add(new MenuItem(null, new Label("\u2190")));
			navPane2.getItems().add(new MenuItem(null, new Label("\u2630")));
			navPane2.getItems().add(new MenuItem("Alarme", new Label("\u2612")));
			navPane2.getItems().add(new MenuItem("Geräte", new Label("\u2614")));
			navPane2.getItems().add(new NavigationPane.SpacingMenuItem());
			navPane2.getItems().add(new MenuItem("BlaBla", new Label("\u2616")));
			navPane2.setDisplayMode(FoldingMode.OPEN);

			Region buffer = new Region();
			HBox.setHgrow(buffer, Priority.ALWAYS);
			HBox box1 = new HBox(navPane1, buffer, navPane2);
			box1.setPrefSize(400, 400);
			box1.setStyle("-fx-background-image: url('foo/background.jpg'); -fx-text-fill: yellow");
			content = box1;
			break;
		case 9:
			NavigationView navView = new NavigationView();
//			navView.setBackEnabled(true);
			navView.setHeader("Dies ist die Überschrift");
			navView.getNavPane().getItems().add(new MenuItem("Alarme", new Label("\uE1A4")));
			navView.getNavPane().getItems().add(new MenuItem("Geräte", new Label("\u2614")));
			navView.getNavPane().getItems().add(new NavigationPane.SpacingMenuItem());
			navView.getNavPane().getItems().add(new MenuItem("Settings", new Label("\uE115")));
			navView.setWindowMode(WindowMode.EXPANDED);
			navView.setPrefSize(500, 400);
			navView.setStyle("-fx-background-image: url('foo/background.jpg'); -fx-background-color: yellow");
			Label navContent= new Label("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.");
			navContent.setWrapText(true);
			navContent.setStyle("-fx-font-size: 120%; -fx-background-color: white");
			navContent.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			navView.setContent(navContent);

			manager = new ScreenManager();
//			manager.show(navView);
			content = manager;

			break;
		case 10:
			TableView<Pair<String, Integer>> grid = new TableView<>();
			grid.setSkin(new GridPaneTableViewSkin<>(grid));
			TableColumn<Pair<String,Integer>, String> colStr = new TableColumn<>("String-Col");
			TableColumn<Pair<String,Integer>, Number> colInt = new TableColumn<>("Integer-Col");
			grid.getColumns().addAll(colStr, colInt);
			colStr.setCellValueFactory(celldata -> new SimpleStringProperty(celldata.getValue().getKey()));
			colInt.setCellValueFactory(celldata -> new SimpleIntegerProperty(celldata.getValue().getValue()));

			grid.getItems().add(new Pair<String, Integer>("Hello World", 815));
			grid.getItems().add(new Pair<String, Integer>("Hallo Welt", 4711));
			content = grid;
			break;
		}

		Scene scene = new Scene(content, -1,-1);
		primaryStage.setTitle("Test");
		primaryStage.setScene(scene);

//		if (content instanceof ScreenManager) {
//			primaryStage.widthProperty().addListener( (ov,o,n) -> ((ScreenManager)content).);
//		}
		primaryStage.show();

		ModernUI.initialize(scene);

		System.out.println("Showing "+content);
	}

}
