module javafx.extensions {
	exports org.prelle.javafx;
	exports org.prelle.javafx.layout;
	exports org.prelle.javafx.skin;
	exports org.prelle.javafx.fxml;

	opens org.prelle.javafx;

	requires javafx.base;
	requires transitive javafx.controls;
	requires javafx.fxml;
	requires transitive javafx.graphics;
	requires org.apache.logging.log4j;
}