/**
 *
 */
package org.prelle.javafx;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.WizardSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class Wizard extends ManagedDialog {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	protected ObservableList<WizardPage> pages;

	private ObjectProperty<WizardPage> currentPage  = new SimpleObjectProperty<WizardPage>();
	private Callback<Wizard, Boolean> canBeFinishedCallback;
	private Callback<Wizard, Boolean> confirmCancelCallback;

	private WizardBehaviour behaviour;
	private Map<CloseType, EventHandler<ActionEvent>> otherButtonHandlers;

	//--------------------------------------------------------------------
	public Wizard() {
		super(null, null, CloseType.CANCEL, CloseType.PREVIOUS, CloseType.NEXT, CloseType.FINISH);
		pages = FXCollections.observableArrayList();
		otherButtonHandlers = new HashMap<CloseType, EventHandler<ActionEvent>>();
		behaviour = new WizardBehaviour(this);
		// Setting the skin must be done before listening
		setSkin(new WizardSkin(this, behaviour));

		pages.addListener(new ListChangeListener<WizardPage>() {
			public void onChanged(Change<? extends WizardPage> c) {
				if (getPages().size()>0 && currentPage.get()==null) {
					logger.debug("Default current page");
					currentPage.set(getPages().get(0));
				}
			}});


	}

	//--------------------------------------------------------------------
	public Wizard(WizardPage...pages) {
		this();
		this.pages.addAll(pages);
	}

	//--------------------------------------------------------------------
	public ObjectProperty<WizardPage> currentPageProperty() { return currentPage; }
	public WizardPage  getCurrentPage() { return currentPage.get(); }
	public void setCurrentPage(WizardPage value) { currentPage.set(value); }

	//--------------------------------------------------------------------
	public boolean canBeFinished() {
		if (canBeFinishedCallback!=null)
			return canBeFinishedCallback.call(this);
		return !behaviour.hasNextPage();
	}

	//--------------------------------------------------------------------
	public ObservableList<WizardPage> getPages() {
		return pages;
	}

	//--------------------------------------------------------------------
	public boolean cancelConfirmed() {
		if (confirmCancelCallback!=null)
			return confirmCancelCallback.call(this);
		return true;
	}

	//--------------------------------------------------------------------
	public void setConfirmCancelCallback(Callback<Wizard, Boolean> callback) {
		this.confirmCancelCallback = callback;
	}

	//--------------------------------------------------------------------
	public void refresh() {
		((WizardSkin)getSkin()).manageButtons();
	}

	//--------------------------------------------------------------------
	public void addExtraButton(CloseType type, EventHandler<ActionEvent> handler) {
		otherButtonHandlers.put(type, handler);
		buttons.add(type);
	}

	//--------------------------------------------------------------------
	void handleExtraButton(CloseType type, ActionEvent event) {
		EventHandler<ActionEvent> handler = otherButtonHandlers.get(type);
		if (handler!=null) {
			if (logger.isTraceEnabled())
				logger.trace("Call "+handler);
			handler.handle(event);
		} else
			logger.warn("No EventHandler for "+type+" defined");
	}

	//--------------------------------------------------------------------
	public boolean isDisabled(CloseType save) {
		return false;
	}

}
