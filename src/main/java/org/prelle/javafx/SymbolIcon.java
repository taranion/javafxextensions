/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.SymbolIconSkin;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;

/**
 * @author Stefan Prelle (stefan@prelle.org)
 *
 */
public class SymbolIcon extends IconElement {

	private static final String DEFAULT_STYLE_CLASS = "symbol-icon";
	
	@FXML
	private StringProperty symbolProperty = new SimpleStringProperty();

	//-------------------------------------------------------------------
	public SymbolIcon() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		setSkin(new SymbolIconSkin(this));
		setMouseTransparent(true);
	}

	//-------------------------------------------------------------------
	public SymbolIcon(String value) {
		this();
		symbolProperty.set(value);
	}

	/******************************************
	 * Property SYMBOL
	 ******************************************/
	public StringProperty symbolProperty() { return symbolProperty; }
	public String getSymbol() { return symbolProperty.get(); }
	public void setSymbol(String value) { symbolProperty.set(value); }

}
