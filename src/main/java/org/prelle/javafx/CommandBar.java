/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.CommandBarSkin;

import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;

/**
 * <img src="doc-files/CommandBar.png" /><br/>
 * <em>Command bar in the open state</em>
 * 
 * <pre>
 * &lt;CommandBar xmlns:fx="http://javafx.com/fxml" stylesheets="org/prelle/javafx/css/fluent.css" &gt;
   &lt;content&gt;
     &lt;Label text="Now playing ..." /&gt;
   &lt;/content&gt;

   &lt;AppBarToggleButton text="%button.shuffle" icon="Shuffle"/&gt;
   &lt;AppBarToggleButton text="%button.repeat" icon="RepeatAll"/&gt;
   &lt;AppBarSeperator /&gt;
   &lt;AppBarButton text="%button.back" icon="Back"/&gt;
   &lt;AppBarButton text="%button.stop" icon="Stop"/&gt;
   &lt;AppBarButton text="%button.play" icon="Play"/&gt;
   &lt;AppBarButton text="%button.forward" icon="Forward"/&gt;

 * &lt;/CommandBar&gt;
 * </pre>
 * 
 * @author Stefan Prelle
 * @see <a href="https://docs.microsoft.com/en-us/windows/uwp/design/controls-and-patterns/app-bars">Microsoft UWP docs</a>
 */
@DefaultProperty(value="primaryCommands")
public class CommandBar extends Control implements ResponsiveControl {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	/**
	 * Defines how the command bar shall be displayed to the user
	 */
	public enum DisplayState {
		/** Shows content, primary command icons without labels, and the "see more" [•••] button. */
		COMPACT,
		/** Shows only a thin bar that acts as the "see more" [•••] button. The user can press anywhere on the bar to open it. */
		MINIMAL,
		/** 
		 * The command bar is not shown when it's closed. This can be useful
		 * for showing contextual commands with an inline command bar. In 
		 * this case, you must open the command bar programmatically by 
		 * setting the IsOpen property or changing the ClosedDisplayMode to
		 * Minimal or Compact. */
		HIDDEN
	}
	
	private static final String DEFAULT_STYLE_CLASS = "command-bar";
	
	@FXML
	private ObjectProperty<ObservableList<MenuItem>> primaryCommandsProperty = new SimpleObjectProperty<ObservableList<MenuItem>>(FXCollections.observableArrayList());
	@FXML
	private ObjectProperty<Node> contentProperty = new SimpleObjectProperty<Node>();
//	private ObjectProperty<WindowMode> modeProperty = new SimpleObjectProperty<WindowMode>(WindowMode.COMPACT);
	/**
	 * Content Display for MenuItems
	 */
	@FXML
	private ObjectProperty<ContentDisplay> contentDisplayProperty = new SimpleObjectProperty<ContentDisplay>(ContentDisplay.LEFT);
	@FXML
	private ObjectProperty<ObservableList<MenuItem>> secondaryCommandsProperty = new SimpleObjectProperty<ObservableList<MenuItem>>(FXCollections.observableArrayList());
	
	private transient BooleanProperty openProperty = new SimpleBooleanProperty(false); 
	private transient ObjectProperty<DisplayState> displayStateProperty = new SimpleObjectProperty<DisplayState>();

	//-------------------------------------------------------------------
	public CommandBar() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new CommandBarSkin(this));
	}

	/******************************************
	 * Property CONTENT
	 ******************************************/
	public ObjectProperty<Node> contentProperty() { return contentProperty; }
	public Node getContent() { return contentProperty.get(); }
	public CommandBar setContent(Node value) { contentProperty.set(value); return this;}

	/******************************************
	 * Property CONTENT DISPLAY
	 ******************************************/
	public ObjectProperty<ContentDisplay> contentDisplayProperty() { return contentDisplayProperty; }
	public ContentDisplay getContentDisplay() { return contentDisplayProperty.get(); }
	public CommandBar setContentDisplay(ContentDisplay value) { contentDisplayProperty.set(value); return this;}

	/*-*****************************************
	 * Property PRIMARY COMMANDS
	 *-*****************************************/
	public final ObservableList<MenuItem> getPrimaryCommands() { return primaryCommandsProperty.get(); }
	public final ObjectProperty<ObservableList<MenuItem>> primaryCommandsProperty() { return primaryCommandsProperty; }

	/*-*****************************************
	 * Property SECONDARY COMMANDS
	 *-*****************************************/
	public final ObservableList<MenuItem> getSecondaryCommands() { return secondaryCommandsProperty.get(); }
	public final ObjectProperty<ObservableList<MenuItem>> secondaryCommandsProperty() { return secondaryCommandsProperty; }

	/*-*****************************************
	 * Property OPEN
	 *-*****************************************/
	public BooleanProperty openProperty() { return openProperty; }
	public boolean isOpen() { return openProperty.get(); }
	public void setOpen(boolean value) { openProperty.set(value); }

	/*-*****************************************
	 * Property DISPLAY_STATE
	 *-*****************************************/
	public ObjectProperty<DisplayState> displayStateProperty() { return displayStateProperty; }
	public DisplayState getDisplayState() { return displayStateProperty.get(); }
	public void setDisplayState(DisplayState value) { displayStateProperty.set(value); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		switch (value) {
		case COMPACT:
		case MINIMAL:
			openProperty.set(false);
			break;
		case EXPANDED:
			openProperty.set(true);
			break;
		}
	}

}
