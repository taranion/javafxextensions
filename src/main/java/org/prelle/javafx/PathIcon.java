/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.PathIconSkin;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Skin;

/**
 * @author spr
 *
 */
public class PathIcon extends IconElement {

	private static final String DEFAULT_STYLE_CLASS = "path-icon";
	
	@FXML
	private StringProperty dataProperty = new SimpleStringProperty();

	//-------------------------------------------------------------------
	public PathIcon() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		setSkin(new PathIconSkin(this));
		setMouseTransparent(true);
	}

	//-------------------------------------------------------------------
	public PathIcon(String data) {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		dataProperty.set(data);
	}

	//-------------------------------------------------------------------
	@Override
	public Skin<?> createDefaultSkin() {
		return new PathIconSkin(this);
	}

	/******************************************
	 * Property ICON
	 ******************************************/
	public StringProperty dataProperty() { return dataProperty; }
	public String getData() { return dataProperty.get(); }
	public void setData(String value) { dataProperty.set(value); }

}
