/**
 * 
 */
package org.prelle.javafx;

/**
 * @author prelle
 *
 */
public interface ScreenManagerProvider {

	public ScreenManager getScreenManager();
	
}
