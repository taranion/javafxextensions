package org.prelle.javafx;

import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class AttentionMenuItem extends MenuItem implements IAttentionSeeker {
	
	private BooleanProperty attentionFlag = new SimpleBooleanProperty();
	private ObservableList<String> tooltip = FXCollections.observableArrayList();

	//-------------------------------------------------------------------
	public AttentionMenuItem() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param text
	 */
	public AttentionMenuItem(String text) {
		super(text);
	}

	//-------------------------------------------------------------------
	/**
	 * @param text
	 * @param graphic
	 */
	public AttentionMenuItem(String text, Node graphic) {
		super(text, graphic);
	}

	//-------------------------------------------------------------------
	public BooleanProperty attentionFlagProperty() {
		return attentionFlag;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#setAttentionFlag(boolean)
	 */
	@Override
	public void setAttentionFlag(boolean requireAttention) {
		attentionFlag.set(requireAttention);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#setAttentionToolTip(java.util.List)
	 */
	@Override
	public void setAttentionToolTip(List<String> list) {
		tooltip.clear();
		tooltip.addAll(list);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#attentionToolTipProperty()
	 */
	@Override
	public ObservableList<String> attentionToolTipProperty() {
		// TODO Auto-generated method stub
		return tooltip;
	}
}
