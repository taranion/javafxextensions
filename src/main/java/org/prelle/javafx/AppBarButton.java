/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

/**
 * A button, where the text is visible depending on the compact state of the {@link CommandBar} it appears in.
 * 
 * <pre class="fxml">
 *     &lt;AppBarButton text="SymbolIcon" icon="Like" /&gt;
 * 	&lt;AppBarButton text="BitmapIcon"&gt;
 * 		&lt;icon&gt;
 * 			&lt;BitmapIcon UriSource="/org/prelle/javafx/crosshair.png"/&gt;
 * 		&lt;/icon&gt;
 * 	&lt;/AppBarButton&gt;
 *     &lt;AppBarButton text="FontIcon"&gt;
 *         &lt;icon&gt;
 *             &lt;FontIcon FontFamily="Caladea" Glyph="&amp;#x03A3;"/&gt;
 *         &lt;/icon&gt;
 *     &lt;/AppBarButton&gt;
 *     &lt;AppBarButton text="PathIcon"&gt;
 *         &lt;icon&gt;
 *             &lt;PathIcon Data="M 16,12 20,2L 20,16 1,16" /&gt;
 *         &lt;/icon&gt;
 *     &lt;/AppBarButton&gt;
 * </pre>
 * <em>FXML example of buttons with all four icon types</em>
 * 
 * @author Stefan Prelle
 *
 */
public class AppBarButton extends MenuItem implements ICommandBarElement {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private static final String DEFAULT_STYLE_CLASS = "app-bar-button";
	
	@FXML
	private ObjectProperty<IconElement> iconProperty = new SimpleObjectProperty<IconElement>();
	
	private transient BooleanProperty compactProperty = new SimpleBooleanProperty(false); 

	//-------------------------------------------------------------------
	public AppBarButton() {
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
		super.graphicProperty().bind(iconProperty);
//		super.setContentDisplay(ContentDisplay.TOP);
		
//		if (compactProperty.get()) {
//			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//		} else {
//			setContentDisplay(ContentDisplay.TOP);
//		}
		compactProperty.addListener( (ov,o,n) -> {
			logger.debug("compact of "+this+" is "+n);
//			if (n==Boolean.TRUE) {
//				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//			} else {
//				setContentDisplay(ContentDisplay.TOP);
//			}
		});
	}

	//-------------------------------------------------------------------
	public AppBarButton(String text, IconElement icon) {
		this();
		setText(text);
		setIcon(icon);
	}

	/*-*****************************************
	 * Property ICON
	 *-*****************************************/
	public ObjectProperty<IconElement> iconProperty() { return iconProperty; }
	public IconElement getIcon() { return iconProperty.get(); }
	public AppBarButton setIcon(IconElement value) { iconProperty.set(value); return this; }

	/*-*****************************************
	 * Property COMPACT
	 *-*****************************************/
	public BooleanProperty compactProperty() { return compactProperty; }
	public boolean isCompact() { return compactProperty.get(); }
	public void setCompact(boolean value) { compactProperty.set(value); }

}
