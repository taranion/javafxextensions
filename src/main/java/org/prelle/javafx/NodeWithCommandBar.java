package org.prelle.javafx;

import org.prelle.javafx.skin.NodeWithCommandBarSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Control;

/**
 * @author Stefan Prelle
 *
 */
public class NodeWithCommandBar extends Control implements ScreenManagerProvider, IHasCommandBar {
	
	@FXML
	private ObjectProperty<CommandBar> commandBar = new SimpleObjectProperty<CommandBar>(new CommandBar());
	@FXML
	protected SimpleObjectProperty<Node> content = new SimpleObjectProperty<Node>();
	/** The manager who currently displays the screen */
	private ObjectProperty<ScreenManager>  manager = new SimpleObjectProperty<ScreenManager>();

	//-------------------------------------------------------------------
	public NodeWithCommandBar() {
		commandBar.get().setMaxWidth(Double.MAX_VALUE);
		setSkin(new NodeWithCommandBarSkin(this));
	}

	//-------------------------------------------------------------------
	@Override
	public CommandBar getCommandBar() { return commandBar.get(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IHasCommandBar#commandBarProperty()
	 */
	@Override
	public ReadOnlyObjectProperty<CommandBar> commandBarProperty() {
		return commandBar;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#getContent()
	 */
	public Node getContent() { return content.get(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#setContent(javafx.scene.Node)
	 */
	public void setContent(Node value) { content.setValue(value); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#contentProperty()
	 */
	public ReadOnlyObjectProperty<Node> contentProperty() { return content; }

	//-------------------------------------------------------------------
	public ObjectProperty<ScreenManager> managerProperty() { return manager; }
	public void setManager(ScreenManager value) {manager.setValue(value); }
	public ScreenManager getManager() { return manager.get(); }
	public ScreenManager getScreenManager() { return manager.get(); }

}
