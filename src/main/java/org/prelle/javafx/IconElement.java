/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.control.Control;

/**
 * @author spr
 *
 */
public class IconElement extends Control {

	private static final String DEFAULT_STYLE_CLASS = "icon-element";

	//-------------------------------------------------------------------
	public static IconElement valueOf(String val) {
        return new SymbolIcon(val);
    }

	//-------------------------------------------------------------------
	public IconElement() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
	}

}
