/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.KeywordInputControlSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class KeywordInputControl<T> extends Control {

	private static final String DEFAULT_STYLE_CLASS = "keyword-input";
	
	private ObjectProperty<ObservableList<T>> itemsProperty = new SimpleObjectProperty<ObservableList<T>>(FXCollections.observableArrayList());
	private ObjectProperty<ObservableList<T>> optionsProperty = new SimpleObjectProperty<ObservableList<T>>(FXCollections.observableArrayList());
	private ObjectProperty<StringConverter<T>> converterProperty = new SimpleObjectProperty<>();
	private ReadOnlyObjectProperty<Class<T>> classProperty;

	//-------------------------------------------------------------------
	public KeywordInputControl(Class<T> cls) {
        classProperty = new SimpleObjectProperty<>(cls);
		setSkin(new KeywordInputControlSkin<>(this));
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
	}

	/******************************************
	 * Property ORIENTATION
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ObjectProperty<ObservableList<T>> itemsProperty() {
		return itemsProperty;
	}

	//-------------------------------------------------------------------
	public ObservableList<T> getItems() {
		return itemsProperty.get();
	}

	//-------------------------------------------------------------------
	public void setItems(ObservableList<T> value) {
		itemsProperty.setValue(value);
	}

	/******************************************
	 * Property OPTIONS
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ObjectProperty<ObservableList<T>> optionsProperty() {
		return optionsProperty;
	}

	//-------------------------------------------------------------------
	public ObservableList<T> getOptions() {
		return optionsProperty.get();
	}

	//-------------------------------------------------------------------
	public void setOptions(ObservableList<T> value) {
		optionsProperty.setValue(value);
	}

	//--------------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	/******************************************
	 * Property CONVERTER
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ObjectProperty<StringConverter<T>> converterProperty() {
		return converterProperty;
	}

	//-------------------------------------------------------------------
	public StringConverter<T> getConverter() {
		return converterProperty.get();
	}

	//-------------------------------------------------------------------
	public void setConverter(StringConverter<T> value) {
		converterProperty.setValue(value);
	}

	/******************************************
	 * Property CLASS
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<Class<T>> classProperty() {
		return classProperty;
	}

}
