package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.ActionEvent;

/**
 * @author Stefan Prelle
 *
 */
public class WizardBehaviour {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private Wizard control;

	//-------------------------------------------------------------------
	public WizardBehaviour(Wizard control) {
		this.control = control;
	}

	//-------------------------------------------------------------------
	public boolean hasNextPage() {
		int next = control.getPages().indexOf(control.getCurrentPage());
		if (next==-1) return true;
		while (next<(control.getPages().size()-1)) {
			next++;
			if (control.getPages().get(next).isActive())
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public boolean hasPriorPage() {
		int prev = control.getPages().indexOf(control.getCurrentPage());
		while (prev>0) {
			prev--;
			if (control.getPages().get(prev).isActive())
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void nextPage() {
		int current = control.getPages().indexOf(control.getCurrentPage());
		if (hasNextPage()) {
			int next = current+1;
			while (!control.getPages().get(next).isActive()) {
				next++;
			}
			control.setCurrentPage(control.getPages().get(next));
//			control.navTo(next, true, CloseType.NEXT);
		}
	}

	//-------------------------------------------------------------------
	public void priorPage() {
		int current = control.getPages().indexOf(control.getCurrentPage());
		if (hasPriorPage()) {
			int prev = current-1;
			while (!control.getPages().get(prev).isActive()) {
				prev--;
			}
			control.setCurrentPage(control.getPages().get(prev));
//			control.navTo(prev, false, CloseType.PREVIOUS);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Cancel this wizard. When this method is called, a user confirmation
	 * has been given (via confirmCancelCallback) .
	 */
	public void cancel() {
		control.getScreenManager().close(control, CloseType.CANCEL);
	}

	//-------------------------------------------------------------------
	/**
	 * Finish this wizard. When this method is called, it was confirmed
	 * (via canBeFinishedCallback) that the wizard can be finished or
	 * the wizard was one the last page.
	 */
	public void finish() {
		control.getScreenManager().close(control, CloseType.FINISH);
	}

	//-------------------------------------------------------------------
	public void otherButton(CloseType save, ActionEvent event) {
		control.handleExtraButton(save, event);
	}

}
