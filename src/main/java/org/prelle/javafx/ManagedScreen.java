package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.MenuItem;
import javafx.util.Callback;

/**
 *
 * @author Stefan Prelle
 *
 */
public class ManagedScreen extends NavigationView implements ResponsiveControl, ScreenManagerProvider {

	private final static org.apache.logging.log4j.Logger logger = LogManager.getLogger("prelle.jfx");

	/** The manager who currently displays the screen */
	private ObjectProperty<ScreenManager>  managerProperty = new SimpleObjectProperty<ScreenManager>();
	/** The page to be shown, when navigated to */
	private ObjectProperty<ManagedScreenPage>  landingPageProperty = new SimpleObjectProperty<ManagedScreenPage>(new ManagedScreenPage(null));
//	/** The items to display in the navigation view */
//	private ObservableList<MenuItem> menuItems = FXCollections.observableArrayList();
	/** Optional callback when the page shall be left */
	private Callback<ManagedScreen, Boolean> canBeLeftCallback;
	/** The currently showed page */
	private ObjectProperty<ManagedScreenPage>  visiblePageProperty = new SimpleObjectProperty<ManagedScreenPage>();

	//-------------------------------------------------------------------
	public ManagedScreen() {
		getStyleClass().add("screen");
//		setSkin(new ManagedScreenSkin(this));
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		visiblePageProperty.addListener( (ov,o,n) -> {
			logger.debug("Visible changed from "+o+" to "+n);
			if (n!=null) {
				this.setContent(n);
				this.setHeader(n.getTitle());
			}
		});
		
		landingPageProperty.addListener( (ov,o,n) -> {
			if (visiblePageProperty.get()==null)
				visiblePageProperty.set(n);
		});
		
		selectedItemProperty().addListener( (ov,o,n) -> navigationItemChanged(o, n));
		setOnBackAction(ev -> getManager().impl_onBackRequested(this));
	}

	//-------------------------------------------------------------------
	public ObjectProperty<ScreenManager> managerProperty() { return managerProperty; }
	public void setManager(ScreenManager value) {
		managerProperty.setValue(value);
		if (landingPageProperty.get()!=null)
			landingPageProperty.get().setManager(value);
		if (visiblePageProperty.get()!=null)
			visiblePageProperty.get().setManager(value);
	}
	public ScreenManager getManager() { return managerProperty.get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<ManagedScreenPage> visiblePageProperty() { return visiblePageProperty; }
	public void setVisiblePage(ManagedScreenPage value) {visiblePageProperty.setValue(value); }
	public ManagedScreenPage getVisiblePage() { return visiblePageProperty.get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<ManagedScreenPage> landingPageProperty() { return landingPageProperty; }
	public void setLandingPage(ManagedScreenPage value) {landingPageProperty.setValue(value); }
	public ManagedScreenPage getLandingPage() { return landingPageProperty.get(); }

	//-------------------------------------------------------------------
	public ObservableList<MenuItem> getNavigationItems() { return super.getItems(); }

//	//-------------------------------------------------------------------
//	public NavigationView getNavigationView() { return ((ManagedScreenSkin)getSkin()).getNavigationView(); }

	//--------------------------------------------------------------------
	public void setCanBeLeftCallback(Callback<ManagedScreen, Boolean> callback) { this.canBeLeftCallback = callback; }
	public Callback<ManagedScreen, Boolean> getCanBeLeftCallback() {return canBeLeftCallback; }

	//-------------------------------------------------------------------
	public void navigationItemChanged(MenuItem oldValue, MenuItem newValue) {
	}

	//-------------------------------------------------------------------
	/**
	 * Called when the screen is displayed
	 */
	public void onOpen() { }

	//-------------------------------------------------------------------
	/**
	 * Called when the screen is closed
	 */
	public void onClose() { }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) { }

	//-------------------------------------------------------------------
	public void showPage(ManagedScreenPage value) { 
		if (getManager()==null)
			throw new NullPointerException("manager");
		value.setManager(getManager());
		visiblePageProperty.set(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return managerProperty.get();
	}

	//-------------------------------------------------------------------
	public String[] getStyleSheets() { return new String[0]; }

}
