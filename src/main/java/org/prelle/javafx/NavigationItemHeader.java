/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class NavigationItemHeader extends MenuItem implements ICommandBarElement {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private static final String DEFAULT_STYLE_CLASS = "navigation-item-header";
	
	private transient BooleanProperty compactProperty = new SimpleBooleanProperty(false); 

	//-------------------------------------------------------------------
	public NavigationItemHeader() {
		getStyleClass().addAll(NavigationItem.DEFAULT_STYLE_CLASS, DEFAULT_STYLE_CLASS);
//		super.setContentDisplay(ContentDisplay.TOP);
		
//		if (compactProperty.get()) {
//			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//		} else {
//			setContentDisplay(ContentDisplay.TOP);
//		}
		compactProperty.addListener( (ov,o,n) -> {
			logger.info("compact of "+this+" is "+n);
//			if (n==Boolean.TRUE) {
//				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//			} else {
//				setContentDisplay(ContentDisplay.TOP);
//			}
		});
	}

	//-------------------------------------------------------------------
	public NavigationItemHeader(String text) {
		this();
		setText(text);
	}

	/*-*****************************************
	 * Property COMPACT
	 *-*****************************************/
	public BooleanProperty compactProperty() { return compactProperty; }
	public boolean isCompact() { return compactProperty.get(); }
	public void setCompact(boolean value) { compactProperty.set(value); }

}
