package org.prelle.javafx;

import org.prelle.javafx.skin.ExtendedListViewSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

/**
 * @author prelle
 *
 */
public class ExtendedListView<T> extends ListView<T> {
	
	private ObjectProperty<Button> addButton      = new SimpleObjectProperty<Button>();
	private ObjectProperty<Button> deleteButton   = new SimpleObjectProperty<Button>();
	private ObjectProperty<Button> settingsButton = new SimpleObjectProperty<Button>();
	private StringProperty title = new SimpleStringProperty();

	//-------------------------------------------------------------------
	public ExtendedListView() {
		getStyleClass().add("extended-list-view");
		setSkin(new ExtendedListViewSkin<T>(this));
		this.setStyle("-fx-background-color: transparent");
	}

	//-------------------------------------------------------------------
	public ExtendedListView(String title) {
		getStyleClass().add("extended-list-view");
		setTitle(title);
		setSkin(new ExtendedListViewSkin<T>(this));
		this.setStyle("-fx-background-color: transparent");
	}
	
	//-------------------------------------------------------------------
	public ExtendedListView(ObservableList<T> items) {
		super(items);
		setSkin(new ExtendedListViewSkin<T>(this));
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Button> addButtonProperty() { return addButton; }
	public Button getAddButton() { return addButton.get(); }
	public void setAddButton(Button val) { addButton.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<Button> deleteButtonProperty() { return deleteButton; }
	public Button getDeleteButton() { return deleteButton.get(); }
	public void setDeleteButton(Button val) { deleteButton.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<Button> settingsButtonProperty() { return settingsButton; }
	public Button getSettingsButton() { return settingsButton.get(); }
	public void setSettingsButton(Button val) { settingsButton.set(val); }

	//-------------------------------------------------------------------
	public StringProperty titleProperty() { return title; }
	public String getTitle() { return title.get(); }
	public void setTitle(String val) { title.set(val); }
	
}
