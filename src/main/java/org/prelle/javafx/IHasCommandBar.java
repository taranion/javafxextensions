package org.prelle.javafx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;

/**
 * @author Stefan Prelle
 *
 */
public interface IHasCommandBar {
	
	public ReadOnlyObjectProperty<CommandBar> commandBarProperty();
	
	public CommandBar getCommandBar();

}
