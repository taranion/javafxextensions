package org.prelle.javafx;

import org.prelle.javafx.skin.CircleIconPaneSkin;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class CircleIconPane extends Control {
	
	private ObjectProperty<Image> image;
	private StringProperty title;
	private StringProperty subtitle;
	
	private BooleanProperty editButton;
	private ObjectProperty<EventHandler<ActionEvent>> onEditAction;

	//-------------------------------------------------------------------
	public CircleIconPane() {
		image = new SimpleObjectProperty<Image>();
		title = new SimpleStringProperty();
		subtitle = new SimpleStringProperty();
		editButton = new SimpleBooleanProperty();
		onEditAction = new SimpleObjectProperty<>();
		setSkin(new CircleIconPaneSkin(this));
	}

	//-------------------------------------------------------------------
	public StringProperty titleProperty() { return title; }
	public String getTitle() { return title.get(); }
	public void setTitle(String val) { title.set(val); }

	//-------------------------------------------------------------------
	public StringProperty subtitleProperty() { return subtitle; }
	public String getSubtitle() { return subtitle.get(); }
	public void setSubtitle(String val) { subtitle.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<Image> imageProperty() { return image; }
	public Image getImage() { return image.get(); }
	public void setImage(Image val) { image.set(val); }

	//-------------------------------------------------------------------
	public BooleanProperty editButtonProperty() { return editButton; }
	public boolean getEditButton() { return editButton.get(); }
	public void setEditButton(boolean val) { editButton.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<EventHandler<ActionEvent>> onEditActionProperty() { return onEditAction; }
	public EventHandler<ActionEvent> getOnEditAction() { return onEditAction.get(); }
	public void setOnEditAction(EventHandler<ActionEvent> val) { onEditAction.set(val); }

}
