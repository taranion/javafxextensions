/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public interface ICommandBarElement {

	/**
	 * In compact mode the element is shown with no label and reduced padding.
	 * @return TRUE, if compact mode
	 */
	public boolean isCompact();
	
}
