package org.prelle.javafx;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;

/**
 * @author Stefan Prelle
 *
 */
public class ManagedScreenPage extends NodeWithCommandBar implements NodeWithTitle, IManagable {

	@FXML
	private StringProperty titleProperty = new SimpleStringProperty("No title");

	//-------------------------------------------------------------------
	public ManagedScreenPage() {
	}

	//-------------------------------------------------------------------
	public ManagedScreenPage(String title) {
		titleProperty.set(title);
	}
	//-------------------------------------------------------------------
	public ManagedScreenPage(String title, Node content) {
		titleProperty.set(title);
		super.content.set(content);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#getTitle()
	 */
	@Override
	public String getTitle() { return titleProperty.get(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String value) { titleProperty.setValue(value); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#titleProperty()
	 */
	@Override
	public ReadOnlyStringProperty titleProperty() { return titleProperty; }

}
