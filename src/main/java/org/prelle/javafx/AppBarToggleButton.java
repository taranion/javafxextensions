/**
 * 
 */
package org.prelle.javafx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioMenuItem;

/**
 * @author spr
 *
 */
public class AppBarToggleButton extends RadioMenuItem implements ICommandBarElement {

	private static final String DEFAULT_STYLE_CLASS = "app-bar-toggle-button";
	
	@FXML
	private ObjectProperty<IconElement> iconProperty = new SimpleObjectProperty<IconElement>();
	
	private transient BooleanProperty compactProperty = new SimpleBooleanProperty(true); 

	//-------------------------------------------------------------------
	public AppBarToggleButton() {
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
//		setSkin(new AppBarButtonSkin(this));
		super.graphicProperty().bind(iconProperty);
//		super.setContentDisplay(ContentDisplay.TOP);
//		
//		
//		if (compactProperty.get()) {
//			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//		} else {
//			setContentDisplay(ContentDisplay.TOP);
//		}
//		compactProperty.addListener( (ov,o,n) -> {
//			if (n==Boolean.TRUE) {
//				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//			} else {
//				setContentDisplay(ContentDisplay.TOP);
//			}
//		});
	}

	//-------------------------------------------------------------------
	public AppBarToggleButton(String text) {
		super(text);
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
//		setSkin(new AppBarButtonSkin(this));
		super.graphicProperty().bind(iconProperty);
	}

	//-------------------------------------------------------------------
	public AppBarToggleButton(String text, Node graphic) {
		super(text, graphic);
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
//		setSkin(new AppBarButtonSkin(this));
		super.graphicProperty().bind(iconProperty);
	}

	/*-*****************************************
	 * Property ICON
	 *-*****************************************/
	public ObjectProperty<IconElement> iconProperty() { return iconProperty; }
	public IconElement getIcon() { return iconProperty.get(); }
	public AppBarToggleButton setIcon(IconElement value) { iconProperty.set(value); return this; }

	/*-*****************************************
	 * Property COMPACT
	 *-*****************************************/
	public BooleanProperty compactProperty() { return compactProperty; }
	public boolean isCompact() { return compactProperty.get(); }
	public void setCompact(boolean value) { compactProperty.set(value); }


}
