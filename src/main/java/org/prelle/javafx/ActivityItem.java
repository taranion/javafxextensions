package org.prelle.javafx;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Control;

/**
 * @author prelle
 *
 */
public class ActivityItem extends Control {

	private static final String DEFAULT_STYLE_CLASS = "activity-item";

	@FXML
	private StringProperty activityDescriptionProperty = new SimpleStringProperty();

	//-------------------------------------------------------------------
	/**
	 */
	public ActivityItem() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
	}

	/******************************************
	 * Property ACTIVITY DESCRIPTION
	 ******************************************/
	public StringProperty activityDescriptionProperty() { return activityDescriptionProperty; }
	public String getActivityDescriptionProperty() { return activityDescriptionProperty.get(); }
	public void setActivityDescriptionProperty(String value) { activityDescriptionProperty.set(value); }

}
