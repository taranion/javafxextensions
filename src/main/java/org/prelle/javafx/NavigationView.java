/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.NavigationViewSkin;

import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
@DefaultProperty(value="items")
public class NavigationView extends Control {

	public enum DisplayMode {
		LEFT,
		LEFT_COMPACT,
		LEFT_MINIMAL,
		AUTO
	}

	private static final String DEFAULT_STYLE_CLASS = "navigation-view";
	
	@FXML
	private ObservableList<MenuItem> itemsProperty = FXCollections.observableArrayList();
	/** An optional header text to appear above the content */
	@FXML
	private StringProperty headerProperty = new SimpleStringProperty();
	// --- Content
	@FXML
	private ObjectProperty<Node> contentProperty = new SimpleObjectProperty<Node>();
	/** Toggles if the back button should be visible */
	@FXML
	private BooleanProperty backButtonVisibleProperty = new SimpleBooleanProperty(false);
	/** Toggles if the back button should be enabled */
	@FXML
	private BooleanProperty backButtonEnabledProperty = new SimpleBooleanProperty(true);
	/** Toggles if the setting item should be visible */
	private ObjectProperty<EventHandler<ActionEvent>> onBackAction = new SimpleObjectProperty<EventHandler<ActionEvent>>();

	@FXML
	private BooleanProperty settingsVisibleProperty = new SimpleBooleanProperty(true);
	// --- Display Mode
	private ObjectProperty<DisplayMode> displayModeProperty = new SimpleObjectProperty<DisplayMode>(DisplayMode.AUTO);
	/** Free form content used as a footer in the navigation pane */
	@FXML
	private ObjectProperty<Node> footerProperty = new SimpleObjectProperty<Node>();
	/** Custom navigation pane content */
	@FXML
	private ObjectProperty<Node> paneContentProperty = new SimpleObjectProperty<Node>();
	/** Currently selected menu item */
	private ObjectProperty<MenuItem> selectedItemProperty = new SimpleObjectProperty<MenuItem>();

	//-------------------------------------------------------------------
	/**
	 */
	public NavigationView() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new NavigationViewSkin(this));
		
		selectedItemProperty.bind( ((NavigationViewSkin)getSkin()).getNavigationPane().getSelectionModel().selectedItemProperty() );
	}

	/*-*****************************************
	 * Property CONTENT
	 *-*****************************************/
	public ObjectProperty<Node> contentProperty() { return contentProperty; }
	public Node getContent() { return contentProperty.get(); }
	public NavigationView setContent(Node value) { contentProperty.set(value); return this;}

	/*-*****************************************
	 * Property HEADER
	 *-*****************************************/
	public StringProperty headerProperty() { return headerProperty; }
	public String getHeader() { return headerProperty.get(); }
	public NavigationView setHeader(String value) { headerProperty.set(value); return this;}

	/*-*****************************************
	 * Property ITEMS
	 *-*****************************************/
//	public ObservableList<MenuItem> itemsProperty() { return itemsProperty; }
	public ObservableList<MenuItem> getItems() { return itemsProperty; }
//	public NavigationView setItems(ObservableList<MenuItem> value) { itemsProperty.set(value); return this;}

	/*-*****************************************
	 * Property BACK_BUTTON_VISIBLE
	 *-*****************************************/
	public BooleanProperty backButtonVisibleProperty() { return backButtonVisibleProperty; }
	public boolean isBackButtonVisible() { return backButtonVisibleProperty.get(); }
	public NavigationView setBackButtonVisible(boolean value) { backButtonVisibleProperty.set(value); return this;}

	/*-*****************************************
	 * Property BACK_BUTTON_ENABLED
	 *-*****************************************/
	public BooleanProperty backButtonEnabledProperty() { return backButtonEnabledProperty; }
	public boolean isBackButtonEnabled() { return backButtonEnabledProperty.get(); }
	public NavigationView setBackButtonEnabled(boolean value) { backButtonEnabledProperty.set(value); return this;}

	/*-*****************************************
	 * Property ON_BACK_ACTION
	 *-*****************************************/
	public ObjectProperty<EventHandler<ActionEvent>> onBackActionProperty() { return onBackAction; }
	public EventHandler<ActionEvent> getOnBackAction() { return onBackAction.get(); }
	public void setOnBackAction(EventHandler<ActionEvent> value) { onBackAction.set(value);}

	/*-*****************************************
	 * Property SETTINGS_VISIBLE
	 *-*****************************************/
	public BooleanProperty settingsVisibleProperty() { return settingsVisibleProperty; }
	public boolean isSettingsVisible() { return settingsVisibleProperty.get(); }
	public NavigationView setSettingsVisible(boolean value) { settingsVisibleProperty.set(value); return this;}

	/*-*****************************************
	 * Property FOOTER
	 *-*****************************************/
	public ObjectProperty<Node> footerProperty() { return footerProperty; }
	public Node getFooter() { return footerProperty.get(); }
	public NavigationView setFooter(Node value) { footerProperty.set(value); return this;}

	/*-*****************************************
	 * Property PANE_CONTENT
	 *-*****************************************/
	public ObjectProperty<Node> paneContentProperty() { return paneContentProperty; }
	public Node getPaneContent() { return paneContentProperty.get(); }
	public NavigationView setPaneContent(Node value) { paneContentProperty.set(value); return this;}

	/*-*****************************************
	 * Property DISPLAY_MODE
	 *-*****************************************/
	public ObjectProperty<DisplayMode> displayModeProperty() { return displayModeProperty; }
	public DisplayMode getDisplayMode() { return displayModeProperty.get(); }
	public NavigationView setDisplayMode(DisplayMode value) { displayModeProperty.set(value); return this;}

	/*-*****************************************
	 * Property CONTENT
	 *-*****************************************/
	public ObjectProperty<MenuItem> selectedItemProperty() { return selectedItemProperty; }
	public MenuItem getSelectedItem() { return selectedItemProperty.get(); }
	
	//-------------------------------------------------------------------
	public void clearSelection() {
		((NavigationViewSkin)getSkin()).clearSelection();
	}
}
