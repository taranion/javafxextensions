package org.prelle.javafx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.image.Image;

/**
 * @author prelle
 */
public class WizardPage  {

	protected Wizard wizard;
	@FXML
	private ObjectProperty<String> title;
	@FXML
	private ObjectProperty<Node>   content;
	@FXML
	private ObjectProperty<Image>  image;
	@FXML
	private ObjectProperty<Insets>  imageInsets;
	@FXML
	private ObjectProperty<Node>   side;
	private BooleanProperty active;

	//-------------------------------------------------------------------
	public WizardPage(Wizard wizard) {
		this.wizard = wizard;
		image   = new SimpleObjectProperty<Image>();
		content = new SimpleObjectProperty<Node>();
		title   = new SimpleObjectProperty<String>();
		imageInsets   = new SimpleObjectProperty<Insets>();
		side    = new SimpleObjectProperty<Node>();
		active  = new SimpleBooleanProperty(true);
	}

	//-------------------------------------------------------------------
	public void setWizard(Wizard wizard) {
		this.wizard = wizard;
	}

	//-------------------------------------------------------------------
	protected Wizard getWizard() {
		return wizard;
	}

	//--------------------------------------------------------------------
	public Node getContent() { return content.get(); }
	public void setContent(Node value) { content.set(value);}
	public ObjectProperty<Node> contentProperty() { return content; }

	//--------------------------------------------------------------------
	public String getTitle() { return title.get(); }
	public void setTitle(String value) { title.set(value);}
	public ObjectProperty<String> titleProperty() { return title; }

	//--------------------------------------------------------------------
	public ObjectProperty<Image> imageProperty() { return image; }
	public void setImage(Image value) {	image.set(value); }
	public Image getImage() {return image.get();}

	//--------------------------------------------------------------------
	public ObjectProperty<Insets> imageInsetsProperty() { return imageInsets; }
	public void setImageInsets(Insets value) {	imageInsets.set(value); }
	public Insets getImageInsets() {return imageInsets.get();}

	//--------------------------------------------------------------------
	public Node getSide() { return side.get(); }
	public void setSide(Node value) { side.set(value);}
	public ObjectProperty<Node> sideProperty() { return side; }

	//--------------------------------------------------------------------
	public BooleanProperty activeProperty() {return active;}
	public boolean isActive() {return active.get();}
	public void setActive(boolean value) {active.set(value);}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is shown to user
	 */
	public void pageVisited() {
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft() {
	}

}