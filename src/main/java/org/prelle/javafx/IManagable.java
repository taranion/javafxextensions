package org.prelle.javafx;

/**
 * @author Stefan Prelle
 *
 */
public interface IManagable {

	public void setManager(ScreenManager value);
	
}
