package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.ScreenManagerBehaviour;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;

/**
 * @author Stefan Prelle
 *
 */
public class ScreenManager extends StackPane {

	private final static Logger logger = LogManager.getLogger("prelle.jfx");

	// --- Primary navigation items
	private ObjectProperty<ObservableList<Node>> overlaysProperty = new SimpleObjectProperty<ObservableList<Node>>(FXCollections.observableArrayList());
	private int inNested;

	private ScreenManagerBehaviour behaviour;

	//-------------------------------------------------------------------
	public ScreenManager() {
		behaviour = new ScreenManagerBehaviour(this);
		ResponsiveControlManager.manageResponsiveControls(this, 640, 1200);
	}

	/*-*****************************************
	 * Property OVERLAYS
	 *-*****************************************/
	public final ObservableList<Node> getOverlays() { return overlaysProperty.get(); }
	public final ObjectProperty<ObservableList<Node>> overlaysProperty() { return overlaysProperty; }

	//-------------------------------------------------------------------
	/**
	 * Change header and content of the NavigationView to the given node,
	 * but don't change the navigation items
	 *
	 * @param screen Screen/Page to show
	 */
	public void changeScreen(NodeWithTitleSkeleton screen) {
		Parent visibleChild = null;
		if (getChildren().size()>0)
			visibleChild = (Parent)this.getChildren().get(getChildren().size()-1);

		if (visibleChild==null)
			throw new IllegalStateException("ScreenManager does not show anything");
		if (!(visibleChild instanceof NavigationView))
			throw new IllegalStateException("Upmost child of ScreenManager isn't a ManagedScreen");

		logger.info("Change content to "+screen.getClass());
		((NavigationView)visibleChild).setContent(screen.getContent());
		((NavigationView)visibleChild).setHeader(screen.getTitle());
	}

	//-------------------------------------------------------------------
	/**
	 * Replace navigation items, header and content with new content. The old content
	 * is put on a stack and is restored when the back button is used.
	 */
	public void navigateTo(ManagedScreen screen) {
		screen.setManager(this);
		behaviour.navigateTo(screen);
	}

	//-------------------------------------------------------------------
	public ManagedScreen getCurrentScreen() {
		return behaviour.getCurrentScreen();
	}

	//-------------------------------------------------------------------
	/**
	 * The back button has been pressed.
	 * This method should only be called internally
	 * @param screen
	 */
	public void impl_onBackRequested(ManagedScreen screen) {
		behaviour.onBackRequested(screen);
	}

	//-------------------------------------------------------------------
	public void closeScreen() {
		getCurrentScreen().setCanBeLeftCallback(ev -> true);
		behaviour.onBackRequested(getCurrentScreen());
	}

	//--------------------------------------------------------------------
	/**
	 * Show a dialog
	 */
	public Object showAndWait(Node page) {
		logger.info("----------showAndWait:"+page.getClass()+"--------------------------------");
		if (getOverlays().contains(page)) {
			logger.error("Showing a page already on the stack not implemented yet");
			return null;
		}

		logger.info("show screen with styleclasses "+page.getStyleClass());
//		if (!page.getStyleClass().contains("screen"))
//			page.getStyleClass().add("screen");

		if ( (page instanceof ManagedDialog)) {
			((ManagedDialog)page).setManager(this);
		}

		// Memorize page
		logger.debug("Add dialog "+page+" and make it visible ");
		getOverlays().add(page);
		getChildren().add(page); // Add to StackPane
//		page.setScreenManager(this);
		page.setVisible(true);
		page.requestFocus();

		logger.debug("Block on key "+page);
		inNested++;

		Object retVal;
		try {
			retVal = Platform.enterNestedEventLoop(page);
		} catch (IllegalStateException e) {
			logger.warn("Trying to enter nested event loop, but failed: "+e);
			inNested--;
			return null;
		}
		logger.debug("----------showAndWait-done----------------------------"+retVal);
		return retVal;
	}

//	//--------------------------------------------------------------------
//	private boolean canCloseDialog(ManagedDialog screen, CloseType close) {
//		logger.debug("  canBeLeft: callback is "+screen.getCanBeLeftCallback() );
//		if (screen.getCanBeLeftCallback()!=null)
//			return screen.getCanBeLeftCallback().apply(screen, close);
//		return true;
//	}

	//--------------------------------------------------------------------
	public void close(Node page, CloseType type) {
		logger.info("Close "+page+" with "+type);
		logger.debug("Stack is "+getOverlays());

		// Do nothing if page is unknown
		if (!getOverlays().contains(page)) {
			logger.error("Closing a page not on the stack");
			return;
		}
		// Don't close first page
		if (getOverlays().size()<1) {
			logger.error("Don't close first page");
			return;
		}

//		// Check if page can be closed
//		if (page instanceof ManagedDialog && !canCloseDialog((ManagedDialog)page, type)) {
//			logger.warn("Close "+type+" rejected by dialog");
//			return;
//		}
		
		// Remove
//		page.getScene().getWindow().hide();
		logger.debug("Unblock key "+page);
		if (inNested>0) {
			inNested--;

			Platform.exitNestedEventLoop(page, type);
		}

		logger.debug("Make invisible");
		page.setVisible(false);
		getChildren().remove(page);
		getOverlays().remove(getOverlays().size()-1);

		// Check if page can be closed
		if (page instanceof ManagedDialog) {
			((ManagedDialog)page).onClose(type);
		}
	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(AlertType type, String headerText, Node content, NavigButtonControl control) {
		ManagedDialog screen = null;
		switch (type) {
		case QUESTION:
			screen = new ManagedDialog(headerText, content, CloseType.OK, CloseType.CANCEL);
			break;
		case CONFIRMATION:
			screen = new ManagedDialog(headerText, content, CloseType.YES, CloseType.NO);
			break;
		case YES_NO_CANCEL:
			screen = new ManagedDialog(headerText, content, CloseType.YES, CloseType.NO, CloseType.CANCEL);
			break;
		default:
			screen = new ManagedDialog(headerText, content, CloseType.OK);
			break;
		}
		
		ManagedDialog finalDialog = screen;
		if (content instanceof TextField) {
			((TextField)content).setFocusTraversable(true);
			((TextField)content).setOnAction(event ->  {
			close(finalDialog, CloseType.OK);
		});
		}
		content.requestFocus();
		
		return showAlertAndCall(screen, control);
	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(ManagedDialog dialog, NavigButtonControl control) {
		if (control!=null)
			control.initialize(this, dialog);

		// Wait for buttons
//		if (control==null) {
//			for (CloseType key : CloseType.values()) {
//				dialog.setOnAction(key, event -> control.fireEvent(key, event));
//			}
//		}
//		
//		dialog.setOnAction(CloseType.OK, event -> {
//			logger.debug("OK");
//			if (control!=null && !control.call(CloseType.OK)) {
//				logger.debug("Not OK");
//				return;
//			}
//			close(dialog, CloseType.OK);
//		});
//		dialog.setOnAction(CloseType.CANCEL, event -> {
//			logger.debug("CANCEL");
//			if (control!=null && !control.call(CloseType.CANCEL)) {
//				logger.debug("Not CANCEL");
//				return;
//			}
//			close(dialog, CloseType.CANCEL);
//		});

		return (CloseType)showAndWait(dialog);
	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(AlertType type, String headerText, Node content) {
		return showAlertAndCall(type, headerText, content, null);
	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(AlertType type, String headerText, String contentText) {
		Label content= new Label(contentText);
		content.setWrapText(true);
		content.getStyleClass().add("text-body");

		return showAlertAndCall(type, headerText, content);
	}

}
