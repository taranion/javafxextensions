/**
 * 
 */
package org.prelle.javafx.fxml;

import org.prelle.javafx.AppBarButton;

import javafx.util.Builder;

/**
 * @author spr
 *
 */
public class AppBarButtonBuilder implements Builder<AppBarButton> {

	//-------------------------------------------------------------------
	/**
	 * @see javafx.util.Builder#build()
	 */
	@Override
	public AppBarButton build() {
		System.out.println("call AppBarButton.build()");
		return new AppBarButton();
	}

	//-------------------------------------------------------------------

}
