/**
 * 
 */
package org.prelle.javafx.fxml;

import org.prelle.javafx.IconElement;

import javafx.util.Builder;

/**
 * @author spr
 *
 */
public class IconElementBuilder implements Builder<IconElement> {

	//-------------------------------------------------------------------
	/**
	 * @see javafx.util.Builder#build()
	 */
	@Override
	public IconElement build() {
		System.out.println("call IconElementBuilder.build()");
		return new IconElement();
	}

}
