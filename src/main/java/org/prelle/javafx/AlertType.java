/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public enum AlertType {

	CONFIRMATION,
	YES_NO_CANCEL,
	NOTIFICATION,
	QUESTION,
	ERROR,
	
}
