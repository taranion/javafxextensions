package org.prelle.javafx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Control;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class Persona extends Control {

	private static final String DEFAULT_STYLE_CLASS = "persona";
	
	public enum Size {
		SIZE8,
		SIZE24,
		SIZE32,
		SIZE40,
		SIZE48,
		SIZE56,
		SIZE72,
		SIZE100,
		SIZE120
	}

	@FXML
	private ObjectProperty<Size> sizeProperty = new SimpleObjectProperty<Size>(Size.SIZE48);
	@FXML
	private ObjectProperty<Image> imageProperty = new SimpleObjectProperty<Image>();
	@FXML
	private StringProperty imageInitialsProperty = new SimpleStringProperty();
	@FXML
	private StringProperty textProperty = new SimpleStringProperty();
	@FXML
	private StringProperty secondaryTextProperty = new SimpleStringProperty();
	@FXML
	private StringProperty tertiaryTextProperty = new SimpleStringProperty();
	@FXML
	private StringProperty optionalTextProperty = new SimpleStringProperty();
	
	private transient BooleanProperty compactProperty = new SimpleBooleanProperty(true); 

	//-------------------------------------------------------------------
	public Persona() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
	}

	/******************************************
	 * Property IMAGE
	 ******************************************/
	public ObjectProperty<Image> imageProperty() { return imageProperty; }
	public Image getImage() { return imageProperty.get(); }
	public void setImage(Image value) { imageProperty.set(value); }

	/******************************************
	 * Property IMAGE INITIALS
	 ******************************************/
	public StringProperty imageInitialsProperty() { return imageInitialsProperty; }
	public String getImageInitials() { return imageInitialsProperty.get(); }
	public void setImageInitials(String value) { imageInitialsProperty.set(value); }

	/******************************************
	 * Property TEXT
	 ******************************************/
	public StringProperty textProperty() { return textProperty; }
	public String getText() { return textProperty.get(); }
	public void setText(String value) { textProperty.set(value); }

	/******************************************
	 * Property SECONDARY TEXT
	 ******************************************/
	public StringProperty secondaryTextProperty() { return secondaryTextProperty; }
	public String getSecondaryText() { return secondaryTextProperty.get(); }
	public void setSecondaryText(String value) { secondaryTextProperty.set(value); }

	/******************************************
	 * Property TERTIARY TEXT
	 ******************************************/
	public StringProperty tertiaryTextProperty() { return tertiaryTextProperty; }
	public String getTertiaryText() { return tertiaryTextProperty.get(); }
	public void setTertiaryText(String value) { tertiaryTextProperty.set(value); }

	/******************************************
	 * Property OPTIONAL TEXT
	 ******************************************/
	public StringProperty optionalTextProperty() { return optionalTextProperty; }
	public String getOptionalText() { return optionalTextProperty.get(); }
	public void setOptionalText(String value) { optionalTextProperty.set(value); }

	/*-*****************************************
	 * Property COMPACT
	 *-*****************************************/
	public BooleanProperty compactProperty() { return compactProperty; }
	public boolean isCompact() { return compactProperty.get(); }
	public void setCompact(boolean value) { compactProperty.set(value); }

}
