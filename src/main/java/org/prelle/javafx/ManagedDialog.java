package org.prelle.javafx;

import java.util.HashMap;
import java.util.Map;

import org.prelle.javafx.skin.ManagedDialogSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class ManagedDialog extends Control implements ScreenManagerProvider {

	private final static String STYLE_CLASS = "managed-dialog";

	private ScreenManager manager;
	private StringProperty            title   = new SimpleStringProperty();
	private ObjectProperty<Node>      content = new SimpleObjectProperty<>();
	protected ObservableList<CloseType> buttons = FXCollections.observableArrayList();
	private Callback<CloseType, Boolean> optionalCallback;

	private Map<CloseType, EventHandler<ActionEvent>> actionMap;
//	/** Optional callback when the page shall be left */
//	private BiFunction<ManagedDialog, CloseType, Boolean> canBeLeftCallback;

	//-------------------------------------------------------------------
	public ManagedDialog(String title, Node content, CloseType ...buttons) {
		actionMap = new HashMap<CloseType, EventHandler<ActionEvent>>();
		this.title.set(title);
		this.content.set(content);
		this.buttons.addAll(buttons);

		getStyleClass().add(STYLE_CLASS);
		setSkin(new ManagedDialogSkin(this));
		
		((ManagedDialogSkin)getSkin()).refreshButtons();
	}

	//-------------------------------------------------------------------
	public ObservableList<CloseType> getButtons() { return buttons; }

	//-------------------------------------------------------------------
	public StringProperty titleProperty() { return title; }
	public String getTitle() { return title.get(); }
	public void setTitle(String value) { title.set(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<Node> contentProperty() { return content; }
	public Node getContent() { return content.get(); }
	public void setContent(Node value) { content.set(value); }

	//-------------------------------------------------------------------
	public void setButtonPredicateCheck(Callback<CloseType, Boolean> value) { optionalCallback = value; }
	public Callback<CloseType, Boolean> getButtonPredicateCheck() { return optionalCallback; }

	//-------------------------------------------------------------------
	public void setManager(ScreenManager manager) {
		this.manager = manager;
	}

//	//--------------------------------------------------------------------
//	public void setCanBeLeftCallback(BiFunction<ManagedDialog, CloseType, Boolean> callback) { this.canBeLeftCallback = callback; }
//	public BiFunction<ManagedDialog, CloseType, Boolean> getCanBeLeftCallback() {return canBeLeftCallback; }

	//-------------------------------------------------------------------
	public void onClose(CloseType closeType) { }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return manager;
	}

	//-------------------------------------------------------------------
	public void setOnAction(CloseType key, EventHandler<ActionEvent> value) { actionMap.put(key, value); }
	public EventHandler<ActionEvent> getOnAction(CloseType key) { return actionMap.get(key); }

}
