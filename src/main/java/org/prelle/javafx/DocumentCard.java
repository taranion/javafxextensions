package org.prelle.javafx;

import org.prelle.javafx.skin.DocumentCardSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Control;

/**
 * @author prelle
 *
 */
public class DocumentCard extends Control {
	
	public enum DocumentCardType {
		/** Standard DocumentCard. */
		NORMAL,
		/** Compact layout. Displays the preview beside the details, rather than above. */
		COMPACT
	}

	private static final String DEFAULT_STYLE_CLASS = "document-card";

	//-------------------------------------------------------------------
	/**
	 */
	public DocumentCard() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		setSkin(new DocumentCardSkin(this));
	}

    /**
     * The button's action, which is invoked whenever the button is fired. This
     * may be due to the user clicking on the button with the mouse, or by
     * a touch event, or by a key press, or if the developer programmatically
     * invokes the {@link #fire()} method.
     * @return the property to represent the button's action, which is invoked
     * whenever the button is fired
     */
    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() { return onAction; }
    public final void setOnAction(EventHandler<ActionEvent> value) { onActionProperty().set(value); }
    public final EventHandler<ActionEvent> getOnAction() { return onActionProperty().get(); }
    private ObjectProperty<EventHandler<ActionEvent>> onAction = new ObjectPropertyBase<EventHandler<ActionEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(ActionEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return DocumentCard.this;
        }

        @Override
        public String getName() {
            return "onAction";
        }
    };

}
