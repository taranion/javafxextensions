/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.BitmapIconSkin;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;

/**
 * @author spr
 *
 */
public class BitmapIcon extends IconElement {

	private static final String DEFAULT_STYLE_CLASS = "bitmap-icon";
	
	@FXML
	private StringProperty uriSourceProperty = new SimpleStringProperty();

	//-------------------------------------------------------------------
	public BitmapIcon() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		setSkin(new BitmapIconSkin(this));
		setMouseTransparent(true);
	}

	//-------------------------------------------------------------------
	public BitmapIcon(String uriSource) {
		this();
		uriSourceProperty.set(uriSource);
	}

	/******************************************
	 * Property ICON
	 ******************************************/
	public StringProperty uriSourceProperty() { return uriSourceProperty; }
	public String getUriSource() { return uriSourceProperty.get(); }
	public void setUriSource(String value) { uriSourceProperty.set(value); }

}
