package org.prelle.javafx;

import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.collections.ObservableList;

/**
 * @author Stefan Prelle
 *
 */
public interface IAttentionSeeker {

	public BooleanProperty attentionFlagProperty();
	public void setAttentionFlag(boolean requireAttention);

	//-------------------------------------------------------------------
	public ObservableList<String> attentionToolTipProperty();
	public void setAttentionToolTip(List<String> list);
	
}
