/**
 *
 */
package org.prelle.javafx;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;

/**
 * This class should be used as a parent for all screens that should appear in
 * a NavigationView.
 *
 * @author prelle
 *
 */
public class NodeWithTitleSkeleton implements NodeWithTitle {

	@FXML
	private StringProperty titleProperty = new SimpleStringProperty();
	@FXML
	private SimpleObjectProperty<Node> contentProperty = new SimpleObjectProperty<Node>();

	//-------------------------------------------------------------------
	public NodeWithTitleSkeleton() { }
	
	public NodeWithTitleSkeleton(String title, Node content) {
		titleProperty.set(title);
		contentProperty.set(content);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#getContent()
	 */
	@Override
	public Node getContent() { return contentProperty.get(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#setContent(javafx.scene.Node)
	 */
	@Override
	public void setContent(Node value) { contentProperty.setValue(value); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#contentProperty()
	 */
	@Override
	public ReadOnlyObjectProperty<Node> contentProperty() { return contentProperty; }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#getTitle()
	 */
	@Override
	public String getTitle() { return titleProperty.get(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String value) { titleProperty.setValue(value); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.NodeWithTitle#titleProperty()
	 */
	@Override
	public ReadOnlyStringProperty titleProperty() { return titleProperty; }

}
