/**
 *
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 * @author prelle
 *
 */
public class ModernUI {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	//-------------------------------------------------------------------
	public static void initialize(Scene scene) {
		logger.debug("initialize(Scene)");

		Font.loadFont(ClassLoader.getSystemResourceAsStream(JavaFXConstants.PREFIX+"fonts/seguisym.ttf"), 20);

		/*
		 * Load CSS
		 */
		scene.getStylesheets().add(ModernUI.class.getResource("css/fluent.css").toString());
		logger.info("Primary screen = "+Screen.getPrimary());
		logger.info("Primary screen DPI = "+Screen.getPrimary().getDpi());
		logger.info("Primary screen scale = "+Screen.getPrimary().getOutputScaleX());
	}
}
