package org.prelle.javafx;

import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class AttentionPane extends StackPane implements IAttentionSeeker {

	private BooleanProperty attentionFlag  = new SimpleBooleanProperty();
	private ObservableList<String> toolTip = FXCollections.observableArrayList();

	private Canvas signLayer;
	private Canvas signLayerCompact;
	private Node child;
	private Tooltip tip;
	
	private transient BooleanProperty compactProperty = new SimpleBooleanProperty(false); 

	//-------------------------------------------------------------------
	public AttentionPane(Node child) {
		super(child);
		this.child = child;

		signLayer = new Canvas(35,25);
		signLayer.setVisible(false);
		tip = new Tooltip();
		//			signLayer.setPickOnBounds(value);
		this.getChildren().add(signLayer);
		StackPane.setAlignment(signLayer, Pos.BOTTOM_LEFT);
		GraphicsContext gc = signLayer.getGraphicsContext2D();
		gc.setFont(new Font("Segoe UI Symbol", 15));
		gc.setFill(Color.RED);
		gc.fillText("\uE218", 10, 18);
		gc.setFill(Color.WHITE);
		gc.fillText("\uE171", 10, 18);
		signLayer.setOnMouseClicked(event -> {
			child.fireEvent(event);
		});

		signLayer.visibleProperty().bind(attentionFlag);
		toolTip.addListener(new ListChangeListener<String>() {

			@Override
			public void onChanged(Change<? extends String> c) {
				List<String> list = toolTip;
				if (list==null || list.isEmpty()) {
					Tooltip.uninstall(signLayer, tip);
				} else {
					String txt = String.join("\n",  list);
					tip.setText(txt);
					Tooltip.install(signLayer, tip);
				}
			}});
	}

	//-------------------------------------------------------------------
	public AttentionPane(Node child, Pos alignment) {
		this(child);
		StackPane.setAlignment(signLayer, alignment);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#setAttentionFlag(boolean)
	 */
	@Override
	public void setAttentionFlag(boolean needsAttention) {
		//signLayer.setVisible(needsAttention);
		attentionFlag.set(needsAttention);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#setAttentionToolTip(java.util.List)
	 */
	@Override
	public void setAttentionToolTip(List<String> list) {
		if (list==null) {
			Tooltip.uninstall(signLayer, tip);
			toolTip.clear();
			return;
		}
		//			if (list==null || list.isEmpty()) {
		//				Tooltip.uninstall(signLayer, tip);
		//			} else {
		//				String txt = String.join("\n",  list);
		//				tip.setText(txt);
		//				Tooltip.install(signLayer, tip);
		//			}
		toolTip.setAll(list);
	}

	//-------------------------------------------------------------------
	public Node getChild() {
		return child;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#attentionFlagProperty()
	 */
	@Override
	public BooleanProperty attentionFlagProperty() {
		return attentionFlag;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.IAttentionSeeker#attentionToolTipProperty()
	 */
	@Override
	public ObservableList<String> attentionToolTipProperty() {
		return toolTip;
	}

	/*-*****************************************
	 * Property COMPACT
	 *-*****************************************/
	public BooleanProperty compactProperty() { return compactProperty; }
	public boolean isCompact() { return compactProperty.get(); }
	public void setCompact(boolean value) { 
		compactProperty.set(value);
		if (value==true) {
			
		}
	}

}