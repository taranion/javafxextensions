package org.prelle.javafx.layout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.beans.property.DoubleProperty;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableDoubleProperty;
import javafx.css.StyleableProperty;
import javafx.css.converter.SizeConverter;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class ClockPane extends Pane {

    private static final String INDEX_CONSTRAINT = "clock-index";
    private static final String LARGE_CONSTRAINT = "clock-large";

	//-------------------------------------------------------------------
    // Unfortunately Pane.setConstraint is not exposed
    static void setConstraint(Node node, Object key, Object value) {
        if (value == null) {
            node.getProperties().remove(key);
        } else {
            node.getProperties().put(key, value);
        }
        if (node.getParent() != null) {
            node.getParent().requestLayout();
        }
    }

	//-------------------------------------------------------------------
    // Unfortunately Pane.getConstraint is not exposed
    static Object getConstraint(Node node, Object key) {
        if (node.hasProperties()) {
            Object value = node.getProperties().get(key);
            if (value != null) {
                return value;
            }
        }
        return null;
    }

	//-------------------------------------------------------------------
	public static void setIndex(Node child, int value) { setConstraint(child, INDEX_CONSTRAINT, value);  }
    public static int getIndex(Node child) { return (Integer)getConstraint(child, INDEX_CONSTRAINT); }

	//-------------------------------------------------------------------
	public static void setLarge(Node child, boolean value) { setConstraint(child, LARGE_CONSTRAINT, value); }
    public static boolean isLarge(Node child) {return (getConstraint(child, LARGE_CONSTRAINT)!=null)?(Boolean)getConstraint(child, LARGE_CONSTRAINT):false; }

    private static class StyleableProperties {

        private static final CssMetaData<ClockPane,Number> SPACING =
            new CssMetaData<ClockPane,Number>("-fx-spacing", SizeConverter.getInstance(), 0.0){
           @Override
           public boolean isSettable(ClockPane node) {
               return node.spacing == null || !node.spacing.isBound();
           }
           @Override
           public StyleableProperty<Number> getStyleableProperty(ClockPane node) {
               return (StyleableProperty<Number>)node.spacingProperty();
           }

        };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
        static {
           final List<CssMetaData<? extends Styleable, ?>> styleables =
               new ArrayList<CssMetaData<? extends Styleable, ?>>(Pane.getClassCssMetaData());
           styleables.add(SPACING);
           STYLEABLES = Collections.unmodifiableList(styleables);
        }
   }


    /**
     * The amount of horizontal space between each child in the grid.
     * @return the amount of horizontal space between each child in the grid
     */
    public final DoubleProperty spacingProperty() {
        if (spacing == null) {
            spacing = new StyleableDoubleProperty() {
                public void invalidated() { requestLayout(); }
                public CssMetaData getCssMetaData () { return StyleableProperties.SPACING; }
                public Object getBean() {return ClockPane.this; }
                public String getName() { return "spacing"; }
            };
        }
        return spacing;
    }

    private DoubleProperty spacing;
    public final void setSpacing(double value) { spacingProperty().set(value); }
    public final double getSpacing() { return spacing == null ? 0 : spacing.get(); }
    
	//-------------------------------------------------------------------
	public ClockPane() {
	}

	//-------------------------------------------------------------------
	public ClockPane(Node... children) {
		super(children);
	}

	//-------------------------------------------------------------------
	public void add(int index, Node child) {
		ClockPane.setConstraint(child, INDEX_CONSTRAINT, index);
		getChildren().add(child);
	}

	//-------------------------------------------------------------------
	public void add(int index, Node child, boolean large) {
		ClockPane.setIndex(child, index);
		ClockPane.setLarge(child, large);
		getChildren().add(child);
	}

    //-------------------------------------------------------------------
    /**
     * @see javafx.scene.Parent#layoutChildren()
     */
    @Override protected void layoutChildren() {
    	// Detect space for square center
    	double centerSize = Math.min(getWidth(), getHeight())/2;
    	
        List<Node> managed = getManagedChildren();
        Insets insets = getInsets();
        Pos align = Pos.CENTER; // getAlignmentInternal();
        HPos alignHpos = align.getHpos();
        VPos alignVpos = align.getVpos();
        double width = getWidth();
        double height = getHeight();
        double top = snapSpaceY(insets.getTop());
        double left = snapSpaceX(insets.getLeft());
        double bottom = snapSpaceY(insets.getBottom());
        double right = snapSpaceX(insets.getRight());
        double space = snapSpaceX(getSpacing());
        boolean shouldFillHeight = true; //shouldFillHeight();

//        final double[][] actualAreaWidths = getAreaWidths(managed, height, false);
//        double contentWidth = adjustAreaWidths(managed, actualAreaWidths, width, height);
//        double contentHeight = height - top - bottom;
//
//        double x = left + computeXOffset(width - left - right, contentWidth, align.getHpos());
//        double y = top;
//        double baselineOffset = -1;
//        if (alignVpos == VPos.BASELINE) {
//            double baselineComplement = getMinBaselineComplement();
//            baselineOffset = getAreaBaselineOffset(managed, marginAccessor, i -> actualAreaWidths[0][i],
//                    contentHeight, shouldFillHeight, baselineComplement);
//        }
//
//        for (int i = 0, size = managed.size(); i < size; i++) {
//            Node child = managed.get(i);
//            Insets margin = getMargin(child);
//            layoutInArea(child, x, y, actualAreaWidths[0][i], contentHeight,
//                    baselineOffset, margin, true, shouldFillHeight,
//                    alignHpos, alignVpos);
//            x += actualAreaWidths[0][i] + space;
//        }
        
        // Calculate begin of rows and columns
        double width0 = (getWidth() - centerSize -2*space)/2; //
        double width1 = (centerSize - space)/2;
        double width2 = width1;
        double width3 = width0;
        double height0 = (getHeight() - centerSize - 2*space)/2;
        double height1 = (centerSize - space)/2;
        double height2 = height1;
        double height3 = height0;
       
        double col0 = left;
        double col1 = col0 + width0 + space;
        double col2 = col1 + width1 + space;
        double col3 = col2 + width2 + space;
        double row0 = top;
        double row1 = row0 + height0 + space;
        double row2 = row1 + height1 + space;
        double row3 = row2 + height2 + space;

        
        
    	space = spacing.get();
        for (Node child : managed) {            
    		int index = getIndex(child);
        	boolean isLarge = isLarge(child);
        	switch (index) {
        	case 0: // Center
        		layoutInArea(child, col1, row1, centerSize, centerSize, 0, HPos.CENTER, VPos.CENTER);
        		break;
        	case 1:
        		layoutInArea(child, col2, row0, isLarge?(width2+space+width3):width2, height0, 0, alignHpos, alignVpos);
        		break;
        	case 2:
        		layoutInArea(child, col3, row0, width3, isLarge?(height0+space+height1):height0, 0, alignHpos, alignVpos);
        		break;
        	case 3:
        		layoutInArea(child, col3, row1, width3, isLarge?(height1+space+height2):height1, 0, alignHpos, alignVpos);
        		break;
        	case 4:
        		layoutInArea(child, col3, row2, width3, isLarge?(height2+space+height3):height2, 0, alignHpos, alignVpos);
        		break;
        	case 5:
        		layoutInArea(child, col3, row3, width3, height3, 0, alignHpos, alignVpos);
        		break;
        	case 6:
        		layoutInArea(child, col2, row3, isLarge?(width2+space+width3):width2, height3, 0, alignHpos, alignVpos);
        		break;
        	case 7:
        		layoutInArea(child, col1, row3, isLarge?(width1+space+width2):width1, height3, 0, alignHpos, alignVpos);
        		break;
        	case 8:
        		layoutInArea(child, col0, row3, isLarge?(width0+space+width1):width0, height3, 0, alignHpos, alignVpos);
        		break;
        	case 9:
        		layoutInArea(child, col0, row2, width0, isLarge?(height2+space+height3):height2, 0, alignHpos, alignVpos);
        		break;
        	case 10:
         		layoutInArea(child, col0, row1, width0, isLarge?(height1+space+height2):height1, 0, alignHpos, alignVpos);
        		break;
        	case 11:
        		layoutInArea(child, col0, row0, width0, isLarge?(height0+space+height1):height0, 0, alignHpos, alignVpos);
        		break;
        	case 12:
        		layoutInArea(child, col1, row0, isLarge?(width1+space+width2):width1, height0, 0, alignHpos, alignVpos);
        		break;
        	}
        	
        }
    }

}
