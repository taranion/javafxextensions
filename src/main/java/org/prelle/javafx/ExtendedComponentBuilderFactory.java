/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.fxml.IconElementBuilder;

import javafx.util.Builder;
import javafx.util.BuilderFactory;

/**
 * @author spr
 *
 */
public class ExtendedComponentBuilderFactory implements BuilderFactory {

	//-------------------------------------------------------------------
	/**
	 * @see javafx.util.BuilderFactory#getBuilder(java.lang.Class)
	 */
	@Override
	public Builder<?> getBuilder(Class<?> type) {
		System.out.println("ExtendedComponentBuilderFactory.getBuilder("+type+")");
		if (type==IconElement.class)
			return new IconElementBuilder();
		if (type==AppBarButton.class) {
			
		}
		return null;
	}

}
