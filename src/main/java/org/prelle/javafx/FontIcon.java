/**
 *
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.FontIconSkin;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;

/**
 * @author spr
 *
 */
public class FontIcon extends IconElement {

	private static final String DEFAULT_STYLE_CLASS = "font-icon";

	@FXML
	private StringProperty glyphProperty = new SimpleStringProperty();
	@FXML
	private StringProperty fontFamilyProperty = new SimpleStringProperty();

	//-------------------------------------------------------------------
	public FontIcon() {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		setSkin(new FontIconSkin(this));
		setMouseTransparent(true);
	}

	//-------------------------------------------------------------------
	public FontIcon(String fontFamily, String glyph) {
		this();
		fontFamilyProperty.set(fontFamily);
		glyphProperty.set(glyph);
	}

	//-------------------------------------------------------------------
	public FontIcon(String glyph) {
		this();
		fontFamilyProperty.set("Segoe UI Symbol");
		glyphProperty.set(glyph);
	}

	/******************************************
	 * Property GLYPH
	 ******************************************/
	public StringProperty glyphProperty() { return glyphProperty; }
	public String getGlyph() { return glyphProperty.get(); }
	public void setGlyph(String value) { glyphProperty.set(value); }

	/******************************************
	 * Property FONTFAMILY
	 ******************************************/
	public StringProperty fontFamilyProperty() { return fontFamilyProperty; }
	public String getFontFamily() { return fontFamilyProperty.get(); }
	public void setFontFamily(String value) { fontFamilyProperty.set(value); }

}
