/**
 *
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.SlideInBorderPaneBehaviour;
import org.prelle.javafx.skin.SlideInBorderPaneSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Control;

/**
 * @author Stefan
 *
 */
public class SlideInBorderPane extends Control {

	@FXML
	private ObjectProperty<Node> content;
	@FXML
	private ObjectProperty<SlideInPanel> visibleLeft;
	@FXML
	private ObjectProperty<SlideInPanel> visibleRight;
	@FXML
	private ObjectProperty<SlideInPanel> visibleTop;
	@FXML
	private ObjectProperty<SlideInPanel> visibleBottom;

	@FXML
	private ObservableList<SlideInPanel> rightSideBars;
	@FXML
	private ObservableList<SlideInPanel> leftSideBars;
	@FXML
	private ObservableList<SlideInPanel> topSideBars;
	@FXML
	private ObservableList<SlideInPanel> bottomSideBars;

	//--------------------------------------------------------------------
	public SlideInBorderPane() {
		content = new SimpleObjectProperty<Node>();
		visibleLeft   = new SimpleObjectProperty<>();
		visibleRight  = new SimpleObjectProperty<>();
		visibleTop    = new SimpleObjectProperty<>();
		visibleBottom = new SimpleObjectProperty<>();

		rightSideBars = FXCollections.observableArrayList();
		leftSideBars  = FXCollections.observableArrayList();
		topSideBars   = FXCollections.observableArrayList();
		bottomSideBars  = FXCollections.observableArrayList();

		SlideInBorderPaneSkin skin = new SlideInBorderPaneSkin(this);
		setSkin(skin);
		new SlideInBorderPaneBehaviour(this, skin);
	}

	//--------------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//--------------------------------------------------------------------
	public void setCenter(Node center) {
		content.set(center);
	}

	//--------------------------------------------------------------------
	public ObjectProperty<Node> centerProperty() {
		return content;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getLeftSidePanels() {
		return leftSideBars;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getRightSidePanels() {
		return rightSideBars;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getTopSidePanels() {
		return topSideBars;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getBottomSidePanels() {
		return bottomSideBars;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleLeftProperty() {
		return visibleLeft;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleRightProperty() {
		return visibleRight;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleTopProperty() {
		return visibleTop;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleBottomProperty() {
		return visibleBottom;
	}

}
