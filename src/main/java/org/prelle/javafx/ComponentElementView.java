package org.prelle.javafx;

import org.prelle.javafx.skin.ComponentElementCircleViewSkin;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class ComponentElementView extends Control {
	
	private IntegerProperty radius;
	private StringProperty  title;
	private StringProperty  subtitle;
	private ObjectProperty<Image> placeholder;
	private ObjectProperty<Image> image;
	
	private StringProperty[] names;
	private ObjectProperty<ObservableList<Object>>[] lists;
	private ObjectProperty<Node>[] overrideComponent;
	private ObjectProperty<Callback<ListView<Object>, ListCell<Object>>>[] cellFactory;
	private ObjectProperty<EventHandler<ActionEvent>>[] onAddAction;

	private BooleanProperty editButton;
	private ObjectProperty<EventHandler<ActionEvent>> onEditAction;

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public ComponentElementView() {
		radius   = new SimpleIntegerProperty(100);
		title    = new SimpleStringProperty();
		subtitle = new SimpleStringProperty();
		placeholder = new SimpleObjectProperty<Image>();
		image    = new SimpleObjectProperty<Image>();
		onEditAction = new SimpleObjectProperty<>();
		editButton  = new SimpleBooleanProperty();
		
		names = new SimpleStringProperty[12];
		for (int i=0; i<names.length; i++) names[i] = new SimpleStringProperty();
		
		lists = new SimpleObjectProperty[12];
		for (int i=0; i<lists.length; i++) lists[i] = new SimpleObjectProperty<>(FXCollections.observableArrayList());
		
		overrideComponent = new SimpleObjectProperty[12];
		for (int i=0; i<overrideComponent.length; i++) overrideComponent[i] = new SimpleObjectProperty<>();
		
		cellFactory = new SimpleObjectProperty[12];
		for (int i=0; i<cellFactory.length; i++) cellFactory[i] = new SimpleObjectProperty<>();
		
		onAddAction = new SimpleObjectProperty[12];
		for (int i=0; i<onAddAction.length; i++) onAddAction[i] = new SimpleObjectProperty<>();
		
		setSkin(new ComponentElementCircleViewSkin(this));
		getStyleClass().add("component-element-view");
	}

	//-------------------------------------------------------------------
	public IntegerProperty radiusProperty() { return radius; }
	public int getRadius() { return radius.get(); }
	public void setRadius(int val) { radius.set(val); }

	//-------------------------------------------------------------------
	public StringProperty titleProperty() { return title; }
	public String getTitle() { return title.get(); }
	public void setTitle(String val) { title.set(val); }

	//-------------------------------------------------------------------
	public StringProperty subtitleProperty() { return subtitle; }
	public String getSubtitle() { return subtitle.get(); }
	public void setSubtitle(String val) { subtitle.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<Image> placeholderProperty() { return placeholder; }
	public Image getPlaceholder() { return placeholder.get(); }
	public void setPlaceholder(Image val) { placeholder.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<Image> imageProperty() { return image; }
	public Image getImage() { return image.get(); }
	public void setImage(Image val) { image.set(val); }

	//-------------------------------------------------------------------
	public StringProperty nameProperty(int index) { return names[index]; }
	public String getName(int index) { return names[index].get(); }
	public void setName(int index, String val) { names[index].set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<ObservableList<Object>> listProperty(int index) { return lists[index]; }
	public ObservableList<Object> getList(int index) { return lists[index].get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<Node> overrideComponentProperty(int index) { return overrideComponent[index]; }
	public Node getOverrideComponent(int index) { return overrideComponent[index].get(); }
	public void setOverrideComponent(int index, Node val) { overrideComponent[index].set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<Callback<ListView<Object>, ListCell<Object>>> cellFactoryProperty(int index) { return cellFactory[index]; }
	public Callback<ListView<Object>, ListCell<Object>> getCellFactory(int index) { return cellFactory[index].get(); }
	public void setCellFactory(int index, Callback<ListView<Object>, ListCell<Object>> val) { cellFactory[index].set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<EventHandler<ActionEvent>> onAddActionProperty(int index) { return onAddAction[index]; }
	public EventHandler<ActionEvent> getOnAddAction(int index) { return onAddAction[index].get(); }
	public void setOnAddAction(int index, EventHandler<ActionEvent> val) { onAddAction[index].set(val); }

	//-------------------------------------------------------------------
	public BooleanProperty editButtonProperty() { return editButton; }
	public boolean getEditButton() { return editButton.get(); }
	public void setEditButton(boolean val) { editButton.set(val); }

	//-------------------------------------------------------------------
	public ObjectProperty<EventHandler<ActionEvent>> onEditActionProperty() { return onEditAction; }
	public EventHandler<ActionEvent> getOnEditAction() { return onEditAction.get(); }
	public void setOnEditAction(EventHandler<ActionEvent> val) { onEditAction.set(val); }

}
