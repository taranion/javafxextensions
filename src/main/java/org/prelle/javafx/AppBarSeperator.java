/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.control.SeparatorMenuItem;

/**
 * @author spr
 *
 */
public class AppBarSeperator extends SeparatorMenuItem implements ICommandBarElement {

	private static final String DEFAULT_STYLE_CLASS = "app-bar-seperator";

	//-------------------------------------------------------------------
	public AppBarSeperator() {
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
//		setSkin(new AppBarSeperatorSkin(this));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ICommandBarElement#isCompact()
	 */
	@Override
	public boolean isCompact() {
		return false;
	}

}
