/**
 *
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.ManagedDialogSkin;

import javafx.util.Callback;

/**
 * Used to control navigation buttons in dialogs
 *
 * @author prelle
 *
 */
public class NavigButtonControl {

	private ScreenManager manager;
	private ManagedDialog dialog;
	private ManagedDialogSkin skin;
	private Callback<CloseType, Boolean> optionalCallback;
	
	//--------------------------------------------------------------------
	public void initialize(ScreenManager manager, ManagedDialog dialog) {
		this.manager = manager;
		this.dialog  = dialog;
		skin = (ManagedDialogSkin)dialog.getSkin();
		skin.setDisabled(CloseType.OK, true);
		
		for (CloseType type : dialog.getButtons()) {
			dialog.setOnAction(type, ev -> manager.close(dialog, type));
			skin.setDisabled(type, !tryCall(type));
		}
	}

	//--------------------------------------------------------------------
	public void setDisabled(CloseType button, boolean disabled) {
		if (skin!=null)
			skin.setDisabled(button, disabled);
	}

//	//--------------------------------------------------------------------
//	public void setSkin(ManagedDialogSkin skin) {
//		this.skin = skin;
//		if (optionalCallback==null)
//			skin.setDisabled(CloseType.OK, true);
//	}

	//--------------------------------------------------------------------
	public void setCallback(Callback<CloseType, Boolean> callback) {
		this.optionalCallback = callback;
	}

	//--------------------------------------------------------------------
	public boolean tryCall(CloseType button) {
		if (optionalCallback==null)
			return true;

		return optionalCallback.call(button);
	}

	//-------------------------------------------------------------------
	/**
	 * Called e.g. when a event in a dialog component (other than buttons)
	 * shall result in a dialog closing - e.h. a ENTER in a TextField
	 * @param type
	 */
	public void fireEvent(CloseType type) {
		manager.close(dialog, type);
	}

}
