/**
 *
 */
package org.prelle.javafx;

import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public enum CloseType {

	APPLY,
	BACK,
	CANCEL,
	CLOSE,
	FINISH,
	NEXT,
	NO,
	OK,
	PREVIOUS,
	YES,
	QUIT,
	RANDCOMIZE,
	SAVE,
	;

	private final static ResourceBundle JFX_EXT = JavaFXConstants.RES;

	//-------------------------------------------------------------------
	public String getText() {
		return JFX_EXT.getString("button."+this.name().toLowerCase());
	}
}
