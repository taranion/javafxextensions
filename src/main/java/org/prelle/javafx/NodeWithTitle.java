package org.prelle.javafx;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.scene.Node;

public interface NodeWithTitle {

	//-------------------------------------------------------------------
	public Node getContent();
	public void setContent(Node value);
	public ReadOnlyObjectProperty<Node> contentProperty();

	//-------------------------------------------------------------------
	public String getTitle();
	public void setTitle(String value);
	public ReadOnlyStringProperty titleProperty();

}