package org.prelle.javafx.skin;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class ManagedDialogSkin extends SkinBase<ManagedDialog> {

	private ButtonBar buttons;

	private Label heading;
	private VBox content;
	private DialogLayout layout;

	private Map<CloseType, Button> buttonsByType;

	//-------------------------------------------------------------------
	public ManagedDialogSkin(ManagedDialog control) {
		super(control);
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		heading = new Label(getSkinnable().getTitle());

		buttons = new ButtonBar();
		buttonsByType = new HashMap<>();
		for (CloseType type : getSkinnable().getButtons()) {
			Button btn = new Button(JavaFXConstants.getButtonText(type));
			btn.setUserData(type);
			buttons.getButtons().add(btn);
			switch (type) {
			case APPLY : ButtonBar.setButtonData(btn, ButtonData.APPLY); break;
			case CANCEL:
			case CLOSE :
				ButtonBar.setButtonData(btn, ButtonData.CANCEL_CLOSE);
				break;
			case BACK  :
			case PREVIOUS:
				ButtonBar.setButtonData(btn, ButtonData.BACK_PREVIOUS);
				break;
			case FINISH:
			case QUIT:
				ButtonBar.setButtonData(btn, ButtonData.FINISH);
				break;
			case NEXT:
				ButtonBar.setButtonData(btn, ButtonData.NEXT_FORWARD);
				break;
			case NO: ButtonBar.setButtonData(btn, ButtonData.NO); break;
			case OK: ButtonBar.setButtonData(btn, ButtonData.OK_DONE); break;
			case RANDCOMIZE: ButtonBar.setButtonData(btn, ButtonData.OTHER); break;
			case YES  :  ButtonBar.setButtonData(btn, ButtonData.YES); break;
			default:
				break;
			}

			buttonsByType.put(type, btn);
		}

		content = new VBox();
		layout  = new DialogLayout(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		buttons.getStyleClass().addAll("dialog-buttonbar");
		heading.getStyleClass().addAll("text-subheader");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		heading.setMaxWidth(Double.MAX_VALUE);
		buttons.setMaxWidth(Double.MAX_VALUE);
//		buttons.setAlignment(Pos.CENTER_RIGHT);
		content.getChildren().add(heading);
		if (getSkinnable().getContent()!=null)
			content.getChildren().addAll(getSkinnable().getContent());
		content.getChildren().add(buttons);
		content.setSpacing(20);


		/*
		 * Tell control to take all the possible space
		 */
		getSkinnable().setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		getSkinnable().setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);

		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (Entry<CloseType, Button> entry : buttonsByType.entrySet()) {
			entry.getValue().setOnAction( ev -> {
				if (getSkinnable().getOnAction(entry.getKey())!=null) {
					getSkinnable().getOnAction(entry.getKey()).handle(ev);
				} else {
					getSkinnable().getScreenManager().close(getSkinnable(), entry.getKey());
				}
				});
		}

		heading.textProperty().bind(getSkinnable().titleProperty());
		getSkinnable().contentProperty().addListener( (ov,o,n) -> {
			if (o!=null)
				content.getChildren().remove(o);
			if (n!=null)
				content.getChildren().add(1, n);
		});
	}

	//-------------------------------------------------------------------
	public void refreshButtons() {
		Callback<CloseType, Boolean> check = getSkinnable().getButtonPredicateCheck();
		if (check==null)
			return;

		for (CloseType type : getSkinnable().getButtons()) {
			Boolean isEnabled = check.call(type);
			Button btn = buttonsByType.get(type);
			if (btn!=null)
				btn.setDisable(isEnabled!=null && !isEnabled);
		}
	}

	//-------------------------------------------------------------------
	public void setDisabled(CloseType type, boolean disabled) {
		if (buttonsByType.containsKey(type))
			buttonsByType.get(type).setDisable(disabled);
	}

}
