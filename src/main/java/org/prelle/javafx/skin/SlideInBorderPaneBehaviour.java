/**
 *
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.SlideInBorderPane;
import org.prelle.javafx.SlideInPanel;

/**
 * @author Stefan
 *
 */
public class SlideInBorderPaneBehaviour {

//	private SlideInBorderPane model;

	//--------------------------------------------------------------------
	public SlideInBorderPaneBehaviour(SlideInBorderPane model, SlideInBorderPaneSkin skin) {

		skin.setOnActionLeft(ev -> {
			model.visibleLeftProperty().set((SlideInPanel)ev.getSource());
		});
		skin.setOnActionRight(ev -> {
			model.visibleRightProperty().set((SlideInPanel)ev.getSource());
		});
		skin.setOnActionTop(ev -> {
			model.visibleTopProperty().set((SlideInPanel)ev.getSource());
		});
		skin.setOnActionBottom(ev -> {
			model.visibleBottomProperty().set((SlideInPanel)ev.getSource());
		});
	}

}
