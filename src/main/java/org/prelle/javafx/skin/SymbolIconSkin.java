/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.SymbolIcon;

import javafx.scene.control.SkinBase;
import javafx.scene.text.Text;

/**
 * @author spr
 *
 */
public class SymbolIconSkin extends SkinBase<SymbolIcon> {
	
	private Text icon;

	//-------------------------------------------------------------------
	public SymbolIconSkin(SymbolIcon control) {
		super(control);
		icon = new Text();
		icon.setText(getStringForSymbol(getSkinnable().getSymbol()));
		icon.setStyle("-fx-font-family: 'Segoe MDL2 Assets'");
		icon.getStyleClass().add("symbol-icon");
		getSkinnable().symbolProperty().addListener( (ov,o,n) -> icon.setText(getStringForSymbol(getSkinnable().getSymbol())));
		
		getChildren().add(icon);
	}

	//-------------------------------------------------------------------
	/**
	 * https://docs.microsoft.com/de-de/uwp/api/windows.ui.xaml.controls.symbol
	 * https://docs.microsoft.com/en-us/windows/uwp/design/style/segoe-ui-symbol-font
	 */
	public static char getGlyphForSymbol(String symbol) {
		switch (symbol.toLowerCase()) {
		case "accept"      : return '\uE10B'; 
		case "account"     : return '\uE168';
		case "add"         : return '\uE109';
		case "addfriend"   : return '\uE1E2';
		case "admin"       : return '\uE1A7';
		case "aligncenter" : return '\uE1A1';
		case "alignleft"   : return '\uE1A2';
		case "alignright"  : return '\uE1A0';
		case "allapps"     : return '\uE179';
		case "attach"      : return '\uE16C';
		case "attachcamera": return '\uE12D';
		case "audio"       : return '\uE189';
		case "back"        : return '\uE112';
		case "backtowindow": return '\uE1D8';
		case "blockcontact": return '\uE1E0';
		case "bold"        : return '\uE19B';
		case "bookmarks"   : return '\uE12F';
		case "browsephotos": return '\uE155';
		case "bullets"     : return '\uE133';
		case "calculator"  : return '\uE1D0';
		case "calendar"    : return '\uE163';
		case "calendarday" : return '\uE161';
		case "calendarreply":return '\uE1DB';
		case "calendarweek": return '\uE162';
		case "camera"      : return '\uE114';
		case "cancel"      : return '\uE10A';
		case "caption"     : return '\uE15A';
		case "cellphone"   : return '\uE1C9';
		case "character"   : return '\uE164';
		case "clear"       : return '\uE106';
		case "clearselection":return '\uE1C5';
		case "clock"       : return '\uE121';
		case "closedcaption":return '\uE190';
		case "closepane"   : return '\uE127';
		case "comment"     : return '\uE134';
		case "contact"     : return '\uE13D';
		case "contact2"    : return '\uE187';
		case "contactinfo" : return '\uE136';
		case "copy"        : return '\uE16F';
		case "crop"        : return '\uE123';
		case "cut"         : return '\uE16B';
		case "delete"      : return '\uE107';
		case "directions"  : return '\uE1D1';
		case "disableupdates": return '\uE194';
		case "disconnectdrive": return '\uE17A';
		case "dislike"     : return '\uE19E';
		case "dockbottom"  : return '\uE147';
		case "dockleft"    : return '\uE145';
		case "dockright"   : return '\uE146';
		case "document"    : return '\uE130';
		case "download"    : return '\uE118';
		case "edit"        : return '\uE104';
		case "emoji"       : return '\uE11D';
		case "emoji2"      : return '\uE170';
		case "favorite"    : return '\uE113';
		case "filter"      : return '\uE16E';
		case "find"        : return '\uE11A';
		case "flag"    	   : return '\uE129';
		case "folder"      : return '\uE188';
		case "font"        : return '\uE185';
		case "fontcolor"   : return '\uE186';
		case "fontdecrease": return '\uE1C6';
		case "fontincrease": return '\uE1C7';
		case "fontsize"    : return '\uE1C8';
		case "forward"     : return '\uE111';
		case "fourbars"    : return '\uE1E9';
		case "fullscreen"  : return '\uE1D9';
		case "globalnavigationbutton": return '\uE700';
		case "globe"       : return '\uE12B';
		case "go"          : return '\uE143';
		case "gotostart"   : return '\uE1E4';
		case "gototoday"   : return '\uE184';
		case "hangup"      : return '\uE137';
		case "help"        : return '\uE11B';
		case "hidebcc"     : return '\uE16A';
		case "highlight"   : return '\uE193';
		case "home"        : return '\uE10F'; // \uE80F
		case "import"      : return '\uE150';
		case "importall"   : return '\uE151';
		case "imprtant"    : return '\uE171';
		case "italic"      : return '\uE199';
		case "keyboard"    : return '\uE144';
		case "leavechat"   : return '\uE11F';
		case "library"     : return '\uE1D3';
		case "like"        : return '\uE19F';
		case "likedislike" : return '\uE19D';
		case "link"        : return '\uE167';
		case "list"        : return '\uE14C';
		case "mail"        : return '\uE119';
		case "mailfilled"  : return '\uE135';
		case "mailforward" : return '\uE120';
		case "mailreply"   : return '\uE172';
		case "mailreplyall": return '\uE165';
		case "manage"      : return '\uE178';
		case "map"         : return '\uE1C4';
		case "mapdrive"    : return '\uE17B';
		case "mappin"      : return '\uE139';
		case "memo"        : return '\uE1D5';
		case "message"     : return '\uE15F';
		case "microphone"  : return '\uE1D6';
		case "more"        : return '\uE10C';
		case "movetofolder": return '\uE19C';
		case "musicinfo"   : return '\uE142';
		case "mute"        : return '\uE198';
		
		case "outlinestar" : return '\uE1CE';
		case "solidstar"   : return '\uE1CF';

		case "people"      : return '\uE125';
		case "phone"       : return '\uE13A';
		case "phonebook"   : return '\uE1D4';
		case "play"        : return '\uE102';
		case "repeatall"   : return '\uE1CD';
		case "remove"      : return '\uE108';

		case "save"        : return '\uE105';
		case "setting"     : return '\uE115';
		case "shuffle"     : return '\uE14B';
		case "sort"        : return '\uE174';
		case "stop"        : return '\uE15B';
		case "street"      : return '\uE1C3';
		
		case "undo"        : return '\uE10E';
		
		case "webcam"      : return '\uE156';
		case "world"       : return '\uE128';  // or E909

		case "adjusthologram"     : return '\uEBD2';
		case "car"         : return '\uE804';
		case "calculatoraddition" : return '\uE948';
		case "calculatorsubstract": return '\uE949';
		case "connect"     : return '\uE703';
		case "construction": return '\uE822';
		case "developertools": return '\uEC7A';
		case "drivingmode" : return '\uE7EC';
		case "education"   : return '\uE7BE';
		case "fileexplorer": return '\uEC50';
		case "lock"        : return '\uE72E';
		case "partyleader" : return '\uECA7';
		case "print"       : return '\uE749';
		case "redeye"      : return '\uE7B3';
		case "remote"      : return '\uE8AF';
		case "tiltdown"    : return '\uE80A';
		case "shoppingcart": return '\uE7BF';
		case "vehicle"     : return '\uE7EC';
		case "walksolid"   : return '\uE726';
		
		// Self defined names
		case "bell"        : return '\uE1FA';
		case "robot"       : return '\uE99A';
		}
		throw new IllegalArgumentException("Unknown symbol: "+symbol);
	}

	//-------------------------------------------------------------------
	/**
	 * https://docs.microsoft.com/de-de/uwp/api/windows.ui.xaml.controls.symbol
	 * https://docs.microsoft.com/en-us/windows/uwp/design/style/segoe-ui-symbol-font
	 */
	public static String getExtendedGlyphForSymbol(String symbol) {
		switch (symbol.toLowerCase()) {
		// Emojis
		case "lock"        : return "\uD83D\uDD12";
		case "footprints"  : return "\uD83D\uDC63";
		case "mage"        : return "\uD83E\uDDD9";
		}
		throw new IllegalArgumentException("Unknown symbol: "+symbol);
	}

	//-------------------------------------------------------------------
	public static String getStringForSymbol(String symbol) {
		if (symbol==null)
			return null;
		try {
			return String.valueOf(getGlyphForSymbol(symbol));
		} catch (IllegalArgumentException e) {
		}
		return getExtendedGlyphForSymbol(symbol);
	}

}
