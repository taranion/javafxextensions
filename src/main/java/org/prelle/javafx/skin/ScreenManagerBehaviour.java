package org.prelle.javafx.skin;

import java.util.Arrays;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManager;

/**
 * @author Stefan Prelle
 *
 */
public class ScreenManagerBehaviour {

	private final static Logger logger = LogManager.getLogger("prelle.jfx");

	private ScreenManager manager;
	private Stack<ManagedScreen> stack;

	//-------------------------------------------------------------------
	public ScreenManagerBehaviour(ScreenManager manager) {
		this.manager = manager;
		stack = new Stack<>();
	}

	//--------------------------------------------------------------------
	private boolean canBeLeft(ManagedScreen screen) {
		logger.debug("  canBeLeft: callback is "+screen.getCanBeLeftCallback() );
		if (screen.getCanBeLeftCallback()!=null)
			return screen.getCanBeLeftCallback().call(screen);
		return true;
	}

	//-------------------------------------------------------------------
	public void navigateTo(ManagedScreen screen) {
		logger.trace("START: navigateTo("+screen+")");

		try {
			logger.info("Navigate to "+screen.getClass());
			screen.setManager(manager);

			/*
			 * Add additional stylesheets
			 */
			if (screen.getStyleSheets().length>0) {
				logger.debug("Add stylesheet "+Arrays.toString(screen.getStyleSheets()));
				manager.getScene().getStylesheets().addAll(screen.getStyleSheets());
			}
			
			/*
			 * Remove previous NavigationView, if exists
			 */
			if (!stack.isEmpty()) {
				screen.clearSelection();;
				ManagedScreen previous = stack.peek();
				previous.clearSelection();
				manager.getChildren().remove(previous);
			}

			logger.debug("Add "+screen+" to "+manager.getChildren());
			manager.getChildren().add(screen);

			stack.add(screen);

			// Enable back button
			screen.setBackButtonEnabled(stack.size()>1);
			screen.setBackButtonVisible(stack.size()>1);
			screen.setSettingsVisible(stack.size()==1);

			// Call hook of screen to inform it that it has been opened
			screen.onOpen();
		} finally {
			logger.trace("STOP : navigateTo("+screen+")");
		}
	}

	//-------------------------------------------------------------------
	public void onBackRequested(ManagedScreen screen) {
		logger.debug("START: onBackRequested("+screen+")");

		try {
			/*
			 * That that the screen to be closed is the one on top of the stack
			 */
			if (screen!=stack.peek()) {
				logger.error("A screen shall be closed that is not the top most");
				logger.warn("  to close: "+screen);
				logger.warn("  topmost : "+stack.peek());
				return;
			}

			/*
			 * Ask screen itself, if it is okay to close it. This allows to present
			 * a confirmation dialog.
			 */
			boolean closeAllowed = canBeLeft(screen);
			if (closeAllowed) {
				logger.info("close current screen");
				/*
				 * Remove additional stylesheets
				 */
				if (screen.getStyleSheets().length>0) {
					logger.debug("Remove stylesheets "+Arrays.toString(screen.getStyleSheets()));
					manager.getScene().getStylesheets().removeAll(screen.getStyleSheets());
				}
				
				manager.getChildren().remove(screen);
				stack.pop();
				screen.onClose();

				ManagedScreen presentNow = stack.peek();
				manager.getChildren().add(presentNow);
			} else {
				logger.warn("BACK requested by user, but current screen prevents it");
				return;
			}
		} finally {
			logger.debug("STOP : onBackRequested("+screen+")");
		}

	}

	//-------------------------------------------------------------------
	public ManagedScreen getCurrentScreen() {
		return stack.peek();
	}

}
