/**
 *
 */
package org.prelle.javafx.skin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.css.PseudoClass;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author Stefan
 *
 */
/**
 * @author Stefan
 *
 * @param <S>
 */
public class GridPaneTableViewSkin<S> extends SkinBase<TableView<S>> {

	/** Taken from com.sun.javafx.scene.control.Properties.RECREATE */
	private final static String REFRESH_PROPERTY = "recreateKey";

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private GridPane grid;
	private Map<S,TableRow<S>> rows;

	private ObjectProperty<TableRow<S>> highlightRow;

	//--------------------------------------------------------------------
	public GridPaneTableViewSkin(TableView<S> control) {
		super(control);
		rows =  new HashMap<S, TableRow<S>>();
		highlightRow = new SimpleObjectProperty<>();
		initComponents();
		initLayout();
		initInteractivity();
		rebuildAll();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		grid = new GridPane();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		getChildren().add(grid);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		ListChangeListener<TableColumn<S,?>> colList = new ListChangeListener<TableColumn<S,?>>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends TableColumn<S, ?>> change) {
				while (change.next()) {
					logger.info("Change "+change);
					if (change.wasAdded()) {

					}
					rebuildAll();
				}
			}};
		getSkinnable().getColumns().addListener(colList);

		getSkinnable().getItems().addListener(new ListChangeListener<S>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends S> arg0) {
				rebuildAll();
			}});

		highlightRow.addListener( (ov,o,n) -> {
			if (o!=null) {
				getCellsOfRow(o).forEach(cell -> {
					cell.pseudoClassStateChanged(PseudoClass.getPseudoClass("rowhover"), false);
				});
			}
			if (n!=null) {
				getCellsOfRow(n).forEach(cell -> {
					cell.pseudoClassStateChanged(PseudoClass.getPseudoClass("rowhover"), true);
				});
			}
		});

		getSkinnable().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (o!=null) {
				getCellsOfRow(rows.get(o)).forEach(cell -> cell.pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), false));
			}
			if (n!=null) {
				getCellsOfRow(rows.get(n)).forEach(cell -> cell.pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), true));
			}
		});

		grid.prefWidthProperty().bind(getSkinnable().prefWidthProperty());

		getSkinnable().getProperties().addListener(new MapChangeListener<Object, Object>(){

			@Override
			public void onChanged(javafx.collections.MapChangeListener.Change<? extends Object, ? extends Object> change) {
				if (String.valueOf(change.getKey()).equals(REFRESH_PROPERTY)) {
//					logger.warn("Refresh");
					rebuildAll();
			        getSkinnable().getProperties().put(REFRESH_PROPERTY, Boolean.FALSE);
				}
//				else
//					logger.warn("Unhandled property change: "+change.getKey());
			}});
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private Collection<TableCell<S,?>>  getCellsOfRow(TableRow<S> row) {
		List<TableCell<S,?>> ret = new ArrayList<TableCell<S,?>>();
		for (Node node : grid.getChildren()) {
			if (node instanceof TableCell && ((TableCell<S,?>)node).getTableRow()==row) {
				ret.add((TableCell<S, ?>) node);
			}
		}
		return ret;
	}

	//--------------------------------------------------------------------
	private <T> void rebuildColumn(TableColumn<S,T> col, int x) {
		if (col==null) return;
		int y=0;
		Label lbHead = new Label(col.getText());
		lbHead.getStyleClass().addAll("table-head","base");
		lbHead.textProperty().bind(col.textProperty());
		lbHead.prefWidthProperty().bind(col.prefWidthProperty());
		lbHead.minWidthProperty().bind(col.minWidthProperty());
		lbHead.maxWidthProperty().bind(col.maxWidthProperty());
		grid.add(lbHead, x, y);


		/*
		 * Now column values
		 */
		for (S item : getSkinnable().getItems()) {
			y++;
			if (col.getCellValueFactory()==null)
				continue;
//			ObservableValue<T> obs = col.getCellValueFactory().call(new TableColumn.CellDataFeatures<S,T>(getSkinnable(),col,item));
//			logger.debug("Value = "+obs);
//			logger.warn("rowItem = "+rows.get(item)+"   and item = "+item);
			TableCell<S,T> cell = col.getCellFactory().call(col);
			cell.getStyleClass().add("table-column");
			cell.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
//			cell.setMaxWidth(Double.MAX_VALUE);
			cell.updateTableColumn(col);
			cell.updateTableView(getSkinnable());
			cell.updateTableRow(rows.get(item));
			rows.get(item).setItem(item);
			cell.updateIndex(y-1);
			String mergedStyle = (col.getStyle()!=null && col.getStyle().length()>1)?(col.getStyle()+";"):"";
			if (cell.getStyle()!=null)
				mergedStyle+=cell.getStyle();
			cell.setStyle(mergedStyle);
			cell.prefWidthProperty().bind(col.prefWidthProperty());
			cell.minWidthProperty().bind(col.minWidthProperty());
			cell.maxWidthProperty().bind(col.maxWidthProperty());
			cell.applyCss();

			// Place
			grid.add(cell, x, y);
			GridPane.setVgrow(cell, Priority.NEVER);
			GridPane.setHgrow(cell, Priority.SOMETIMES);

			// Interactivity
			cell.setOnMouseEntered(ev -> highlightRow.setValue(cell.getTableRow()));
			cell.setOnMouseClicked(ev -> getSkinnable().getSelectionModel().select(cell.getIndex()));
		}

		// Size
		ColumnConstraints cons = new ColumnConstraints();
		grid.getColumnConstraints().add(cons);
		cons.setMinWidth(col.getMinWidth());
		cons.setPrefWidth(col.getPrefWidth());
		cons.setMaxWidth(col.getMaxWidth());
//		cons.setPercentWidth(col.getPrefWidth());
//		cons.prefWidthProperty().bind(col.prefWidthProperty());
//		cons.maxWidthProperty().bind(col.maxWidthProperty());
//		cons.minWidthProperty().bind(col.minWidthProperty());
//		logger.warn("Col."+col.getText()+" : pref="+col.getPrefWidth());
		cons.prefWidthProperty().addListener( (ov,o,n) -> logger.error("Column "+col.getId()+" changes pref width to "+n));
	}

	//--------------------------------------------------------------------
	private void rebuildAll() {
		grid.getChildren().clear();
		grid.setGridLinesVisible(true);
		grid.getColumnConstraints().clear();

		rows.clear();
		for (S item : getSkinnable().getItems()) {
			TableRow<S> row = new TableRow<S>();
			rows.put(item, row);
		}

		int x=-1;
		for (TableColumn<S, ?> col : getSkinnable().getColumns()) {
			x++;
			rebuildColumn(col, x);
		}

	}
}
