package org.prelle.javafx.skin;

import org.prelle.javafx.NavigationItemHeader;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.WritableValue;
import javafx.css.PseudoClass;
import javafx.css.StyleableProperty;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

class NavigationItemButton extends Button {
	
	private static final String DEFAULT_STYLE_CLASS = "navigation-item";

    // --- Pseudo class: SELECTED
    private static final PseudoClass PSEUDO_CLASS_SELECTED = PseudoClass.getPseudoClass("selected");
    public BooleanProperty selectedProperty = new BooleanPropertyBase(false) {
    	        @Override protected void invalidated() { pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, selectedProperty.get()); }
    	        @Override public Object getBean() { return NavigationItemButton.this; }
    	        @Override public String getName() { return "selected"; }
    	   };
    public boolean isSelected() { return selectedProperty.get(); }
    public BooleanProperty selectedProperty() { return selectedProperty; }

    // --- item
    private ObjectProperty<MenuItem> item = new SimpleObjectProperty<MenuItem>(this, "item");
    public final ObjectProperty<MenuItem> itemProperty() { return item; }
    public final void setItem(MenuItem value) { item.set(value); }
    public final MenuItem getItem() { return item.get(); }

    protected void updateItem(MenuItem item) {
        setItem(item);
    }
    
    //-----------------------------------------------------------------
    public NavigationItemButton(NavigationPaneSkin skin, MenuItem item, boolean withText) {
//    	this.setUserData(item);
    	itemProperty().set(item);
    	textProperty().bind(item.textProperty());
    	graphicProperty().bind(item.graphicProperty());
    	
    	if (getGraphic()!=null && !getGraphic().getStyleClass().contains("navigation-icon"))
    		getGraphic().getStyleClass().add("navigation-icon");
    	graphicProperty().addListener( (ov,o,n) -> {
    		if (n!=null && !n.getStyleClass().contains("navigation-icon"))
    			n.getStyleClass().add("navigation-icon");
    	});
    	
    	/*
    	 * When clicked change selection model and fire event on MenuItem
    	 */
    	setOnAction(ev -> {
    		skin.getSkinnable().getSelectionModel().select(item);
    		item.fire();
    	});
    	skin.getSkinnable().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
    		selectedProperty.set(n==item);
    	});
    	
        // focusTraversable is styleable through css. Calling setFocusTraversable
        // makes it look to css like the user set the value and css will not
        // override. Initializing focusTraversable by calling set on the
        // CssMetaData ensures that css will be able to override the value.
        ((StyleableProperty<Boolean>)(WritableValue<Boolean>)focusTraversableProperty()).applyStyle(null, Boolean.FALSE);
        getStyleClass().clear();
//       getStyleClass().addAll(DEFAULT_STYLE_CLASS);
//        if (item instanceof NavigationItem)
        if (item instanceof NavigationItemHeader)
        	getStyleClass().addAll("navigation-item-header");
        else if (item instanceof SeparatorMenuItem) {
        	getStyleClass().addAll("navigation-item-separator");
        } else 
        	getStyleClass().addAll(DEFAULT_STYLE_CLASS);
        
   }

}