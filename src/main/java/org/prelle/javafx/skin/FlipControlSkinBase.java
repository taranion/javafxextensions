/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.FlipControl;

import javafx.scene.Node;
import javafx.scene.control.SkinBase;

/**
 * @author prelle
 *
 */
public abstract class FlipControlSkinBase extends SkinBase<FlipControl> {

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public FlipControlSkinBase(FlipControl control) {
		super(control);
		// TODO Auto-generated constructor stub
	}
	
	//-------------------------------------------------------------------
    public abstract void flip(Node front, Node back);

}
