/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.FontIcon;
import org.prelle.javafx.Persona;

import javafx.scene.control.SkinBase;
import javafx.scene.text.Text;

/**
 * @author spr
 *
 */
public class PersonaSkin extends SkinBase<Persona> {
	
	private Text icon;

	//-------------------------------------------------------------------
	public PersonaSkin(Persona control) {
		super(control);
		icon = new Text();
//		icon.setText(getSkinnable().getGlyph());
//		icon.getStyleClass().add("font-icon-text");
//		icon.setStyle("-fx-font-family: '"+getSkinnable().getFontFamily()+"'");
//		getSkinnable().glyphProperty().addListener( (ov,o,n) -> icon.setText(getSkinnable().getGlyph()));
//		getSkinnable().fontFamilyProperty().addListener( (ov,o,n) -> icon.setStyle("-fx-font-family: '"+getSkinnable().getFontFamily()+"'"));
		
		getChildren().add(icon);
	}

}
