package org.prelle.javafx.skin;

import java.util.Collections;
import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.KeywordInputControl;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class KeywordInputControlSkin<T> extends SkinBase<KeywordInputControl<T>> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private ComboBox<T> input;
	private FlowPane flow;
	
	private T lastAdded;
	
	private Comparator<T> comparator;

	//-------------------------------------------------------------------
	public KeywordInputControlSkin(KeywordInputControl<T> control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
		
		listChange(control.getItems());
		historyChange(control.getOptions());
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		flow = new FlowPane();
		flow.setStyle("-fx-min-height: 5em; -fx-vgap: 0.3em; -fx-hgap: 0.4em; -fx-border-width: 1px; -fx-border-style: solid");
		
		comparator = new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				if (getSkinnable().getConverter()!=null)
					return getSkinnable().getConverter().toString(o1).compareTo(getSkinnable().getConverter().toString(o2));
				return 0;
			}
		}; 
		
		input = new ComboBox<T>();
		input.setEditable(true);
		if (getSkinnable().getConverter()!=null) {
			input.setConverter(getSkinnable().getConverter());
			Collections.sort(input.getItems(), comparator);
		}
		
		Class<T> cls = getSkinnable().classProperty().get();
		if (cls.isEnum()) {
			logger.debug(cls+" is an Enum");
			input.setEditable(false);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		flow.setStyle("-fx-min-height: 10em");
		ScrollPane scroll = new ScrollPane(flow);
		scroll.setStyle("-fx-min-height: 6em");
		scroll.setFitToWidth(true);
		VBox layout = new VBox(10);
		layout.getChildren().addAll(input, scroll);
		VBox.setVgrow(layout, Priority.SOMETIMES);
		
		getSkinnable().impl_getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().itemsProperty().addListener( (ov,o,n) -> listChange(n));
		getSkinnable().optionsProperty().addListener( (ov,o,n) -> historyChange(n));
		input.setOnAction(ev -> inputConfirmed(ev));
		getSkinnable().converterProperty().addListener( (ov,o,n) -> {
			input.setConverter(n);
			if (getSkinnable().getConverter()!=null)
				Collections.sort(input.getItems(), comparator);
			});
		
	}

	//-------------------------------------------------------------------
	private void addToFlow(T value) {
		logger.debug("addToFlow("+value+")");
		Button btn = new Button("\uE0CA");
		btn.getStyleClass().add("keyword-button");
		StringConverter<T> converter = getSkinnable().getConverter();
		Label label = new Label((converter!=null)?converter.toString(value):String.valueOf(value), btn);
		label.setGraphicTextGap(1);
		label.setContentDisplay(ContentDisplay.RIGHT);
		label.getStyleClass().add("keyword");
		label.setUserData(value);
		
		flow.getChildren().add(label);
		
		btn.setOnAction(ev -> getSkinnable().getItems().remove(value));
	}

	//-------------------------------------------------------------------
	private void removeFromFlow(T value) {
		logger.debug("removeFromFlow("+value+")");
		for (Node node : flow.getChildren()) {
			if (value.equals(node.getUserData())) {
				flow.getChildren().remove(node);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	private void listChange(ObservableList<T> n) {
		flow.getChildren().clear();		
		if (n==null)
			return;
		
		for (T selected : n) {
			addToFlow(selected);
		}
		
		n.addListener(new ListChangeListener<T>() {

			@Override
			public void onChanged(Change<? extends T> c) {
				logger.warn("TODO: change = "+c+"  .... getSkinnable().getItems()="+getSkinnable().getItems());
				while (c.next()) {
					if (c.wasAdded()) {
						c.getAddedSubList().forEach(data -> addToFlow(data));
					}
					if (c.wasRemoved()) {
						input.getSelectionModel().clearSelection();
						c.getRemoved().forEach(data -> removeFromFlow(data));
					}
				}
			}
		});
		if (getSkinnable().getConverter()!=null) {
			Collections.sort(input.getItems(), comparator);
		}

	}

	//-------------------------------------------------------------------
	private void historyChange(ObservableList<T> n) {
		input.getItems().clear();		
		if (n==null)
			return;
		
		input.getItems().addAll(n);
		
		n.addListener(new ListChangeListener<T>() {
			public void onChanged(Change<? extends T> c) {
				while (c.next()) {
					if (c.wasAdded()) {
						c.getAddedSubList().forEach(data -> input.getItems().add(data));
					}
					if (c.wasPermutated()) {
						input.getItems().clear();		
						input.getItems().addAll(getSkinnable().getOptions());
					}
				}
				if (getSkinnable().getConverter()!=null) {
					Collections.sort(input.getItems(), new Comparator<T>() {
						@Override
						public int compare(T o1, T o2) {
							if (getSkinnable().getConverter()!=null)
								return getSkinnable().getConverter().toString(o1).compareTo(getSkinnable().getConverter().toString(o2));
							return 0;
						}
					});
				}
			}
		});
	}

	//-------------------------------------------------------------------
	private void inputConfirmed(ActionEvent ev) {
		logger.debug("input confirmed "+ev);
		
		T data = input.getValue();
		if (data==null)
			return;
		if ((data instanceof String) && getSkinnable().converterProperty().get()!=null)
			data = getSkinnable().converterProperty().get().fromString( (String)data);
		if (data==lastAdded)
			return;
		input.setValue(null);
		
		try {
			if (getSkinnable().getItems().contains(data))
				return;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.warn("Calling getSkinnable().getItems().add");
		getSkinnable().getItems().add(data);
		lastAdded = data;
		if (!getSkinnable().getOptions().contains(data)) {
			getSkinnable().getOptions().add(data);
			logger.warn("**** "+getSkinnable().getClass().getTypeParameters() );
//			Collections.sort(getSkinnable().getHistory());
		}
				
	}
	
}
