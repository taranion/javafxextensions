/**
 * 
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.NavigationView;
import org.prelle.javafx.NavigationView.DisplayMode;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author spr
 *
 */
public class NavigationViewSkin extends SkinBase<NavigationView> {
	
	private final static int WIDTH_EXPANDED = 1400;
	private final static int WIDTH_COMPACT  = 640;

	private final static Logger logger = LogManager.getLogger("prelle.jfx");

	private NavigationViewHeadingLine heading;
	private StackPane stack;
	private VBox contentColumn;

	private NavigationPane leftPane;
	private HBox overlayLayout, verticalLayout;
	private transient Region swap;

	private transient WindowMode windowMode;

	//-------------------------------------------------------------------
	public NavigationViewSkin(NavigationView control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
//		control.selectedItemProperty().bind( getNavigationPane().getSelectionModel().selectedItemProperty() );
	}

	//-----------------------------------------------------------------
	private void initComponents() {
		heading = new NavigationViewHeadingLine();
		leftPane= new NavigationPane(getSkinnable());
		stack   = new StackPane();
		swap    = new Region();
		swap.setMaxWidth(Double.MAX_VALUE);
	}

	//-----------------------------------------------------------------
	/**
	 * +--------+--------------ContentColumn----------------------+
	 * |        |  Title                                          |
	 * | PlaceH +-------------------------------------------------+
	 * |        |  Content                                        |
	 * |        |                                                 |
	 * |        |                                                 |
	 * |        |                                                 |
	 * +--------+-------------------------------------------------+
	 */
	private void initLayout() {
		contentColumn = new VBox();
		contentColumn.getChildren().add(heading);
		contentColumn.getStyleClass().add("navigation-view-content");
		contentColumn.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		if (getSkinnable().getContent()!=null) {
			contentColumn.getChildren().add(getSkinnable().getContent());
			VBox.setVgrow(getSkinnable().getContent(), Priority.ALWAYS);
		}

		// Vertical layout
		verticalLayout = new HBox(leftPane, contentColumn);
		HBox.setHgrow(contentColumn, Priority.ALWAYS);

		// Overlay layout
		Region overlayBuffer = new Region();
		overlayBuffer.setStyle("-fx-background-color: transparent");
		overlayBuffer.setManaged(true);
		overlayBuffer.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);		
		overlayLayout = new HBox(overlayBuffer);
		HBox.setHgrow(overlayBuffer, Priority.ALWAYS);

		// Stack and content
		stack.getChildren().addAll(verticalLayout, overlayLayout);
		getChildren().add(stack);

		overlayLayout.setVisible(false);
		overlayLayout.setManaged(false);
	}

	//-----------------------------------------------------------------
	private void initInteractivity() {
		heading.textProperty().bind(getSkinnable().headerProperty());
		getSkinnable().contentProperty().addListener( (ov,o,n) -> {
			logger.debug("++++++++++++++++++++++++content changed from "+o+" to "+n);
			if (o!=null)
				contentColumn.getChildren().remove(o);
			// Content changed
			if (n!=null) {
				contentColumn.getChildren().add(n);
				VBox.setVgrow(n, Priority.ALWAYS);
			}
		});

		getSkinnable().displayModeProperty().addListener( (ov,o,n) -> {
			if (n!=null && n!=DisplayMode.AUTO)
				changeDisplayMode(n);
			else {
				setAutoDisplayMode();
			}
		});
		getSkinnable().widthProperty().addListener( (ov,o,n) -> sizeChanged());
		//		getSkinnable().windowModeProperty().addListener( (ov,o,n) -> windowModeChanged(o,n));
		//
		//		navigationBar.setOnMenuAction(ev -> toggleNavigationOpenClose());
		leftPane.getMenuItem().setOnAction( ev -> menuClicked(ev));
		heading.getMenuButton().setOnAction(ev -> menuClicked(ev));

		swap.prefWidthProperty().bind(leftPane.widthProperty());
		swap.minWidthProperty().bind(leftPane.widthProperty());
		
		leftPane.backButtonVisibleProperty().bind(getSkinnable().backButtonVisibleProperty());
		
		leftPane.onBackActionProperty().bind(getSkinnable().onBackActionProperty());
		heading.getBackButton().onActionProperty().bind(getSkinnable().onBackActionProperty());
	}

	//-------------------------------------------------------------------
	private void swapWithOverlay(DisplayMode mode) {
		if (mode==DisplayMode.LEFT_COMPACT) {
			if (verticalLayout.getChildren().contains(leftPane)) {
				// Show NavigationPane in overlay
				verticalLayout.getChildren().remove(leftPane);
				verticalLayout.getChildren().add(0,swap);
				leftPane.setCompact(false);
				overlayLayout.getChildren().add(0,leftPane);
			} else if (overlayLayout.getChildren().contains(leftPane)) {
				overlayLayout.getChildren().remove(leftPane);
				verticalLayout.getChildren().remove(swap);
				verticalLayout.getChildren().add(0,leftPane);
				leftPane.setCompact(true);
			}
		} else if (mode==DisplayMode.LEFT_MINIMAL) {
			logger.debug("Overlay = "+overlayLayout.getChildren().contains(leftPane));
			if (!overlayLayout.getChildren().contains(leftPane)) {
				overlayLayout.getChildren().remove(swap);
				overlayLayout.getChildren().add(0,leftPane);
				leftPane.setVisible(true);
				leftPane.setManaged(true);
				leftPane.setCompact(false);
			} else {
				overlayLayout.getChildren().remove(leftPane);
				verticalLayout.getChildren().add(0,leftPane);
				leftPane.setVisible(false);
				leftPane.setManaged(false);
				leftPane.setCompact(true);
			}
		}
	}

	//-------------------------------------------------------------------
	private void menuClicked(ActionEvent ev) {

		DisplayMode mode = getSkinnable().getDisplayMode();
		if (mode==DisplayMode.AUTO) {
			mode = getAutoMode();
		}
		logger.info("Menu clicked in mode "+mode);

		switch (mode) {
		case LEFT:
			leftPane.setCompact(!leftPane.isCompact());
			break;
		case LEFT_COMPACT:
		case LEFT_MINIMAL:
			swapWithOverlay(mode);
			overlayLayout.setVisible(!overlayLayout.isVisible());
			overlayLayout.setManaged(!overlayLayout.isManaged());
			logger.debug("OverlayLayout is now visible = "+overlayLayout.isVisible());
			break;
		case AUTO:
			break;
		}
	}

	//-----------------------------------------------------------------
	private void setAutoDisplayMode() {
		logger.info("Auto  for "+getSkinnable().getWidth());
	}

	//-----------------------------------------------------------------
	private void changeDisplayMode(DisplayMode n) {
		logger.info("DisplayMode changed to "+n);
		switch (n) {
		case AUTO: 
			break;
		case LEFT:
			leftPane.setCompact(false);
			leftPane.setVisible(true);
			leftPane.setManaged(true);
			heading.setBackVisible(false);
			heading.setMenuVisible(false);
			break;
		case LEFT_COMPACT:
			leftPane.setCompact(true);
			leftPane.setVisible(true);
			leftPane.setManaged(true);
			heading.setBackVisible(false);
			heading.setMenuVisible(false);
			break;
		case LEFT_MINIMAL:
			leftPane.setCompact(true);
			leftPane.setVisible(false);
			leftPane.setManaged(false);
			heading.setBackVisible(getSkinnable().isBackButtonVisible());
			heading.setMenuVisible(true);
			break;
		}
		overlayLayout.setVisible(false);
		overlayLayout.setManaged(false);
	}

	//-------------------------------------------------------------------
	private DisplayMode getAutoMode() {
		double width = getSkinnable().getWidth();
		if (width<=640) return DisplayMode.LEFT_MINIMAL;
		if (width<=1008) return DisplayMode.LEFT_COMPACT;
		return DisplayMode.LEFT;
	}

	//-------------------------------------------------------------------
	private void sizeChanged() {
		double width = getSkinnable().getWidth();
		WindowMode newMode = WindowMode.MINIMAL;
		if (width<=WIDTH_COMPACT) newMode = WindowMode.MINIMAL;
		else if (width<=WIDTH_EXPANDED) newMode = WindowMode.COMPACT;
		else newMode = WindowMode.EXPANDED;

		if (newMode!=windowMode) {
			changeMode(newMode);
		}
	}

	//--------------------------------------------------------------------
	private void changeMode(WindowMode value) {
		this.windowMode = value;
		logger.info("Responsive: WindowMode changed to "+value);
		if (getSkinnable().getDisplayMode()==DisplayMode.AUTO) {
			switch (value) {
			case EXPANDED:
				contentColumn.getStyleClass().setAll("navigation-view-content");
				changeDisplayMode(DisplayMode.LEFT); 
				break;
			case COMPACT : 
				contentColumn.getStyleClass().setAll("navigation-view-content");
				changeDisplayMode(DisplayMode.LEFT_COMPACT); 
				break;
			case MINIMAL : 
				contentColumn.getStyleClass().setAll("navigation-view-content","navigation-view-content-minimal");
				changeDisplayMode(DisplayMode.LEFT_MINIMAL); 
				break;
			}
		}

		changeModeRecursivly(getSkinnable(), value);
	}

	//--------------------------------------------------------------------
	private void changeModeRecursivly(Parent parent, WindowMode value) {
		for (Node tmp : parent.getChildrenUnmodifiable()) {
			if (tmp instanceof ResponsiveControl) 
				((ResponsiveControl)tmp).setResponsiveMode(value);
			if (tmp instanceof Parent)
				changeModeRecursivly((Parent) tmp, value);
		}
	}

	//--------------------------------------------------------------------
	public NavigationPane getNavigationPane() {
		return leftPane;
	}

	//--------------------------------------------------------------------
	public void clearSelection() {
		leftPane.getSelectionModel().clearSelection();
	}

}
