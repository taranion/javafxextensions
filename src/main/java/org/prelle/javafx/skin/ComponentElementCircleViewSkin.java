package org.prelle.javafx.skin;

import java.util.ArrayList;
import java.util.List;

import org.prelle.javafx.CircleIconPane;
import org.prelle.javafx.ComponentElementView;
import org.prelle.javafx.ExtendedListView;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.layout.ClockPane;

import javafx.application.Platform;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;

/**
 * @author prelle
 *
 */
public class ComponentElementCircleViewSkin extends SkinBase<ComponentElementView> {

	private CircleIconPane circlePane;
	
	private ExtendedListView<Object>[] listViews;
	private Button[] btnAdd;
	private Button[] btnDelete;
	
	private ClockPane grid;
	private StackPane gridAndLines;
	private LineLayer lines;
	
	private double centerX, centerY;
	private Polyline[] line;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ComponentElementCircleViewSkin(ComponentElementView control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initComponents() {
		Color darkRed = Color.web("#b2598d");
		circlePane = new CircleIconPane();
		
		btnAdd = new Button[12];
		for (int i=0; i<btnAdd.length; i++) {
			btnAdd[i] =  new Button(null, new SymbolIcon("add"));
			btnAdd[i].getStyleClass().add("extended-list-view-button");
			btnAdd[i].onActionProperty().bind(getSkinnable().onAddActionProperty(i));
		}
		btnDelete = new Button[12];
		for (int i=0; i<btnDelete.length; i++) {
			btnDelete[i] =  new Button(null, new SymbolIcon("delete"));
			btnDelete[i].getStyleClass().add("extended-list-view-button");
		}

		listViews = new ExtendedListView[12];
		for (int i=0; i<listViews.length; i++) {
			listViews[i] =  new ExtendedListView<>();
			listViews[i].setAddButton(btnAdd[i]);
//			listViews[i].setDeleteButton(btnDelete[i]);
		}
		
		line = new Polyline[12];
		for (int i=0; i<line.length; i++) {
			line[i] = new Polyline();
			line[i].getStyleClass().add("circle-view-line");
			line[i].setStroke(darkRed);
			line[i].setStrokeWidth(2);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		grid = new ClockPane();
		grid.setSpacing(20);
		grid.add(0, circlePane);
		for (int i=1; i<=12; i++) {
			grid.add(i, listViews[i-1]);
		}
		
		lines = new LineLayer(line);
		lines.prefWidthProperty().bind(grid.widthProperty());
		lines.prefHeightProperty().bind(grid.heightProperty());
		StackPane.setAlignment(lines, Pos.TOP_CENTER);
		gridAndLines = new StackPane(lines, grid);
		gridAndLines.setPickOnBounds(true);

		getChildren().add(gridAndLines);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		circlePane.layoutBoundsProperty().addListener( (ov,o,n) -> System.out.println("cpane.layoutBounds="+circlePane.getBoundsInParent()));
		circlePane.titleProperty().bind(getSkinnable().titleProperty());
		circlePane.subtitleProperty().bind(getSkinnable().subtitleProperty());
		getSkinnable().imageProperty().addListener( (ov,o,n) -> refreshImageView());
		getSkinnable().placeholderProperty().addListener( (ov,o,n) -> refreshImageView());
		circlePane.widthProperty().addListener( (ov,o,n) -> centerX=(circlePane.getWidth())/2 +circlePane.getBoundsInParent().getMinX());
		circlePane.heightProperty().addListener( (ov,o,n) -> centerY=(circlePane.getHeight())/2 +circlePane.getBoundsInParent().getMinY());
		circlePane.widthProperty().addListener( (ov,o,n) -> refreshLines());
		circlePane.editButtonProperty().bind(getSkinnable().editButtonProperty());
		circlePane.onEditActionProperty().bind(getSkinnable().onEditActionProperty());
		
		for (int i=0; i<12; i++) {
			listViews[i].titleProperty().bind(getSkinnable().nameProperty(i));
			listViews[i].itemsProperty().bind(getSkinnable().listProperty(i));
			listViews[i].cellFactoryProperty().bind(getSkinnable().cellFactoryProperty(i));
			final int index = i+1;
			getSkinnable().overrideComponentProperty(i).addListener( (ov,o,n) -> {
				if  (o!=null)
					grid.getChildren().remove(o);
				if (n==null) {
					if (!grid.getChildren().contains(listViews[index-1])) {
						grid.add(index, listViews[index-1]);
					}
				} else {
					grid.add(index, n);
				}
			});
		}
		
//		lvLong1.layoutBoundsProperty().addListener( (ov,o,n) -> refreshLines());
		getSkinnable().widthProperty().addListener( (ov,o,n) -> refreshLines());
	}
	
	//-------------------------------------------------------------------
	private void refreshLines() {
//		System.out.println("Skin.refreshLines");
			Platform.runLater(() ->  {
				centerX=(circlePane.getWidth())/2 +circlePane.getBoundsInParent().getMinX();
				centerY=(circlePane.getHeight())/2 +circlePane.getBoundsInParent().getMinY();
				for (int i=0; i<12; i++) {
					configureLine(line[i], listViews[i]);
					if (listViews[i].getTitle()==null || getSkinnable().getOverrideComponent(i)!=null) {
						line[i].setVisible(false);
						listViews[i].setVisible(false);
					} else {
						line[i].setVisible(true);
						listViews[i].setVisible(true);
					}
				}
			});
	}
	
	//-------------------------------------------------------------------
	private void configureLine(Polyline line, ExtendedListView<?> elist) {
		ListView<?> list = ((ExtendedListViewSkin)elist.getSkin()).getListView();
		Bounds bounds = grid.localToParent(elist.localToParent(list.getBoundsInParent()));
		List<Double> ret = new ArrayList<Double>();
		ret.add(centerX);
		ret.add(centerY);
		double x1 = (Math.abs(centerX-bounds.getMinX()) < Math.abs(centerX-bounds.getMaxX()))?bounds.getMinX():bounds.getMaxX();
		double y1 = (Math.abs(centerY-bounds.getMinY()) < Math.abs(centerY-bounds.getMaxY()))?bounds.getMinY():bounds.getMaxY();
		ret.add(x1); ret.add(y1);
		
		double lastY = y1;
		if (y1==bounds.getMaxY()) {
			ret.add(x1);
			ret.add(bounds.getMinY());
			lastY = bounds.getMinY();
		}
		// Other side of element
		if (x1==bounds.getMaxX()) {
			ret.add(bounds.getMinX());
			ret.add(lastY);
		} else {
			ret.add(bounds.getMaxX());
			ret.add(lastY);
		}

		
		/*
		 * Now instead of originating from the circle center, 
		 * find the point of the circle itself at the same angle
		 */
		double diffX = ret.get(2)-ret.get(0);
		double diffY = ret.get(3)-ret.get(1);
		double angle = Math.atan( diffX / diffY );
		double radius = ((CircleIconPaneSkin)circlePane.getSkin()).getRadius();  //getSkinnable().getRadius();
		double a = Math.sin(angle)* radius;
		double b = Math.cos(angle)* radius;
		if ((a>0 && diffX<0) || (a<0 && diffX>0)) a*=-1;
		if ((b>0 && diffY<0) || (b<0 && diffY>0)) b*=-1;
		ret.add(0, ret.get(0)+a); ret.remove( 1);
		ret.add(1, ret.get(1)+b); ret.remove( 2);
		
//		line.getPoints().setAll(ret);
		line.getPoints().clear();
		line.getPoints().addAll(ret);
	}

//	//-------------------------------------------------------------------
//	private void configureLine(Line line, VBox box, ListView<Object> list) {
//		Bounds bounds = grid.localToParent(box.localToParent(list.getBoundsInParent()));
//		line.setStartX(lines.getCenterX());
//		line.setStartY(lines.getCenterY());
//		line.setEndX(getLineCoordinates(box, list));
//		line.setEndY(bounds.getMinY());
//	}

	//-------------------------------------------------------------------
	private void refreshImageView() {
		if (getSkinnable().getImage()!=null) {
			circlePane.setImage(getSkinnable().getImage());
		} else {
			circlePane.setImage(getSkinnable().getPlaceholder());
		}
	}
	
	//-------------------------------------------------------------------
	public ExtendedListView<Object> geListView(int index) {
		return listViews[index];
	}

}

class LineLayer extends Region {
	
	private double centerX, centerY;
	
	public LineLayer(Polyline... lines ) {
		this.setMaxWidth(Double.MAX_VALUE);
		Line tl = new Line(0, 0, 0,0);
		tl.setStroke(Color.DARKRED);
		getChildren().add(tl);
//		Line line = new Line(0,0, 300,300);
		getChildren().addAll(lines);
		
		widthProperty().addListener( (ov,o,n) -> centerX=((double)n)/2);
		heightProperty().addListener( (ov,o,n) -> centerY=((double)n)/2);
	}
	
	public double getCenterX() { return centerX; }
	public double getCenterY() { return centerY; }
	
}

