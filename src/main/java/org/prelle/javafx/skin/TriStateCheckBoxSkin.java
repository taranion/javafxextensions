/**
 * 
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.TriStateCheckBox;
import org.prelle.javafx.TriStateCheckBox.State;

import javafx.collections.ListChangeListener;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 * @author prelle
 *
 */
public class TriStateCheckBoxSkin extends SkinBase<TriStateCheckBox>  {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private TriStateCheckBox control;
	
    private StackPane slider;
    private ResizeableCanvas canvas;
    private GraphicsContext g;

    private HBox layout;
    private Label label;
    
	//-------------------------------------------------------------------
	public TriStateCheckBoxSkin(TriStateCheckBox checkbox) {
		super(checkbox);
//		logger.debug("<init>");
		this.control = checkbox;

		label = new Label("Foo");
		label.setMaxWidth(Double.MAX_VALUE);
		label.setAlignment(Pos.CENTER_LEFT);
		
		canvas = new ResizeableCanvas();
        canvas.setStyle("-fx-pref-width: 8em;");
//		logger.debug("Canvas is resizeable = "+canvas.isResizable());
		g = canvas.getGraphicsContext2D();
        slider = new StackPane();
        slider.getStyleClass().setAll("slider");
        slider.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        slider.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        slider.setStyle("-fx-pref-width: 8em;");
        slider.getChildren().add(canvas);
        slider.setMinWidth(100);
//        slider.setStyle("-fx-background-color: aqua");
        
        layout = new HBox(5);
        layout.getChildren().addAll(label, slider);
         HBox.setHgrow(label, Priority.ALWAYS);
        control.impl_getChildren().add(layout);
         
       updateChildren();
       
        initInteractivity();
 	}

//    //--------------------------------------------------------------------
//    /**
//     * @see com.sun.javafx.scene.control.skin.LabeledSkinBase#updateChildren()
//     */
//    @Override 
    protected void updateChildren() {
//        super.updateChildren();
        setTextFor(getSkinnable().getState());
        updateTrack();
    }
	

	//-------------------------------------------------------------------
	private void setTextFor(TriStateCheckBox.State newState) {
		String newText = null;
		if (getSkinnable().getStateFormatter()!=null)
			newText = getSkinnable().getStateFormatter().toString(newState);
		else
			newText = newState.name();
//		getSkinnable().setText(newText);
		label.setText(newText);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		control.stateProperty().addListener( (ov,o,n) -> setTextFor(n));
		
		canvas.widthProperty().bind(slider.widthProperty());
		canvas.heightProperty().bind(label.heightProperty());
		
		slider.widthProperty().addListener( (ov,o,n) -> {
			canvas.resize((Double)n, canvas.getHeight());
			updateTrack();
			});
		label.heightProperty().addListener( (ov,o,n) -> {
//			logger.debug("Height changed to "+n);
			slider.resize(slider.getWidth(), (Double)n);
			updateTrack();
		});
		
		slider.setOnMouseClicked(event -> {
			double rel = event.getX()/slider.getWidth()*100;
			State pos = State.SELECTION2;
			if (rel<30) pos=State.SELECTION1;
			else if (rel>70) pos=State.SELECTION3;
			else pos=State.SELECTION2;
			if (control.isDisabled())
				return;
//			logger.debug("ev = "+pos);
			control.select(pos);
//			control.fire();
			updateTrack();
		});
		
		control.getAllowedStates().addListener(new ListChangeListener<State>() {
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends State> c) {
				updateChildren();
			}
		});
		
		control.stateProperty().addListener( (ov,o,n) -> updateChildren());
	}

	//--------------------------------------------------------------------
	private void drawSlot(State state, double x) {
		double width = canvas.getWidth();
		double height = canvas.getHeight();
		double snapWidth = width/10.0;
		double snapHeight= height/5.0;
		
		if (getSkinnable().isAllowed(state)) {
			// Upper half
			g.strokeLine(x, snapHeight, x, 0);
			g.strokeLine(x, 0, x+snapWidth, 0);
			g.strokeLine(x+snapWidth, 0, x+snapWidth, snapHeight);
			// Lower half
			g.strokeLine(x, height-snapHeight, x, height);
			g.strokeLine(x, height, x+snapWidth, height);
			g.strokeLine(x+snapWidth, height, x+snapWidth, height-snapHeight);
		} else {
			g.strokeLine(x, height-snapHeight, x+snapWidth, height-snapHeight);
			g.strokeLine(x, snapHeight, x+snapWidth, snapHeight);
		}
	}

	//--------------------------------------------------------------------
	private void updateTrack() {
		if (g==null)
			return;
//		logger.debug("updateTrack("+canvas.getWidth()+"x"+canvas.getHeight()+")   ("+slider.getWidth());
		
		double width = canvas.getWidth();
		double height = canvas.getHeight();
		double snapWidth = width/10.0;
		double snapHeight= height/5.0;
		
		g.setStroke(Color.BLACK);
		g.clearRect(0, 0, width, height);
		
//		g.setFill(Color.ROSYBROWN);
//		g.fillRect(3, snapHeight+3, width-6, height-snapHeight*2-6);
//		g.strokeRect(0, 0, canvas.getWidth()/2, canvas.getHeight()/2);

		g.strokeLine(0, snapHeight, 0, height-snapHeight);
		drawSlot(TriStateCheckBox.State.SELECTION1, 0);
		g.strokeLine(snapWidth, snapHeight, width/2.0-snapWidth/2.0, snapHeight);
		g.strokeLine(snapWidth, height-snapHeight, width/2.0-snapWidth/2.0, height-snapHeight);
		drawSlot(TriStateCheckBox.State.SELECTION2, width/2.0-snapWidth/2.0);
		g.strokeLine(width/2.0+snapWidth/2.0, snapHeight, width-snapWidth, snapHeight);
		g.strokeLine(width/2.0+snapWidth/2.0, height-snapHeight, width-snapWidth, height-snapHeight);
		drawSlot(TriStateCheckBox.State.SELECTION3, width-snapWidth);
		g.strokeLine(width, snapHeight, width, height-snapHeight);
		
		// Draw knob
		g.setFill(Color.BLACK);
		switch (getSkinnable().getState()) {
		case SELECTION1:
			g.fillRect(0, 0, snapWidth, height);
			break;
		case SELECTION2:
			g.fillRect(width/2.0-snapWidth/2.0, 0, snapWidth, height);
			break;
		case SELECTION3:
			g.fillRect(width-snapWidth, 0, snapWidth, height);
			break;
		}
	}

//	//--------------------------------------------------------------------
//	private static double measureText(String text) {
//		final Text text2 = new Text(text);
//	    Scene scene = new Scene(new Group(text2));
//	    ModernUI.initialize(scene); 
//
//	    return text2.getLayoutBounds().getWidth();
//	}
//
//	//-------------------------------------------------------------------
//	private void calculateMaxKnobWidth() {
//		StringConverter<TriStateCheckBox.State> format = getSkinnable().getStateFormatter();
//		String text1 = format.toString(TriStateCheckBox.State.SELECTION1);
//		String text2 = format.toString(TriStateCheckBox.State.SELECTION2);
//		String text3 = format.toString(TriStateCheckBox.State.SELECTION3);
//		
//		double width1 = measureText(text1);
//		double width2 = measureText(text2);
//		double width3 = measureText(text3);
//		
////		text.set
//		double knobWidth = Math.max(width1, Math.max(width2, width3));
//		
////		knobWidth += knob.getPadding().getRight() + knob.getPadding().getLeft();
//		
//		logger.debug("Knob width is "+knobWidth);
////		knob.setMinWidth(knobWidth);
////		knob.setPrefWidth(knobWidth);
////		
////		getSkinnable().setPrefWidth(knobWidth*2);
////		getSkinnable().resize(getSkinnable().getPrefWidth(), getSkinnable().getPrefHeight());
//	}

}