/**
 *
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.FlipControl;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * @author prelle
 *
 */
public class FlipControlBehavior {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private FlipControl control;

    //-----------------------------------------------------------------
	protected FlipControl getControl() {
		return control;
	}

	//-----------------------------------------------------------------
    private final ChangeListener<ObservableList<Node>> itemsListener = new ChangeListener<ObservableList<Node>>() {
        @Override
        public void changed(
                ObservableValue<? extends ObservableList<Node>> observable,
                ObservableList<Node> oldValue, ObservableList<Node> newValue) {
        	logger.debug("changed: "+newValue);
//            if (oldValue != null) {
//                 oldValue.removeListener(weakItemsListListener);
//             } if (newValue != null) {
//                 newValue.addListener(weakItemsListListener);
//             }
        }
    };

    //-----------------------------------------------------------------
    private final EventHandler<MouseEvent> clickedListener = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			logger.debug("clicked "+event.getSource());
			flipForward();
			event.consume();
		}
    };

    // Listen to changes when the whole item list is replaced
    private final WeakChangeListener<ObservableList<Node>> weakItemsListener =
            new WeakChangeListener<ObservableList<Node>>(itemsListener);


	//-------------------------------------------------------------------
	public FlipControlBehavior(FlipControl control) {
		this.control = control;

        control.itemsProperty().addListener(weakItemsListener);
        // Auskommentiert, weil sonst AdaptingFlipControls nicht mehr funktionieren
//        if (control.getItems() != null)
//            control.getItems().addListener(weakItemsListListener);
        control.setOnMouseClicked(clickedListener);
	}

    //-----------------------------------------------------------------
	public void makeVisible(int index) {
		logger.debug("makeVisible("+index+")");
		// Make all nodes invisible
		for (Node node : getControl().getItems())
			if (node.isVisible())
				node.setVisible(false);

		// Make the new node visible
		Node makeVisible = getControl().getItems().get(index);
//		logger.debug("makeVisible("+index+") = "+makeVisible);
//		logger.debug("Make "+makeVisible+" visible    "+getControl().getChildrenUnmodifiable());
		makeVisible.setVisible(true);
		getControl().visibleIndexProperty().set(index);
		getControl().visibleNodeProperty().set(makeVisible);
	}

    //-----------------------------------------------------------------
    /**
     * When flipped forward which is the next node?
     */
    private Node getNextSide() {
    	int current = getControl().getVisibleIndex();
    	int next    = current+1;
    	if (next>= getControl().getItems().size())
    		next = 0;

    	return getControl().getItems().get(next);
    }

    //-----------------------------------------------------------------
    public void flipForward() {
		Node current = getControl().getVisibleNode();
		Node next    = getNextSide();
		if (getControl().getSkin() instanceof FlipControlSkinBase)
			((FlipControlSkinBase)getControl().getSkin()).flip(current, next);
    }

    //-----------------------------------------------------------------
    protected void callAction(String name) {
    	logger.debug("callAction("+name+")");
    	if ("FlipForward".equals(name)) {
    		flipForward();
    		return;
    	}
    }

}
