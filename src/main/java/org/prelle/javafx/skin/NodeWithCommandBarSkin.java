package org.prelle.javafx.skin;

import org.prelle.javafx.NodeWithCommandBar;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class NodeWithCommandBarSkin extends SkinBase<NodeWithCommandBar> {
	
//	private ScrollPane scroll;
	private VBox layout;

	//-------------------------------------------------------------------
	public NodeWithCommandBarSkin(NodeWithCommandBar control) {
		super(control);
		
//		scroll = new ScrollPane(control.getContent());
//		scroll.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
//		scroll.setFitToWidth(true);
		
		 layout = new VBox();
		layout.getChildren().addAll(control.getCommandBar());
		if (control.getContent()!=null) {
			layout.getChildren().add(control.getContent());
			VBox.setVgrow(control.getContent(), Priority.ALWAYS);
		}
		getChildren().add(layout);
//		control.getCommandBar().setStyle("-fx-background-color: pink");
		
		// Updates
		control.contentProperty().addListener( (ov,o,n) -> {
			if (o!=null)
				layout.getChildren().remove(o);
			if (n!=null) {
				layout.getChildren().add(n);
				VBox.setVgrow(control.getContent(), Priority.ALWAYS);
			}
		});
//		scroll.contentProperty().bind(control.contentProperty());
	}

}
