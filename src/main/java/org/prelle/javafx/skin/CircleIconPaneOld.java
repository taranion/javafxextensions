/**
 * 
 */
package org.prelle.javafx.skin;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

/**
 * @author prelle
 *
 */
public class CircleIconPaneOld extends StackPane {
	
	private IntegerProperty radius;
	private StringProperty  line1;
	private StringProperty  line2;
	private StringProperty  line3;
	private ObjectProperty<ImageView> icon;
	
	private Circle circle;
	private Label labelLine1;
	private Label labelLine2;
	private Label labelLine3;

	//-----------------------------------------------------------------
	/**
	 */
	public CircleIconPaneOld(int radius) {
		this.radius = new SimpleIntegerProperty(radius);
		line1 = new SimpleStringProperty();
		line2 = new SimpleStringProperty();
		line3 = new SimpleStringProperty();
		icon  = new SimpleObjectProperty<ImageView>();
		
		getStyleClass().add("device-circle");

		circle = new Circle(radius);
		circle.getStyleClass().add("device-circle");
		labelLine1 = new Label();
		labelLine1.getStyleClass().add("heading");
		labelLine2 = new Label();
		labelLine3 = new Label();
		VBox labelBox = new VBox(5);
		labelBox.setAlignment(Pos.CENTER);
		labelBox.getChildren().addAll(labelLine1, labelLine2, labelLine3);
		
		// Add nodes
		getChildren().addAll(circle, labelBox);
		
		// Property change listener to automatically set label texts
		labelLine1.textProperty().bind(line1);
		labelLine2.textProperty().bind(line2);
		labelLine3.textProperty().bind(line3);
		icon.addListener(new ChangeListener<ImageView>() {
			@Override
			public void changed(ObservableValue<? extends ImageView> iconProp,
					ImageView oldIcon, ImageView newIcon) {
				getChildren().remove(oldIcon);
				getChildren().add(0,newIcon);
			}
		});
		
		// Initial size
		setPrefSize(radius, radius);
		setMinSize(radius, radius);
	}

	//-----------------------------------------------------------------
	public int getRadius() {
		return radius.get();
	}

	//-----------------------------------------------------------------
	public void setRadius(int radius) {
		this.radius.set(radius);
	}

	//-----------------------------------------------------------------
	public String getLine1() {
		return line1.get();
	}

	//-----------------------------------------------------------------
	public void setLine1(String text) {
		line1.set(text);
	}

	//-----------------------------------------------------------------
	public String getLine2() {
		return line2.get();
	}

	//-----------------------------------------------------------------
	public void setLine2(String text) {
		line2.set(text);
	}

	//-----------------------------------------------------------------
	public String getLine3() {
		return line3.get();
	}

	//-----------------------------------------------------------------
	public void setLine3(String text) {
		line3.set(text);
	}

	//-----------------------------------------------------------------
	public ImageView getIcon() {
		return icon.get();
	}

	//-----------------------------------------------------------------
	public void setIcon(ImageView icon) {
		this.icon.set(icon);
	}

}
