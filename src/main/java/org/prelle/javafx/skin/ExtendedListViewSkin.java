package org.prelle.javafx.skin;

import org.prelle.javafx.ExtendedListView;

import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ExtendedListViewSkin<T> extends SkinBase<ExtendedListView<T>> {
	
	private HBox buttons;
	private Label heading;
	private ListView<T> list;

	//-------------------------------------------------------------------
	public ExtendedListViewSkin(ExtendedListView<T> control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list = new ListView<T>(getSkinnable().getItems());
		list.cellFactoryProperty().bind(getSkinnable().cellFactoryProperty());
		buttons = new HBox(5);
		heading = new Label(getSkinnable().getTitle());
		heading.getStyleClass().addAll("title","extended-list-view-heading");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		heading.setMaxWidth(Double.MAX_VALUE);
		HBox bxHeading = new HBox(5, heading, buttons);
		bxHeading.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(heading, Priority.ALWAYS);

		VBox layout = new VBox(2, bxHeading, list);
		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.itemsProperty().bind(getSkinnable().itemsProperty());
		heading.textProperty().bind(getSkinnable().titleProperty());
		getSkinnable().addButtonProperty().addListener( (ov,o,n) -> updateButtons());
		getSkinnable().deleteButtonProperty().addListener( (ov,o,n) -> updateButtons());
		getSkinnable().settingsButtonProperty().addListener( (ov,o,n) -> updateButtons());
	}

	//-------------------------------------------------------------------
	private void updateButtons() {
		buttons.getChildren().clear();
		if (getSkinnable().getAddButton()!=null) buttons.getChildren().add(getSkinnable().getAddButton());
		if (getSkinnable().getDeleteButton()!=null) buttons.getChildren().add(getSkinnable().getDeleteButton());
		if (getSkinnable().getSettingsButton()!=null) buttons.getChildren().add(getSkinnable().getSettingsButton());
	}

	//-------------------------------------------------------------------
	public ListView<T> getListView() {
		return list;
	}
}
