/**
 * 
 */
package org.prelle.javafx.skin;

import java.util.HashMap;
import java.util.Map;

import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Polyline;

/**
 * @author prelle
 *
 */
public class CircleGridPaneLower extends Pane {

	private Map<Node, Polyline> tableToLine;
	
	private double centerX;
	private double centerY;
	private int radius;
	
	//-----------------------------------------------------------------
	/**
	 */
	public CircleGridPaneLower() {
		tableToLine = new HashMap<Node, Polyline>();
	}

	//-----------------------------------------------------------------
	public void makeLine(Node label, int col, int row) {
		Polyline line = new Polyline(0,0, 0,0);
		line.getStyleClass().add("sidepaneline");
		tableToLine.put(label, line);
		getChildren().add(line);
	}

	//-----------------------------------------------------------------
	public void circleChanged(int radius, double totalWidth, double totalHeight) {
		centerX = totalWidth/2.0;
		centerY = totalHeight/2;
		this.radius = radius;
	}

	//-----------------------------------------------------------------
	public void tableChanged(VBox pane, Node label, Node node, CircleGridPane.Position pos) {
		Polyline line = tableToLine.get(label);
		
		Bounds bounds = pane.getBoundsInParent();
		if (bounds.getMaxX()==0) return;
		double endY = bounds.getMinY() + label.getBoundsInParent().getHeight();
		double[] points = null;
		switch (pos) {
		case TOP:
			points = new double[]{
					centerX,centerY, 
					bounds.getMinX(), endY+node.getBoundsInParent().getHeight(), 
					bounds.getMinX(), endY};
			break;
		case TOP_RIGHT:
			points = new double[]{
					centerX,centerY, 
					bounds.getMinX(), endY+node.getBoundsInParent().getHeight()/1.5, 
					bounds.getMinX(), endY};
			break;
		case UPPER_RIGHT:
		case LOWER_RIGHT:
		case BOTTOM_RIGHT:
			points = new double[]{
					centerX,centerY, 
					bounds.getMinX(), endY};
			break;
		case BOTTOM:
			points = new double[]{
					centerX,centerY, 
					bounds.getMinX(), bounds.getMinY(), 
					bounds.getMinX(), endY};
			break;
		case BOTTOM_LEFT:
		case LOWER_LEFT:
		case UPPER_LEFT:
			points = new double[]{
					centerX,centerY, 
					bounds.getMaxX(), endY};
			break;
		case TOP_LEFT:
			points = new double[]{
					centerX,centerY, 
					bounds.getMaxX(), endY+node.getBoundsInParent().getHeight(), 
					bounds.getMaxX(), endY};
			break;
		default:
			points = new double[]{centerX,centerY};
		}
		
		/*
		 * Now instead of originating from the circle center, 
		 * find the point of the circle itself at the same angle
		 */
		double diffX = points[2]-points[0];
		double diffY = points[3]-points[1];
		double angle = Math.atan( (points[2]-points[0]) / (points[3]-points[1]) );
		double a = Math.sin(angle)* radius;
		double b = Math.cos(angle)* radius;
		if ((a>0 && diffX<0) || (a<0 && diffX>0)) a*=-1;
		if ((b>0 && diffY<0) || (b<0 && diffY>0)) b*=-1;
		points[0] = points[0]+a;
		points[1] = points[1]+b;
		
		// Need array of Double, instead double
		Double[] dPoints = new Double[points.length];
		for (int i=0; i<dPoints.length; i++)
			dPoints[i] = points[i];
		ObservableList<Double> opoints = line.getPoints();
		opoints.clear();
		opoints.addAll(dPoints);
	}

}
