/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.SymbolIcon;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;

/**
 * @author spr
 *
 */
public class NavigationViewHeadingLineSkin extends SkinBase<NavigationViewHeadingLine> {

	private Button btnBack;
	private Button btnMenu;
	private Label heading;
	private HBox contentColumn;

	//-------------------------------------------------------------------
	public NavigationViewHeadingLineSkin(NavigationViewHeadingLine control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-----------------------------------------------------------------
	private void initComponents() {
		btnBack = new Button(null, new SymbolIcon("Back"));
		btnMenu = new Button(null, new SymbolIcon("globalnavigationbutton"));
		btnBack.getStyleClass().addAll("navigation-icon","app-bar-button");
		btnMenu.getStyleClass().addAll("navigation-icon","app-bar-button");
		heading = new Label(getSkinnable().getText());
		heading.setMaxWidth(Double.MAX_VALUE);
		heading.getStyleClass().addAll("text-header");
	}
	
	//-----------------------------------------------------------------
	private void initLayout() {
		contentColumn = new HBox();
		contentColumn.getStyleClass().addAll("navigation-view-heading-line");
		contentColumn.getChildren().addAll(btnBack, btnMenu, heading);
		contentColumn.setMaxWidth(Double.MAX_VALUE);

		getChildren().add(contentColumn);
	}

	//-----------------------------------------------------------------
	private void initInteractivity() {
		heading.textProperty().bind(getSkinnable().textProperty());
		btnBack.visibleProperty().bind(getSkinnable().backVisibleProperty());
		btnBack.managedProperty().bind(getSkinnable().backVisibleProperty());
		btnMenu.visibleProperty().bind(getSkinnable().menuVisibleProperty());
		btnMenu.managedProperty().bind(getSkinnable().menuVisibleProperty());
	}

	//-----------------------------------------------------------------
	public Button getMenuButton() { return btnMenu; }
	public Button getBackButton() { return btnBack; }

}
