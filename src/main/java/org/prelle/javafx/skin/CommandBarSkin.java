/**
 *
 */
package org.prelle.javafx.skin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AppBarSeperator;
import org.prelle.javafx.AppBarToggleButton;
import org.prelle.javafx.CommandBar;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;

import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Labeled;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SkinBase;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public class CommandBarSkin extends SkinBase<CommandBar> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private final static String OVERFLOW_ITEM = "overflow-item";

	private HBox layout;
	private Menu moreMenu;
	private MenuBar moreMenuBar;
	//	private List<MenuItem> overflow;

	private Map<MenuItem, Node> mapping;

	//-------------------------------------------------------------------
	public CommandBarSkin(CommandBar control) {
		super(control);
		mapping = new HashMap<>();
		initComponents();
		refreshLayout();
		initInteractivity();

		for (Node button : layout.getChildrenUnmodifiable()) {
			if (button instanceof Labeled) {
				((Labeled)button).setContentDisplay(control.isOpen()?control.getContentDisplay():ContentDisplay.GRAPHIC_ONLY);
			}
		}
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		//		overflow = new ArrayList<>();
		layout = new HBox();
		layout.setStyle("-fx-spacing: 2em; -fx-min-height: 2em;");
		layout.setMaxWidth(Double.MAX_VALUE);
		layout.setAlignment(Pos.CENTER_LEFT);
		getChildren().add(layout);

		moreMenu = new Menu(null, SymbolIcon.valueOf("More"));
		//		moreMenu.setOnAction( ev -> overflowClicked());

		moreMenuBar = new MenuBar(moreMenu);
		moreMenuBar.getStyleClass().add("app-bar-button");
	}

	//-------------------------------------------------------------------
	private Node convertToNode(MenuItem item) {
		if (item==null)
			throw new NullPointerException("Item is null");
		if (mapping.containsKey(item))
			return mapping.get(item);

		if (item instanceof AppBarToggleButton) {
//			RadioMenuItem casted = (RadioMenuItem)item;
			ToggleButton button = new ToggleButton( item.getText(), item.getGraphic() );
			button.setWrapText(false);
			button.setUserData(item);
			button.getStyleClass().add("app-bar-toggle-button");
			button.textProperty().bind(item.textProperty());
			button.graphicProperty().bind(item.graphicProperty());
			button.onActionProperty().bind(item.onActionProperty());
			button.mnemonicParsingProperty().bind(item.mnemonicParsingProperty());
			//			button.toggleGroupProperty().bind( casted.toggleGroupProperty() );
			button.setContentDisplay(getSkinnable().isOpen()?getSkinnable().getContentDisplay():ContentDisplay.GRAPHIC_ONLY);
			button.disableProperty().bind(item.disableProperty());
			mapping.put(item, button);
			logger.debug("Converted "+item.getClass()+" to "+button);
			return button;
		}

		if (item instanceof AppBarSeperator) {
			Separator button = new Separator(Orientation.VERTICAL);
			mapping.put(item, button);
			logger.debug("Converted "+item.getClass()+" to "+button);
			return button;
		}

		// Default
		Button button = new Button();
		button.setUserData(item);
		button.getStyleClass().add("app-bar-button");
		button.textProperty().bind(item.textProperty());
		button.graphicProperty().bind(item.graphicProperty());
		button.onActionProperty().bind(item.onActionProperty());
		button.mnemonicParsingProperty().bind(item.mnemonicParsingProperty());
		button.minWidthProperty().bind(button.prefWidthProperty());
		button.setContentDisplay(getSkinnable().isOpen()?getSkinnable().getContentDisplay():ContentDisplay.GRAPHIC_ONLY);
		button.disableProperty().bind(item.disableProperty());
		mapping.put(item, button);
		logger.debug("Converted "+item.getClass()+" to "+button);

		return button;
	}

	//-------------------------------------------------------------------
	private void refreshLayout() {
		logger.trace("refreshLayout");
		layout.getChildren().clear();

		if (getSkinnable().getContent()!=null) {
			Node node = getSkinnable().getContent();
			HBox.setHgrow(node, Priority.ALWAYS);
			layout.getChildren().add(node);
			HBox.setMargin(node, new Insets(3));
		}

		//		menuBar.getMenus().clear();
		for (MenuItem item : getSkinnable().getPrimaryCommands()) {
			logger.debug("Add item "+item);
			Node node = convertToNode(item);
			layout.getChildren().add(node);
		}

		layout.getChildren().add(moreMenuBar);
	}

	//--------------------------------------------------------------------
	private void changeModeRecursivly(Parent parent, boolean isOpen) {
		for (Node tmp : parent.getChildrenUnmodifiable()) {
			if (tmp == getSkinnable().getContent())
				continue;
			if (tmp instanceof Labeled) {
				((Labeled)tmp).setContentDisplay(isOpen?getSkinnable().getContentDisplay():ContentDisplay.GRAPHIC_ONLY);
			} else if (tmp instanceof Parent) {
				changeModeRecursivly((Parent)tmp, isOpen);
			} else
				logger.warn("Don't know how to handle COMPACT for "+tmp.getClass());
		}
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().widthProperty().addListener( (ov,o,n) -> {
			logger.trace("Width of "+ov+" changed from "+o+" to "+n);
			if (((Double)n)<(Double)o) calculateMatchingItems((double) n);
			if (((Double)n)>(Double)o) calculateMatchingItems((double) n);
		});
		getSkinnable().getPrimaryCommands().addListener( (ListChangeListener<MenuItem>) c -> {
			logger.debug("List changed "+c);
			refreshLayout();
		});
		getSkinnable().primaryCommandsProperty().addListener( (ov,o,n) -> refreshLayout());
		getSkinnable().contentProperty().addListener( (ov,o,n) -> refreshLayout());

		getSkinnable().openProperty().addListener( (ov,o,n) -> {
			logger.debug("openProperty changed to "+n);
			changeModeRecursivly(layout, n);
//			for (Node button : layout.getChildrenUnmodifiable()) {
//				if (button == getSkinnable().getContent())
//					continue;
//				if (button instanceof Labeled) {
//					((Labeled)button).setContentDisplay(n?getSkinnable().getContentDisplay():ContentDisplay.GRAPHIC_ONLY);
//				} else
//					logger.warn("Don't know how to handle COMPACT for "+button.getClass());
//			}
		});
		
		getSkinnable().displayStateProperty().addListener( (ov,o,n) -> {
			if (n==null)
				return;
			switch (n) {
			case HIDDEN:
				layout.setVisible(false);
				layout.setManaged(false);
				break;
			default:
				layout.setVisible(true);
				layout.setManaged(true);
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * Calculate which items fit in the command bar and which must be
	 * moved into the overflow menu
	 */
	private void calculateMatchingItems(double n) {
		logger.trace("START: calculateMatchingItems "+n);
		try {
			moreMenu.getItems().clear();
			double reservedForMore = 20; // moreMenu.getWidth();

			double prefWidth = 0;
			List<Node> placeInMenu = new ArrayList<>();
			/*
			 * Add content, if present
			 */
			Node content = getSkinnable().getContent();
			if (content!=null) {
				//			logger.trace("  content "+content.getLayoutBounds());
				double widthPerNode =((Region)content).getWidth();
				placeInMenu.add(content);
				//			logger.debug("* ("+prefWidth+" + "+widthPerNode+") = "+ (prefWidth + widthPerNode)+" > "+n+" \tContent node");
				prefWidth += widthPerNode;
				prefWidth += layout.getSpacing();
				//			HBox.setHgrow(content, Priority.SOMETIMES);
				//			((Region)content).setMaxWidth(Double.MAX_VALUE);

				Region buffer = new Region();
				HBox.setHgrow(buffer, Priority.ALWAYS);
				buffer.setMaxWidth(Double.MAX_VALUE);
				placeInMenu.add(buffer);
			}
			// Assume at least 200 pixel for content
			if (prefWidth<200)
				prefWidth=200;

			/*
			 * Try to fit as many controls into the bar as possible
			 */
			boolean stopTrying = false;
			for (MenuItem tmp : getSkinnable().getPrimaryCommands()) {
				if (tmp==moreMenu)
					continue;
				Node node = mapping.get(tmp);
				if (node==null) {
					logger.error("Missing mapped item for MenuItem "+tmp);
					continue;
				}
				node.getStyleClass().remove(OVERFLOW_ITEM);

				double widthPerNode = (node!=null) ? ((Region)node).getWidth() : 20;
				boolean doesNotMatch = ((prefWidth + widthPerNode) > (double)n);
				//			logger.debug("* ("+prefWidth+" + "+widthPerNode+") = "+ (prefWidth + widthPerNode)+" > "+n+" = "+doesNotMatch+"   \t"+node);
				if (doesNotMatch || stopTrying) {
					moreMenu.getItems().add(tmp);
					node.getStyleClass().add(OVERFLOW_ITEM);
					stopTrying = true;
				} else {
					placeInMenu.add(node);
					prefWidth += widthPerNode;
					prefWidth += layout.getSpacing();
				}
			}

			if ( stopTrying) {
				// Not all items fitted - "More" needed
				if ((prefWidth+reservedForMore) > n && !placeInMenu.isEmpty()) {
					// "More" does not fit itself - (re)move the last item
					Node last = placeInMenu.remove(placeInMenu.size()-1);
					MenuItem item = (MenuItem) last.getUserData();
					if (item!=null)
						moreMenu.getItems().add(0, item);
				}
			}

			/*
			 * Add secondary commands to more menu
			 */
			moreMenu.getItems().addAll(getSkinnable().getSecondaryCommands());

			// Make more menu visible if necessary
			if (!moreMenu.getItems().isEmpty())
				placeInMenu.add(moreMenuBar);

			/*
			 * Update layout
			 */
			boolean changed = !layout.getChildren().equals(placeInMenu);
			if (changed) {
				layout.getChildren().clear();
				if (logger.isTraceEnabled())
					logger.trace("placeInMenu = "+placeInMenu);
				layout.getChildren().addAll(placeInMenu);
			}



//			logger.debug("In bar     : "+placeInMenu);
//			logger.debug("In overflow: "+moreMenu.getItems());
		} finally {
			logger.trace("STOP : calculateMatchingItems "+n);
		}
	}
}
