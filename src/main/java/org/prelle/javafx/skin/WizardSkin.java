package org.prelle.javafx.skin;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardBehaviour;
import org.prelle.javafx.WizardPage;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ListChangeListener;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * +------------------+------------------------------------------+
 * |                  |  Title                                   |
 * |  Image           +------------------------------------------+
 * |                  |                                          |
 * |                  |  Content                                 |
 * +------------------+                                          |
 * |  Side            |                                          |
 * |                  +------------------------------------------+
 * |                  |  Buttons                                 |
 * +------------------+------------------------------------------+
 *
 * All elements (except button bar) are stackpanes containing the
 * elements of each page. This way the layout doesn't change from
 * page to page.
 *
 * @author Stefan Prelle
 *
 */
public class WizardSkin extends SkinBase<Wizard> {

	class ElementsOfPage {
		Label lbl;
		ImageView iview;
	}

	private final static Logger logger = LogManager.getLogger("prelle.jfx");

	private StackPane stackImage;
	private StackPane stackSide;
	private StackPane stackTitle;
	private StackPane stackContent;
	private ButtonBar buttons;
	private HBox dialogContent;
	private DialogLayout dialogLayout;

	private Map<WizardPage, ElementsOfPage> helperMap;

	private WizardBehaviour behaviour;
	private Button btnPREV;
	private Button btnNEXT;
	private Button btnCANCEL;
	private Button btnFINISH;
	private Button btnSAVE;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public WizardSkin(Wizard wizard, WizardBehaviour behaviour) {
		super(wizard);
		this.behaviour = behaviour;
		if (behaviour==null)
			throw new NullPointerException("behaviour");

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		for (WizardPage page : wizard.getPages()) {
			stackContent.getChildren().add(page.getContent());
			addPage(page);
		}
		wizard.getPages().addListener(new InvalidationListener() {
			public void invalidated(Observable observable) {
				logger.debug("TODO: pages invalidated");
			}
		});
		wizard.getPages().addListener(new ListChangeListener<WizardPage>(){
			public void onChanged(ListChangeListener.Change<? extends WizardPage> c) {
				logger.debug("Pages changed: "+c);
				while (c.next()) {
					if (c.wasAdded())  {
						for (WizardPage page : c.getAddedSubList()) {
							page.setWizard(wizard);
							addPage(page);
//							initInteractivity(page);
//							if (curPageIdx==UNDEFINED)
//								navTo(0, true, null);
						}
					}
					if (c.wasRemoved()) {
						for (WizardPage page : c.getRemoved()) {
							removePage(page);
							page.setWizard(null);
						}
					}
				}
			}});
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		stackContent = new StackPane();
		stackImage   = new StackPane();
		stackSide    = new StackPane();
		stackTitle   = new StackPane();
		buttons      = new ButtonBar();

		helperMap  = new HashMap<>();

		// Buttons
		buttons.setButtonOrder(ButtonBar.BUTTON_ORDER_NONE);
		btnPREV = new Button(JavaFXConstants.RES.getString("button.back"));
		ButtonBar.setButtonData(btnPREV, ButtonData.BACK_PREVIOUS);
		btnNEXT = new Button(JavaFXConstants.RES.getString("button.next"));
		ButtonBar.setButtonData(btnNEXT, ButtonData.NEXT_FORWARD);
		btnFINISH= new Button(JavaFXConstants.RES.getString("button.finish"));
		ButtonBar.setButtonData(btnPREV, ButtonData.FINISH);
		btnCANCEL= new Button(JavaFXConstants.RES.getString("button.cancel"));
		ButtonBar.setButtonData(btnPREV, ButtonData.CANCEL_CLOSE);
		btnSAVE  = new Button(JavaFXConstants.RES.getString("button.save"));
		ButtonBar.setButtonData(btnSAVE, ButtonData.OTHER);

		buttons.getButtons().addAll(btnCANCEL, btnPREV, btnNEXT,btnFINISH);
		if (getSkinnable().getButtons().contains(CloseType.SAVE))
			buttons.getButtons().add(btnSAVE);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		dialogContent.getStyleClass().add("dialog-content");
		buttons.getStyleClass().addAll("dialog-buttonbar");
		
		stackImage.getStyleClass().add("wizard-image");
		stackSide.getStyleClass().add("wizard-side");
		stackTitle.getStyleClass().add("wizard-title");
		stackContent.getStyleClass().add("wizard-content");
		buttons.getStyleClass().add("dialog-buttonbar");
//		stackSide .setStyle("-fx-background-color: orange");
//		stackTitle.setStyle("-fx-background-color: lightgreen");
//		stackContent.setStyle("-fx-background-color: cyan");
//		buttons   .setStyle("-fx-background-color: lightblue");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		buttons.setMaxWidth(Double.MAX_VALUE);

		VBox column1 = new VBox(stackImage, stackSide);
		column1.setStyle("-fx-spacing: 1em");
		stackSide.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(stackSide, Priority.ALWAYS);

		VBox column2 = new VBox(stackTitle, stackContent, buttons);
		column2.setStyle("-fx-spacing: 1em");
		stackContent.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(stackContent, Priority.ALWAYS);

		dialogContent = new HBox(column1, column2);
		dialogContent.setStyle("-fx-spacing: 1em;");
		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		dialogContent.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		dialogLayout = new DialogLayout(dialogContent);
		getChildren().clear();
		getChildren().add(dialogLayout);

	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnNEXT.setOnAction(ev -> behaviour.nextPage());
		btnPREV.setOnAction(ev -> behaviour.priorPage());
		btnCANCEL.setOnAction(ev -> {
			if (getSkinnable().cancelConfirmed()) {
				logger.debug("CANCEL");
				behaviour.cancel();
			}
		});
		btnFINISH.setOnAction(ev -> behaviour.finish());
		btnSAVE.setOnAction( ev -> behaviour.otherButton(CloseType.SAVE, ev));

		getSkinnable().currentPageProperty().addListener( (ov,o,n) -> changePage(o,n));
		getSkinnable().getButtons().addListener(new ListChangeListener<CloseType>() {
			public void onChanged(Change<? extends CloseType> c) {
				while (c.next()) {
					for (CloseType toAdd : c.getAddedSubList()) {
						if (toAdd==CloseType.SAVE) {
							buttons.getButtons().add(btnSAVE);
							logger.debug("Add "+toAdd+" button");
						} else {
							logger.warn("Ignore added "+toAdd+" button");
						}
					}
				}
			}});
	}

	//-------------------------------------------------------------------
	/**
	 * Read GUI components from WizardPage and store them for layout.
	 * WARNING: If the components of a WizardPage are replaced after
	 *   this point, they are not taken into account.
	 */
	private void addPage(WizardPage page) {
		logger.debug("  addPage("+page+")");
		ElementsOfPage elem = new ElementsOfPage();
		helperMap.put(page, elem);
		// Label
		elem.lbl = new Label(page.getTitle());
		elem.lbl.setWrapText(true);
		elem.lbl.setVisible(false);
		elem.lbl.getStyleClass().addAll("text-subheader","wizard-title");
		stackTitle.getChildren().add(elem.lbl);

		// Image and insets
		elem.iview = new ImageView(page.getImage());
		stackImage.getChildren().add(elem.iview);
		StackPane.setAlignment(elem.iview, Pos.TOP_CENTER);
		if (page.getImageInsets()!=null) {
			StackPane.setMargin(elem.iview, page.getImageInsets());
		}
		elem.iview.setVisible(false);
		elem.iview.getStyleClass().addAll("wizard-image");
		if (page.getImage()!=null) {
			elem.iview.setImage(page.getImage());
		}
		page.imageProperty().addListener( (ov,o,n) -> elem.iview.setImage(n));
		// Side
		if (page.getSide()!=null) {
			stackSide.getChildren().add(page.getSide());
			page.getSide().setVisible(false);
			page.getSide().getStyleClass().addAll("wizard-side");
			StackPane.setAlignment(page.getSide(), Pos.TOP_LEFT);
		}
		StackPane.setAlignment(elem.lbl, Pos.CENTER_LEFT);
		// Content
		stackContent.getChildren().add(page.getContent());
		StackPane.setAlignment(page.getContent(), Pos.TOP_LEFT);
		page.getContent().setVisible(false);
		page.getContent().getStyleClass().addAll("wizard-content");

		manageButtons();
	}

	//-------------------------------------------------------------------
	private void removePage(WizardPage page) {
		ElementsOfPage elem = helperMap.get(page);
		helperMap.remove(page);

		if (elem.iview!=null)
			stackImage.getChildren().remove(elem.iview);
		if (page.getSide()!=null)
			stackSide .getChildren().remove(page.getSide());
		stackTitle.getChildren().remove(elem.lbl);
		stackContent.getChildren().remove(page.getContent());
	}

//	//-------------------------------------------------------------------
//	private WizardPage getCurrentPage() {
//		if (curPageIdx==UNDEFINED || curPageIdx>=getSkinnable().getPages().size())
//			return null;
//		return getSkinnable().getPages().get(curPageIdx);
//	}

	//-------------------------------------------------------------------
	public void manageButtons() {
		btnNEXT.setDisable( !behaviour.hasNextPage() );
		btnPREV.setDisable( !behaviour.hasPriorPage() );
		btnFINISH.setDisable( !getSkinnable().canBeFinished() );
		logger.debug("manageButtons: Call isDisabled");
		btnSAVE.setDisable( getSkinnable().isDisabled(CloseType.SAVE));
	}

	//-------------------------------------------------------------------
	private void changePage(WizardPage oldPage, WizardPage newPage) {
		int oldIdx = getSkinnable().getPages().indexOf(oldPage);
		int newIdx = getSkinnable().getPages().indexOf(newPage);
		logger.debug("Change wizard page from "+oldPage+"("+oldIdx+") to "+newPage+"("+newIdx+")");
		System.err.println("WizardSkin: Change wizard page from "+oldPage+"("+oldIdx+") to "+newPage+"("+newIdx+")");

		if (oldPage!=null) {
			ElementsOfPage elem = helperMap.get(oldPage);
			if (elem!=null) {
				if (elem.iview!=null) elem.iview.setVisible(false);
				if (elem.lbl!=null) elem.lbl.setVisible(false);
			}
			if (oldPage.getSide()!=null) oldPage.getSide().setVisible(false);
			oldPage.getContent().setVisible(false);
			oldPage.pageLeft();
		}
		if (newPage!=null) {
			ElementsOfPage elem = helperMap.get(newPage);
			if (elem.iview!=null) elem.iview.setVisible(true);
			if (elem.lbl!=null) elem.lbl.setVisible(true);
			if (newPage.getSide()!=null) newPage.getSide().setVisible(true);
			newPage.getContent().setVisible(true);

			newPage.pageVisited();
		}

		manageButtons();
	}

}
