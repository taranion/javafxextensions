/**
 *
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.FlipControl;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Orientation;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 * @author prelle
 *
 */
public class FlipControlSkin extends FlipControlSkinBase {

	private FlipControlBehavior behaviour;

	private Rotate   rotate;
    private double   flipTime;
	private Timeline flip;
    private Rotate   backRotate;

	private transient Node back;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public FlipControlSkin(FlipControl control, FlipControlBehavior behavior) {
		super(control);
		this.behaviour = behavior;

        flipTime   = 500;
		flip       = new Timeline();
		rotate     = new Rotate(  0, Orientation.HORIZONTAL == control.getOrientation() ? Rotate.X_AXIS : Rotate.Y_AXIS);
        backRotate = new Rotate(180, Orientation.HORIZONTAL == control.getOrientation() ? Rotate.X_AXIS : Rotate.Y_AXIS);

		control.getTransforms().add(rotate);

		registerListeners();
	}

    //-------------------------------------------------------------------
    /**
     * @see org.prelle.javafx.FlipControlSkinBase#flip(javafx.scene.Node, javafx.scene.Node)
     */
    public void flip(Node front, Node back) {
     	this.back  = back;

    	int startAngle = (rotate.getAngle()==180)?180:0;
    	int endAngle   = ((rotate.getAngle()%360)==0)?180:360;
        KeyValue kvStart = new KeyValue(rotate.angleProperty(), startAngle, Interpolator.EASE_IN);
        KeyValue kvStop  = new KeyValue(rotate.angleProperty(), endAngle, Interpolator.EASE_OUT);
        KeyFrame kfStart = new KeyFrame(Duration.ZERO, kvStart);
        KeyFrame kfStop  = new KeyFrame(Duration.millis(flipTime), kvStop);
        flip.getKeyFrames().setAll(kfStart, kfStop);

        front.setCache(true);
        front.setCacheHint(CacheHint.ROTATE);
        back.setCache(true);
        back.setCacheHint(CacheHint.ROTATE);

        flip.setOnFinished(event -> {
        	front.setCache(false);
        	back.setCache(false);
        	//        fireEvent(new FlipEvent(FlipPanel.this, FlipPanel.this, FlipEvent.FLIP_TO_BACK_FINISHED));
        	if (rotate.getAngle()==360)
        		rotate.setAngle(0);
        });

        flip.play();
    }

	//-------------------------------------------------------------------
    private void registerListeners() {
        getSkinnable().widthProperty() .addListener( (ov,o,n) -> adjustRotationAxis());
        getSkinnable().heightProperty().addListener( (ov,o,n) -> adjustRotationAxis());
        rotate.angleProperty().addListener((ov,o,n) -> {
	            if (Double.compare(o.doubleValue(), 90) < 0 && Double.compare(n.doubleValue(), 90) >= 0) {
	            	behaviour.makeVisible(getSkinnable().getItems().indexOf(back));
	            	getSkinnable().getTransforms().add(backRotate);
	            } else
	            if (Double.compare(o.doubleValue(), 270) < 0 && Double.compare(n.doubleValue(), 270) >= 0) {
	            	behaviour.makeVisible(getSkinnable().getItems().indexOf(back));
	            	getSkinnable().getTransforms().remove(backRotate);
	            }
		});
    }

	//-------------------------------------------------------------------
    private void adjustRotationAxis() {
    	if (getSkinnable()==null) {
    		System.err.println("FlipControlSkin.adjustRotationAxis: No skinnable for FlipControlSkin");
    		return;
    	}
        double width  = getSkinnable().getWidth();
        double height = getSkinnable().getHeight();

        if (Orientation.HORIZONTAL == getSkinnable().getOrientation()) {
            rotate.setAxis(Rotate.Y_AXIS);
            rotate.setPivotX(0.5 * width);
            backRotate.setAxis(Rotate.Y_AXIS);
            backRotate.setPivotX(0.5 * width);
        } else {
            rotate.setAxis(Rotate.X_AXIS);
            rotate.setPivotY(0.5 * height);
            backRotate.setAxis(Rotate.X_AXIS);
            backRotate.setPivotY(0.5 * height);
        }
    }

}
