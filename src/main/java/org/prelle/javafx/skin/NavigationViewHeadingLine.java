/**
 * 
 */
package org.prelle.javafx.skin;

import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Control;

/**
 * @author Stefan Prelle
 *
 */
@DefaultProperty(value="items")
public class NavigationViewHeadingLine extends Control {

	private static final String DEFAULT_STYLE_CLASS = "navigation-view-heading-line";
	
	@FXML
	private StringProperty textProperty = new SimpleStringProperty();
	// --- Content
	@FXML
	private BooleanProperty menuVisibleProperty = new SimpleBooleanProperty();
	/** Toggles if the back button should be visible */
	@FXML
	private BooleanProperty backVisibleProperty = new SimpleBooleanProperty();

	//-------------------------------------------------------------------
	/**
	 */
	public NavigationViewHeadingLine() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new NavigationViewHeadingLineSkin(this));
	}

	//-----------------------------------------------------------------
	public Button getMenuButton() { return ((NavigationViewHeadingLineSkin)getSkin()).getMenuButton(); }
	public Button getBackButton() { return ((NavigationViewHeadingLineSkin)getSkin()).getBackButton(); }

	/*-*****************************************
	 * Property TEXT
	 *-*****************************************/
	public StringProperty textProperty() { return textProperty; }
	public String getText() { return textProperty.get(); }
	public NavigationViewHeadingLine setText(String value) { textProperty.set(value); return this;}

	/*-*****************************************
	 * Property BACK_VISIBLE
	 *-*****************************************/
	public BooleanProperty backVisibleProperty() { return backVisibleProperty; }
	public boolean isBackVisible() { return backVisibleProperty.get(); }
	public NavigationViewHeadingLine setBackVisible(boolean value) { backVisibleProperty.set(value); return this;}

	/*-*****************************************
	 * Property MENU_VISIBLE
	 *-*****************************************/
	public BooleanProperty menuVisibleProperty() { return menuVisibleProperty; }
	public boolean isMenuVisible() { return menuVisibleProperty.get(); }
	public NavigationViewHeadingLine setMenuVisible(boolean value) { menuVisibleProperty.set(value); return this;}

}
