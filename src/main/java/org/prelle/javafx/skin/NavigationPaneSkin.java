/**
 * 
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AttentionMenuItem;
import org.prelle.javafx.AttentionPane;
import org.prelle.javafx.NavigationView;

import javafx.collections.ListChangeListener;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Labeled;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class NavigationPaneSkin extends SkinBase<NavigationPane> {
	
	private final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private NavigationView view;
	
	private NavigationItemButton navBack;
	private NavigationItemButton navMenu;
	private NavigationItemButton navSett;
	private Region settingSpacing;
	
	private VBox layout;

	//-------------------------------------------------------------------
	public NavigationPaneSkin(NavigationPane control) {
		super(control);
		this.view = control.getNavigationView();
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		navBack = (NavigationItemButton)createNavItem(getSkinnable().getBackItem());
		navMenu = (NavigationItemButton)createNavItem(getSkinnable().getMenuItem());
		navSett = new NavigationItemButton(this, getSkinnable().getMenuSettings(), true);
		navSett.setOnAction(ev -> {
    		getSkinnable().getSelectionModel().select(getSkinnable().getMenuSettings());
    		getSkinnable().getMenuSettings().fire();
    	});
		settingSpacing = new Region();
	}

	//-------------------------------------------------------------------
	NavigationItemButton getMenuButton() { 
		return navMenu;
	}

	//-------------------------------------------------------------------
	private Node createNavItem(MenuItem item) {
		logger.debug("  create "+item);
		if (item instanceof SeparatorMenuItem) {
			Separator sep = new Separator(Orientation.HORIZONTAL);
			return sep;
		}
		
		NavigationItemButton navItem = new NavigationItemButton(this, item, true);
//		navItem.onActionProperty().bind(item.onActionProperty());
		
		navItem.visibleProperty().bind(item.visibleProperty());
		navItem.disableProperty().bind(item.disableProperty());
		navItem.managedProperty().bind(item.visibleProperty());
//		try {
//			switch (item.getId()) {
//			case "menu": case "back": case "settings":
//				break;
//			default:
//				throw new RuntimeException("Trace");
//			}
//		} catch (Exception e) {
//			logger.error("Foo",e);
//		}
		
		if (item instanceof AttentionMenuItem) {
			AttentionPane att = new AttentionPane(navItem, Pos.BOTTOM_RIGHT);
			att.attentionFlagProperty().bind( ((AttentionMenuItem)item).attentionFlagProperty() );
			((AttentionMenuItem)item).attentionToolTipProperty().addListener( new ListChangeListener<String>() {
				public void onChanged(Change<? extends String> c) {
					att.setAttentionToolTip(((AttentionMenuItem)item).attentionToolTipProperty());
				}
				
			});
			return att;
		}
		
		return navItem;
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		settingSpacing.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(settingSpacing, Priority.ALWAYS);
		layout = new VBox();
//		layout.getStyleClass().addAll(getSkinnable().getStyleClass());
//		layout.getStyleClass().addAll("navigation-pane");
		layout.getChildren().addAll(navBack, navMenu, settingSpacing, navSett);
		for (MenuItem item : getSkinnable().getNavigationView().getItems()) {
			layout.getChildren().add(createNavItem(item));
		}
		
		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		view.getItems().addListener(new ListChangeListener<MenuItem>() {
			@Override
			public void onChanged(Change<? extends MenuItem> c) {
				if (c.next()) {
					if (c.wasAdded()) {
						int pos=c.getFrom()+2;
						for (MenuItem item : c.getAddedSubList()) {
							layout.getChildren().add(pos, createNavItem(item));
							pos++;
						}
					} else
					if (c.wasRemoved()) {
						int pos=c.getFrom()+2;
						for (@SuppressWarnings("unused") MenuItem item : c.getAddedSubList()) {
							layout.getChildren().remove(pos);
						}
					}
					else{
						logger.warn("Did not implement the following list change: "+c);
					}
				}
			}});
		
		view.footerProperty().addListener( (ov,o,n) -> {
			if (o!=null)
				layout.getChildren().remove(o);
			if (n!=null) {
				int pos = layout.getChildren().indexOf(navSett);
				logger.debug("Set pane footer at position "+pos);
				layout.getChildren().add(pos+1, n);
			}
		});
		
		view.paneContentProperty().addListener( (ov,o,n) -> {
			if (o!=null)
				layout.getChildren().remove(o);
			if (n!=null) {
				int pos = layout.getChildren().indexOf(navSett);
				logger.debug("Set pane content at position "+pos);
				layout.getChildren().add(pos, n);
			}
		});

		view.settingsVisibleProperty().addListener( (ov,o,n) -> {
			navSett.setVisible(n);
		});
//		view.backButtonVisibleProperty().addListener( (ov,o,n) -> {
//			logger.info("backVisible = "+n);
//			navBack.setVisible(n);
//		});
		getSkinnable().menuButtonVisibleProperty().addListener( (ov,o,n) -> {
			navMenu.setVisible(n);
		});
		
		getSkinnable().compactProperty().addListener( (ov,o,isCompact) -> {
			logger.debug("Compact changed to "+isCompact);
			if (isCompact!=null) {
				setCompact(layout, isCompact);
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void setCompact(Parent layout, boolean isCompact ) {
		for (Node node : layout.getChildrenUnmodifiable()) {
			if (node instanceof Labeled) {
				Labeled tmp = (Labeled)node;
				boolean empty = tmp.getText()==null || tmp.getText().trim().isEmpty();
				tmp.setContentDisplay( isCompact?ContentDisplay.GRAPHIC_ONLY:ContentDisplay.LEFT);
				tmp.setTooltip((isCompact && !empty)?(new Tooltip(tmp.getText())):null);
			} else if ((node instanceof AttentionPane) && ((AttentionPane)node).getChild() instanceof Labeled) {
				((AttentionPane)node).setCompact(isCompact);
				Labeled tmp = (Labeled)((AttentionPane)node).getChild();
				boolean empty = tmp.getText()==null || tmp.getText().trim().isEmpty();
				tmp.setContentDisplay( isCompact?ContentDisplay.GRAPHIC_ONLY:ContentDisplay.LEFT);
				tmp.setTooltip((isCompact && !empty)?(new Tooltip(tmp.getText())):null);
			} else if (node instanceof Parent) {
				setCompact((Parent) node, isCompact);
			} else
				logger.warn("??? "+node.getClass());
		}
	}

}
