/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.FontIcon;

import javafx.scene.control.SkinBase;
import javafx.scene.text.Text;

/**
 * @author spr
 *
 */
public class FontIconSkin extends SkinBase<FontIcon> {
	
	private Text icon;

	//-------------------------------------------------------------------
	public FontIconSkin(FontIcon control) {
		super(control);
		icon = new Text();
		icon.setText(getSkinnable().getGlyph());
		icon.getStyleClass().add("font-icon-text");
		icon.setStyle("-fx-font-family: '"+getSkinnable().getFontFamily()+"'");
		getSkinnable().glyphProperty().addListener( (ov,o,n) -> icon.setText(getSkinnable().getGlyph()));
		getSkinnable().fontFamilyProperty().addListener( (ov,o,n) -> icon.setStyle("-fx-font-family: '"+getSkinnable().getFontFamily()+"'"));
		
		getChildren().add(icon);
	}

}
