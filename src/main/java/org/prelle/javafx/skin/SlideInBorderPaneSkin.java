/**
 *
 */
package org.prelle.javafx.skin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.SlideInBorderPane;
import org.prelle.javafx.SlideInPanel;

import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class SlideInBorderPaneSkin extends SkinBase<SlideInBorderPane> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private transient List<EventHandler<ActionEvent>> handlerLeft;
	private transient List<EventHandler<ActionEvent>> handlerRight;
	private transient List<EventHandler<ActionEvent>> handlerTop;
	private transient List<EventHandler<ActionEvent>> handlerBottom;

	private BorderPane outer;
	private BorderPane inner;

	private HBox top;
	private HBox bottom;
	private VBox left;
	private VBox right;

	private Map<Button, SlideInPanel> buttonMap;

	//--------------------------------------------------------------------
	public SlideInBorderPaneSkin(SlideInBorderPane control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();

		handlerLeft  = new ArrayList<EventHandler<ActionEvent>>();
		handlerRight = new ArrayList<EventHandler<ActionEvent>>();
		handlerTop   = new ArrayList<EventHandler<ActionEvent>>();
		handlerBottom= new ArrayList<EventHandler<ActionEvent>>();

		refreshIcons();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		buttonMap = new HashMap<>();
		outer = new BorderPane();
		inner = new BorderPane();

		top = new HBox();
		bottom = new HBox();
		left  = new VBox();
		right = new VBox();

		top.getStyleClass().add("sidepanel-buttons");
		bottom.getStyleClass().add("sidepanel-buttons");
		left .getStyleClass().add("sidepanel-buttons");
		right.getStyleClass().add("sidepanel-buttons");

//		left.setStyle("-fx-min-width: 4em; -fx-background-color: grey");
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		outer.setCenter(inner);

		left.setAlignment(Pos.CENTER);
		right.setAlignment(Pos.CENTER);
		top.setAlignment(Pos.CENTER);
		bottom.setAlignment(Pos.CENTER);

		outer.setLeft(left);
		outer.setRight(right);
		outer.setTop(top);
		outer.setBottom(bottom);

		getSkinnable().impl_getChildren().add(outer);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().centerProperty().addListener( (ov,o,n) -> inner.setCenter(n));

		getSkinnable().visibleLeftProperty().addListener( (ov,o,n) -> {
			if (n==null || n.getContent()==null) { inner.setLeft(null); return;}
			Button close = new Button("\uE0C7");
			close.getStyleClass().add("icon-button");
			close.setOnAction(ev -> ((SlideInBorderPane)getSkinnable()).visibleLeftProperty().set(null));
			StackPane stack = new StackPane();
			stack.getChildren().addAll(n.getContent(), close);
			StackPane.setAlignment(close, Pos.TOP_RIGHT);
			inner.setLeft(stack);
		});

		getSkinnable().visibleRightProperty().addListener( (ov,o,n) -> {
			if (n==null || n.getContent()==null) { inner.setRight(null); return;}
			Button close = new Button("\uE0C7");
			close.getStyleClass().add("icon-button");
			close.setOnAction(ev -> ((SlideInBorderPane)getSkinnable()).visibleRightProperty().set(null));
			StackPane stack = new StackPane();
			stack.getChildren().addAll(n.getContent(), close);
			StackPane.setAlignment(close, Pos.TOP_RIGHT);
			inner.setRight(stack);
		});

		getSkinnable().visibleTopProperty().addListener( (ov,o,n) -> {
			if (n==null || n.getContent()==null) { inner.setTop(null); return;}
			Button close = new Button("\uE0C7");
			close.getStyleClass().add("icon-button");
			close.setOnAction(ev -> ((SlideInBorderPane)getSkinnable()).visibleTopProperty().set(null));
			StackPane stack = new StackPane();
			stack.getChildren().addAll(n.getContent(), close);
			StackPane.setAlignment(close, Pos.TOP_RIGHT);
			inner.setTop(stack);
		});

		getSkinnable().visibleBottomProperty().addListener( (ov,o,n) -> {
			if (n==null || n.getContent()==null) { inner.setBottom(null); return;}
			Button close = new Button("\uE0C7");
			close.getStyleClass().add("icon-button");
			close.setOnAction(ev -> ((SlideInBorderPane)getSkinnable()).visibleBottomProperty().set(null));
			StackPane stack = new StackPane();
//			VBox withSep = new VBox(new Separator(Orientation.HORIZONTAL), n.getContent());
			stack.getChildren().addAll(n.getContent(), close);
			StackPane.setAlignment(close, Pos.TOP_RIGHT);
			inner.setBottom(stack);
		});

		getSkinnable().getLeftSidePanels().addListener(new ListChangeListener<SlideInPanel>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends SlideInPanel> change) {
				logger.debug("Change "+change);
				while (change.next()) {
					if (change.wasAdded()) {
						logger.debug("Added "+change.getAddedSubList());
						int index = change.getFrom();
						for (SlideInPanel tmp : change.getAddedSubList()) {
							left.getChildren().add(index, convertToButton(tmp, Pos.CENTER_LEFT));
							logger.debug("Added "+tmp);
							index++;
						}
					}
				}
//				refreshIcons();
			}});

		getSkinnable().getRightSidePanels().addListener(new ListChangeListener<SlideInPanel>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends SlideInPanel> change) {
				logger.warn("Change "+change);
				while (change.next()) {
					if (change.wasAdded()) {
						logger.warn("Added "+change.getAddedSubList());
						int index = change.getFrom();
						for (SlideInPanel tmp : change.getAddedSubList()) {
							right.getChildren().add(index, convertToButton(tmp, Pos.CENTER_RIGHT));
							logger.warn("Added "+tmp);
							index++;
						}
					}
				}
			}});

		getSkinnable().getTopSidePanels().addListener(new ListChangeListener<SlideInPanel>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends SlideInPanel> change) {
				logger.debug("Change "+change);
				while (change.next()) {
					if (change.wasAdded()) {
						logger.warn("Added "+change.getAddedSubList());
						int index = change.getFrom();
						for (SlideInPanel tmp : change.getAddedSubList()) {
							top.getChildren().add(index, convertToButton(tmp, Pos.TOP_CENTER));
							logger.warn("Added "+tmp);
							index++;
						}
					}
				}
			}});

		getSkinnable().getBottomSidePanels().addListener(new ListChangeListener<SlideInPanel>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends SlideInPanel> change) {
				logger.debug("Change "+change);
				while (change.next()) {
					if (change.wasAdded()) {
						logger.warn("Added "+change.getAddedSubList());
						int index = change.getFrom();
						for (SlideInPanel tmp : change.getAddedSubList()) {
							bottom.getChildren().add(index, convertToButton(tmp, Pos.BOTTOM_CENTER));
							logger.warn("Added "+tmp);
							index++;
						}
					}
				}
			}});
	}

	//--------------------------------------------------------------------
	private Button convertToButton(SlideInPanel panel, Pos pos) {
		Button button = new Button(null, panel.getIcon());
		if (panel.getTooltip()!=null)
			button.setTooltip(new Tooltip(panel.getTooltip()));
		switch (pos) {
		case TOP_CENTER:
			button.setOnAction(ev -> fireTopPanelSelected(new ActionEvent(panel, button)));
			panel.getIcon().getStyleClass().add("sidebar-icon-top");
			break;
		case CENTER_LEFT:
			button.setOnAction(ev -> fireLeftPanelSelected(new ActionEvent(panel, button)));
			panel.getIcon().getStyleClass().add("sidebar-icon-left");
			break;
		case BOTTOM_CENTER:
			button.setOnAction(ev -> fireBottomPanelSelected(new ActionEvent(panel, button)));
			panel.getIcon().getStyleClass().add("sidebar-icon-bottom");
			break;
		case CENTER_RIGHT:
			button.setOnAction(ev -> fireRightPanelSelected(new ActionEvent(panel, button)));
			panel.getIcon().getStyleClass().add("sidebar-icon-right");
			break;
		default:
		}

		buttonMap.put(button, panel);
		return button;
	}

	//--------------------------------------------------------------------
	public void refreshIcons() {
		left.getChildren().clear();
		for (SlideInPanel panel : getSkinnable().getLeftSidePanels()) {
			logger.error("Add "+panel.getClass());
			left.getChildren().add(convertToButton(panel, Pos.CENTER_LEFT));
		}

		right.getChildren().clear();
		for (SlideInPanel panel : getSkinnable().getRightSidePanels()) {
			logger.error("Add "+panel.getClass());
			right.getChildren().add(convertToButton(panel, Pos.CENTER_RIGHT));
		}
		
		top.getChildren().clear();
		for (SlideInPanel panel : getSkinnable().getLeftSidePanels()) {
			logger.error("Add "+panel.getClass());
			top.getChildren().add(convertToButton(panel, Pos.TOP_CENTER));
		}

		bottom.getChildren().clear();
		for (SlideInPanel panel : getSkinnable().getRightSidePanels()) {
			logger.error("Add "+panel.getClass());
			bottom.getChildren().add(convertToButton(panel, Pos.BOTTOM_CENTER));
		}

	}

	//--------------------------------------------------------------------
	void setOnActionLeft(EventHandler<ActionEvent> handler) {
		if (!this.handlerLeft.contains(handler))
			this.handlerLeft.add(handler);
	}

	//--------------------------------------------------------------------
	void setOnActionRight(EventHandler<ActionEvent> handler) {
		if (!this.handlerRight.contains(handler))
			this.handlerRight.add(handler);
	}

	//--------------------------------------------------------------------
	void setOnActionTop(EventHandler<ActionEvent> handler) {
		if (!this.handlerTop.contains(handler))
			this.handlerTop.add(handler);
	}

	//--------------------------------------------------------------------
	void setOnActionBottom(EventHandler<ActionEvent> handler) {
		if (!this.handlerBottom.contains(handler))
			this.handlerBottom.add(handler);
	}

	//--------------------------------------------------------------------
	public void fireLeftPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerLeft) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void fireRightPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerRight) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void fireTopPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerTop) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void fireBottomPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerBottom) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
