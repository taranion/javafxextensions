/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.BitmapIcon;

import javafx.scene.control.SkinBase;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author spr
 *
 */
public class BitmapIconSkin extends SkinBase<BitmapIcon> {
	
	private ImageView icon;

	//-------------------------------------------------------------------
	public BitmapIconSkin(BitmapIcon control) {
		super(control);
		icon = new ImageView();
		if (getSkinnable().getUriSource()!=null)
			icon.setImage(new Image(getSkinnable().getUriSource()));
		getSkinnable().uriSourceProperty().addListener( (ov,o,n) -> icon.setImage(
				(getSkinnable().getUriSource()!=null)?(new Image(getSkinnable().getUriSource())):null));
		
		getChildren().add(icon);
	}

}
