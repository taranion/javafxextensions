/**
 *
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.NavigationView;
import org.prelle.javafx.SymbolIcon;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;

/**
 * @author Stefan Prelle
 *
 */
public class NavigationPane extends Control {

	private final static Logger logger = LogManager.getLogger("prelle.jfx");

	static final String DEFAULT_STYLE_CLASS = "navigation-pane";

	private NavigationView view;
	private MenuItem itemBack;
	private MenuItem itemMenu;
	private MenuItem itemSett;
	private BooleanProperty backButtonVisibleProperty = new SimpleBooleanProperty(false);
	private BooleanProperty menuButtonVisibleProperty = new SimpleBooleanProperty(false);

	private transient BooleanProperty compactProperty = new SimpleBooleanProperty(false);

	// --- Selection Model
	private SelectionModel<MenuItem> selectionModel = new SingleSelectionModel<MenuItem>() {
		@Override protected MenuItem getModelItem(int index) { if (index==-1) return null; else return view.getItems().get(index);}
		@Override protected int getItemCount() { return view.getItems().size(); }
	};

    // --- Pseudo class: CLOSED
    private static final PseudoClass PSEUDO_CLASS_COLLAPSED = PseudoClass.getPseudoClass("collapsed");

	//-------------------------------------------------------------------
	public NavigationPane(NavigationView view) {
		this.view = view;
		itemBack = new MenuItem(" ", new SymbolIcon("Back"));
		itemMenu = new MenuItem(" ", new SymbolIcon("GlobalNavigationButton"));
		itemSett = new MenuItem(JavaFXConstants.RES.getString("menuitem.settings"), new SymbolIcon("Setting"));
		itemBack.setId("back");
		itemMenu.setId("menu");
		itemSett.setId("settings");
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new NavigationPaneSkin(this));

		/* Initialize pseudo class and adapt to changes */
		pseudoClassStateChanged(PSEUDO_CLASS_COLLAPSED, isCompact());
		compactProperty.addListener( (ov,o,n) -> {
			pseudoClassStateChanged(PSEUDO_CLASS_COLLAPSED, isCompact());
		});
		

		// Listen to selection changes
		selectionModel.selectedItemProperty().addListener( (ov,o,n) -> logger.debug("Selection changes to "+selectionModel.getSelectedItem()));
		
		// Button state changed
		itemBack.visibleProperty().bind(view.backButtonVisibleProperty());
		itemBack.disableProperty().bind(view.backButtonEnabledProperty().not());
		itemSett.visibleProperty().bind(view.settingsVisibleProperty());
		itemSett.disableProperty().bind(view.settingsVisibleProperty().not());
	}

	//-------------------------------------------------------------------
	NavigationView getNavigationView() { return view; }
	MenuItem getBackItem() { return itemBack; }
	MenuItem getMenuItem() { return itemMenu; }
	MenuItem getMenuSettings() { return itemSett; }

	/******************************************
	 * Property SELECTION MODEL
	 ******************************************/
	public final SelectionModel<MenuItem> getSelectionModel() { return selectionModel; }

	/******************************************
	 * Property ON_BACK
	 ******************************************/
	public final ObjectProperty<EventHandler<ActionEvent>> onBackActionProperty() { return itemBack.onActionProperty(); }

	/******************************************
	 * Property ON_MENU
	 ******************************************/
	public final ObjectProperty<EventHandler<ActionEvent>> onMenuActionProperty() { return itemMenu.onActionProperty(); }

	/*-*****************************************
	 * Property BACK_BUTTON_VISIBLE
	 *-*****************************************/
	public BooleanProperty backButtonVisibleProperty() { return backButtonVisibleProperty; }
	public boolean isBackButtonVisible() { return backButtonVisibleProperty.get(); }
	public NavigationPane setBackButtonVisible(boolean value) { backButtonVisibleProperty.set(value); return this;}

	/*-*****************************************
	 * Property MENU_BUTTON_VISIBLE
	 *-*****************************************/
	public BooleanProperty menuButtonVisibleProperty() { return menuButtonVisibleProperty; }
	public boolean isMenuButtonVisible() { return menuButtonVisibleProperty.get(); }
	public NavigationPane setMenuButtonVisible(boolean value) { menuButtonVisibleProperty.set(value); return this;}

	/*-*****************************************
	 * Property COMPACT
	 *-*****************************************/
	public BooleanProperty compactProperty() { return compactProperty; }
	public boolean isCompact() { return compactProperty.get(); }
	public void setCompact(boolean value) { compactProperty.set(value); }

}
