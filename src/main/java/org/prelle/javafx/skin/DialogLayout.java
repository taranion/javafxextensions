package org.prelle.javafx.skin;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class DialogLayout extends VBox {

	private HBox ltrLayout;
	private Node dialogContent;
	private Region upperSpacing;
	private Region lowerSpacing;
	private Region leftSpacing;
	private Region rightSpacing;

	//-------------------------------------------------------------------
	public DialogLayout(Node dialogContent) {
		this.dialogContent = dialogContent;
		initComponents();
		initLayout();
		initStyle();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		ltrLayout = new HBox();
		upperSpacing = new Region();
		lowerSpacing = new Region();
		leftSpacing  = new Region();
		rightSpacing = new Region();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		getStyleClass().add("dialog");

		upperSpacing.getStyleClass().add("dialog-outside");
		lowerSpacing.getStyleClass().add("dialog-outside");

		ltrLayout.getStyleClass().add("dialog-line");
 		leftSpacing.getStyleClass().add("dialog-left-spacing");
		rightSpacing.getStyleClass().add("dialog-right-spacing");

		dialogContent.getStyleClass().add("dialog-content");
	}

	//-------------------------------------------------------------------
	private void initLayout() {

		/*
		 * Layout dialog line with TTB with spacing on both sides
		 */
//		HBox.setMargin(iView, control.getImageInsets());
		leftSpacing.setMaxWidth(Double.MAX_VALUE);
		rightSpacing.setMaxWidth(Double.MAX_VALUE);
//		leftSpacing.setStyle("-fx-background-color: pink");
//		rightSpacing.setStyle("-fx-background-color: maroon");
		HBox.setHgrow(leftSpacing, Priority.SOMETIMES);
		HBox.setHgrow(rightSpacing, Priority.SOMETIMES);
//		dialogContent.getChildren().addAll(imgAndSide, ttbLayout);
		HBox.setHgrow(dialogContent, Priority.SOMETIMES);
//		ltrLayout.getChildren().addAll(leftSpacing, imgAndSide, dialogContent, rightSpacing);
		ltrLayout.getChildren().addAll(leftSpacing,  dialogContent, rightSpacing);

		/*
		 * Frame content with a space above and below
		 */
		upperSpacing.setMaxHeight(Double.MAX_VALUE);
		lowerSpacing.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(upperSpacing, Priority.ALWAYS);
		VBox.setVgrow(lowerSpacing, Priority.ALWAYS);
		getChildren().clear();
		getChildren().addAll(upperSpacing, ltrLayout, lowerSpacing);

		/*
		 * Tell control to take all the possible space
		 */
		setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
	}

}
