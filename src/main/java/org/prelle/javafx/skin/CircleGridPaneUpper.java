/**
 * 
 */
package org.prelle.javafx.skin;

import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 * @author prelle
 *
 */
public class CircleGridPaneUpper extends GridPane {

	//-----------------------------------------------------------------
	/**
	 */
	public CircleGridPaneUpper() {
		this.setGridLinesVisible(false);
		for (int i = 0; i < 5; i++) {
			ColumnConstraints column = new ColumnConstraints();
			column.setHgrow(Priority.ALWAYS);
			column.setFillWidth(true);
			if ( (i%2)==1) {
				column.setMinWidth(50);
				column.setMaxWidth(100);
			} else {
				column.setMinWidth(100);
			}
			this.getColumnConstraints().add(column);
		}
		
		for (int i = 0; i < 7; i++) {
			RowConstraints row = new RowConstraints();
			row.setVgrow(Priority.ALWAYS);
			row.setFillHeight(true);
			if ( (i%2)==1) {
				row.setMinHeight(20);
				row.setMaxHeight(50);
			} else {
				row.setMinHeight(100);
//				row.setPrefHeight(150);
				if (i==2 || i==4) {
					row.setMaxHeight(120);
				}
				else
					row.setPrefHeight(150);
			}
			this.getRowConstraints().add(row);
		}
		
		
	}

}
