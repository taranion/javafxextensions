/**
 * 
 */
package org.prelle.javafx.skin;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class CircleGridPane extends StackPane {
	
	enum Position {
		TOP,
		TOP_RIGHT,
		UPPER_RIGHT,
		LOWER_RIGHT,
		BOTTOM_RIGHT,
		BOTTOM,
		BOTTOM_LEFT,
		LOWER_LEFT,
		UPPER_LEFT,
		TOP_LEFT
	}
	
	private CircleGridPaneLower lower;
	private CircleGridPaneUpper upper;

	//-----------------------------------------------------------------
	/**
	 */
	public CircleGridPane() {
		lower = new CircleGridPaneLower();
		upper = new CircleGridPaneUpper();
		
		getChildren().addAll(lower, upper);
		
		// React to size changed
		upper.widthProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> arg0,
					Number arg1, Number arg2) {
				redrawLines();
			}
		});
		upper.heightProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> arg0,
					Number arg1, Number arg2) {
				redrawLines();
			}
		});
	}

	//-----------------------------------------------------------------
	public void add(CircleIconPaneOld circle, int x, int y, int spanX, int spanY) {
		upper.add(circle, x, y, spanX, spanY);
	}

	//-----------------------------------------------------------------
	public void add(String heading, Node table, int x, int y) {
		add(heading, null, table, x,y, 1,1);
	}

	//-----------------------------------------------------------------
	public void add(String heading, Button editButton, Node table, int x, int y) {
		add(heading, editButton, table, x,y, 1,1);
	}

	//-----------------------------------------------------------------
	public void add(String heading, Button editButton, Node table, int x, int y, int spanX, int spanY) {
		HBox box = new HBox(0);
		
		Label alarmLabel = new Label(heading);
		alarmLabel.getStyleClass().add("sidepanehead");
		alarmLabel.setMaxWidth(Double.MAX_VALUE);
		box.getChildren().add(alarmLabel);
		if (editButton!=null) {
			box.getChildren().add(editButton);
			HBox.setHgrow(alarmLabel, Priority.NEVER);
		}
		HBox.setHgrow(alarmLabel, Priority.ALWAYS);
		
		VBox alarmPane = new VBox();
		alarmPane.setId(heading);
		alarmPane.getStyleClass().add("sidepane");
		alarmPane.getChildren().addAll(box, table);
		
		upper.add(alarmPane, x, y, spanX, spanY);
		lower.makeLine(box, x, y);
		
	}
	
	//-----------------------------------------------------------------
	void redrawLines() {
		for (Node child : upper.getChildrenUnmodifiable()) {
			if (child instanceof CircleIconPaneOld)
				lower.circleChanged(((CircleIconPaneOld)child).getRadius(), this.getWidth(), this.getHeight());
			else if (child instanceof VBox) {
				VBox  box   = (VBox)child;
//				Label label = (Label)box.getChildren().get(0);
				HBox label = (HBox)box.getChildren().get(0);
				Node  node  = box.getChildren().get(1);
				Position pos = convertToPosition(
						GridPane.getColumnIndex(child), 
						GridPane.getRowIndex(child));
				lower.tableChanged(
						box, 
						label, 
						node, 
						pos
							);
			}
		}
	}
	
	//-----------------------------------------------------------------
	private Position convertToPosition(int col, int row) {
		switch (col) {
		case 0:
			switch (row) {
			case 0: return Position.TOP_LEFT;
			case 2: return Position.UPPER_LEFT;
			case 4: return Position.LOWER_LEFT;
			case 6: return Position.BOTTOM_LEFT;
			}
			break;
		case 2:
			switch (row) {
			case 0: return Position.TOP;
			case 6: return Position.BOTTOM;
			}
			break;
		case 4:
			switch (row) {
			case 0: return Position.TOP_RIGHT;
			case 2: return Position.UPPER_RIGHT;
			case 4: return Position.LOWER_RIGHT;
			case 6: return Position.BOTTOM_RIGHT;
			}
			break;
		}
		throw new IllegalArgumentException("Cannot convert to position: "+col+","+row);
	}

}
