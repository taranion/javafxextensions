/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.PathIcon;

import javafx.scene.control.SkinBase;
import javafx.scene.shape.SVGPath;

/**
 * @author spr
 *
 */
public class PathIconSkin extends SkinBase<PathIcon> {
	
	private SVGPath icon;

	//-------------------------------------------------------------------
	public PathIconSkin(PathIcon control) {
		super(control);
		icon = new SVGPath();
		icon.setContent(getSkinnable().getData());
		getSkinnable().dataProperty().addListener( (ov,o,n) -> icon.setContent(getSkinnable().getData()));
		
		getChildren().add(icon);
	}

}
