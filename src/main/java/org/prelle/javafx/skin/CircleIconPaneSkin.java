/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.CircleIconPane;
import org.prelle.javafx.SymbolIcon;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * @author prelle
 *
 */
public class CircleIconPaneSkin extends SkinBase<CircleIconPane> {
	
	private Circle circle;
	private ImageView icon;
	private Label labelLine1;
	private Label labelLine2;
	private Button btnEdit;

	//-----------------------------------------------------------------
	public CircleIconPaneSkin(CircleIconPane control) {
		super(control);

		icon   = new ImageView();
		btnEdit = new Button(null,new SymbolIcon("edit"));
		btnEdit.setOpacity(0.5);
		btnEdit.setScaleX(3);
		btnEdit.setScaleY(3);
		btnEdit.setVisible(false);
		btnEdit.setManaged(getSkinnable().getEditButton());
		StackPane stack = new StackPane(icon, btnEdit);

		circle = new Circle();
		circle.getStyleClass().add("device-circle");
		circle.setFill(Color.TRANSPARENT);
		circle.setStyle("-fx-stroke: black; -fx-stroke-width: 2px");
		labelLine1 = new Label();
		labelLine1.getStyleClass().add("title");
		labelLine2 = new Label();
		labelLine1.getStyleClass().add("subtitle");
		VBox labelBox = new VBox(5);
		labelBox.setAlignment(Pos.CENTER);
		labelBox.getChildren().addAll(stack,labelLine1, labelLine2);
		
		// Add nodes
		getChildren().addAll(circle, labelBox);
		
		// Property change listener to automatically set label texts
		labelLine1.textProperty().bind(control.titleProperty());
		labelLine2.textProperty().bind(control.subtitleProperty());
		icon.imageProperty().bind(control.imageProperty());
//		icon.imageProperty().addListener( (ov,o,n) -> System.out.println("Image changed "+n.getProgress()));
		
		control.widthProperty().addListener( (ov,o,n) -> updateCircle());
		control.heightProperty().addListener( (ov,o,n) -> updateCircle());
				
		stack.setOnMouseEntered(ev -> btnEdit.setVisible(getSkinnable().getEditButton()));
		stack.setOnMouseExited(ev -> btnEdit.setVisible(false));
		btnEdit.onActionProperty().bind(getSkinnable().onEditActionProperty());
		
		getSkinnable().editButtonProperty().addListener( (ov,o,n) -> {
			btnEdit.setManaged(n);
		});
	}

	//-------------------------------------------------------------------
	private void updateCircle() {
//		System.out.println("w="+getSkinnable().getWidth()+"  icon="+icon.getImage().getWidth());
		
		double size = Math.min(getSkinnable().getWidth(), getSkinnable().getHeight());
		double iconScaleX = (icon.getImage()!=null) ? (size / icon.getImage().getWidth()):1.0;
		double iconScaleY = (icon.getImage()!=null) ? (size / icon.getImage().getHeight()):1.0;
		double iconScale = Math.min(iconScaleX, iconScaleY);
//		System.out.println(" ... scale "+icon.getImage().getWidth()+"x"+icon.getImage().getHeight()+"  to  "+icon.getImage().getWidth()*iconScale+"x"+icon.getImage().getHeight()*iconScale);
		if (icon.getImage()!=null) {
			icon.setFitHeight(icon.getImage().getHeight()*iconScale*0.7);
			icon.setFitWidth(icon.getImage().getWidth()*iconScale*0.7);
		}
		double radius = size/2.0;
		double offX = (getSkinnable().getWidth()-size)/2;
		double offY = (getSkinnable().getHeight()-size)/2;
		circle.setCenterX(offX+radius);
		circle.setCenterY(offY+radius);
		circle.setRadius(radius-10);
	}

	//-------------------------------------------------------------------
	public double getRadius() {
		return circle.getRadius();
	}
		
}
