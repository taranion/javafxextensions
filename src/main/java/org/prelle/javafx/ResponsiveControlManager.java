package org.prelle.javafx;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Region;

/**
 * @author Stefan Prelle
 *
 */
public class ResponsiveControlManager {

	//-------------------------------------------------------------------
	public static void manageResponsiveControls(Region toManage, int compact, int expanded) {
		toManage.widthProperty().addListener( (ov,o,n) -> {
			double width = (Double)n;
			WindowMode newMode = WindowMode.MINIMAL;
			if (width<=compact) newMode = WindowMode.MINIMAL;
			else if (width<=expanded) newMode = WindowMode.COMPACT;
			else newMode = WindowMode.EXPANDED;

			changeModeRecursivly(toManage, newMode);
			
		});
	}
	
	//--------------------------------------------------------------------
	private static void changeModeRecursivly(Parent parent, WindowMode value) {
		for (Node tmp : parent.getChildrenUnmodifiable()) {
			if (tmp instanceof ResponsiveControl) 
				((ResponsiveControl)tmp).setResponsiveMode(value);
			if (tmp instanceof Parent)
				changeModeRecursivly((Parent) tmp, value);
		}
	}

}
