/**
 *
 */
package org.prelle.javafx;

import java.util.PropertyResourceBundle;

/**
 * @author prelle
 *
 */
public interface JavaFXConstants {
	
	public final static String STYLE_HEADING1 = "text-header";
	public final static String STYLE_HEADING2 = "text-subheader";
	public final static String STYLE_HEADING3 = "title";
	public final static String STYLE_HEADING4 = "subtitle";
	public final static String STYLE_HEADING5 = "base";
	public final static String STYLE_TEXT_SECONDARY = "text-secondary-info";
	public final static String STYLE_TEXT_TERTIARY = "text-tertiary-info";
	public final static String STYLE_SYMBOL_ICON = "symbol-icon";

	public static String PREFIX = "org/prelle/javafx/";
	public static PropertyResourceBundle RES = (PropertyResourceBundle)PropertyResourceBundle.getBundle("org/prelle/javafx/i18n/javafx-ext");

	//-------------------------------------------------------------------
	public static String getButtonText(CloseType type) {
		if (type==CloseType.APPLY   ) return RES.getString("button.apply");
		if (type==CloseType.CANCEL  ) return RES.getString("button.cancel");
		if (type==CloseType.CLOSE   ) return RES.getString("button.close");
		if (type==CloseType.FINISH  ) return RES.getString("button.finish");
		if (type==CloseType.PREVIOUS) return RES.getString("button.previous");
		if (type==CloseType.NEXT    ) return RES.getString("button.next");
		if (type==CloseType.OK      ) return RES.getString("button.ok");
		if (type==CloseType.SAVE    ) return RES.getString("button.save");
		return type.getText();
	}

}
