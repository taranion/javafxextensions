/**
 * JavaFX Components to use in your application.
 * 
 * @author Stefan Prelle &lt;stefan@prelle.org&gt;
 *
 */
package org.prelle.javafx;