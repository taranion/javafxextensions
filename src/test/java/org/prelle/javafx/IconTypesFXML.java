/**
 * 
 */
package org.prelle.javafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class IconTypesFXML extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-----------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(
				ClassLoader.getSystemResource("org/prelle/javafx/IconTypes.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root, 600,300);

		stage.setScene(scene);
		stage.show();

	}

}
