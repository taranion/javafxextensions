/**
 *
 */
package org.prelle.javafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class AppBarButtonIconTypesFXML extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-----------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(
				getClass().getResource("AppBarButtonIconTypes.fxml")
				);
		loader.setBuilderFactory(new ExtendedComponentBuilderFactory());
		CommandBar root = loader.load();
		root.setOpen(true);
		Scene scene = new Scene(root, 1000, 400);

		stage.setScene(scene);
		stage.show();

	}

}
