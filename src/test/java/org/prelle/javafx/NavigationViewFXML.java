/**
 * 
 */
package org.prelle.javafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class NavigationViewFXML extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-----------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(
				ClassLoader.getSystemResource("org/prelle/javafx/NavigationView.fxml")
				);
		loader.setBuilderFactory(new ExtendedComponentBuilderFactory());
		loader.setController(this);
		NavigationView root = loader.load();
		System.err.println(NavigationView.class.getResource("css/fluent.css"));
		root.getStylesheets().add(NavigationView.class.getResource("css/fluent.css").toExternalForm());
		root.setBackButtonVisible(false);
//		root.setOpen(true);
		Scene scene = new Scene(root, 1000, 400);
//		new JMetro(JMetro.Style.LIGHT).applyTheme(root);

		stage.setScene(scene);
		stage.show();
		
//		root.setSettingsVisible(false);
//		root.setDisplayMode(DisplayMode.LEFT_COMPACT);
	}

	@FXML
	private void item1Clicked(ActionEvent ev) {
		System.out.println("item1Clicked "+ev);
	}

}
