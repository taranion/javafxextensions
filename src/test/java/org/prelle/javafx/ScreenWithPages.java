/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author spr
 *
 */
public class ScreenWithPages extends ManagedScreen {
	
	ManagedScreenPage page1 = new ManagedScreenPage("Page 1", new Label("This is the first page"));
	ManagedScreenPage page2 = new ManagedScreenPage("Page 2", new Label("This is the second page."));
	private MenuItem item1 = new MenuItem("Item B-1", new SymbolIcon("add"));
	private MenuItem item2 = new MenuItem("Item B-2");

	//-------------------------------------------------------------------
	public ScreenWithPages() {
		landingPageProperty().set(page1);
		getNavigationItems().addAll(item1, item2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#navigationItemChanged(javafx.scene.control.MenuItem, javafx.scene.control.MenuItem)
	 */
	@Override
	public void navigationItemChanged(MenuItem oldValue, MenuItem newValue) {
		System.out.println("ScreenB: navItem changed to "+newValue);
		if (newValue==item1)
			showPage(page1);
		else if (newValue==item2)
			showPage(page2);
	}

}
