/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author spr
 *
 */
public class ScreenB extends ManagedScreen {
	
	ManagedScreenPage landing = new ManagedScreenPage("Landing Page B", new Label("This is landing Page B"));
	private MenuItem item1 = new MenuItem("Item B-1");
	private MenuItem item2 = new MenuItem("Item B-2");

	//-------------------------------------------------------------------
	public ScreenB() {
		landingPageProperty().set(landing);
		getNavigationItems().addAll(item1, item2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#navigationItemChanged(javafx.scene.control.MenuItem, javafx.scene.control.MenuItem)
	 */
	@Override
	public void navigationItemChanged(MenuItem oldValue, MenuItem newValue) {
		System.out.println("ScreenB: navItem changed to "+newValue);
	}

}
