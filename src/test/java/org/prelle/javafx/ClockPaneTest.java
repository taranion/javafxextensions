/**
 *
 */
package org.prelle.javafx;

import org.prelle.javafx.layout.ClockPane;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class ClockPaneTest extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		ClockPane  view = new ClockPane();
		view.setSpacing(20);
		
		CircleIconPane circle = new CircleIconPane();
		circle.setTitle("Hallo");
		circle.setSubtitle("Welt");
		circle.setImage(new Image(ClockPaneTest.class.getResourceAsStream("Van.png")));
		
		ExtendedListView<String> extended = new ExtendedListView<String>("Überschrift 7");
		extended.setAddButton(new Button(null,new SymbolIcon("add")));
		
		ExtendedListView<String> extended2 = new ExtendedListView<String>("Überschrift 10");
		extended2.setDeleteButton(new Button(null,new SymbolIcon("delete")));
		
		Label l1 = new Label("1");
		l1.setStyle("-fx-background-color: pink");
		view.add(0, circle);
		view.add(1, l1);
		view.add(3, new Label("3L"));
		view.add(7, extended, true);
		view.add(8, new ListView());
		view.add(10, extended2, true);
		view.add(11, new ListView());
		Scene scene = new Scene(view, 800,600);
		scene.getStylesheets().add(this.getClass().getResource("css/fluent.css").toExternalForm());
		scene.getStylesheets().add(this.getClass().getResource("shadowrun.css").toExternalForm());
		stage.setScene(scene);
		stage.show();
		view.add(3, new Label("3B"));
		
	}

}
