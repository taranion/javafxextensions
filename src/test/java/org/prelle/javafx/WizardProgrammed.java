/**
 *
 */
package org.prelle.javafx;

import java.util.Optional;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class WizardProgrammed extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		Wizard wizard = new Wizard();
		WizardPage page1 = new WizardPage(wizard);
		page1.setTitle("Test page 1");
		page1.setContent(new Label("This is the content.\n\nReally cool, isn't it?"));
		page1.setImage(new Image(getClass().getResourceAsStream("page1.jpg")));
		WizardPage page2 = new WizardPage(wizard);
		page2.setTitle("Test page 2");
		page2.setContent(new Label("The second page"));
		page2.setSide(new Label("Side content on pg.2"));

		wizard.getPages().addAll(page1, page2);

		// Wizard cancel confirmation
		wizard.setConfirmCancelCallback( wizardParam -> {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you really want to cancel the assistant?", ButtonType.YES, ButtonType.NO);
			Optional<ButtonType> result = alert.showAndWait();
			System.out.println("result = "+result);
			return result.isPresent() && result.get()==ButtonType.YES;
		});

		ScreenManager manager = new ScreenManager();
		VBox box = new VBox(manager);
		Scene scene = new Scene(box, 800,600);
		scene.getStylesheets().add(this.getClass().getResource("css/fluent.css").toExternalForm());
		stage.setScene(scene);
		stage.show();

		manager.showAndWait(wizard);
	}

}
