/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public class AppBarButtonIconTypesFXMLLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		AppBarButtonIconTypesFXML.main(args);
	}

}
