/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public class CommandBarFXMLLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		CommandBarFXML.main(args);
	}

}
