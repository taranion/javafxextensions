/**
 *
 */
package org.prelle.javafx;

import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class CommandBarFXML extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-----------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(
				ClassLoader.getSystemResource("org/prelle/javafx/CommandBar.fxml"),
				ResourceBundle.getBundle("org/prelle/javafx/CommandBarFXML")
				);
		FXMLLoader.setDefaultClassLoader(JavaFXConstants.class.getClassLoader());
		loader.setBuilderFactory(new ExtendedComponentBuilderFactory());
		loader.setController(this);
		CommandBar root = loader.load();
		Scene scene = new Scene(root, 800,600);
//		new JMetro(JMetro.Style.LIGHT).applyTheme(root);

		stage.setScene(scene);
		stage.show();
//		root.setOpen(true);

	}

	@FXML
	private void toggleShuffle(ActionEvent ev) {
		System.out.println("ToggleShuffle "+ev);
	}
}
