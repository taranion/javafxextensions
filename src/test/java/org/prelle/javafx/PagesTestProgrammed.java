package org.prelle.javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class PagesTestProgrammed extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		ScreenManager manager = new ScreenManager();
		
		NodeWithCommandBar bar = new NodeWithCommandBar();
		bar.setContent(new Label("Content"));
		
		ManagedScreenPage page1 = new ManagedScreenPage("Page 0", new Label("This is the upmost page"));
		
//		NavigationView view = new NavigationView();
//		view.setContent(page1);
//		view.setHeader(page1.getTitle());
		
		ManagedScreen screen = new ManagedScreen();
		screen.setLandingPage(page1);
		screen.getItems().add(new MenuItem("Hello"));
		manager.navigateTo(screen);
		
		Scene scene = new Scene(manager, 800,600);
		scene.getStylesheets().add(this.getClass().getResource("css/fluent.css").toExternalForm());
		stage.setScene(scene);
		stage.show();

		ScreenWithPages scrA = new ScreenWithPages();
		manager.navigateTo(scrA);

	}

}
