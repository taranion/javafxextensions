/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.fxml.AppBarButtonBuilder;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class CommandBarProgrammed extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		CommandBar commandBar = new CommandBar();
		commandBar.setContent(new Label("Now playing"));
		
		AppBarButton btnShuffle = (new AppBarButtonBuilder()).build().setIcon(IconElement.valueOf("Back"));
		commandBar.getPrimaryCommands().add(btnShuffle);
		
		VBox box = new VBox(commandBar);
		Scene scene = new Scene(box, 800,600);
		stage.setScene(scene);
		stage.show();

	}

}
