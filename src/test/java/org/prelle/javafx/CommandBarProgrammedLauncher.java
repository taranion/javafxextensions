/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public class CommandBarProgrammedLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		CommandBarProgrammed.main(args);
	}

}
