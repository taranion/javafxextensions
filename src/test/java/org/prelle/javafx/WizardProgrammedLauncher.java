/**
 *
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public class WizardProgrammedLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		WizardProgrammed.main(args);
	}

}
