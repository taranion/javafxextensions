/**
 *
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public class PagesTestProgrammedLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		PagesTestProgrammed.main(args);
	}

}
