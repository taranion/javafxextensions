package org.prelle.javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class ScreenManagerProgrammed extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		ScreenManager manager = new ScreenManager();

		Scene scene = new Scene(manager, 800,600);
		scene.getStylesheets().add(this.getClass().getResource("css/fluent.css").toExternalForm());
		stage.setScene(scene);
		stage.show();

		ScreenA scrA = new ScreenA();
		manager.navigateTo(scrA);

	}

}
