/**
 *
 */
package org.prelle.javafx;

import java.util.Optional;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author spr
 *
 */
public class ComponentElementViewTest extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		ComponentElementView view = new ComponentElementView();
		view.setRadius(200);
		
		int x=0;
		if (x==0) {
			view.setTitle("Ares Predator VI");
			view.setSubtitle("Heavy Pistol");
			view.setImage(new Image(ComponentElementView.class.getResourceAsStream("Ares.png")));

			view.setName(8,"External accessory");
			view.getList(8).add("Holster");
			view.setName(11,"Top");
			view.getList(11).add("Imaging scope");
			view.setName(0,"Barrel");
			view.getList(0).add("Silencer");
		} else {
			view.setTitle("GMC Bulldog Step Van");
			view.setSubtitle("Van");
			view.setImage(new Image(ComponentElementView.class.getResourceAsStream("Van.png")));
			
//			view.setSlotLong1Name("Chassis");
//			view.getSlotLong1List().add("Weapon Mount");
//			view.getSlotLong1List().add("Weapon Mount");
//			view.setSlotLong2Name("Sensors");
//			view.getSlotLong2List().add("Camera");
//			view.getSlotLong2List().add("Geiger counter");
//			view.getSlotLong2List().add("Motion sensor");
//			view.setSlotShort1Name("Electronics");
//			view.getSlotShort1List().add("ECM");
//			view.setSlotShort2Name("Protection");
//			view.getSlotShort2List().add("Anti-Theft");
//			view.setSlotShort3Name("Cosmetics");
//			view.getSlotShort3List().add("Metahuman-Adaption");
//			view.getSlotShort3List().add("Riggerinterface");
//			view.setSlotShort4Name("Motor");
//			view.getSlotShort4List().add("Multi-Fuel-Motor");
			
		}
		Scene scene = new Scene(view, 800,600);
		scene.getStylesheets().add(this.getClass().getResource("css/fluent.css").toExternalForm());
		scene.getStylesheets().add(this.getClass().getResource("shadowrun.css").toExternalForm());
		stage.setScene(scene);
		stage.show();
//		stage.maximizedProperty().addListener( (ov,o,n) -> {
//			System.out.println("Stage maximized");
//			view.applyCss();
//			view.layout();
////			try {
////				Thread.sleep(1000);
////			} catch (InterruptedException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
////			view.requestLayout();
//		});
	}

}
