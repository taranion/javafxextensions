/**
 *
 */
package org.prelle.javafx;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

/**
 * @author spr
 *
 */
public class ScreenA extends ManagedScreen {

	ManagedScreenPage landing = new ManagedScreenPage("Landing Page A", new Label("This is landing Page A"));
	private MenuItem item1 = new MenuItem("Item A-1");
	private MenuItem item2 = new MenuItem("Item A-2");

	//-------------------------------------------------------------------
	public ScreenA() {
		landingPageProperty().set(landing);
		getNavigationItems().addAll(item1, item2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#navigationItemChanged(javafx.scene.control.MenuItem, javafx.scene.control.MenuItem)
	 */
	@Override
	public void navigationItemChanged(MenuItem oldValue, MenuItem newValue) {
		// TODO Auto-generated method stub
		System.out.println("ScreenA: navItem changed to "+newValue);

		if (newValue==item1) {
			System.out.println("ScreenA: navigate to screen B");
			getManager().navigateTo(new ScreenB());
		} else if (newValue==item2) {
			System.out.println("ScreenA: ignore choice");
			
			Label content = new Label("This is the dialog content");
			ChoiceBox<String> bx = new ChoiceBox<>();
			bx.getItems().addAll("Hallo","Welt");
			VBox layout = new VBox(content, bx);
			ManagedDialog dialog = new ManagedDialog("This is the dialog title", layout, CloseType.OK, CloseType.CANCEL);
			
			getManager().showAndWait(dialog);
		}
	}

}
