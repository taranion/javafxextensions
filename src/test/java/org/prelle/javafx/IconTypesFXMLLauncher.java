/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public class IconTypesFXMLLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		IconTypesFXML.main(args);
	}

}
