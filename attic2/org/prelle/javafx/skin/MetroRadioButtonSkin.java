/**
 * 
 */
package org.prelle.javafx.skin;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.MetroRadioButton;

import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 * @author prelle
 *
 */
public class MetroRadioButtonSkin extends SkinBase<MetroRadioButton>  {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private MetroRadioButton control;
	
	private StackPane labelStack;
    private StackPane slider;
    private ResizeableCanvas canvas;
    private GraphicsContext g;

    private HBox layout;
    private Map<Boolean,Label> label;
    
	//-------------------------------------------------------------------
	public MetroRadioButtonSkin(MetroRadioButton checkbox) {
		super(checkbox);
		logger.debug("<init>");
		this.control = checkbox;
		
		initComponents();
	}
	
    
	//-------------------------------------------------------------------
	private void initComponents() {
		labelStack = new StackPane();
		labelStack.setMaxWidth(Double.MAX_VALUE);
		labelStack.setAlignment(Pos.CENTER_LEFT);
		
		Label labelTrue  = new Label();
		Label labelFalse = new Label();
		label = new HashMap<>();
		label.put(true , labelTrue);
		label.put(false, labelFalse);
		labelStack.getChildren().addAll(labelTrue, labelFalse);
		if (control.getStateFormatter()!=null) {
			label.get(true).setText(control.getStateFormatter().toString(true));
			label.get(false).setText(control.getStateFormatter().toString(false));
		}
		
		canvas = new ResizeableCanvas();
        canvas.setStyle("-fx-pref-width: 5em;");
		logger.debug("Canvas is resizeable = "+canvas.isResizable());
		g = canvas.getGraphicsContext2D();
        slider = new StackPane();
        slider.getStyleClass().setAll("slider");
        slider.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        slider.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        slider.setStyle("-fx-pref-width: 5em;");
        slider.getChildren().add(canvas);
        slider.setMinWidth(100);
//        slider.setStyle("-fx-background-color: aqua");
        
        layout = new HBox(10);
        layout.getChildren().addAll(labelStack, slider);
        control.impl_getChildren().add(layout);
         
       updateChildren();
       
        initInteractivity();
 	}

//    //--------------------------------------------------------------------
//    /**
//     * @see com.sun.javafx.scene.control.skin.LabeledSkinBase#updateChildren()
//     */
//    @Override 
    protected void updateChildren() {
//        super.updateChildren();
        setTextFor(getSkinnable().getState());
        updateTrack();
    }
	

	//-------------------------------------------------------------------
	private void setTextFor(boolean newState) {
		Label toShow = label.get(newState);
		Label toHide = label.get(!newState);
		
		toShow.setVisible(true);
		toHide.setVisible(false);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		control.stateFormatterProperty().addListener((ov,o,n) -> {
			if (n!=null) {
				label.get(true).setText(control.getStateFormatter().toString(true));
				label.get(false).setText(control.getStateFormatter().toString(false));
			}
		});
		control.stateProperty().addListener( (ov,o,n) -> setTextFor(n));
		
		canvas.widthProperty().bind(slider.widthProperty());
		canvas.heightProperty().bind(labelStack.heightProperty());
		
		slider.widthProperty().addListener( (ov,o,n) -> {
			canvas.resize((Double)n, canvas.getHeight());
			updateTrack();
			});
		labelStack.heightProperty().addListener( (ov,o,n) -> {
//			logger.debug("Height changed to "+n);
			slider.resize(slider.getWidth(), (Double)n);
			updateTrack();
		});
		
		slider.setOnMouseClicked(event -> {
			double rel = event.getX()/slider.getWidth()*100;
			boolean pos = (rel<50)?true:false;
//			logger.debug("ev = "+pos);
			control.select(pos);
//			control.fire();
			updateTrack();
		});
	}

//	//--------------------------------------------------------------------
//	private void drawdSlot(State state, double x) {
//		double width = canvas.getWidth();
//		double height = canvas.getHeight();
//		double snapWidth = width/10.0;
//		double snapHeight= height/5.0;
//
//		// Upper half
//		g.strokeLine(x, snapHeight, x, 0);
//		g.strokeLine(x, 0, x+snapWidth, 0);
//		g.strokeLine(x+snapWidth, 0, x+snapWidth, snapHeight);
//		// Lower half
//		g.strokeLine(x, height-snapHeight, x, height);
//		g.strokeLine(x, height, x+snapWidth, height);
//		g.strokeLine(x+snapWidth, height, x+snapWidth, height-snapHeight);
//	}

	//--------------------------------------------------------------------
	private void updateTrack() {
		if (g==null)
			return;
//		logger.debug("updateTrack("+canvas.getWidth()+"x"+canvas.getHeight()+")   ("+slider.getWidth());
		
		double width = canvas.getWidth();
		double height = canvas.getHeight();
		double snapWidth = width/5.0;
//		double snapHeight= height/5.0;
		
		g.setStroke(Color.BLACK);
		g.setLineWidth(3);
		g.clearRect(0, 0, width, height);
		
		g.strokeRect(2, 3, width-4, height-6);
		
		// Draw knob
		g.setFill(Color.BLACK);
		if (getSkinnable().getState()) {
			g.fillRect(0, 0, snapWidth, height);			
		} else {
			g.fillRect(width-snapWidth, 0, snapWidth, height);			
		}
	}

//	//--------------------------------------------------------------------
//	private static double measureText(String text) {
//		final Text text2 = new Text(text);
//	    Scene scene = new Scene(new Group(text2));
//	    ModernUI.initialize(scene); 
//
//	    return text2.getLayoutBounds().getWidth();
//	}
//
//	//-------------------------------------------------------------------
//	private void calculateMaxKnobWidth() {
//		StringConverter<Boolean> format = getSkinnable().getStateFormatter();
//		String text1 = format.toString(true);
//		String text2 = format.toString(false);
//		
//		double width1 = measureText(text1);
//		double width2 = measureText(text2);
//		
////		text.set
//		double knobWidth = Math.max(width1, width2);
//		
////		knobWidth += knob.getPadding().getRight() + knob.getPadding().getLeft();
//		
//		logger.debug("Knob width is "+knobWidth);
////		knob.setMinWidth(knobWidth);
////		knob.setPrefWidth(knobWidth);
////		
////		getSkinnable().setPrefWidth(knobWidth*2);
////		getSkinnable().resize(getSkinnable().getPrefWidth(), getSkinnable().getPrefHeight());
//	}

}
