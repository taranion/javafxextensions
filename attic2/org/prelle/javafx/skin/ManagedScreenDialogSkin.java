/**
 *
 */
package org.prelle.javafx.skin;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.StockButton;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ManagedScreenDialogSkin implements Skin<ManagedScreen> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private ManagedScreen control;
	private VBox layout;

	private ImageView iView;
	private Label heading;
	private Node  content;
	private Node  side;
	private TilePane  navig;
	private Map<CloseType, StockButton> buttons = new HashMap<>();

	private VBox imgAndSide;
	private HBox ltrLayout;
	private HBox dialogContent;
	private VBox ttbLayout;
	private Region upperSpacing;
	private Region lowerSpacing;
	private Region leftSpacing;
	private Region rightSpacing;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ManagedScreenDialogSkin(ManagedScreen control) {
		this.control = control;
		control.getStyleClass().add("dialog");

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		// Now add buttons
		for (CloseType btn : control.getNavigButtons())
			addNavigButton(btn);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		logger.debug("Init components");
		navig   = new TilePane();
		layout  = new VBox();
		iView   = new ImageView();

		imgAndSide= new VBox();
		ltrLayout = new HBox();
		ttbLayout = new VBox();
		dialogContent= new HBox();
		upperSpacing = new Region();
		lowerSpacing = new Region();
		leftSpacing  = new Region();
		rightSpacing = new Region();
		heading      = new Label();

		control.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		updateHeading(control.getTitle());
		ttbLayout.getChildren().add(0, heading);
		updateContent(control.getContent());
		updateImage(control.getImage());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		heading.setMaxWidth(Double.MAX_VALUE);

//		ltrLayout.setSpacing(40);

		/*
		 * Layout title, content and button bar as TopToBottom
		 * Heading already added via "updateHeading"
		 * Content already added via "updateContent"
		 */
		navig.setHgap(20);
		navig.setMaxWidth(Double.MAX_VALUE);
		navig.setAlignment(Pos.CENTER_RIGHT);
		ttbLayout.getChildren().addAll(navig);
		ttbLayout.setSpacing(20);

		/*
		 * Layout image and side node below
		 */
		imgAndSide.setSpacing(20);
		imgAndSide.getChildren().add(iView);

		/*
		 * Layout dialog line with TTB with spacing on both sides
		 */
		HBox.setMargin(iView, control.getImageInsets());
		leftSpacing.setMaxWidth(Double.MAX_VALUE);
		rightSpacing.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(leftSpacing, Priority.SOMETIMES);
		HBox.setHgrow(rightSpacing, Priority.SOMETIMES);
		dialogContent.getChildren().addAll(imgAndSide, ttbLayout);
		HBox.setHgrow(ttbLayout, Priority.SOMETIMES);
		HBox.setHgrow(dialogContent, Priority.SOMETIMES);
		ltrLayout.getChildren().addAll(leftSpacing, imgAndSide, dialogContent, rightSpacing);
//		ltrLayout.setStyle("-fx-background-color: pink");

		/*
		 * Frame content with a space above and below
		 */
		upperSpacing.setMaxHeight(Double.MAX_VALUE);
		lowerSpacing.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(upperSpacing, Priority.ALWAYS);
		VBox.setVgrow(lowerSpacing, Priority.ALWAYS);
		layout.getChildren().addAll(upperSpacing, ltrLayout, lowerSpacing);


		/*
		 * Tell control to take all the possible space
		 */
		control.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		control.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);

		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		layout.getStyleClass().add("dialog");

		upperSpacing.getStyleClass().add("dialog-outside");
		lowerSpacing.getStyleClass().add("dialog-outside");

		ltrLayout.getStyleClass().add("dialog-line");
 		leftSpacing.getStyleClass().add("dialog-left-spacing");
		rightSpacing.getStyleClass().add("dialog-right-spacing");

		dialogContent.getStyleClass().add("dialog-content");
		navig.getStyleClass().addAll("dialog-buttonbar","text-body");
		heading.getStyleClass().addAll("text-subheader");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		control.titleProperty().addListener((ov,o,n) -> updateHeading(n));
		control.getNavigButtons().addListener(new ListChangeListener<CloseType>(){
			public void onChanged(ListChangeListener.Change<? extends CloseType> c) {
				logger.warn("Changed "+c);
				while (c.next()) {
					for (CloseType added : c.getAddedSubList())
						addNavigButton(added);
					for (CloseType removed : c.getRemoved())
						removeNavigButton(removed);
				}
			}});

		control.contentProperty().addListener((ov,o,n) -> updateContent(n));
		control.sideProperty().addListener((ov,o,n) -> updateSide(n));
		control.imageProperty().addListener((ov,o,n) -> updateImage(n));
		control.imageInsetsProperty().addListener((ov,o,n) -> HBox.setMargin(iView, n));
		control.imageSizeProperty().addListener((ov,o,n) -> {
			iView.setFitHeight(n.getHeight());
			iView.setFitWidth(n.getWidth());
		} );
	}

	//-------------------------------------------------------------------
	void updateHeading(String value) {
		heading.setText(value);
	}

	//-------------------------------------------------------------------
	void updateContent(Node value) {
		if (content!=null) {
			// Remove existing heading
			ttbLayout.getChildren().remove(content);
			content = null;
		}

		if (value!=null) {
			content = value;
//			content.getStyleClass().addAll("dialog-content","text-body");
			((Region)content).setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			VBox.setVgrow(content, Priority.ALWAYS);
			ttbLayout.getChildren().add(1, content);
		}
	}

	//-------------------------------------------------------------------
	void updateSide(Node value) {
		if (side!=null) {
			// Remove existing heading
			imgAndSide.getChildren().remove(side);
			side = null;
		}

		if (value!=null) {
			side = value;
			side.getStyleClass().addAll("dialog-content","dialog-side","text-secondary-info");
			imgAndSide.getChildren().add(1, side);
		}
	}

	//-------------------------------------------------------------------
	void updateImage(Image value) {
		logger.info("updateImage");
		iView.setImage(value);
	}

	//-------------------------------------------------------------------
	void addNavigButton(CloseType type) {
		logger.debug("add navig button "+type);

		StockButton value = new StockButton(type);
		value.setMaxWidth(Double.MAX_VALUE);
		navig.getChildren().add(value);
		buttons.put(type, value);

		value.setOnAction(event -> control.impl_navigClicked(value.getType(), event));
	}

	//-------------------------------------------------------------------
	void removeNavigButton(CloseType type) {
		logger.debug("remove navig button "+type);

		StockButton value = buttons.remove(type);
		if (value!=null)
			navig.getChildren().remove(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getSkinnable()
	 */
	@Override
	public ManagedScreen getSkinnable() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getNode()
	 */
	@Override
	public Node getNode() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
    /**
     * Returns the children of the skin.
     */
    public final ObservableList<Node> getChildren() {
        return control.getChildren();
    }

	//--------------------------------------------------------------------
	public void setDisabled(CloseType button, boolean disabled) {
		if (buttons.containsKey(button))
			buttons.get(button).setDisable(disabled);
		else
			logger.error("No button "+button+" in ManagedScreen");
	}

}
