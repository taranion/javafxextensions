/**
 * 
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.PageableScreen;
import org.prelle.javafx.PageableScreenBehaviour;
import org.prelle.javafx.StockButton;

import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Skin;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class PageSkin implements Skin<ManagedScreen> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private PageableScreen model;
	private PageableScreenBehaviour control;
	private VBox layout;

	private Button leftIcon;
	private Label heading;
	private Button rightIcon;
	
	private HBox  navig;
	private Node  content;
	private HBox  action;
	private Region spacing;
	
	//-------------------------------------------------------------------
	/**
	 * @param model
	 */
	public PageSkin(PageableScreen model, PageableScreenBehaviour behavior) {
		this.model = model;
		this.control = behavior;
		model.getStyleClass().add("page");
		logger.debug("<init>");
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		leftIcon = new Button();
		rightIcon= new Button();
		navig = new HBox();
		heading  = new Label();
		action  = new HBox();
		layout  = new VBox();
		content = model.getContent();
		model.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		spacing = new Region();

		heading.getStyleClass().addAll("text-header");
		heading.setText(model.getTitle());
		updateContent(model.getContent());
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		// Spacing consumes empty space in the action area
		spacing.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(spacing, Priority.ALWAYS);
		action.getChildren().add(spacing);
		
		/*
		 * Navigation buttons
		 */
		heading.setAlignment(Pos.CENTER);
		heading.setMaxWidth(Double.MAX_VALUE);
		navig.getChildren().addAll(leftIcon, heading, rightIcon);
		HBox.setHgrow(heading, Priority.ALWAYS);
		
//		navig.setSpacing(20);
//		navig.setAlignment(Pos.TOP_CENTER);
		for (CloseType type : model.getNavigButtons()) {
//			logger.debug("Found navigation button "+type);
			addNavigButton(type);
		}
		
		// Now add static buttons
		for (MenuItem btn : model.getStaticButtons()) {
//			logger.debug("Found static button "+btn);
			addStaticButton(btn);
		}
		
		// Now add static buttons
		for (Button btn : model.getContextButtons()) {
//			logger.warn("TODO Found context button "+btn);
			addContextButton(btn);
		}
		
	    /*
		 * Tell control to take all the possible space 
		 */
		model.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		model.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
		heading.setMaxWidth(Double.MAX_VALUE);
		
		layout.getChildren().addAll(navig, content, action);
//		if (content instanceof Region)
//			((Region)content).setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
		VBox.setVgrow(content, Priority.ALWAYS);
		
		getChildren().add(layout);
		
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {
		leftIcon.setStyle("-fx-min-width: 20px");
		leftIcon.getStyleClass().add("navig-button");
		rightIcon.setStyle("-fx-min-width: 20px");
		rightIcon.getStyleClass().add("navig-button");
		heading.getStyleClass().add("text-header");
		
		if (action!=null)
			action.getStyleClass().add("action-area");
		if (navig!=null)
			navig.getStyleClass().add("heading-area");
		if (content!=null)
			content.getStyleClass().add("content-area");
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		rightIcon.setOnAction(event -> control.navigClicked(CloseType.NEXT));
		leftIcon .setOnAction(event -> control.navigClicked(CloseType.PREVIOUS));
		
		model.titleProperty().addListener((ov,o,n) -> heading.setText(n));
		model.getNavigButtons().addListener(new ListChangeListener<CloseType>(){
			public void onChanged(ListChangeListener.Change<? extends CloseType> c) {
				updateNavigButtons(c);
			}});
		model.getStaticButtons().addListener(new ListChangeListener<MenuItem>(){
			public void onChanged(ListChangeListener.Change<? extends MenuItem> c) {
				while (c.next()) {
					for (MenuItem added : c.getAddedSubList())
						addStaticButton(added);
					for (MenuItem removed : c.getRemoved())
						action.getChildren().remove((Button)removed.getUserData());
				}
			}});
		model.getContextButtons().addListener(new ListChangeListener<Button>(){
			public void onChanged(ListChangeListener.Change<? extends Button> c) {
				while (c.next()) {
					for (Button added : c.getAddedSubList())
						addContextButton(added);
					for (Button removed : c.getRemoved())
						action.getChildren().remove(removed);
				}
			}});
		
		model.contentProperty().addListener((ov,o,n) -> updateContent(n));
	}
	
	//-------------------------------------------------------------------
	private void updateNavigButtons(Change<? extends CloseType> c) {
		logger.info("updateNavigButtons "+c);
		while (c.next()) {
			if (c.wasAdded()) {
				for (CloseType type : c.getAddedSubList()) 
					addNavigButton(type);				
			}
			if (c.wasRemoved())
				logger.warn("TODO: implement removing navig buttons");
		}
	}
	
	//-------------------------------------------------------------------
	void updateContent(Node value) {
		if (content!=null) {
			// Remove existing heading
			layout.getChildren().remove(content);
			content = null;
		} 

		if (value!=null) {
			content = value;
			content.getStyleClass().add("content-area");
			if (layout!=null && layout.getChildren().size()>1)
				layout.getChildren().add(1,content);
			if (content instanceof Region)
				((Region)content).setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			VBox.setVgrow(content, Priority.ALWAYS);
			VBox.setMargin(content, new Insets(0,3,3,3));
		}
	}
	
	//-------------------------------------------------------------------
	void addNavigButton(CloseType type) {
		FontIcon icon = new FontIcon();
		if (type==CloseType.PREVIOUS) icon.addFontSymbol("\uE09E");
		else if (type==CloseType.NEXT)   icon.addFontSymbol("\uE09F");
		else if (type==CloseType.CANCEL) icon.addFontSymbol("\uE17E\uE10A");
		else if (type==CloseType.APPLY ) icon.addFontSymbol("\uE17E\uE10B");
		else if (type==CloseType.BACK) icon.addFontSymbol("\uE17E\uE112");
		else {
			logger.warn("TODO: Button for "+type);
			return;
		}
		
		StockButton button = new StockButton(null, icon, type);
		button.setTooltip(new Tooltip(type.getText()));
		button.setGraphicTextGap(0);
		
		button.setOnAction(event -> navigClicked(type));
		
		switch (type) {
		case NEXT:
			rightIcon.setGraphic(icon);
			break;
		case PREVIOUS:
			leftIcon.setGraphic(icon);
			break;
		default:
		}
	}
	
	//-------------------------------------------------------------------
	void addStaticButton(MenuItem value) {
		int index = action.getChildren().indexOf(spacing);
		if (!value.getStyleClass().contains("action-button"))
			value.getStyleClass().add("action-button");
		
		// Grow from left to right - add button directly behind spacing
		Button valueB = new Button(value.getText(), value.getGraphic());
		action.getChildren().add(index+1, valueB);
		HBox.setMargin(valueB, new Insets(20));
		value.setUserData(valueB);
	}
	
	//-------------------------------------------------------------------
	void addContextButton(Button value) {
		int index = action.getChildren().indexOf(spacing);
		if (!value.getStyleClass().contains("action-button"))
			value.getStyleClass().add("action-button");
		
		// Grow from left to right - add button directly behind spacing
		action.getChildren().add(index, value);
		HBox.setMargin(value, new Insets(10));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getSkinnable()
	 */
	@Override
	public ManagedScreen getSkinnable() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getNode()
	 */
	@Override
	public Node getNode() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
    
	//-------------------------------------------------------------------
    /**
     * Returns the children of the skin.
     */
    public final ObservableList<Node> getChildren() {
        return model.getChildren();
    }
	
	//-------------------------------------------------------------------
    private void navigClicked(CloseType type) {
    	logger.warn("TODO: clicked "+type+" on "+model);
    	if (model.close(type)) {
    		// Close
    		logger.debug("Manager of "+model+" is "+model.getScreenManager());
    		model.getScreenManager().close(model, type);
    	} else {
    		// Don't close
    	}
    }

}
