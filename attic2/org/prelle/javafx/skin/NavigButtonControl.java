/**
 * 
 */
package org.prelle.javafx.skin;

import org.prelle.javafx.CloseType;

import javafx.event.ActionEvent;
import javafx.util.Callback;

/**
 * Used to control navigation buttons in dialogs
 * 
 * @author prelle
 *
 */
public class NavigButtonControl {

	private ManagedScreenDialogSkin skin;
	private Callback<CloseType, Boolean> optionalCallback;

	//--------------------------------------------------------------------
	public void setDisabled(CloseType button, boolean disabled) {
		if (skin!=null)
			skin.setDisabled(button, disabled);
	}

	//--------------------------------------------------------------------
	public void setSkin(ManagedScreenDialogSkin skin) {
		this.skin = skin;
		if (optionalCallback==null)
			skin.setDisabled(CloseType.OK, true);
	}

	//--------------------------------------------------------------------
	public void fireEvent(CloseType type, ActionEvent event) {
		if (skin!=null)
			skin.getSkinnable().impl_navigClicked(type, event);
	}

	//--------------------------------------------------------------------
	public void setCallback(Callback<CloseType, Boolean> callback) {
		this.optionalCallback = callback;
	}

	//--------------------------------------------------------------------
	public boolean tryCall(CloseType button) {
		if (optionalCallback==null)
			return true;
		
		return optionalCallback.call(button);
	}

}
