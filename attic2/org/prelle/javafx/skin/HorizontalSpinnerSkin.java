/**
 * 
 */
package org.prelle.javafx.skin;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import org.prelle.javafx.HorizontalSpinner;

/**
 * @author prelle
 *
 */
public class HorizontalSpinnerSkin<T> extends SkinBase<HorizontalSpinner<T>> {

	private HBox   layout;
	private Button btnDown;
	private Label  label;
	private Button btnUp;
	/*
	 * Used to hold all possible labels and thus always resize 
	 * to max. preferred size of all children
	 */
	private StackPane stack;

	//-------------------------------------------------------------------
	public HorizontalSpinnerSkin(HorizontalSpinner<T> control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
		updateButtons();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		btnDown = new Button("\uE949"); // E016
		btnUp   = new Button("\uE948"); // E017
		btnDown.getStyleClass().add("font-icon");
		btnUp.getStyleClass().add("font-icon");
		label = new Label("No data set");
		label.setWrapText(false);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new HBox();
		layout.setAlignment(Pos.TOP_LEFT);
		getChildren().add(layout);
		
		label.setAlignment(Pos.CENTER_LEFT);
		label.setContentDisplay(ContentDisplay.TEXT_ONLY);
		label.setGraphicTextGap(0);
		label.setMaxHeight(Double.MAX_VALUE);
		label.getStyleClass().addAll("spinner-label");
		
		stack   = new StackPane();
		stack.getChildren().add(label);
		
		layout.getChildren().addAll(btnDown, stack, btnUp);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().valueProperty().addListener( (ov,o,n) -> updateText(n));
		getSkinnable().getItems().addListener((ListChangeListener<T>)c -> listChanged(c));
		
		// Buttons
		btnDown.setOnAction(event -> decrement());
		btnUp  .setOnAction(event -> increment());
	}

	//-------------------------------------------------------------------
	private String toText(T value) {
		if (getSkinnable().getConverter()!=null)
			return getSkinnable().getConverter().call(value);
		else
			return String.valueOf(value);
	}

	//-------------------------------------------------------------------
	private void updateText(T value) {
		label.setText(toText(value));
	}

	//-------------------------------------------------------------------
	private void listChanged(javafx.collections.ListChangeListener.Change<? extends T> c) {
		HorizontalSpinner<T> control = getSkinnable();
		// See if the current selection still exists in new list
		T current = control.getValue();
		if (current!=null && control.getItems().contains(current))
			return;

		if (control.getItems().isEmpty())
			label.setText(null);
		else
			control.setValue(control.getItems().get(0));

		while (c.next()) {
			if (c.wasAdded()) {
				for (T added : c.getAddedSubList()) {
					Label tmp = new Label(toText(added));
					tmp.setUserData(added);
					tmp.setVisible(false);
					stack.getChildren().add(tmp);
				}
			}
		}
		
		updateButtons();
	}

	//-------------------------------------------------------------------
	private void updateButtons() {
		if (getSkinnable().isCyclic()) {
			btnDown.setDisable(false);
			btnUp.setDisable(false);
			return;
		}
		
		ObservableList<T> items = getSkinnable().getItems();
		T current = getSkinnable().getValue();
		int index = (items.indexOf(current));
		
		btnDown.setDisable(index==0);
		btnUp.setDisable(index==(items.size()-1));
	}

	//-------------------------------------------------------------------
	private void decrement() {
		ObservableList<T> items = getSkinnable().getItems();
		if (items.isEmpty())
			return;
		T current = getSkinnable().getValue();
		int index = items.indexOf(current);
		index--;
		if (index<0) {
			if (getSkinnable().isCyclic())
				index = items.size()-1;
			else
				index = 0;
		}
		T newVal = items.get(index);
		getSkinnable().setValue(newVal);
		
		// Enable or disable buttons
		updateButtons();
	}

	//-------------------------------------------------------------------
	private void increment() {
		ObservableList<T> items = getSkinnable().getItems();
		if (items.isEmpty())
			return;
		T current = getSkinnable().getValue();
		int index = (items.indexOf(current)+1);
		if (index>=items.size()) {
			if (getSkinnable().isCyclic())
				index = 0;
			else
				index = items.size()-1;
		}
		T newVal = items.get(index);
		getSkinnable().setValue(newVal);
		
		// Enable or disable buttons
		updateButtons();
	}

}
