/**
 *
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;

import javafx.scene.Node;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public class ManagedScreenContentOnlySkin extends SkinBase<ManagedScreen> implements Skin<ManagedScreen> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private ManagedScreen control;
	private Node  content;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ManagedScreenContentOnlySkin(ManagedScreen control) {
		super(control);
		this.control = control;
		logger.debug("<init>");
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		content = control.getContent();
		if (content instanceof Region)
			((Region)content).setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		if (content!=null)
			content.getStyleClass().add("content-area");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		control.contentProperty().addListener((ov,o,n) -> updateContent(n));
	}

	//-------------------------------------------------------------------
	void updateContent(Node value) {
		if (content!=null) {
			// Remove existing heading
			getChildren().remove(content);
			content = null;
		}

		if (value!=null) {
			content = value;
			content.getStyleClass().add("content-area");
			getChildren().add(content);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
