/**
 * 
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ModernUI;

import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Slider;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class MetroSliderSkin extends SkinBase<Slider> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private Canvas background;
	private transient GraphicsContext g;
	private Label knob;
	
	private double knobWidth;
	
	//-------------------------------------------------------------------
	public MetroSliderSkin(Slider control) {
		super(control);
		
		initComponents();
		initLayout();
		initInteractivity();
		initBehaviour();
		
		selectedStateChanged();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		background = new Canvas();
		knob       = new Label();
		
		background.getStyleClass().add("slider-background");
		knob.getStyleClass().add("slider-knob");
		
		redrawCanvas();
//		getSkinnable().setMinWidth(140);
		getSkinnable().setStyle("-fx-background-color: red");
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		knob.setAlignment(Pos.CENTER);
		getChildren().addAll(background, knob);
//		StackPane.setAlignment(knob, Pos.CENTER_LEFT);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().widthProperty().addListener( (ov,o,n) -> positionSlider());
		getSkinnable().heightProperty().addListener( (ov,o,n) -> positionSlider());
		background.widthProperty().addListener( (ov,o,n) -> redrawCanvas());
		background.heightProperty().addListener( (ov,o,n) -> redrawCanvas());
		background.widthProperty().bind(getSkinnable().widthProperty());
		background.heightProperty().bind(getSkinnable().heightProperty());
		
		knob.paddingProperty().addListener( (ov,o,n) -> calculateMaxKnobWidth());
		
		getSkinnable().labelFormatterProperty().addListener( (ov,o,n) -> {
			// Label formatter set
			calculateMaxKnobWidth();
			selectedStateChanged();
		});

		getSkinnable().valueProperty().addListener((ov, o, n) -> {
			if (n != o)
				selectedStateChanged();
		});

	}

	//--------------------------------------------------------------------
	private void initBehaviour() {
		getSkinnable().setOnMouseClicked(event -> {
			double value = (event.getX() / getSkinnable().getWidth()) * getSkinnable().getMax();
			logger.debug("Set to "+value+"  "+Math.round(value));
			getSkinnable().setValue(Math.round(value));
		});
	}

	//-------------------------------------------------------------------
	private void calculateMaxKnobWidth() {
		StringConverter<Double> format = getSkinnable().getLabelFormatter();
		String text1 = format.toString(getSkinnable().getMin())+">";
		String text2 = "<"+format.toString( (getSkinnable().getMax() - getSkinnable().getMin())/2.0) +">";
		String text3 = "<"+format.toString(getSkinnable().getMax());
		
		double width1 = measureText(text1);
		double width2 = measureText(text2);
		double width3 = measureText(text3);
		knobWidth = Math.max(width1, Math.max(width2, width3));
		
		knobWidth += knob.getPadding().getRight() + knob.getPadding().getLeft();
		
		logger.debug("Knob width is "+knobWidth);
		knob.setMinWidth(knobWidth);
		knob.setPrefWidth(knobWidth);
		
		getSkinnable().setPrefWidth(knobWidth*2);
		getSkinnable().resize(getSkinnable().getPrefWidth(), getSkinnable().getPrefHeight());
	}

	//-------------------------------------------------------------------
	private void positionSlider() {
		double perValue = knobWidth / getSkinnable().getMax();
		double posX = getSkinnable().getValue() * perValue;
		// Remove offset from center
		posX -= knobWidth/2.0;
		
		logger.debug("positionSlider to "+posX);
		
		if (knob.getTranslateX()!=posX) {
			TranslateTransition transition = new TranslateTransition(Duration.millis(100), knob);
			transition.setToX(posX);
			transition.setCycleCount(1);
			transition.play();
		} 
		
//		knob.setTranslateX(posX);
	}

	//-------------------------------------------------------------------
	private void selectedStateChanged() {
		String label = String.valueOf(getSkinnable().getValue());
		if (getSkinnable().getLabelFormatter()!=null) {
			double value = Math.round(getSkinnable().getValue());
			label = getSkinnable().getLabelFormatter().toString(value);	
			if (value==0.0) label+="\u00BB";
			if (value==1.0) label="<"+label+"\u00BB";
			if (value==2.0) label="<"+label;
		}
		knob.setText(label);
		positionSlider();
	}

	//--------------------------------------------------------------------
	private void redrawCanvas() {
		logger.debug("redrawCanvas("+getSkinnable().getWidth()+"x"+getSkinnable().getHeight()+")");
		g = background.getGraphicsContext2D();
//		g.setFill(Color.ORANGE);
		g.strokeRect(0, 0, background.getWidth(), background.getHeight());
		
//		double thumbWidth = snapSize(knob.prefWidth(-1));
//		double thumbHeight = snapSize(knob.prefHeight(-1));
//		knob.resize(thumbWidth, thumbHeight);
	}

	//--------------------------------------------------------------------
	private static double measureText(String text) {
		final Text text2 = new Text(text);
	    Scene scene = new Scene(new Group(text2));
	    ModernUI.initialize(scene); 

	    return text2.getLayoutBounds().getWidth();
	}

}
