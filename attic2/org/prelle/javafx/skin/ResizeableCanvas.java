package org.prelle.javafx.skin;

import javafx.scene.canvas.Canvas;

public class ResizeableCanvas extends Canvas {
	
	//--------------------------------------------------------------------
	public ResizeableCanvas() {
		
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.Node#isResizable()
	 */
	@Override
	public boolean isResizable() {
		return true;
	}
}