/**
 * 
 */
package org.prelle.javafx.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AdaptingFlipControlBehavior;
import org.prelle.javafx.FlipControl;
import org.prelle.javafx.FlipControlSkinBase;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 * @author prelle
 *
 */
public class AdaptingFlipControlSkin extends FlipControlSkinBase {

	private AdaptingFlipControlBehavior behaviour;
	
	private StackPane stack;
	private Label changeIcon;

	private Rotate   rotate;
    private double   flipTime;
	private Timeline flip;
    private Rotate   backRotate;
	
	private transient Node back;

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

    //-----------------------------------------------------------------
    private final ListChangeListener<Node> itemsListListener = c -> {
    	logger.debug("list changed: "+c);
        while (c.next()) {
        	if (c.wasAdded()) {
//        		behaviour.impl_getChildren().addAll(c.getAddedSubList());
        		
        		if (AdaptingFlipControlSkin.this.getSkinnable().getVisibleIndex()==-1) {
        			makeVisible(0);
        		}
        	}
        }
    };

    //-----------------------------------------------------------------
    private final ChangeListener<ObservableList<Node>> itemsListener = new ChangeListener<ObservableList<Node>>() {
        @Override
        public void changed(
                ObservableValue<? extends ObservableList<Node>> observable,
                ObservableList<Node> oldValue, ObservableList<Node> newValue) {
        	logger.debug("changed: "+newValue);
        }
    };

    //-----------------------------------------------------------------
    private final EventHandler<MouseEvent> clickedListener = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			logger.trace("clicked "+event.getSource());
			AdaptingFlipControlSkin.this.behaviour.flipForward();
			event.consume();
		}
    };
   
    // Listen to changes when the whole item list is replaced
    private final WeakChangeListener<ObservableList<Node>> weakItemsListener = 
            new WeakChangeListener<ObservableList<Node>>(itemsListener);
    // Listen to changes within the items list
    private final WeakListChangeListener<Node> weakItemsListListener = 
            new WeakListChangeListener<>(itemsListListener);

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public AdaptingFlipControlSkin(FlipControl control, AdaptingFlipControlBehavior behavior) {
		super(control);
		stack = new StackPane();
//		stack.setStyle("-fx-background-color: red;");
		behavior.impl_getChildren().add(stack);
		changeIcon = new Label("\uE13C");
		changeIcon.getStyleClass().add("flip-icon");
		changeIcon.setFont(new Font("Segoe UI Symbol", 20));
        changeIcon.setOnMouseClicked(clickedListener);
        changeIcon.setPickOnBounds(true);
        stack.setOnMouseClicked(event -> {event.consume(); logger.trace("fire"); getSkinnable().fireEvent(event); });
//        stack.setMouseTransparent(true);

		StackPane.setAlignment(changeIcon, Pos.TOP_RIGHT);
		stack.getChildren().add(changeIcon);
		this.behaviour = behavior;
		
        flipTime   = 500;
		flip       = new Timeline();
		rotate     = new Rotate(  0, Orientation.HORIZONTAL == control.getOrientation() ? Rotate.X_AXIS : Rotate.Y_AXIS);
        backRotate = new Rotate(180, Orientation.HORIZONTAL == control.getOrientation() ? Rotate.X_AXIS : Rotate.Y_AXIS);
	
		control.getTransforms().add(rotate);
		
		registerListeners();

        control.itemsProperty().addListener(weakItemsListener);
        if (control.getItems() != null) 
            control.getItems().addListener(weakItemsListListener);
	}

    //-----------------------------------------------------------------
	public void makeVisible(int index) {
		// Make all nodes invisible
		for (Node node : getSkinnable().getItems())
			stack.getChildren().remove(node);

		// Make the new node visible
		Node makeVisible = getSkinnable().getItems().get(index);
		makeVisible.setMouseTransparent(true);
		logger.debug("makeVisible("+index+") = "+makeVisible);
//		logger.debug("Make "+makeVisible+" visible    "+getControl().getChildrenUnmodifiable());
		stack.getChildren().add(0,makeVisible);
		getSkinnable().visibleIndexProperty().set(index);
		getSkinnable().visibleNodeProperty().set(makeVisible);
	}
	
    //-------------------------------------------------------------------
    /**
     * @see org.prelle.javafx.FlipControlSkinBase#flip(javafx.scene.Node, javafx.scene.Node)
     */
    public void flip(Node front, Node back) {
    	logger.debug("Flip from "+front+" to "+back);
    	if (stack.getChildren().contains(back)) {
    		logger.warn("Already flipped to "+back);
    		return;
    	}
    	
    	stack.getChildren().remove(front);
    	stack.getChildren().add(0,back);
//    	if (true)
//    		return;
    	
     	this.back  = back;
    	
    	int startAngle = (rotate.getAngle()==180)?180:0;
    	int endAngle   = ((rotate.getAngle()%360)==0)?180:360;
        KeyValue kvStart = new KeyValue(rotate.angleProperty(), startAngle, Interpolator.EASE_IN);
        KeyValue kvStop  = new KeyValue(rotate.angleProperty(), endAngle, Interpolator.EASE_OUT);
        KeyFrame kfStart = new KeyFrame(Duration.ZERO, kvStart);
        KeyFrame kfStop  = new KeyFrame(Duration.millis(flipTime), kvStop);
        flip.getKeyFrames().setAll(kfStart, kfStop);

        front.setCache(true);
        front.setCacheHint(CacheHint.ROTATE);
        back.setCache(true);
        back.setCacheHint(CacheHint.ROTATE);
        
        flip.setOnFinished(event -> {
        	front.setCache(false);
        	back.setCache(false);
        	//        fireEvent(new FlipEvent(FlipPanel.this, FlipPanel.this, FlipEvent.FLIP_TO_BACK_FINISHED));
        	if (rotate.getAngle()==360) 
        		rotate.setAngle(0);
        });
        
        flip.play();
    }

	//-------------------------------------------------------------------
    private void registerListeners() {
        getSkinnable().widthProperty() .addListener( (ov,o,n) -> adjustRotationAxis());
        getSkinnable().heightProperty().addListener( (ov,o,n) -> adjustRotationAxis());
        rotate.angleProperty().addListener((ov,o,n) -> {
//        	logger.debug("rotation = "+n);
	            if (Double.compare(o.doubleValue(), 90) < 0 && Double.compare(n.doubleValue(), 90) >= 0) {
	            	behaviour.makeVisible(getSkinnable().getItems().indexOf(back));
	            	getSkinnable().getTransforms().add(backRotate);
	            } else
	            if (Double.compare(o.doubleValue(), 270) < 0 && Double.compare(n.doubleValue(), 270) >= 0) {
	            	behaviour.makeVisible(getSkinnable().getItems().indexOf(back));
	            	getSkinnable().getTransforms().remove(backRotate);
	            }
		});
    }

	//-------------------------------------------------------------------
    private void adjustRotationAxis() {
        double width  = getSkinnable().getWidth();
        double height = getSkinnable().getHeight();

        if (Orientation.HORIZONTAL == getSkinnable().getOrientation()) {
            rotate.setAxis(Rotate.Y_AXIS);
            rotate.setPivotX(0.5 * width);
            backRotate.setAxis(Rotate.Y_AXIS);
            backRotate.setPivotX(0.5 * width);
        } else {
            rotate.setAxis(Rotate.X_AXIS);
            rotate.setPivotY(0.5 * height);
            backRotate.setAxis(Rotate.X_AXIS);
            backRotate.setPivotY(0.5 * height);
        }
    }

}
