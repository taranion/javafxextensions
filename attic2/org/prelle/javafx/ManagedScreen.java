/**
 * 
 */
package org.prelle.javafx;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.NavigButtonControl;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Skinnable;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class ManagedScreen extends ManagedScreenSkeleton implements Skinnable, ScreenManagerProvider {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private ObjectProperty<Node>    side = new SimpleObjectProperty<Node>();
	private ObjectProperty<Image>  image = new SimpleObjectProperty<Image>();
	private ObjectProperty<Insets> imageInsets= new SimpleObjectProperty<Insets>();
	private ObjectProperty<Dimension2D> imageSize  = new SimpleObjectProperty<Dimension2D>();
	private ObservableList<CloseType> navigButtons = FXCollections.observableArrayList();
	private ObjectProperty<ObservableList<Button>> contextButtons = new SimpleObjectProperty<>(FXCollections.observableArrayList());

	private transient Map<CloseType, EventHandler<ActionEvent>> handlers = new HashMap<CloseType, EventHandler<ActionEvent>>();
	private transient CloseType closedType;
	private transient NavigButtonControl buttonControl;
	
	//-------------------------------------------------------------------
	/**
	 */
	public ManagedScreen() {
	}
	
	//-------------------------------------------------------------------
	public ObservableList<Node> getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	/*
	 * Property IMAGE
	 */
	public ObjectProperty<Image> imageProperty() { return image; }
	public void setImage(Image value) {image.setValue(value); }
	public Image getImage() { return image.get(); }

	//-------------------------------------------------------------------
	/*
	 * Property SIDE
	 */
	public ObjectProperty<Node> sideProperty() { return side; }
	public void setSide(Node value) {side.setValue(value); }
	public Node getSide() { return side.get(); }

	//-------------------------------------------------------------------
	/*
	 * Property IMAGE INSETS
	 */
	public ObjectProperty<Insets> imageInsetsProperty() { return imageInsets; }
	public void setImageInsets(Insets value) {imageInsets.setValue(value); }
	public Insets getImageInsets() { return imageInsets.get(); }

	//-------------------------------------------------------------------
	/*
	 * Property IMAGE SIZE
	 */
	public ObjectProperty<Dimension2D> imageSizeProperty() { return imageSize; }
	public void setImageSize(Dimension2D value) {imageSize.setValue(value); }
	public Dimension2D getImageSize() { return imageSize.get(); }

	//-------------------------------------------------------------------
	/*
	 * Property NAVIG BUTTONS
	 */
	public ObservableList<CloseType> getNavigButtons() { return navigButtons; }

	//-------------------------------------------------------------------
	/*
	 * Property CONTEXT BUTTONS
	 */
	public ObjectProperty<ObservableList<Button>> contextButtonsProperty() { return contextButtons; }
//	public void setContextButtons(ObservableList<Button> value) {contextButtons.setValue(value); }
	public ObservableList<Button> getContextButtons() { return contextButtons.get(); }

	//-------------------------------------------------------------------
	public boolean close(CloseType closeType) {
		logger.warn("close("+closeType+") not overwritten");
		return true;
	}
	
	//-------------------------------------------------------------------
	public void childClosed(ManagedScreen child, CloseType type) {
		logger.debug("childClosed("+child+", "+type+") not overwritten");
		child.setCloseType(type);
	}

	//-------------------------------------------------------------------
	public void setOnAction(CloseType type, EventHandler<ActionEvent> handler) {
		handlers.put(type, handler);
	}

	//-------------------------------------------------------------------
	public void impl_navigClicked(CloseType type, ActionEvent event) {
		EventHandler<ActionEvent> handler = handlers.get(type);
		logger.debug("impl_navigClicked("+type+")  handler="+handler);
		if (handler!=null) {
//			if (buttonControl!=null) {
//				if (!buttonControl.tryCall(type)) {
//					logger.debug("Calling NavigButtonControl returned false for "+type);
//					return;
//				}
//			}
			handler.handle(event);
		} else {
			if (buttonControl!=null) {
				if (!buttonControl.tryCall(type)) {
					logger.debug("Calling NavigButtonControl returned false for "+type);
					return;
				}
			}
			if (close(type))  // To avoid duplicate calling of close
				manager.close(this, type);
		}
	}

	//-------------------------------------------------------------------
	public CloseType getCloseType() {
		return closedType;
	}

	//-------------------------------------------------------------------
	public void setCloseType(CloseType closedType) {
		this.closedType = closedType;
	}

	//--------------------------------------------------------------------
	public void setButtonControl(NavigButtonControl control) {
		buttonControl = control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.fluent.NavigableContentNode#getStyleSheets()
	 */
	@Override
	public String[] getStyleSheets() {
		// TODO Auto-generated method stub
		return new String[0];
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.fluent.NavigableContentNode#backSelected()
	 */
	@Override
	public CloseType backSelected() {
		logger.warn("backSelected() not overwritten in "+getClass());
		return CloseType.CANCEL;
	}

}
