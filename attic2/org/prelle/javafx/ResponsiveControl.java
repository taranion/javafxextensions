/**
 * 
 */
package org.prelle.javafx;

/**
 * @author prelle
 *
 */
public interface ResponsiveControl {
	
	public void setResponsiveMode(WindowMode value);

}
