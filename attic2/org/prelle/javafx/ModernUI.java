/**
 * 
 */
package org.prelle.javafx;

import java.awt.Toolkit;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 * @author prelle
 *
 */
public class ModernUI {
	
	public enum DPIProfile {
		LOW,
		MEDIUM,
		HIGH
	}
	
	public final static String DPI_KEY = "dpiprofile";

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private final static Preferences PREF = Preferences.userRoot().node("/org/prelle/jfx");
	
	//-------------------------------------------------------------------
	public static DPIProfile getDPIProfile() {
		String val = PREF.get(DPI_KEY, null);
		if (val==null)
			return null;
		return DPIProfile.valueOf(val.toUpperCase());
	}
	
	//-------------------------------------------------------------------
	public static void setDPIProfile(DPIProfile val) {
		PREF.put(DPI_KEY, val.name());
		logger.info("DPI profile set to "+val);
	}
	
	//-------------------------------------------------------------------
	private static void detectDPI(Scene scene) {
		int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
		System.out.println("Toolkit.getScreenResolution() returns "+dpi);
		dpi = (int) Screen.getPrimary().getDpi();
		System.out.println("Screen.getDpi() returns "+dpi);
	}

	//-------------------------------------------------------------------
	public static void initialize(Scene scene) {
		logger.debug("initialize(Scene)");
		
		Font.loadFont(ClassLoader.getSystemResourceAsStream(JavaFXConstants.PREFIX+"fonts/seguisym.ttf"), 20);
		detectDPI(scene);
//		StyleManager.errorsProperty().addListener(new ListChangeListener<CssError>(){
//			public void onChanged(
//					javafx.collections.ListChangeListener.Change<? extends CssError> c) {
//				logger.error("CSS Errors changed = "+c);
//			}});

		/*
		 * Load CSS
		 */
		scene.getStylesheets().add(ClassLoader.getSystemResource(JavaFXConstants.PREFIX+"css/fluent.css").toString());
		
		logger.info("Primary screen = "+Screen.getPrimary());
		logger.info("Primary screen DPI = "+Screen.getPrimary().getDpi());
	}
}
