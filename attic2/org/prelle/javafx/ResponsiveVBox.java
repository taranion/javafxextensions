/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.Node;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public abstract class ResponsiveVBox extends VBox implements ResponsiveControl {

	//-------------------------------------------------------------------
	/**
	 */
	public ResponsiveVBox() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param spacing
	 */
	public ResponsiveVBox(double spacing) {
		super(spacing);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param children
	 */
	public ResponsiveVBox(Node... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param spacing
	 * @param children
	 */
	public ResponsiveVBox(double spacing, Node... children) {
		super(spacing, children);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
