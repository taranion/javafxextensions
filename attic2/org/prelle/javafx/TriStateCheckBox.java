/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.TriStateCheckBoxSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.scene.AccessibleRole;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class TriStateCheckBox extends Control {

	public enum State {
		SELECTION1,
		SELECTION2,
		SELECTION3
	}

	/**
	 * Current state
	 */
	private ObjectProperty<State> state = new SimpleObjectProperty<State>(State.SELECTION1);
	/**
	 * Which states are allowed
	 */
	private ObservableList<State> allowedStates = FXCollections.observableArrayList(State.SELECTION1, State.SELECTION2, State.SELECTION3);
	
	private ObjectProperty<StringConverter<State>> stateFormatter;

	/***************************************************************************
	 *                                                                         *
	 * Stylesheet Handling                                                     *
	 *                                                                         *
	 **************************************************************************/

	private static final String DEFAULT_STYLE_CLASS = "tristate-check-box";
	private static final PseudoClass PSEUDO_CLASS_SELECTION1 = 
			PseudoClass.getPseudoClass("selection1");
	private static final PseudoClass PSEUDO_CLASS_SELECTION2 = 
			PseudoClass.getPseudoClass("selection2");
	private static final PseudoClass PSEUDO_CLASS_SELECTION3 = 
			PseudoClass.getPseudoClass("selection3");

	//-------------------------------------------------------------------
	/**
	 */
	public TriStateCheckBox() {
		setAccessibleRole(AccessibleRole.CHECK_BOX);
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		// initialize pseudo-class state
		pseudoClassStateChanged(PSEUDO_CLASS_SELECTION1, true);
//		setContentDisplay(ContentDisplay.RIGHT);
	}

	//-------------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	public ObjectProperty<State> stateProperty() { return state; }
	public State getState() { return state.get(); }
	public void setState(State newState) { state.set(newState); }

	//-------------------------------------------------------------------
	public ObservableList<State> getAllowedStates() { return allowedStates; }
	public boolean isAllowed(State state) { return allowedStates.contains(state); }

	//-------------------------------------------------------------------
	public final void setStateFormatter(StringConverter<State> value) {stateFormatterProperty().set(value);}
	public final StringConverter<State> getStateFormatter() { return stateFormatter == null ? null : stateFormatter.get(); }
	public final ObjectProperty<StringConverter<State>> stateFormatterProperty() {
		if (stateFormatter == null) {
			stateFormatter = new SimpleObjectProperty<StringConverter<State>>(this, "stateFormatter");
		}
		return stateFormatter;
	}

	//-------------------------------------------------------------------
    /** {@inheritDoc} */
	@Override
	protected Skin<?> createDefaultSkin() {
		return new TriStateCheckBoxSkin(this);
	}

	//-------------------------------------------------------------------
	/**
	 * Toggles the state of the {@code CheckBox}. If allowIndeterminate is
	 * true, then each invocation of this function will advance the CheckBox
	 * through the states checked, unchecked, and undefined. If
	 * allowIndeterminate is false, then the CheckBox will only cycle through
	 * the checked and unchecked states, and forcing indeterminate to equal to
	 * false.
	 */
//	@Override 
	public void fire() {
		if (!isDisabled() && !allowedStates.isEmpty()) {
			State newState = null;
			do {
				newState = State.values()[ (state.get().ordinal()+1)%State.values().length ];
			} while (!allowedStates.contains(newState) );
			setState(newState);
			switch (newState) {
			case SELECTION1: pseudoClassStateChanged(PSEUDO_CLASS_SELECTION1, true); break;
			case SELECTION2: pseudoClassStateChanged(PSEUDO_CLASS_SELECTION2, true); break;
			case SELECTION3: pseudoClassStateChanged(PSEUDO_CLASS_SELECTION3, true); break;
			}
			fireEvent(new ActionEvent());
		}
	}

	//--------------------------------------------------------------------
	public void select(State newState) {
		if (!allowedStates.contains(newState))
			return;
//			throw new IllegalStateException(newState+" not allowed in "+allowedStates);
		if (allowedStates.contains(newState)) {
			setState(newState);
			switch (newState) {
			case SELECTION1: pseudoClassStateChanged(PSEUDO_CLASS_SELECTION1, true); break;
			case SELECTION2: pseudoClassStateChanged(PSEUDO_CLASS_SELECTION2, true); break;
			case SELECTION3: pseudoClassStateChanged(PSEUDO_CLASS_SELECTION3, true); break;
			}
			fireEvent(new ActionEvent());
		}
	}

}
