package org.prelle.javafx;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.fluent.NavigableContentNode;
import org.prelle.javafx.fluent.NavigationView;
import org.prelle.javafx.fluent.NavigationViewBehaviour;
import org.prelle.javafx.fluent.NodeWithTitle;
import org.prelle.javafx.skin.ManagedScreenDialogSkin;
import org.prelle.javafx.skin.NavigButtonControl;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * @author prelle
 *
 */
public class ScreenManager extends NavigationView implements ScreenManagerProvider {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	// --- Primary navigation items
	ObjectProperty<ObservableList<ManagedScreen>> overlaysProperty = new SimpleObjectProperty<ObservableList<ManagedScreen>>(FXCollections.observableArrayList());
	private int inNested;
	
	private NavigationViewBehaviour behaviour;
	
	//--------------------------------------------------------------------
	public ScreenManager() {
		getStyleClass().add("managed-screen");
//		setSkin(new ScreenManagerSkin(this));
		
		behaviour = new NavigationViewBehaviour(this);
		initLayout();
		initInteractivity();
	}
	
	//--------------------------------------------------------------------
	private void initLayout() {
		getStyleClass().add("screen");
		setStyle("-fx-align: top-left");
	}
	
	//--------------------------------------------------------------------
	private void initInteractivity() {
		widthProperty().addListener( (ov,o,n) -> sizeChanged());
//		heightProperty().addListener( (ov,o,n) -> sizeChanged());
	}

	/******************************************
	 * Property OVERLAYS
	 ******************************************/
	public final ObservableList<ManagedScreen> getOverlays() { return overlaysProperty.get(); }
	public final ObjectProperty<ObservableList<ManagedScreen>> overlaysProperty() { return overlaysProperty; }

	//-------------------------------------------------------------------
	private void sizeChanged() {
		WindowMode newMode = WindowMode.MINIMAL;
		if (getWidth()<=640) newMode = WindowMode.MINIMAL;
		else if (getWidth()<=1400) newMode = WindowMode.COMPACT;
		else newMode = WindowMode.EXPANDED;
		
		if (newMode!=getWindowMode()) {
			changeMode(newMode);
		}
	}

	//--------------------------------------------------------------------
	private void changeModeRecursivly(Parent parent, WindowMode value) {
		for (Node tmp : parent.getChildrenUnmodifiable()) {
			if (tmp instanceof ResponsiveControl) 
				((ResponsiveControl)tmp).setResponsiveMode(value);
			if (tmp instanceof Parent)
				changeModeRecursivly((Parent) tmp, value);
		}
	}

	//--------------------------------------------------------------------
	private void changeMode(WindowMode value) {
		setWindowMode(value);
		logger.info("Responsive: Mode changed to "+value);
		for (ManagedScreenSkeleton pane : getOverlays()) {
			setModeRecursivly(value, pane);
		}
		
		changeModeRecursivly(this, value);
	}

	//--------------------------------------------------------------------
	private void setModeRecursivly(WindowMode value, Parent parent) {
		if (parent instanceof ResponsiveControl)
			((ResponsiveControl)parent).setResponsiveMode(value);
		for (Node child : parent.getChildrenUnmodifiable()) {
			if (child instanceof Parent) 
				setModeRecursivly(value, (Parent) child);
		}
	}

	//--------------------------------------------------------------------
	public void show(NavigableContentNode page, String... stylesheets) {
		if (page instanceof ManagedScreenSkeleton)
			((ManagedScreenSkeleton)page).setScreenManager(this);
		
		if (stylesheets.length>0) {
			logger.warn("Outdated stylesheet parameter. Return those from NavigableContentNode.getStylesheets()");
		}
		
		behaviour.open(page);
	}

	//-------------------------------------------------------------------
	public void replaceContent(NodeWithTitle page) {
		if (page instanceof ManagedScreenSkeleton)
			((ManagedScreenSkeleton)page).setScreenManager(this);
		
		behaviour.replaceContent(page);
	}

	//--------------------------------------------------------------------
	public void replace(NavigableContentNode page) {
		if (page instanceof ManagedScreenSkeleton)
			((ManagedScreenSkeleton)page).setScreenManager(this);
		
		behaviour.replace(page);
	}

	//--------------------------------------------------------------------
	public void cancel() {
		behaviour.close(behaviour.getShowing(), CloseType.CANCEL); 
	}
	
	//--------------------------------------------------------------------
	/**
	 * Show a dialog
	 */
	public Object showAndWait(ManagedScreen page, String... stylesheets) {
		logger.info("----------showAndWait:"+page.getClass()+"--------------------------------");
		if (getOverlays().contains(page)) {
			logger.error("Showing a page already on the stack not implemented yet");
			return null;
		}
		
		logger.info("show screen with styleclasses "+page.getStyleClass());
		if (!page.getStyleClass().contains("screen")) 
			page.getStyleClass().add("screen");
			
		// Add stylesheets
		if (stylesheets.length>0) {
			page.setUserData(stylesheets);
			getScene().getStylesheets().addAll(stylesheets);
			logger.debug("Add stylesheets to scene: "+Arrays.toString(stylesheets)+"   result="+getScene().getStylesheets());
		}
		
		// Hide current page
//		if (!pages.isEmpty())
//			pages.peek().setVisible(false);
		
		// Memorize page 
		logger.debug("Add page "+page+" and make it visible   -  skin is "+page.getSkin());
		getOverlays().add(page);
		getChildren().add(page);
		page.setScreenManager(this);
		page.setVisible(true);

		logger.debug("Block on key "+page);
		inNested++;
		
		/*
		 * Java 8 : Toolkit.getToolkit().enterNestedEventLoop
		 * Java 9+: Platform.enterNestedEventLoop
		 */
		String _v = System.getProperty("java.version");
		int version = Integer.parseInt((_v.indexOf(".")>0)?_v.substring(0, _v.indexOf(".")):_v);
		Method method =null;
		Object retVal =null;
		try {
			logger.warn("Version is "+version+"   (raw "+_v+ ")");
			if (version>=9) {
				// Object retVal = javafx.application.Platform.enterNestedEventLoop(page);
				method = Class.forName("javafx.application.Platform").getMethod("enterNestedEventLoop", Object.class);
				logger.warn("method is "+method);
				retVal = method.invoke(null, page);
			} else {
				// Object retVal = com.sun.javafx.tk.Toolkit.getToolkit().enterNestedEventLoop(page);
				Object toolkit = Class.forName("com.sun.javafx.tk.Toolkit").getDeclaredMethod("getToolkit").invoke(null);
				method = Class.forName("com.sun.javafx.tk.Toolkit").getMethod("enterNestedEventLoop", Object.class);
				retVal = method.invoke(toolkit, page);
			}
		} catch (NoSuchMethodException | SecurityException | ClassNotFoundException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			logger.fatal("Failed calling enterNestedEventLoop - tried "+method,e);
		}
		logger.debug("----------showAndWait-done----------------------------"+retVal);
		return retVal;
	}
	

	//-------------------------------------------------------------------
	public ManagedScreenSkeleton peek() {
		if (getOverlays().isEmpty())
			return null;
		return getOverlays().get(getOverlays().size()-1);
	}

	//-------------------------------------------------------------------
	public void closeCurrent(CloseType type) {
		ManagedScreenSkeleton screen = peek();
		close(screen, type);
	}

	//-------------------------------------------------------------------
	public void close(ManagedScreenSkeleton page, CloseType type) {
		logger.info("Close "+page+" with "+type);
		logger.debug("Stack is "+getOverlays());
		
		// Do nothing if page is unknown
		if (!getOverlays().contains(page)) {
			logger.error("Closing a page not on the stack");
			return;
		}
		// Don't close first page
		if (getOverlays().size()<1) {
			logger.error("Don't close first page");
			return;
		}
	
		// Remove stylesheets
		if (page.getUserData()!=null) {
			String[] stylesheets = (String[]) page.getUserData();
			getScene().getStylesheets().removeAll(stylesheets);
			logger.info("Remove stylesheets from scene: "+Arrays.toString(stylesheets));
		}
		
		// Remove
//		page.getScene().getWindow().hide();
		logger.debug("Unblock key "+page);
		if (inNested>0) {
			inNested--;
			/*
			 * Java 8 : Toolkit.getToolkit().enterNestedEventLoop
			 * Java 9+: Platform.enterNestedEventLoop
			 */
			String _v = System.getProperty("java.version");
			int version = Integer.parseInt((_v.indexOf(".")>0)?_v.substring(0, _v.indexOf(".")):_v);
			Method method =null;
			try {
				if (version>=9) {
					// Object retVal = javafx.application.Platform.exitNestedEventLoop(page, type);
					method = Class.forName("javafx.application.Platform").getMethod("exitNestedEventLoop", Object.class, Object.class);
					method.invoke(null, page, type);
				} else {
					// Object retVal = com.sun.javafx.tk.Toolkit.getToolkit().exitNestedEventLoop(page, type);
					Object toolkit = Class.forName("com.sun.javafx.tk.Toolkit").getDeclaredMethod("getToolkit").invoke(null);
					method = Class.forName("com.sun.javafx.tk.Toolkit").getMethod("exitNestedEventLoop", Object.class, Object.class);
					method.invoke(toolkit, page, type);
				}
			} catch (NoSuchMethodException | SecurityException | ClassNotFoundException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				logger.fatal("Failed calling exitNestedEventLoop - tried "+method,e);
			}
		}

		logger.debug("Make invisible");
		page.setVisible(false);
		getChildren().remove(page);
		getOverlays().remove(getOverlays().size()-1);
		
		// Make current page visible again
		ManagedScreenSkeleton now = peek();
		if (now!=null) {
			now.setVisible(true);

			if (peek() instanceof ManagedScreen)
				((ManagedScreen)peek()).childClosed((ManagedScreen) page, type);
			else {
				logger.warn("TODO: implement close()");
				//			pages.peek().childClosed(page, type);
			}
		}
	}

//	//--------------------------------------------------------------------
//	private void handle(ActionEvent event) {
//		Button source = (Button)event.getSource();
//		for (Entry<CloseType, Button> entry : buttonMap.entrySet()) {
//			if (source==entry.getValue()) {
//				CloseType type = entry.getKey();
//				logger.debug("Button "+type+" selected");
//				// Inform dialog
//				ScreenManagerPage current = (ScreenManagerPage) pages.peek();
//				boolean allowed = current.close(type);
//				if (allowed) {
//					if (pages.size()>1) {
//						pages.pop();					
//						refresh();
//						logger.debug("Call childClosed on "+pages.peek());
//						((ScreenManagerPage) pages.peek()).childClosed(current, type);
//					} else
//						logger.error("Cannot close first page");
//					
//				} else {
//					logger.warn("TODO: Give user feedback when closing not allowed");
//					showAlertAndCall(
//							AlertType.WARNING, 
//							"Diese Aktion ist derzeit nicht erlaubt", 
//							"Sie haben noch vermutlich nicht alle nötigen Eingaben gemacht.",
//							new ReturnMethod<ButtonType>() {
//								public void returned(ButtonType response) {
//								}
//							}
//					);
//				}
//				return;
//			}
//		}
//	}
//	
//	//--------------------------------------------------------------------
//	public Scene getScene() {
//		return stack.getScene();
//	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(AlertType type, String headerText, Node content, NavigButtonControl control) {
		ManagedScreen screen = new ManagedScreen();
		screen.setTitle(headerText);
		screen.setContent(content);
		switch (type) {
		case QUESTION:
			screen.getNavigButtons().addAll(CloseType.OK, CloseType.CANCEL);
			break;
		case CONFIRMATION:
			screen.getNavigButtons().addAll(CloseType.YES, CloseType.NO);
			break;
		default:
			screen.getNavigButtons().addAll(CloseType.OK);
			break;
		}
		ManagedScreenDialogSkin skin = new ManagedScreenDialogSkin(screen);
		if (control!=null) {
			control.setSkin(skin);
			screen.setButtonControl(control);
		}
		screen.setSkin(skin);
		new ManagedScreenBehaviour(screen);

		if (content instanceof TextField) {
			((TextField)content).setFocusTraversable(true);
			((TextField)content).setOnAction(event ->  {
			close(screen, CloseType.OK);
		});
		}
		content.requestFocus();
		
		// Wait for buttons
		screen.setOnAction(CloseType.OK, event -> {
			logger.debug("OK");
			if (control!=null && !control.tryCall(CloseType.OK)) {
				logger.debug("Not OK");
				return;
			}
			close(screen, CloseType.OK);
		});
		screen.setOnAction(CloseType.CANCEL, event -> {
			logger.debug("CANCEL");
			if (control!=null && !control.tryCall(CloseType.CANCEL)) {
				logger.debug("Not CANCEL");
				return;
			}
			close(screen, CloseType.CANCEL);
		});
		
		return (CloseType)showAndWait(screen);
	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(AlertType type, String headerText, Node content) {
		return showAlertAndCall(type, headerText, content, null);
	}

	//--------------------------------------------------------------------
	public CloseType showAlertAndCall(AlertType type, String headerText, String contentText) {
		Label content= new Label(contentText);
		content.setWrapText(true);
		content.getStyleClass().add("text-body");

		return showAlertAndCall(type, headerText, content);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return this;
	}
	
//	//-------------------------------------------------------------------
//	public void showAsDialog(Node dialogContent, String... styles) {
//		logger.debug("Show as dialogNode "+dialogContent);
//		/*
//		 * Layout
//		 */
//		Region upperSpacing = new Region();
//		Region lowerSpacing = new Region();
//		Region leftSpacing  = new Region();
//		Region rightSpacing = new Region();
//		HBox   dialogLine   = new HBox();
//		dialogLine.getStyleClass().add("message-dialog");
//		HBox.setHgrow(leftSpacing, Priority.ALWAYS);
//		HBox.setHgrow(rightSpacing, Priority.ALWAYS);
//		HBox.setMargin(dialogContent, new Insets(20));
//		upperSpacing.setStyle("-fx-background-color: rgba(0,0,0, 0.5)");
//		lowerSpacing.setStyle("-fx-background-color: rgba(0,0,0, 0.5)");
//		upperSpacing.setMinHeight(100);
//		lowerSpacing.setMinHeight(100);
////		leftSpacing.setMaxWidth(Double.MAX_VALUE);
//		dialogLine.setMinHeight(100);
//		dialogLine.getChildren().addAll(leftSpacing, dialogContent, rightSpacing);
//		
//		VBox dialogScreen = new VBox();
//		dialogScreen.setId("dialogScreen");
//		dialogScreen.getChildren().addAll(upperSpacing, dialogLine, lowerSpacing);
//		VBox.setVgrow(upperSpacing, Priority.ALWAYS);
//		VBox.setVgrow(lowerSpacing, Priority.ALWAYS);
//		
//		// FIXME
//		this.getScene().getStylesheets().addAll(styles);
//		
//		Platform.runLater(new Runnable() {
//			public void run() {
//				stack.getChildren().add(dialogScreen);
//			}
//		});
//	}
	
}
