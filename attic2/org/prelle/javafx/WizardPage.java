package org.prelle.javafx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.image.Image;

/**
 * @author prelle
 */
public abstract class WizardPage {
	
	protected Wizard wizard;
	private ObjectProperty<String> title;
	private ObjectProperty<Node>   content;
	private ObjectProperty<Image>  image;
	private ObjectProperty<Insets>  imageInsets;
	private ObjectProperty<Node>   side;
	protected BooleanProperty previousButton;
	protected BooleanProperty finishButton;
	protected BooleanProperty nextButton;
	protected BooleanProperty active;

	//-------------------------------------------------------------------
	public WizardPage(Wizard wizard) {
		this.wizard = wizard;
		image   = new SimpleObjectProperty<Image>();
		content = new SimpleObjectProperty<Node>();
		title   = new SimpleObjectProperty<String>();
		imageInsets   = new SimpleObjectProperty<Insets>();
		side    = new SimpleObjectProperty<Node>();
		
		previousButton = new SimpleBooleanProperty(true);
		finishButton   = new SimpleBooleanProperty(true);
		nextButton     = new SimpleBooleanProperty(true);
		active         = new SimpleBooleanProperty(true);
	}

	//-------------------------------------------------------------------
	void setWizard(Wizard wizard) {
		this.wizard = wizard;
	}

	//-------------------------------------------------------------------
	protected Wizard getWizard() {
		return wizard;
	}

	//--------------------------------------------------------------------
	public Node getContent() { return content.get(); }
	public void setContent(Node value) { content.set(value);}
	public ObjectProperty<Node> contentProperty() { return content; }

	//--------------------------------------------------------------------
	public String getTitle() { return title.get(); }
	public void setTitle(String value) { title.set(value);}
	public ObjectProperty<String> titleProperty() { return title; }

	//--------------------------------------------------------------------
	public ObjectProperty<Image> imageProperty() { return image; }
	public void setImage(Image value) {	image.set(value); }
	public Image getImage() {return image.get();}

	//--------------------------------------------------------------------
	public ObjectProperty<Insets> imageInsetsProperty() { return imageInsets; }
	public void setImageInsets(Insets value) {	imageInsets.set(value); }
	public Insets getImageInsets() {return imageInsets.get();}

	//--------------------------------------------------------------------
	public BooleanProperty getPreviousButtonProperty() {return previousButton;}
	public BooleanProperty getNextButtonProperty() {return nextButton;}
	public BooleanProperty getFinishButtonProperty() {return finishButton;}
	public BooleanProperty getActiveProperty() {return active;}
	//--------------------------------------------------------------------
	public boolean isPreviousButtonEnabled() {return previousButton.get();}
	public boolean isNextButtonEnabled() {return nextButton.get();}
	public boolean isFinishButtonEnabled() {return finishButton.get();}
	public boolean isActive() {return active.get();}

	//--------------------------------------------------------------------
	public Node getSide() { return side.get(); }
	public void setSide(Node value) { side.set(value);}
	public ObjectProperty<Node> sideProperty() { return side; }

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is shown to user
	 */
	public void pageVisited() {
		
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard before an action is performed
	 */
	public boolean isCloseTypePermittedByUser(CloseType type) {
		return true;
	}

}