/**
 * 
 */
package org.prelle.javafx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.scene.AccessibleRole;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.util.StringConverter;

import org.prelle.javafx.skin.MetroRadioButtonSkin;

/**
 * @author prelle
 *
 */
public class MetroRadioButton extends Control {

	/**
	 * Current state
	 */
	private BooleanProperty state = new SimpleBooleanProperty();
	
	private ObjectProperty<StringConverter<Boolean>> stateFormatter;

	/***************************************************************************
	 *                                                                         *
	 * Stylesheet Handling                                                     *
	 *                                                                         *
	 **************************************************************************/

	private static final String DEFAULT_STYLE_CLASS = "metro-radio-button";
	private static final PseudoClass PSEUDO_CLASS_SELECTION1 = 
			PseudoClass.getPseudoClass("on");
	private static final PseudoClass PSEUDO_CLASS_SELECTION2 = 
			PseudoClass.getPseudoClass("off");

	//-------------------------------------------------------------------
	/**
	 */
	public MetroRadioButton() {
		setAccessibleRole(AccessibleRole.RADIO_BUTTON);
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		// initialize pseudo-class state
		pseudoClassStateChanged(PSEUDO_CLASS_SELECTION1, true);
//		setContentDisplay(ContentDisplay.RIGHT);
	}

	//-------------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	public BooleanProperty stateProperty() { return state; }
	public boolean getState() { return state.get(); }
	public void setState(boolean newState) { state.set(newState); }

	//-------------------------------------------------------------------
	public final void setStateFormatter(StringConverter<Boolean> value) {stateFormatterProperty().set(value);}
	public final StringConverter<Boolean> getStateFormatter() { return stateFormatter == null ? null : stateFormatter.get(); }
	public final ObjectProperty<StringConverter<Boolean>> stateFormatterProperty() {
		if (stateFormatter == null) {
			stateFormatter = new SimpleObjectProperty<StringConverter<Boolean>>(this, "stateFormatter");
		}
		return stateFormatter;
	}

	//-------------------------------------------------------------------
    /** {@inheritDoc} */
	@Override
	protected Skin<?> createDefaultSkin() {
		return new MetroRadioButtonSkin(this);
	}

	//-------------------------------------------------------------------
	/**
	 * Toggles the state of the {@code CheckBox}. If allowIndeterminate is
	 * true, then each invocation of this function will advance the CheckBox
	 * through the states checked, unchecked, and undefined. If
	 * allowIndeterminate is false, then the CheckBox will only cycle through
	 * the checked and unchecked states, and forcing indeterminate to equal to
	 * false.
	 */
//	@Override 
	public void fire() {
		if (!isDisabled()) {
			if (state.get()) {
				pseudoClassStateChanged(PSEUDO_CLASS_SELECTION1, true);
			} else {
				pseudoClassStateChanged(PSEUDO_CLASS_SELECTION2, true);
			}
			fireEvent(new ActionEvent());
		}
	}

	//--------------------------------------------------------------------
	public void select(boolean newState) {
		if (!isDisabled()) {
			setState(newState);
			if (newState) {
				pseudoClassStateChanged(PSEUDO_CLASS_SELECTION1, true); 
			} else {
				pseudoClassStateChanged(PSEUDO_CLASS_SELECTION2, true);
			}
			fireEvent(new ActionEvent());
		}
	}

}
