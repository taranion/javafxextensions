/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.Node;
import javafx.scene.control.SkinBase;

/**
 * @author prelle
 *
 */
public abstract class FlipControlSkinBase extends SkinBase<FlipControl> {

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public FlipControlSkinBase(FlipControl control) {
		super(control);
		// TODO Auto-generated constructor stub
	}
	
	//-------------------------------------------------------------------
    public abstract void flip(Node front, Node back);

}
