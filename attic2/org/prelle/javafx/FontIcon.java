/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class FontIcon extends StackPane {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private Text label;
	private Text count;
	
	private double lastFontSize;
	
	//-------------------------------------------------------------------
	public FontIcon() {
		label  = new Text();
		label.getStyleClass().add("font-icon-text");
		count = new Text();
		count.getStyleClass().add("font-icon-count");
		
		label.boundsInLocalProperty().addListener(new ChangeListener<Bounds>() {
			public void changed(ObservableValue<? extends Bounds> observable,
					Bounds oldValue, Bounds newValue) {
				double belowBaseline = newValue.getHeight() - label.getBaselineOffset();
				double margin = ( newValue.getHeight() - belowBaseline );
//				logger.debug("Move counter icon up to "+margin+"   baseline="+label.getBaselineOffset()+"   height="+newValue.getHeight());
				StackPane.setMargin(count, new Insets(-margin,0,0,0));
			}
		});
		getChildren().addAll(label, count);
		
		getStyleClass().add("font-icon");
		setStyle("-fx-font-family: 'Segoe UI Symbol'"); 

		count.setFill(Color.RED);
//		StackPane.setAlignment(count, Pos.TOP_CENTER);
		
		
		label.fontProperty().addListener(new ChangeListener<Font>() {
			public void changed(ObservableValue<? extends Font> observable,
					Font oldValue, Font newValue) {
				if (label.getFont().getSize()!=lastFontSize) {
					lastFontSize = newValue.getSize();
					Font newFont = new Font(newValue.getName(), newValue.getSize()*0.7);
					if (logger.isTraceEnabled())
						logger.trace(String.format("Icon font size is %f, set counter font size to %f", newValue.getSize(), newFont.getSize()));
					count.setFont(newFont);
				}
			}
		});
	}
	
	//-------------------------------------------------------------------
	public FontIcon(int size) {
		this();
		if (size>0) {
			label.setStyle("-fx-font-size: "+size);
			lastFontSize = size;
		}
	}
	
	//-------------------------------------------------------------------
	public FontIcon(String value) {
		this();
		addFontSymbol(value);
	}
	
	//-------------------------------------------------------------------
	public FontIcon(String value, int size) {
		this(size);
		addFontSymbol(value);
	}

	//-------------------------------------------------------------------
	public FontIcon addFontSymbol(String value) {
		label.setText(label.getText()+value);
		
		if (label.getFont().getSize()!=lastFontSize) {
			lastFontSize = label.getFont().getSize();
			Font font = label.getFont();
			Font newFont = new Font(font.getName(), font.getSize()*0.7);
			if (logger.isTraceEnabled())
				logger.trace(String.format("Default Icon font size is %f, set counter font size to %f", lastFontSize, newFont.getSize()));
			count.setFont(newFont);
		}
		return this;
	}

	//-------------------------------------------------------------------
	public FontIcon addFontSymbol(String value, int size) {
		label.setText(label.getText()+value);
		label.setStyle("-fx-font-size: "+size);
		
		if (label.getFont().getSize()!=lastFontSize) {
			lastFontSize = label.getFont().getSize();
			Font font = label.getFont();
			Font newFont = new Font(font.getName(), font.getSize()*0.7);
			if (logger.isTraceEnabled())
				logger.trace(String.format("Default Icon font size is %f, set counter font size to %f", lastFontSize, newFont.getSize()));
			count.setFont(newFont);
		}
		return this;
	}

	//-------------------------------------------------------------------
	public FontIcon addFontOverSymbol(String value, int size) {
		Text background = new Text(value);
		Text foreground = new Text(value);
		background.setStyle("-fx-font-size: "+(size+12)+";  -fx-fill: areacolor-action;");
		foreground.setStyle("-fx-font-size: "+ size   +";  -fx-fill: qsc-grau-hell ; -fx-effect: dropshadow(gaussian, areacolor-action, 3, 1.0, 0,0)");
		StackPane over = new StackPane();
		over.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		over.getChildren().addAll( foreground);
		getChildren().add(over);
//		StackPane.setAlignment(over, Pos.BOTTOM_RIGHT);
		StackPane.setMargin(over, new Insets(18,-18,-18,18));
		
		return this;
	}

	//-------------------------------------------------------------------
	public void setCounter(int num) {
//		if (count==null) {
//			count = new Text();
//			getChildren().add(count);
//			count.getStyleClass().add("font-icon-count");
//			count.setStyle("-fx-font-size: smaller; -fx-font-color: red;"); 
//		}
		count.setText(String.valueOf(num));
	}

	//-------------------------------------------------------------------
	public Text getLabel() {
		return label;
	}
	
}
