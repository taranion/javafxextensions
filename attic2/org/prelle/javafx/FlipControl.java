/**
 * 
 */
package org.prelle.javafx;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Control;

import org.prelle.javafx.skin.AdaptingFlipControlSkin;
import org.prelle.javafx.skin.FlipControlSkin;

/**
 * @author prelle
 *
 */
public class FlipControl extends Control {

	private static final String DEFAULT_STYLE_CLASS = "flip-control";
	
	private ObjectProperty<Orientation> orientationProperty = new SimpleObjectProperty<Orientation>(Orientation.VERTICAL);
	private ObjectProperty<ObservableList<Node>> itemsProperty = new SimpleObjectProperty<ObservableList<Node>>(FXCollections.observableArrayList());
	private IntegerProperty visibleIndexProperty = new SimpleIntegerProperty(-1);
	private ObjectProperty<Node> visibleNodeProperty = new SimpleObjectProperty<>();
	
	private transient FlipControlBehavior behavior;

	//-------------------------------------------------------------------
	public FlipControl() {
		this(Orientation.HORIZONTAL);
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
	}

	//-------------------------------------------------------------------
	public FlipControl(Orientation orientation) {
		orientationProperty.set(orientation);
		behavior = new FlipControlBehavior(this);
		setSkin(new FlipControlSkin(this, (FlipControlBehavior) behavior));
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
	}

	//-------------------------------------------------------------------
	public FlipControl(Orientation orientation, boolean adapting) {
		orientationProperty.set(orientation);
		if (adapting) {
			behavior = new AdaptingFlipControlBehavior(this);
			setSkin(new AdaptingFlipControlSkin(this, (AdaptingFlipControlBehavior) behavior));
		} else {
			behavior = new FlipControlBehavior(this);
			setSkin(new FlipControlSkin(this, (FlipControlBehavior) behavior));
		}
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
	}

	/******************************************
	 * Property ORIENTATION
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ObjectProperty<Orientation> getOrientationProperty() {
		return orientationProperty;
	}

	//-------------------------------------------------------------------
	public Orientation getOrientation() {
		return orientationProperty.get();
	}

	//-------------------------------------------------------------------
	public void setOrientation(Orientation value) {
		orientationProperty.setValue(value);
	}

	/******************************************
	 * Property ORIENTATION
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ObjectProperty<ObservableList<Node>> itemsProperty() {
		return itemsProperty;
	}

	//-------------------------------------------------------------------
	public ObservableList<Node> getItems() {
		return itemsProperty.get();
	}

//	//-------------------------------------------------------------------
//	public void setItems(ObservableList<Node> value) {
//		itemsProperty.setValue(value);
//	}

	/******************************************
	 * Property VISIBLE_NODE
	 ******************************************/
	
	//-------------------------------------------------------------------
	public ObjectProperty<Node> visibleNodeProperty() {
		return visibleNodeProperty;
	}

	//-------------------------------------------------------------------
	public Node getVisibleNode() {
		return visibleNodeProperty.get();
	}

	//-------------------------------------------------------------------
	public void setOrientation(Node value) {
		visibleNodeProperty.setValue(value);
	}


	/******************************************
	 * Property VISIBLE_NODE
	 ******************************************/
	
	//-------------------------------------------------------------------
	public IntegerProperty visibleIndexProperty() {
		return visibleIndexProperty;
	}

	//-------------------------------------------------------------------
	public int getVisibleIndex() {
		return visibleIndexProperty.get();
	}

	//-------------------------------------------------------------------
	public void setVisibleIndex(int value) {
		visibleIndexProperty.setValue(value);
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return getChildren();
	}

	public void flip() {
		if (behavior instanceof AdaptingFlipControlBehavior)
			((AdaptingFlipControlBehavior)behavior).flipForward();
		else if (behavior instanceof FlipControlBehavior)
			((FlipControlBehavior)behavior).flipForward();
	}
}
