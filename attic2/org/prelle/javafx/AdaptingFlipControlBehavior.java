/**
 * 
 */
package org.prelle.javafx;

import javafx.collections.ObservableList;
import javafx.scene.Node;

/**
 * @author prelle
 *
 */
public class AdaptingFlipControlBehavior extends FlipControlBehavior {
  
	//-------------------------------------------------------------------
	public AdaptingFlipControlBehavior(FlipControl control) {
		super(control);
	}

    //-----------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return getControl().impl_getChildren();
	}

    //-----------------------------------------------------------------
	public void makeVisible(int index) {
		logger.debug("makeVisible("+index+")");
		// Make all nodes invisible
		for (Node node : getControl().getItems())
			if (node.isVisible())
				node.setVisible(false);

		// Make the new node visible
		Node makeVisible = getControl().getItems().get(index);
		makeVisible.setVisible(true);
		getControl().visibleIndexProperty().set(index);
		getControl().visibleNodeProperty().set(makeVisible);
	}

    //-----------------------------------------------------------------
    /**
     * When flipped forward which is the next node?
     */
    private Node getNextSide() {
    	int current = getControl().getVisibleIndex();
    	int next    = current+1;
    	if (next>= getControl().getItems().size())
    		next = 0;
    	
    	return getControl().getItems().get(next);
    }

    //-----------------------------------------------------------------
    public void flipForward() {
		Node current = getControl().getVisibleNode();
		Node next    = getNextSide();
		((FlipControlSkinBase)getControl().getSkin()).flip(current, next);
		getControl().visibleNodeProperty().set(next);
		getControl().visibleIndexProperty().set(getControl().getItems().indexOf(next));
    }

}
