/**
 * 
 */
package org.prelle.javafx;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * A full customizable content page with partly transparent borders above
 * and below that show the underlying page.
 * 
 * +------------------------------------------+
 * |   dialog-outside                         |
 * +------------------------------------------+
 * |                                          |
 * |   dialog-content                         |
 * |                                          |
 * +------------------------------------------+
 * |   dialog-outside                         |
 * +------------------------------------------|
 *
 * @author prelle
 *
 */
public abstract class DialogBox extends Parent {
	
	private final static String STYLE_OUTSIDE = "dialog-outside";
	private final static String STYLE_CONTENT = "dialog-content";
	private final static String STYLE_ALL     = "dialog";
	
	protected ScreenManager manager;
	private Region upper;
	private Region lower;
	protected StackPane content;
	private VBox layout;

	//-----------------------------------------------------------------
	protected DialogBox() {
		getStyleClass().add(STYLE_ALL);
		
		initComponents();
		initLayout();
		initStyle();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		layout  = new VBox();
		upper   = new Region();
		lower   = new Region();
		content = new StackPane();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout.getChildren().addAll(upper, content, layout);
		
		upper.setMaxHeight(Double.MAX_VALUE);
		lower.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		upper.getStyleClass().add(STYLE_OUTSIDE);
		lower.getStyleClass().add(STYLE_OUTSIDE);
	}

	//-------------------------------------------------------------------
	public void setContent(Node content) {
		if (content.getStyleClass().contains(STYLE_CONTENT))
			content.getStyleClass().add(STYLE_CONTENT);
		
		this.content.getChildren().clear();
		this.content.getChildren().add(content);
	}

}
