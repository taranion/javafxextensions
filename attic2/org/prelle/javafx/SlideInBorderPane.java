/**
 *
 */
package org.prelle.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Control;

import org.prelle.javafx.skin.SlideInBorderPaneSkin;

/**
 * @author Stefan
 *
 */
public class SlideInBorderPane extends Control {

	private ObjectProperty<Node> content;
	private ObjectProperty<SlideInPanel> visibleLeft;
	private ObjectProperty<SlideInPanel> visibleRight;
	private ObjectProperty<SlideInPanel> visibleTop;
	private ObjectProperty<SlideInPanel> visibleBottom;

	private ObservableList<SlideInPanel> rightSideBars;
	private ObservableList<SlideInPanel> leftSideBars;
	private ObservableList<SlideInPanel> topSideBars;
	private ObservableList<SlideInPanel> bottomSideBars;

	private transient List<EventHandler<ActionEvent>> handlerLeft;
	private transient List<EventHandler<ActionEvent>> handlerRight;
	private transient List<EventHandler<ActionEvent>> handlerTop;
	private transient List<EventHandler<ActionEvent>> handlerBottom;

	//--------------------------------------------------------------------
	public SlideInBorderPane() {
		content = new SimpleObjectProperty<Node>();
		visibleLeft   = new SimpleObjectProperty<>();
		visibleRight  = new SimpleObjectProperty<>();
		visibleTop    = new SimpleObjectProperty<>();
		visibleBottom = new SimpleObjectProperty<>();

		rightSideBars = FXCollections.observableArrayList();
		leftSideBars  = FXCollections.observableArrayList();
		topSideBars   = FXCollections.observableArrayList();
		bottomSideBars  = FXCollections.observableArrayList();

		handlerLeft  = new ArrayList<EventHandler<ActionEvent>>();
		handlerRight = new ArrayList<EventHandler<ActionEvent>>();
		handlerTop   = new ArrayList<EventHandler<ActionEvent>>();
		handlerBottom= new ArrayList<EventHandler<ActionEvent>>();

		setSkin(new SlideInBorderPaneSkin(this));
		new SlideInBorderPaneBehaviour(this);
	}

	//--------------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//--------------------------------------------------------------------
	void setOnActionLeft(EventHandler<ActionEvent> handler) {
		if (!this.handlerLeft.contains(handler))
			this.handlerLeft.add(handler);
	}

	//--------------------------------------------------------------------
	void setOnActionRight(EventHandler<ActionEvent> handler) {
		if (!this.handlerRight.contains(handler))
			this.handlerRight.add(handler);
	}

	//--------------------------------------------------------------------
	void setOnActionTop(EventHandler<ActionEvent> handler) {
		if (!this.handlerTop.contains(handler))
			this.handlerTop.add(handler);
	}

	//--------------------------------------------------------------------
	void setOnActionBottom(EventHandler<ActionEvent> handler) {
		if (!this.handlerBottom.contains(handler))
			this.handlerBottom.add(handler);
	}

	//--------------------------------------------------------------------
	public void fireLeftPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerLeft) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void fireRightPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerRight) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void fireTopPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerTop) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void fireBottomPanelSelected(ActionEvent ev) {
		for (EventHandler<ActionEvent> callback : handlerBottom) {
			try {
				callback.handle(ev);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//--------------------------------------------------------------------
	public void setCenter(Node center) {
		content.set(center);
	}

	//--------------------------------------------------------------------
	public ObjectProperty<Node> centerProperty() {
		return content;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getLeftSidePanels() {
		return leftSideBars;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getRightSidePanels() {
		return rightSideBars;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getTopSidePanels() {
		return topSideBars;
	}

	//--------------------------------------------------------------------
	public ObservableList<SlideInPanel> getBottomSidePanels() {
		return bottomSideBars;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleLeftProperty() {
		return visibleLeft;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleRightProperty() {
		return visibleRight;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleTopProperty() {
		return visibleTop;
	}

	//--------------------------------------------------------------------
	public ObjectProperty<SlideInPanel> visibleBottomProperty() {
		return visibleBottom;
	}

}
