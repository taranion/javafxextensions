/**
 *
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.ManagedScreenDialogSkin;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Stefan
 *
 */
public class Wizard extends ManagedScreen implements ScreenManagerProvider {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

    private WizardBehaviour behaviour;

    protected ObservableList<WizardPage> pages;

    protected BooleanProperty canBeFinishedProperty;

    //--------------------------------------------------------------------
    public Wizard() {
        pages = FXCollections.observableArrayList();
        canBeFinishedProperty = new SimpleBooleanProperty(false);

        logger.debug("Add navigation buttons");
        getNavigButtons().addAll(
                CloseType.PREVIOUS,
                CloseType.CANCEL,
                CloseType.FINISH,
                CloseType.NEXT
                );

        setSkin(new ManagedScreenDialogSkin(this));
        behaviour = new WizardBehaviour(this);
    }

    //--------------------------------------------------------------------
    public Wizard(WizardPage...pages) {
        this();
        this.pages.addAll(pages);

        behaviour.navTo(0, true, null);
    }

    //--------------------------------------------------------------------
    public BooleanProperty canBeFinishedProperty() {
    	return canBeFinishedProperty;
    }

    //--------------------------------------------------------------------
    public boolean canBeFinished() {
    	return canBeFinishedProperty.get();
    }

    //--------------------------------------------------------------------
    public void setCanBeFinished(boolean value) {
    	canBeFinishedProperty.set(value);
    }

    //--------------------------------------------------------------------
    public ObservableList<WizardPage> getPages() {
        return pages;
    }

    //--------------------------------------------------------------------
    public WizardBehaviour impl_getBehaviour() {
        return behaviour;
    }

}
