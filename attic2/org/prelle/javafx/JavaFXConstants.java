/**
 * 
 */
package org.prelle.javafx;

import java.util.PropertyResourceBundle;

/**
 * @author prelle
 *
 */
public interface JavaFXConstants {

	public static String PREFIX = "org/prelle/javafx/";
	public static PropertyResourceBundle RES = (PropertyResourceBundle)PropertyResourceBundle.getBundle("org/prelle/javafx/i18n/javafx-ext");

}
