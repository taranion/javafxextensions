/**
 * 
 */
package org.prelle.javafx;

import java.util.ResourceBundle;

import javafx.scene.Node;
import javafx.scene.control.Button;

/**
 * @author prelle
 *
 */
public class StockButton extends Button {

	private final static ResourceBundle JFX_EXT = JavaFXConstants.RES;

	private static final String DEFAULT_STYLE_CLASS = "stock-button";

	private CloseType type;
	
	//-------------------------------------------------------------------
	private static String getButtonText(CloseType type) {
		if (type==CloseType.APPLY   ) return JFX_EXT.getString("button.apply");
		if (type==CloseType.CANCEL  ) return JFX_EXT.getString("button.cancel");
		if (type==CloseType.CLOSE   ) return JFX_EXT.getString("button.close");
		if (type==CloseType.FINISH  ) return JFX_EXT.getString("button.finish");
		if (type==CloseType.PREVIOUS) return JFX_EXT.getString("button.previous");
		if (type==CloseType.NEXT    ) return JFX_EXT.getString("button.next");
		if (type==CloseType.OK      ) return JFX_EXT.getString("button.ok");
		return type.getText();
	}
	
	//-------------------------------------------------------------------
	/**
	 */
	public StockButton(CloseType type) {
		super(getButtonText(type));
		this.type = type;
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
	}

	//-------------------------------------------------------------------
	/**
	 * @param text
	 */
	public StockButton(String text, CloseType type) {
		super(text);
		this.type = type;
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
	}

	//-------------------------------------------------------------------
	/**
	 * @param text
	 * @param graphic
	 */
	public StockButton(String text, Node graphic, CloseType type) {
		super(text, graphic);
		this.type = type;
		getStyleClass().addAll(DEFAULT_STYLE_CLASS);
	}

	public CloseType getType() {
		return type;
	}

	public void setType(CloseType type) {
		this.type = type;
	}

}
