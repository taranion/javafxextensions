/**
 * 
 */
package org.prelle.javafx.fluent;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 * @author prelle
 *
 */
public class CustomMenu extends Menu {

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    /**
     * Creates a default CustomMenuItem instance.
     */
    public CustomMenu() {
        this(null, true);
    }

    /**
     * Constructs a CustomMenuItem and initializes its content with the node specified.
     * @param node to be embedded inside this CustomMenuItem
     */
    public CustomMenu(Node node) {
        this(node, true);
    }

    /**
     * Constructs a CustomMenuItem and sets the content to the node specified.
     * @param node to be embedded inside this CustomMenuItem
     * @param hideOnClick if false the menu will not hide when the user interacts with the node.
     */
    public CustomMenu(Node node, boolean hideOnClick) {
        getStyleClass().add(DEFAULT_STYLE_CLASS);

        setContent(node);
        setHideOnClick(hideOnClick);
    }

	//-------------------------------------------------------------------
	public CustomMenu(Node node, MenuItem... items) {
		super(null, null, items);
        setContent(node);
	}



    /***************************************************************************
     *                                                                         *
     * Properties                                                              *
     *                                                                         *
     **************************************************************************/

    /**
     * The node to display within this CustomMenuItem.
     */
    private ObjectProperty<Node> content;

    public final void setContent(Node value) {
        contentProperty().set(value);
    }

    public final Node getContent() {
        return content == null ? null : content.get();
    }

    public final ObjectProperty<Node> contentProperty() {
        if (content == null) {
            content = new SimpleObjectProperty<Node>(this, "content");
        }
        return content;
    }


    /**
     * If true, this menu item, and all visible menus, will be hidden when this
     * menu item is clicked on.
     *
     * @defaultValue true
     */
    private BooleanProperty hideOnClick;

    public final void setHideOnClick(boolean value) {
        hideOnClickProperty().set(value);
    }

    public final boolean isHideOnClick() {
        return hideOnClick == null ? true : hideOnClick.get();
    }

    public final BooleanProperty hideOnClickProperty() {
        if (hideOnClick == null) {
            hideOnClick = new SimpleBooleanProperty(this, "hideOnClick", true);
        }
        return hideOnClick;
    }



    /***************************************************************************
     *                                                                         *
     * Stylesheet Handling                                                     *
     *                                                                         *
     **************************************************************************/

    private static final String DEFAULT_STYLE_CLASS = "custom-menu-item";

}
