/**
 * 
 */
package org.prelle.javafx.fluent;

import javafx.collections.ObservableList;
import javafx.scene.control.MenuItem;

/**
 * @author prelle
 *
 */
public interface CommandBarActionProvider {

	public ObservableList<MenuItem> getCommandBarActions();
	
}
