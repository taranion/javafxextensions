/**
 * 
 */
package org.prelle.javafx.fluent;

/**
 * @author prelle
 *
 */
public interface CloseableContent {

	public void close();
	
}
