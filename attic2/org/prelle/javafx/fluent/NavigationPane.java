/**
 * 
 */
package org.prelle.javafx.fluent;

import org.prelle.javafx.fluent.skin.NavigationPaneSkin;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;

/**
 * @author spr
 *
 */
public class NavigationPane extends Control {

	public enum FoldingMode {
		FOLDED,
		OPEN
	}

	public static class SpacingMenuItem extends MenuItem {
	}
	public static class HeaderMenuItem extends MenuItem {
		public HeaderMenuItem(String header) {
			super(header, null);
		}
	}

	static final String DEFAULT_STYLE_CLASS = "navigation-pane";


	// --- Items
	private ObjectProperty<ObservableList<MenuItem>> itemsProperty = new SimpleObjectProperty<ObservableList<MenuItem>>(FXCollections.observableArrayList());
	// --- Display Mode
	private ObjectProperty<FoldingMode> displayModeProperty = new SimpleObjectProperty<NavigationPane.FoldingMode>(FoldingMode.FOLDED);
	// --- Is 'Back' button enabled
	private BooleanProperty backEnabledProperty = new SimpleBooleanProperty(false);
	// --- Is 'Settings' button enabled
	private BooleanProperty settingsEnabledProperty = new SimpleBooleanProperty(false);
	// --- Selection Model
	private SelectionModel<MenuItem> selectionModel = new SingleSelectionModel<MenuItem>() {
		@Override protected MenuItem getModelItem(int index) { if (index==-1) return null; else return getItems().get(index);}
		@Override protected int getItemCount() { return getItems().size(); }
	};

    // --- Pseudo class: OPEN
    private static final PseudoClass PSEUDO_CLASS_OPEN = PseudoClass.getPseudoClass("open");
//    public BooleanProperty openProperty = new BooleanPropertyBase(false) {
//    	        @Override protected void invalidated() { pseudoClassStateChanged(PSEUDO_CLASS_OPEN, displayModeProperty.get()==FoldingMode.OPEN); }
//    	        @Override public Object getBean() { return NavigationPane.this; }
//    	        @Override public String getName() { return "open"; }
//    	   };
//    public boolean isOpen() { return displayModeProperty.get()==FoldingMode.OPEN; }
//    public BooleanProperty openProperty() { return openProperty; }

    // --- Pseudo class: CLOSED
    private static final PseudoClass PSEUDO_CLASS_CLOSED = PseudoClass.getPseudoClass("closed");
//    public BooleanProperty closedProperty = new BooleanPropertyBase(false) {
//    	        @Override protected void invalidated() { pseudoClassStateChanged(PSEUDO_CLASS_CLOSED, displayModeProperty.get()==FoldingMode.FOLDED); }
//    	        @Override public Object getBean() { return NavigationPane.this; }
//    	        @Override public String getName() { return "open"; }
//    	   };
//    public boolean isClosed() { return displayModeProperty.get()==FoldingMode.FOLDED; }
//    public BooleanProperty closedProperty() { return closedProperty; }

    private ObjectProperty<EventHandler<ActionEvent>> onBackAction = new SimpleObjectProperty<>();
    private EventHandler<ActionEvent> onMenuAction;
    private ObjectProperty<EventHandler<ActionEvent>> onSettingsAction = new SimpleObjectProperty<>();
	
	//-----------------------------------------------------------------
	public NavigationPane() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new NavigationPaneSkin(this));
		 pseudoClassStateChanged(PSEUDO_CLASS_CLOSED, displayModeProperty.get()==FoldingMode.FOLDED);
		 pseudoClassStateChanged(PSEUDO_CLASS_OPEN, displayModeProperty.get()==FoldingMode.OPEN);
		
		displayModeProperty.addListener( (ov,o,n) -> {
			 pseudoClassStateChanged(PSEUDO_CLASS_CLOSED, n==FoldingMode.FOLDED);
			 pseudoClassStateChanged(PSEUDO_CLASS_OPEN, n==FoldingMode.OPEN);
		});
	}

	/******************************************
	 * Property ITEMS
	 ******************************************/
	public final ObservableList<MenuItem> getItems() { return itemsProperty.get(); }
	public final ObjectProperty<ObservableList<MenuItem>> itemsProperty() { return itemsProperty; }

	/******************************************
	 * Property DISPLAY_MODE
	 ******************************************/
	public final void setDisplayMode(FoldingMode value) { displayModeProperty.set(value); }
	public final FoldingMode getDisplayMode() { return displayModeProperty.get(); }
	public final ObjectProperty<FoldingMode> displayModeProperty() { return displayModeProperty; }

	/******************************************
	 * Property SELECTION MODEL
	 ******************************************/
	public final SelectionModel<MenuItem> getSelectionModel() { return selectionModel; }

	/******************************************
	 * Property BACK_ENABLED
	 ******************************************/
	public final void setBackEnabled(boolean value) { backEnabledProperty.set(value); }
	public final boolean isBackEnabled() { return backEnabledProperty.get(); }
	public final BooleanProperty backEnabledProperty() { return backEnabledProperty; }

	/******************************************
	 * Property SETTINGS_ENABLED
	 ******************************************/
	public final void setSettingsEnabled(boolean value) { settingsEnabledProperty.set(value); }
	public final boolean isSettingsEnabled() { return settingsEnabledProperty.get(); }
	public final BooleanProperty settingsEnabledProperty() { return settingsEnabledProperty; }
    
	/******************************************
	 * Event Back clicked
	 ******************************************/
    public void setOnBackAction(EventHandler<ActionEvent> handler) { this.onBackAction.set(handler); }
    public EventHandler<ActionEvent> getOnBackAction() { return this.onBackAction.get(); }
	public final ObjectProperty<EventHandler<ActionEvent>> onBackProperty() { return onBackAction; }
    
	/******************************************
	 * Event Hamburger Menu clicked
	 ******************************************/
    public void setOnMenuAction(EventHandler<ActionEvent> handler) { this.onMenuAction = handler; }
    public EventHandler<ActionEvent> getOnMenuAction() { return this.onMenuAction; }
    
	/******************************************
	 * Event Settings clicked
	 ******************************************/
    public void setOnSettingsAction(EventHandler<ActionEvent> handler) { this.onSettingsAction.set(handler); }
    public EventHandler<ActionEvent> getOnSettingsAction() { return this.onSettingsAction.get(); }
	public final ObjectProperty<EventHandler<ActionEvent>> onSettingsProperty() { return onSettingsAction; }

}
