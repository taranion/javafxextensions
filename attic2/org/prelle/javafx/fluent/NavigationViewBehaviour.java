/**
 * 
 */
package org.prelle.javafx.fluent;

import java.util.Arrays;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;

/**
 * @author prelle
 *
 */
public class NavigationViewBehaviour {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private NavigationView control;
	private NavigationPane navPane;
	// --- Navigation page history
	private Stack<NavigableContentNode> pages = new Stack<NavigableContentNode>();

	//-------------------------------------------------------------------
	public NavigationViewBehaviour(NavigationView control) {
		this.control = control;
		this.navPane = control.getNavPane();
		navPane.setOnBackAction(ev -> handleBack());
	}

	//--------------------------------------------------------------------
	public NavigableContentNode getShowing() {
		return pages.peek();
	}

	//--------------------------------------------------------------------
	public void open(NavigableContentNode page) {
		if (pages.contains(page)) {
			logger.error("Showing a page already on the stack not implemented yet");
			return;
		}
		
		
		String[] stylesheets = page.getStyleSheets();
		
		// Add stylesheets
		if (stylesheets!=null && stylesheets.length>0 && control.getScene()!=null) {
			control.getScene().getStylesheets().addAll(stylesheets);
			logger.info("Add stylesheets to scene: "+Arrays.toString(stylesheets)+"   result="+control.getScene().getStylesheets());
		}

		// Update elements
		navPane.getItems().clear();
		if (page.getStaticButtons()!=null) {
			navPane.getItems().addAll(page.getStaticButtons());
		} 
		control.headerProperty().set(page.getTitle());
		control.setContent(page.getContent());
		
		navPane.setBackEnabled(pages.size()>0);
		// Show Settings only on first page
		navPane.setSettingsEnabled(pages.size()==0);
		
		// Memorize page 
		logger.debug("Add page "+page+" and make it visible ");
		pages.push(page);
	}

	//--------------------------------------------------------------------
	public void replaceContent(NodeWithTitle page) {
		if (control.getContent() instanceof CloseableContent) {
			((CloseableContent)control.getContent()).close();
		}
		
		control.headerProperty().set(page.getTitle());
		control.setContent(page.getContent());
	}

	//--------------------------------------------------------------------
	public void replace(NavigableContentNode page) {
		// Keep first page - remove all other pages
		while (pages.size()>0) {
			NavigableContentNode toReplace = pages.pop();
			if (toReplace instanceof CloseableContent) {
				((CloseableContent)toReplace).close();
			}
			close(toReplace, CloseType.CLOSE);
			logger.debug("Removed "+toReplace);
		}
		
		open(page);
	}

	//-------------------------------------------------------------------
	public void close(NavigableContentNode page, CloseType type) {
		logger.info("Close "+page+" with "+type);
		logger.debug("Stack is "+pages);
		
		// Do nothing if page is unknown
		if (!pages.contains(page)) {
			logger.error("Closing a page not on the stack");
			return;
		}
		// Don't close first page
		if (pages.size()<2) {
			logger.error("Don't close first page");
			return;
		}
		
		// Determine type of closing
		type = pages.peek().backSelected();
		if (type==null) {
			logger.warn("Page "+pages.peek().getClass()+" does forbid closing");
			return;
		}
		logger.debug("Page "+pages.peek().getClass()+" indicates closing to be of type "+type);
	
		// Remove stylesheets
		if (page.getStyleSheets()!=null && page.getStyleSheets().length>0) {
			String[] stylesheets = page.getStyleSheets();
			control.getScene().getStylesheets().removeAll(stylesheets);
			logger.info("Remove stylesheets from scene: "+Arrays.toString(stylesheets));
		}
		
		// If page wants to be told that it is closed, do it
		if (pages.peek() instanceof CloseableContent) {
			((CloseableContent)pages.peek()).close();
		}
		
		// Remove page
		NavigableContentNode closed = pages.pop();
		logger.info("Closed "+closed.getClass());
		
		// Previous page is new content
		NavigableContentNode prevPage = pages.peek();
		control.setContent(prevPage.getContent());
		navPane.setSettingsEnabled(pages.size()==1);
		navPane.setBackEnabled(pages.size()>1);


		// Update elements
		navPane.getItems().clear();
		if (prevPage.getStaticButtons()!=null) {
			navPane.getItems().addAll(prevPage.getStaticButtons());
		}
		control.headerProperty().set(prevPage.getTitle());

		
		if (pages.peek() instanceof ManagedScreen) {
			if (page instanceof ManagedScreen) {
				((ManagedScreen)pages.peek()).childClosed((ManagedScreen) page, type);
			}
//		} else if (pages.peek() instanceof CloseableContent) {
//			((CloseableContent)pages.peek()).close();
		} else {
			logger.warn("TODO: implement close() for "+pages.peek().getClass());
			logger.warn("Pages = "+pages);
//			pages.peek().childClosed(page, type);
		}
	}

	//-------------------------------------------------------------------
	private void handleBack() {
		logger.debug("Back initiated");
		
		NavigableContentNode page = pages.peek();
		close(page, CloseType.BACK);
	}

}
