/**
 * 
 */
package org.prelle.javafx.fluent;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.scene.Node;

/**
 * @author prelle
 *
 */
public interface NodeWithTitle {

	public Node getContent();
	public ReadOnlyObjectProperty<Node> contentProperty();

	public String getTitle();
	public ReadOnlyStringProperty titleProperty();
	
}
