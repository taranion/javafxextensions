/**
 * 
 */
package org.prelle.javafx.fluent;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.fluent.skin.NavigationViewSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Control;

/**
 * @author spr
 *
 */
public class NavigationView extends Control implements ResponsiveControl  {

	private static final String DEFAULT_STYLE_CLASS = "navigation-view";


	// --- Navigation menu
	private ObjectProperty<NavigationPane> navPaneProperty = new SimpleObjectProperty<NavigationPane>(new NavigationPane());
	// --- Content
	private ObjectProperty<Node> contentProperty = new SimpleObjectProperty<Node>();
	// --- Header
	private ObjectProperty<String> headerProperty = new SimpleObjectProperty<String>();
	// --- Display Mode
	private ObjectProperty<WindowMode> WindowModeProperty = new SimpleObjectProperty<WindowMode>(WindowMode.MINIMAL);

	//-----------------------------------------------------------------
	/**
	 */
	public NavigationView() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new NavigationViewSkin(this));
	}

	/******************************************
	 * Property ITEMS
	 ******************************************/
	public final NavigationPane getNavPane() { return navPaneProperty.get(); }
	public final ObjectProperty<NavigationPane> navPaneProperty() { return navPaneProperty; }

	/******************************************
	 * Property CONTENT
	 ******************************************/
	public final void setContent(Node value) { contentProperty.set(value); }
	public final Node getContent() { return contentProperty.get(); }
	public final ObjectProperty<Node> contentProperty() { return contentProperty; }

	/******************************************
	 * Property HEADER
	 ******************************************/
	public final void setHeader(String value) { headerProperty.set(value); }
	public final String getHeader() { return headerProperty.get(); }
	public final ObjectProperty<String> headerProperty() { return headerProperty; }

	/******************************************
	 * Property DISPLAY_MODE
	 ******************************************/
	public final void setWindowMode(WindowMode value) { WindowModeProperty.set(value); }
	public final WindowMode getWindowMode() { return WindowModeProperty.get(); }
	public final ObjectProperty<WindowMode> windowModeProperty() { return WindowModeProperty; }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		switch (value) {
		case MINIMAL: setWindowMode(WindowMode.MINIMAL); break;
		case COMPACT: setWindowMode(WindowMode.COMPACT); break;
		case EXPANDED: setWindowMode(WindowMode.EXPANDED); break;
		}
	}

}
