/**
 * 
 */
package org.prelle.javafx.fluent;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.AccessibleRole;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public class CommandBarSkin2 extends SkinBase<CommandBar> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private HBox layout;
	private RenderedMenu moreMenu;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public CommandBarSkin2(CommandBar control) {
		super(control);
		initComponents();
		refreshLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		layout = new HBox();
		layout.setStyle("-fx-spacing: 2em");
//		layout.setAlignment(Pos.TOP_LEFT);
		getChildren().add(layout);

		moreMenu = new RenderedMenu("\uE712", null);
		//		moreMenuBar = new MenuBar(moreMenu);
	}

	//-------------------------------------------------------------------
	private void refreshLayout() {
		layout.getChildren().clear();
		for (MenuItem item : getSkinnable().getItems()) {
			logger.debug("Add item "+item);
			//			if (item instanceof SpacingMenuItem)
			//				continue;
			RenderedMenuItem label = new RenderedMenuItem(item.getText(), item.getGraphic());
			label.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
			label.setAlignment(Pos.CENTER);
			layout.getChildren().add(label);
			//			layout..showingProperty().addListener( (ov,o,n)-> {
			//				logger.debug("showing "+n);
			//			});
		}

		layout.getChildren().add(moreMenu);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().widthProperty().addListener( (ov,o,n) -> {
			if (((Double)n)<(Double)o) calculateMatchingItems((double) n);
			if (((Double)n)>(Double)o) calculateMatchingItems((double) n);
		});
		getSkinnable().getItems().addListener( (ListChangeListener<MenuItem>) c -> {
			logger.debug("List changed "+c);
		});
		getSkinnable().itemsProperty().addListener( (ov,o,n) -> refreshLayout());
		//		menuBar.skinProperty().addListener( (ov,o,n) -> {
		//			logger.debug("Skin for menubar = "+n);
		//			logger.debug("  skin node is "+n.getNode()+"  / "+n.getSkinnable());
		//			Skin<MenuBar> skin = (Skin<MenuBar>) n;
		//			try {
		//				Method method = skin.getClass().getMethod("getChildren");
		//				ObservableList<Node> val = (ObservableList<Node>) method.invoke(skin);
		//				logger.debug("  val = "+val);
		//			} catch (Exception e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//			
		//			com.sun.javafx.scene.control.skin.MenuBarSkin skin2 = (com.sun.javafx.scene.control.skin.MenuBarSkin)n;
		//			logger.debug("  children = "+skin2.getChildren());
		//			logger.debug("  children = "+skin2.getChildren().getClass());
		//			layout = (HBox)skin2.getChildren().get(0);
		//		});
	}

	//-------------------------------------------------------------------
	private void calculateMatchingItems(double n) {
		logger.info("calculateMatchingItems "+n);
		double reservedForMore = moreMenu.getWidth();

		List<Node> allNodes = new ArrayList<>(layout.getChildren());
		allNodes.addAll(moreMenu.getItems());

		double prefWidth = 0;
		List<Node> placeInMenu = new ArrayList<>();
		List<Node> placeInMore = new ArrayList<>();
		boolean stopTrying = false;
		for (Node node : allNodes) {
			if (node==moreMenu)
				continue;
			double widthPerNode = ((Region)node).getWidth();
			boolean doesNotMatch = ((prefWidth + widthPerNode) > (double)n);
			logger.debug("* ("+prefWidth+" + "+widthPerNode+") = "+ (prefWidth + widthPerNode)+" > "+n+" = "+doesNotMatch+"   \t"+node);
			if (doesNotMatch || stopTrying) {
				placeInMore.add(node);
				stopTrying = true;
			} else {
				placeInMenu.add(node);
				prefWidth += widthPerNode;
				prefWidth += layout.getSpacing();
			}
		}

		if ( stopTrying) {
			// Not all items fitted - "More" needed	
			if ((prefWidth+reservedForMore) > n && !placeInMenu.isEmpty()) {
				// "More" does not fit itself - (re)move the last item
				Node last = placeInMenu.remove(placeInMenu.size()-1);
				placeInMore.add(0, last);				
			}
			// Add "More" itself
			placeInMenu.add(moreMenu);
		}

		/*
		 * Update layout
		 */
		layout.getChildren().clear();
		layout.getChildren().addAll(placeInMenu);

		moreMenu.getItems().clear();
		moreMenu.getItems().addAll(placeInMore);
	}

//	//-------------------------------------------------------------------
//	private void widthDecreased(Number n) {
//		logger.info("widthDecreased "+n);
//
//		double prefWidth = 0;
//		List<Node> retain = new ArrayList<>();
//		List<Node> remove = new ArrayList<>();
//		for (Node node : layout.getChildren()) {
//			if (node==moreMenu)
//				continue;
//			double widthPerNode = ((Region)node).getWidth();
//			boolean doesNotMatch = ((prefWidth + widthPerNode) > (double)n);
//			//			logger.debug("* "+prefWidth+" + "+widthPerNode+" > "+n+" = "+doesNotMatch);
//			if (doesNotMatch)
//				remove.add(node);
//			else
//				retain.add(node);
//			prefWidth += widthPerNode;
//			prefWidth += layout.getSpacing();
//		}
//		layout.getChildren().retainAll(retain);
//		itemNodesInDropDown.addAll(0, remove);
//	}
//
//	//-------------------------------------------------------------------
//	private void widthIncreased(Number n) {
//		double prefWidth = 0;
//		// Calculate current required width
//		for (Node node : layout.getChildren()) {
//			double widthPerNode = ((Region)node).getWidth();
//			prefWidth += widthPerNode;
//			prefWidth += layout.getSpacing();
//		}
//		// See if more elements fit
//		List<Node> move = new ArrayList<>();
//		for (Node node : itemNodesInDropDown) {
//			prefWidth += layout.getSpacing();
//			double widthPerNode = ((Region)node).getWidth();
//			boolean doesMatch = ((prefWidth + widthPerNode) < (double)n);
//			//			logger.debug("* "+prefWidth+" + "+widthPerNode+" > "+n+" = "+doesMatch);
//			if (doesMatch) {
//				move.add(node);
//			} else
//				break;
//		}
//		layout.getChildren().addAll(move);
//		itemNodesInDropDown.removeAll(move);
//	}

}

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class RenderedMenuItem extends Label {

	private static final String DEFAULT_STYLE_CLASS = "menu-button";

	//-------------------------------------------------------------------
	public RenderedMenuItem(String text, Node graphic) {
		super(text, graphic);
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setAccessibleRole(AccessibleRole.MENU_BUTTON);
	}

}


//-------------------------------------------------------------------
//-------------------------------------------------------------------
class RenderedMenu extends Label {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private static final String DEFAULT_STYLE_CLASS = "menu-button";

	private ObservableList<Node> itemNodesInDropDown;

	//-------------------------------------------------------------------
	public RenderedMenu(String text, Node graphic) {
		super(text, graphic);
		itemNodesInDropDown = FXCollections.observableArrayList();
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setAccessibleRole(AccessibleRole.MENU_BUTTON);
		initInteractivity();
	}

	//-------------------------------------------------------------------
	public ObservableList<Node> getItems() {
		return itemNodesInDropDown;
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnMouseClicked(ev -> clicked());
	}

	//-------------------------------------------------------------------
	private void clicked() {
		logger.debug("Clicked");
	}

}
