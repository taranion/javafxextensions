/**
 *
 */
package org.prelle.javafx.fluent.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.StockButton;
import org.prelle.javafx.fluent.NavigationView;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class NavigationViewClassicSkin extends SkinBase<NavigationView> implements Skin<NavigationView> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private NavigationView control;
	private GridPane layout;

	private Label heading;
	private Node  content;
	private VBox  navig;
	private HBox  action;
	private Region spacing;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public NavigationViewClassicSkin(NavigationView control) {
		super(control);
		this.control = control;
		logger.debug("<init> with title "+control.getHeader());
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		action  = new HBox();
		navig   = new VBox();
		layout  = new GridPane();
		content = control.getContent();
		if (content instanceof Region)
			((Region)content).setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		spacing = new Region();

		updateHeading(control.getHeader());
		updateContent(control.getContent());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Spacing consumes empty space in the action area
		spacing.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(spacing, Priority.ALWAYS);
		action.getChildren().add(spacing);

		/*
		 * Navigation buttons
		 */
		navig.setSpacing(20);
		navig.setAlignment(Pos.TOP_CENTER);
		if (control.getNavPane().isBackEnabled()) {
			logger.debug("Found navigation button "+CloseType.BACK);
			addNavigButton(CloseType.BACK);
		}

		Region spacing2 = new Region();
		spacing2.setMaxHeight(Double.MAX_VALUE);
		HBox.setHgrow(spacing2, Priority.ALWAYS);
		navig.getChildren().add(spacing2);
//		if (control.getSide()!=null) {
//			Region spacing3 = new Region();
//			spacing3.setMaxHeight(Double.MAX_VALUE);
//			VBox.setVgrow(spacing3, Priority.ALWAYS);
//			navig.getChildren().add(spacing3);
//			currentSide = control.getSide();
//			navig.getChildren().add(control.getSide());
//		}
		layout.add(navig, 0, 1, 1,1);
		GridPane.setFillHeight(navig, true);


		ColumnConstraints column1 = new ColumnConstraints();
		ColumnConstraints column2 = new ColumnConstraints();
		ColumnConstraints column3 = new ColumnConstraints();
	    column2.setHgrow(Priority.ALWAYS);
	    layout.getColumnConstraints().addAll(column1, column2, column3);

	    RowConstraints row1 = new RowConstraints();
	    RowConstraints row2 = new RowConstraints();
	    RowConstraints row3 = new RowConstraints();
	    row2.setVgrow(Priority.ALWAYS);
	    row2.setFillHeight(true);
	    row2.setMaxHeight(Double.MAX_VALUE);
	    row2.setValignment(VPos.TOP);
	    layout.getRowConstraints().addAll(row1, row2, row3);

	    /*
		 * Tell control to take all the possible space
		 */
		control.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		control.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);

		getChildren().add(layout);
		logger.debug("Added to layout: "+layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		layout.getStyleClass().add("screen");
		layout.setStyle("-fx-align: top-left");
		if (action!=null)
			action.getStyleClass().add("action-area");
		if (heading!=null)
			heading.getStyleClass().add("heading-area");
		if (navig!=null)
			navig.getStyleClass().add("navigation-area");
		if (content!=null)
			content.getStyleClass().add("content-area");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		control.headerProperty().addListener((ov,o,n) -> updateHeading(n));
		control.contentProperty().addListener((ov,o,n) -> updateContent(n));
	}

	//-------------------------------------------------------------------
	void updateHeading(String value) {
		logger.debug("updateHeading("+value+")");

		if (value==null) {
			if (heading!=null) {
				// Remove existing heading
				layout.getChildren().remove(heading);
				heading = null;
			}
			return;
		} else {
			if (heading==null) {
				heading = new Label(value);
				heading.setMaxWidth(Double.MAX_VALUE);
				heading.getStyleClass().addAll("heading-area","text-header");
				layout.add(heading, 1, 0);
			} else {
				heading.setText(value);
			}
		}
	}

	//-------------------------------------------------------------------
	void updateContent(Node value) {
		logger.debug("Set content to "+content);
		if (content!=null) {
			// Remove existing heading
			layout.getChildren().remove(content);
			content = null;
		}

		if (value!=null) {
			content = value;
			content.getStyleClass().add("content-area");
			layout.add(content, 1, 1);
		}
	}

	//-------------------------------------------------------------------
	void addNavigButton(CloseType type) {
		FontIcon icon = new FontIcon();
		if (type==CloseType.PREVIOUS)    icon.addFontSymbol("\uE17E\uE100");
		else if (type==CloseType.NEXT)   icon.addFontSymbol("\uE17E\uE101");
		else if (type==CloseType.CANCEL) icon.addFontSymbol("\uE17E\uE10A");
		else if (type==CloseType.APPLY ) icon.addFontSymbol("\uE17E\uE10B");
		else if (type==CloseType.BACK)   icon.addFontSymbol("\uE17E\uE112");
		else if (type==CloseType.OK )    icon.addFontSymbol("\uE17E\uE10B");
		else if (type==CloseType.QUIT )  icon.addFontSymbol("\uE07D", 40);
		else {
			logger.warn("TODO: Button for "+type);
			return;
		}

		StockButton button = new StockButton(null, icon, type);
		button.setTooltip(new Tooltip(type.getText()));
		button.setGraphicTextGap(0);
		button.setUserData(type);
		navig.getChildren().add(button);

		button.setOnAction(event -> navigClicked(type));
	}

	//-------------------------------------------------------------------
	void removeNavigButton(CloseType type) {
		for (Node node : navig.getChildren()) {
			if (node.getUserData()==type) {
				navig.getChildren().remove(node);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	void addStaticButton(MenuItem value) {
		int index = action.getChildren().indexOf(spacing);

		// If not added yet, do it
		if (!layout.getChildren().contains(action))
			layout.add(action, 0, 2, 2,1);

		// Grow from left to right - add button directly behind spacing
		Button valueB = new Button(null, value.getGraphic());
		valueB.setStyle("-fx-font-size: 300%; -fx-background-color: transparent; -fx-border-width: 0;-fx-padding: 0px 10px 0px 10px;");
		valueB.setTooltip(new Tooltip(value.getText()));
		action.getChildren().add(index+1, valueB);
		HBox.setMargin(valueB, new Insets(20));
	}

	//-------------------------------------------------------------------
	void addContextButton(Button value) {
		int index = action.getChildren().indexOf(spacing);

		// If not added yet, do it
		if (!layout.getChildren().contains(action))
			layout.add(action, 0, 2, 2,1);

		// Grow from left to right - add button directly behind spacing
		action.getChildren().add(index, value);
		HBox.setMargin(value, new Insets(20));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
    private void navigClicked(CloseType type) {
    	logger.info("clicked "+type+" on "+control.getClass());

//    	control.impl_navigClicked(type, new ActionEvent(this, null));

//    	if (control.close(type)) {
//    		// Close
//    		logger.debug("Manager of "+control+" is "+control.getScreenManager());
//    		if (control.getScreenManager()!=null)
//    			 control.getScreenManager().close(control, type);
//    	} else {
//    		// Don't close
//    	}
    }

}
