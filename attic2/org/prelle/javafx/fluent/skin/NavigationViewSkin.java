/**
 *
 */
package org.prelle.javafx.fluent.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.fluent.NavigationPane;
import org.prelle.javafx.fluent.NavigationPane.FoldingMode;
import org.prelle.javafx.fluent.NavigationView;

import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author spr
 *
 */
public class NavigationViewSkin extends SkinBase<NavigationView> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private Region spaceForFoldedNavPane;
	private Label heading;
	private MinimalFoldedNavigationPane minNavPane;
	private VBox contentColumn;
	private NavigationPane navigationBar;
	private HBox layerBelow;
	private HBox layerAbove;
	private StackPane allLayers;

	//-----------------------------------------------------------------
	public NavigationViewSkin(NavigationView control) {
		super(control);
		if (control==null)
			throw new NullPointerException();
		initComponents();
		initLayout();
		initInteractivity();

		navigationBar.setDisplayMode(FoldingMode.FOLDED);
		refresh();
	}

	//-----------------------------------------------------------------
	private void initComponents() {
		navigationBar = getSkinnable().getNavPane();
	}

	//-----------------------------------------------------------------
	/**
	 * +--------+--------------ContentColumn----------------------+
	 * |        |  Title                                          |
	 * | PlaceH +-------------------------------------------------+
	 * |        |  Content                                        |
	 * |        |                                                 |
	 * |        |                                                 |
	 * |        |                                                 |
	 * +--------+-------------------------------------------------+
	 */
	private void initLayout() {
		spaceForFoldedNavPane = new VBox();
		spaceForFoldedNavPane.getStyleClass().add("navigation-pane-placeholder");

		/*
		 * Content Column
		 */
		// Optional for Minimal view
		minNavPane = new MinimalFoldedNavigationPane(navigationBar);
		// Always present content title
		heading = new Label(getSkinnable().getHeader());
		heading.setMaxWidth(Double.MAX_VALUE);
		heading.getStyleClass().addAll("text-header");
		if (getSkinnable().getContent()!=null)
			heading.setText(getSkinnable().getHeader());
		// Box to combine both
		HBox headingLine = new HBox(20,minNavPane, heading);
		headingLine.setMaxWidth(Double.MAX_VALUE);
		headingLine.getStyleClass().addAll("navigation-view-title");
		HBox.setHgrow(heading, Priority.ALWAYS);

		contentColumn = new VBox();
		contentColumn.getChildren().add(headingLine);
		contentColumn.getStyleClass().add("navigation-view-content");
		contentColumn.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		/*
		 * Lower layer
		 */
		layerBelow = new HBox();
		layerBelow.getChildren().addAll(spaceForFoldedNavPane, contentColumn);
		HBox.setHgrow(contentColumn, Priority.ALWAYS);

		/*
		 * Upper layer consisting of menu and empty space
		 */
		// Prepare transparent dummy
		Region transparentDummy = new Region();
		//		transparentDummy.setStyle("-fx-background-color: rgba(255,0,0, 0.1)");
		transparentDummy.setPickOnBounds(false);
		layerAbove = new HBox(navigationBar,transparentDummy);
		HBox.setHgrow(transparentDummy, Priority.ALWAYS);
		layerAbove.setPickOnBounds(false);

		/*
		 * Stackpane with Menu overlay
		 */
		allLayers = new StackPane(layerBelow, layerAbove);
		allLayers.setPickOnBounds(false);
		getChildren().add(allLayers);
	}

	//-----------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().contentProperty().addListener( (ov,o,n) -> {
			logger.info("++++++++++++++++++++++++content changed from "+o+" to "+n+" in "+this);
			if (o!=null)
				contentColumn.getChildren().remove(o);
			// Content changed
			if (n!=null) {
				contentColumn.getChildren().add(n);
				VBox.setVgrow(n, Priority.ALWAYS);
			}
		});

		getSkinnable().headerProperty().addListener( (ov,o,n) -> heading.setText(n));

		getSkinnable().windowModeProperty().addListener( (ov,o,n) -> windowModeChanged(o,n));

		navigationBar.setOnMenuAction(ev -> toggleNavigationOpenClose());
	}

	//-----------------------------------------------------------------
	private void windowModeChanged(WindowMode oldMode, WindowMode newMode) {
		if (oldMode==newMode || newMode==null)
			return;

		logger.debug("WindowMode changed from "+oldMode+" to "+newMode);
//		updateHeaderLine();
		if (oldMode==WindowMode.EXPANDED)
			navigationBar.setDisplayMode(FoldingMode.FOLDED);
		refresh();
	}

	//-----------------------------------------------------------------
	private void toggleNavigationOpenClose() {
		logger.debug("Toggle NavigationWindowMode   in mode "+getSkinnable().getWindowMode());
		FoldingMode oldState = navigationBar.getDisplayMode();
		FoldingMode newState = (oldState==FoldingMode.FOLDED)?FoldingMode.OPEN:FoldingMode.FOLDED;
		navigationBar.getSelectionModel().clearSelection();

		navigationBar.setDisplayMode(newState);
		refresh();
	}

	//-----------------------------------------------------------------
	private void refresh() {
		if (getSkinnable()==null)
			return;

		boolean isOpen = navigationBar.getDisplayMode()==FoldingMode.OPEN;
		switch (getSkinnable().getWindowMode()) {
		case MINIMAL:
			logger.debug("refresh: MINIMAL");
			spaceForFoldedNavPane.setManaged(false);
			// Remove eventually left menu
			if (isOpen) {
				logger.debug("refresh: MINIMAL, open");
				// Ensure hover menu
				if (!layerAbove.getChildren().contains(navigationBar))
					layerAbove.getChildren().add(0,navigationBar);
				navigationBar.setDisplayMode(FoldingMode.OPEN);
				// Ensure menu
				layerAbove.setVisible(true);
			} else {
				logger.debug("refresh: MINIMAL, closed");
				navigationBar.setDisplayMode(FoldingMode.FOLDED);
				minNavPane.setVisible(true);
				minNavPane.setManaged(true);
				// Remove hover menu
//				layerAbove.getChildren().remove(navigationBar);
				layerAbove.setVisible(false);
			}
			break;
		case COMPACT:
			spaceForFoldedNavPane.setVisible(true);
			spaceForFoldedNavPane.setManaged(true);
			spaceForFoldedNavPane.setStyle("-fx-pref-width: 5em; -fx-min-width: 5em;");
			minNavPane.setVisible(false);
			minNavPane.setManaged(false);
			layerAbove.setVisible(true);
			if (isOpen) {
				logger.debug("refresh: COMPACT, open");
//				optionalNavigPane.getStyleClass().add("navigation-pane-placeholder");
//				optionalNavigPane.getChildren().remove(navigationBar);
//				// Ensure hover menu
//				if (!layerAbove.getChildren().contains(navigationBar))
//					layerAbove.getChildren().add(0,navigationBar);
//				navigationBar.setDisplayMode(FoldingMode.OPEN);
//				// Ensure menu
//				layerAbove.setVisible(true);
			} else {
				logger.debug("refresh: COMPACT, closed");
//				optionalNavigPane.getStyleClass().remove("navigation-pane-placeholder");
//				navigationBar.setDisplayMode(FoldingMode.FOLDED);
//				// Ensure no hover menu
//				layerAbove.getChildren().remove(navigationBar);
//				// Ensure left navigation
//				if (!optionalNavigPane.getChildren().contains(navigationBar))
//					optionalNavigPane.getChildren().add(0,navigationBar);
//				// Ensure no hover
//				layerAbove.setVisible(false);
			}
			break;
		case EXPANDED:
			logger.debug("refresh: EXPANDED");
			navigationBar.setDisplayMode(FoldingMode.OPEN);
			spaceForFoldedNavPane.setStyle("-fx-pref-width: 15em; -fx-min-width: 15em;");
			spaceForFoldedNavPane.setVisible(true);
			spaceForFoldedNavPane.setManaged(true);
			minNavPane.setVisible(false);
			minNavPane.setManaged(false);
			
			// Ensure left navigation
			layerAbove.setVisible(true);
			if (!layerAbove.getChildren().contains(navigationBar))
				layerAbove.getChildren().add(0,navigationBar);
//			if (!optionalNavigPane.getChildren().contains(navigationBar))
//				optionalNavigPane.getChildren().add(0,navigationBar);
//			// Ensure no hover
//			layerAbove.setVisible(false);
		}
	}
}