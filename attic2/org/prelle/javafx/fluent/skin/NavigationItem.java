package org.prelle.javafx.fluent.skin;

import org.prelle.javafx.fluent.NavigationPane;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.WritableValue;
import javafx.css.PseudoClass;
import javafx.css.StyleableProperty;
import javafx.scene.AccessibleRole;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SkinBase;

class NavigationItem extends Button {

	private static final String DEFAULT_STYLE_CLASS = "navigation-item";

    // --- Pseudo class: SELECTED
    private static final PseudoClass PSEUDO_CLASS_SELECTED = PseudoClass.getPseudoClass("selected");
    public BooleanProperty selectedProperty = new BooleanPropertyBase(false) {
    	        @Override protected void invalidated() { pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, selectedProperty.get()); }
    	        @Override public Object getBean() { return NavigationItem.this; }
    	        @Override public String getName() { return "selected"; }
    	   };
    public boolean isSelected() { return selectedProperty.get(); }
    public BooleanProperty selectedProperty() { return selectedProperty; }

    // --- item
    private ObjectProperty<MenuItem> item = new SimpleObjectProperty<MenuItem>(this, "item");
    public final ObjectProperty<MenuItem> itemProperty() { return item; }
    public final void setItem(MenuItem value) { item.set(value); }
    public final MenuItem getItem() { return item.get(); }

    protected void updateItem(MenuItem item) {
        setItem(item);
    }
    
    //-----------------------------------------------------------------
    public NavigationItem(SkinBase<NavigationPane> skin, MenuItem item, boolean withText) {
    	itemProperty().set(item);
        setText(withText?item.getText():null);
        setGraphic(item.getGraphic());
        // focusTraversable is styleable through css. Calling setFocusTraversable
        // makes it look to css like the user set the value and css will not
        // override. Initializing focusTraversable by calling set on the
        // CssMetaData ensures that css will be able to override the value.
        ((StyleableProperty<Boolean>)(WritableValue<Boolean>)focusTraversableProperty()).applyStyle(null, Boolean.FALSE);
        getStyleClass().clear();
        getStyleClass().addAll(DEFAULT_STYLE_CLASS);
        setAccessibleRole(AccessibleRole.MENU_ITEM);
        
        skin.getSkinnable().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
        	selectedProperty.set( n==getUserData());
        });
   }

}