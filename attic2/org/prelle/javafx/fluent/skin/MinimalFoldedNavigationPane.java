/**
 * 
 */
package org.prelle.javafx.fluent.skin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.fluent.NavigationPane;

import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class MinimalFoldedNavigationPane extends HBox {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private NavigationPane navPane;
	private MenuItem menHamburger;
	private MenuItem menBack;
	private NavigationItem btnBack;
	private NavigationItem btnHamburger;
	
	//-------------------------------------------------------------------
	public MinimalFoldedNavigationPane(NavigationPane navPane) {
		this.navPane = navPane;
		initComponents();
		initInteractivity();
		refresh();
	}

	//-----------------------------------------------------------------
	private void initComponents() {
		menBack      = new MenuItem(null, new Label(NavigationPaneSkin.BACK));
		menHamburger = new MenuItem(null, new Label(NavigationPaneSkin.TRIGRAM));
		NavigationPaneSkin skin = (NavigationPaneSkin) navPane.getSkin();
		btnBack      = skin.createNavigationItem(menBack, false);
		btnHamburger = skin.createNavigationItem( menHamburger, false);
	}

	//-----------------------------------------------------------------
	private void initInteractivity() {
		navPane.backEnabledProperty().addListener( (ov,o,n) -> refresh());
		navPane.settingsEnabledProperty().addListener( (ov,o,n) -> refresh());
		btnHamburger.setOnAction( ev -> {
			if (navPane.getOnMenuAction()!=null) { navPane.getOnMenuAction().handle(ev); }
		});
		btnBack.setOnAction( ev -> {
			logger.debug("BACK clicked - handler is "+navPane.getOnBackAction()+" in "+navPane.getClass());
			if (navPane.getOnBackAction()!=null) { navPane.getOnBackAction().handle(ev); }
		});
	}

	//-----------------------------------------------------------------
	private void refresh() {
		getChildren().clear();
		// Back button?
		if (navPane.isBackEnabled())
			getChildren().add(btnBack);
		// Hamburger
		getChildren().add(btnHamburger);
	}

}
