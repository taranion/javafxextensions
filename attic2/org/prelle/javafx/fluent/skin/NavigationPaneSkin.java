/**
 *
 */
package org.prelle.javafx.fluent.skin;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.fluent.NavigationPane;
import org.prelle.javafx.fluent.NavigationPane.FoldingMode;
import org.prelle.javafx.fluent.NavigationPane.SpacingMenuItem;

import javafx.collections.ListChangeListener;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author spr
 *
 */
public class NavigationPaneSkin extends SkinBase<NavigationPane> {

	private PropertyResourceBundle RES = JavaFXConstants.RES;

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private final static String GEAR = "\uE713"; //"\u2699";
	final static String TRIGRAM = "\u2630";
	final static String BACK = "\uE72B"; //"\uE0A9";

	private VBox layout;

	private MenuItem menHamburger;
	private MenuItem menBack;
	private MenuItem menSettings;
	private NavigationItem btnBack;
	private NavigationItem btnHamburger;
	private NavigationItem btnSettings;
	private Map<MenuItem, Node> map;

	//-----------------------------------------------------------------
	public NavigationPaneSkin(NavigationPane control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();

		refresh();
	}

	//-----------------------------------------------------------------
	private void initComponents() {
		map = new HashMap<>();
		menBack      = new MenuItem(null, new Label(BACK));
		menHamburger = new MenuItem(null, new Label(TRIGRAM));
		menSettings  = new MenuItem(RES.getString("menuitem.settings"), new Label(GEAR));
		btnBack      = createNavigationItem(menBack, false);
		btnHamburger = createNavigationItem( menHamburger, false);
		btnSettings  = createNavigationItem(menSettings, getSkinnable().getDisplayMode()==FoldingMode.OPEN);
//		if (menBack.getGraphic()!=null) {
//			if (!menBack.getGraphic().getStyleClass().contains("navigation-icon"))
//				menBack.getGraphic().getStyleClass().add("navigation-icon");
//		}
	}

	//-----------------------------------------------------------------
	private void initLayout() {
		layout = new VBox();
//		layout.getStyleClass().add(NavigationPane.DEFAULT_STYLE_CLASS);
		getChildren().add(layout);
	}

	//-----------------------------------------------------------------
	private void initInteractivity() {
		NavigationPane parent = getSkinnable();
		parent.displayModeProperty().addListener( (ov,o,n) -> displayModeChanged(o,n));
		parent.itemsProperty().addListener( (ov,o,n) -> {
			refresh();
		});
		parent.getItems().addListener((ListChangeListener<MenuItem>)(c) -> {
//			Change<? extends MenuItem> ch = c;
			logger.debug("Change "+c);
			refresh();
		});
		parent.backEnabledProperty().addListener( (ov,o,n) -> refresh());
		parent.settingsEnabledProperty().addListener( (ov,o,n) -> refresh());

		btnHamburger.setOnAction( ev -> {
			if (getSkinnable().getOnMenuAction()!=null) { getSkinnable().getOnMenuAction().handle(ev); }
		});
		btnBack.setOnAction( ev -> {
			logger.debug("BACK clicked - handler is "+getSkinnable().getOnBackAction()+" in "+getSkinnable().getClass());
			if (getSkinnable().getOnBackAction()!=null) { getSkinnable().getOnBackAction().handle(ev); }
		});
		btnSettings.setOnAction( ev -> {
			logger.debug("SETTINGS clicked - handler in NavigationPane is "+getSkinnable().getOnSettingsAction());
			if (getSkinnable().getOnSettingsAction()!=null) { getSkinnable().getOnSettingsAction().handle(ev); }
		});
	}

	//-----------------------------------------------------------------
	NavigationItem createNavigationItem(MenuItem tmp, boolean isOpen) {
		NavigationItem toAdd = new NavigationItem(this, tmp, isOpen);
		if (toAdd.getGraphic()==null) {
			Label dummy = new Label(BACK);
			dummy.setVisible(false);
			toAdd.setGraphic(dummy);
			dummy.getStyleClass().add("navigation-icon");
			toAdd.getStyleClass().add("navigation-heading");
		} else {
			if (!tmp.getGraphic().getStyleClass().contains("navigation-icon"))
				tmp.getGraphic().getStyleClass().add("navigation-icon");
		}
		toAdd.setUserData(tmp);
		toAdd.getStyleClass().add("navigation-item");
		((Labeled)toAdd).setMaxWidth(Double.MAX_VALUE);
		((Labeled)toAdd).setAlignment(Pos.CENTER_LEFT);
		((Button)toAdd).setOnAction(ev -> {
			logger.debug("Clicked "+tmp);
			getSkinnable().getSelectionModel().select(tmp);
			tmp.fire();
			});
		if (!isOpen && tmp.getText()!=null)
			toAdd.setTooltip(new Tooltip(tmp.getText()));
		return toAdd;
	}

	//-----------------------------------------------------------------
	private void refresh() {
//		logger.debug("refresh");
		boolean withText = getSkinnable().getDisplayMode()==FoldingMode.OPEN;
		layout.getChildren().clear();
		// Back button?
		if (getSkinnable().isBackEnabled())
			layout.getChildren().add(btnBack);
		// Hamburger
		layout.getChildren().add(btnHamburger);
		// Normal items
		for (MenuItem tmp : getSkinnable().getItems()) {
			if (!tmp.isVisible())
				continue;
			Node toAdd = map.get(tmp);
			if (toAdd==null) {
				if (tmp instanceof SpacingMenuItem) {
					toAdd = new Region();
					VBox.setVgrow(toAdd, Priority.ALWAYS);
				} else {
//					toAdd = new Button(withText?tmp.getText():null, tmp.getGraphic());
					toAdd = createNavigationItem(tmp, withText);
					boolean hasToolTip = (getSkinnable().getDisplayMode()==FoldingMode.FOLDED) && tmp.getText()!=null;
					((Labeled)toAdd).setTooltip(hasToolTip?(new Tooltip(tmp.getText())):null);
				}
				toAdd.disableProperty().bind(tmp.disableProperty());
				toAdd.visibleProperty().bind(tmp.visibleProperty());
				map.put(tmp, toAdd);
			}
			layout.getChildren().add(toAdd);
		}
		// Settings button?
		if (getSkinnable().isSettingsEnabled()) {
//			logger.debug("Add settings");
			Region toAdd = new Region();
			VBox.setVgrow(toAdd, Priority.ALWAYS);
			layout.getChildren().add(toAdd);
			layout.getChildren().add(btnSettings);
		}
	}

	//-----------------------------------------------------------------
	private void displayModeChanged(FoldingMode oldMode, FoldingMode newMode) {
		if (oldMode==newMode || newMode==null)
			return;

		for (Node tmp : layout.getChildren()) {
			if (tmp instanceof Labeled) {
				Labeled item = (Labeled)tmp;
				MenuItem data = (MenuItem)tmp.getUserData();
				item.setText( (newMode==FoldingMode.OPEN)?data.getText():null);
				boolean hasToolTip = (getSkinnable().getDisplayMode()==FoldingMode.FOLDED) && data.getText()!=null;
				item.setTooltip(hasToolTip?(new Tooltip(data.getText())):null);
			}
		}
	}

}
