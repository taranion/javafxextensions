/**
 *
 */
package org.prelle.javafx.fluent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.fluent.NavigationPane.SpacingMenuItem;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.collections.ListChangeListener;
import javafx.css.PseudoClass;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SkinBase;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * @author prelle
 *
 */
public class CommandBarSkin extends SkinBase<CommandBar> {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private Map<MenuItem, ButtonBase> mapping;
	private HBox layout;
	private MenuButton moreMenu;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public CommandBarSkin(CommandBar control) {
		super(control);
		mapping = new HashMap<>();
		initComponents();
		refreshLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		layout = new HBox();
		layout.setStyle("-fx-spacing: 2em");
		layout.setAlignment(Pos.CENTER_RIGHT);
		getChildren().add(layout);

		moreMenu = new MenuButton("\uE712");
		moreMenu.getStyleClass().add("more-button");
		logger.debug("...."+moreMenu.getStyleClass());
	}

	//-------------------------------------------------------------------
	private Node getOrCreateNode(MenuItem item) {
		Node icon = item.getGraphic();
		if (icon!=null && !icon.getStyleClass().contains("icon")) {
			icon.getStyleClass().addAll("icon","command-icon");
		}

		if (item instanceof Menu) {
			MenuButton ret = (MenuButton) mapping.get(item);
			if (ret==null) {
				ret = new MenuButton(item.getText(), item.getGraphic());
				ret.getStyleClass().add("command-item");
				ret.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
				ret.setUserData(item);
				mapping.put(item, ret);
//				ret.setOnAction(item.getOnAction());
				ret.setOnAction((ev) -> item.getOnAction());
				item.onActionProperty().addListener( (ov,o,n) -> mapping.get(item).setOnAction(n));
				ret.disableProperty().bind(item.disableProperty());
			}
			logger.debug("*******************created Menu "+ret);
			return ret;
		} else {
			ButtonBase ret = (ButtonBase) mapping.get(item);
			if (ret==null) {
				if (item instanceof CheckMenuItem) {
					ret = new RadioButton(item.getText());
					((RadioButton)ret).selectedProperty().addListener( (ov,o,n) -> {logger.warn("CLICK"); ((CheckMenuItem)item).selectedProperty().set(n);});
				} else {
					ret = new Button(item.getText(), item.getGraphic());
				}
				final ButtonBase foo = ret;
				if (!(foo instanceof RadioButton))
					foo.setOnMouseEntered(ev -> foo.pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), true));
				ret.getStyleClass().add("command-item");
				ret.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
				ret.setUserData(item);
				mapping.put(item, ret);
				ret.setOnAction((ev) -> item.getOnAction());
				item.onActionProperty().addListener( (ov,o,n) ->  mapping.get(item).setOnAction(n));
				ret.disableProperty().bind(item.disableProperty());
			}
			logger.debug("*******************created Button "+ret);
			return ret;
		}
	}

	//-------------------------------------------------------------------
	private void refreshLayout() {
		logger.debug("refreshLayout");
//		try {
//			throw new RuntimeException("Trace");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		layout.getChildren().clear();
//		menuBar.getMenus().clear();
		for (MenuItem item : getSkinnable().getItems()) {
			logger.debug("Add item "+item);
			if (item instanceof SpacingMenuItem)
				continue;
			layout.getChildren().add(getOrCreateNode(item));
//			if (item instanceof SpacingMenuItem) {
//				Menu sep = new Menu("|");
//				sep.setDisable(true);
//				menuBar.getMenus().add(sep);
//				continue;
//			}
//			Menu menu = new Menu(item.getText(), item.getGraphic());
//			menu.parentPopupProperty().addListener( (ov,o,n)-> {
//				logger.debug("popup");
//			});
//			menu.showingProperty().addListener( (ov,o,n)-> {
//				logger.debug("showing "+n);
//			});
//			menuBar.getMenus().add(menu);
		}

		layout.getChildren().add(moreMenu);
//		menuBar.getMenus().add(moreMenu);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().widthProperty().addListener( (ov,o,n) -> {
			logger.trace("Width of "+ov+" changed from "+o+" to "+n);
			if (((Double)n)<(Double)o) calculateMatchingItems((double) n);
			if (((Double)n)>(Double)o) calculateMatchingItems((double) n);
			});
		getSkinnable().getItems().addListener( (ListChangeListener<MenuItem>) c -> {
			logger.debug("List changed "+c);
			refreshLayout();
		});
		getSkinnable().itemsProperty().addListener( (ov,o,n) -> refreshLayout());
//		menuBar.skinProperty().addListener( (ov,o,n) -> {
//			logger.debug("Skin for menubar = "+n);
//			logger.debug("  skin node is "+n.getNode()+"  / "+n.getSkinnable());
//			com.sun.javafx.scene.control.skin.MenuBarSkin skin = (com.sun.javafx.scene.control.skin.MenuBarSkin)n;
//			logger.debug("  children = "+skin.getChildren());
//			layout = (HBox)skin.getChildren().get(0);
//		});
	}

	//-------------------------------------------------------------------
	private void calculateMatchingItems(double n) {
		logger.trace("calculateMatchingItems "+n);
		
		double reservedForMore = moreMenu.getWidth();

		List<ButtonBase> allNodes = new ArrayList<>();
		for (MenuItem item : getSkinnable().getItems()) {
			Node node = mapping.get(item);
			if (node!=null)
				allNodes.add( mapping.get(item) );
			else
				logger.warn("No mapping for node "+item);
		}

		double prefWidth = 0;
		List<ButtonBase> placeInMenu = new ArrayList<>();
		List<ButtonBase> placeInMore = new ArrayList<>();
		boolean stopTrying = false;
		for (ButtonBase node : allNodes) {
			if (node==moreMenu)
				continue;
			double widthPerNode = (node!=null) ? ((Region)node).getWidth() : 20;
			boolean doesNotMatch = ((prefWidth + widthPerNode) > (double)n);
			logger.trace("* ("+prefWidth+" + "+widthPerNode+") = "+ (prefWidth + widthPerNode)+" > "+n+" = "+doesNotMatch+"   \t"+node);
			if (doesNotMatch || stopTrying) {
				placeInMore.add(node);
				stopTrying = true;
			} else {
				placeInMenu.add(node);
				prefWidth += widthPerNode;
				prefWidth += layout.getSpacing();
			}
		}

		if ( stopTrying) {
			// Not all items fitted - "More" needed
			if ((prefWidth+reservedForMore) > n && !placeInMenu.isEmpty()) {
				// "More" does not fit itself - (re)move the last item
				ButtonBase last = placeInMenu.remove(placeInMenu.size()-1);
				placeInMore.add(0, last);
			}
			// Add "More" itself
			placeInMenu.add(moreMenu);
		}

		/*
		 * Update layout
		 */
		boolean changed = !layout.getChildren().equals(placeInMenu);
		if (changed) {
			layout.getChildren().clear();
			logger.trace("placeInMenu = "+placeInMenu);
			layout.getChildren().addAll(placeInMenu);
		}

		// More menu
		List<MenuItem> toSet = new ArrayList<>();
		for (ButtonBase key : placeInMore)
			toSet.add( (MenuItem) ((ButtonBase)key).getUserData() );
		changed = !moreMenu.getItems().equals(toSet);
		if (changed) {
			moreMenu.getItems().clear();
			for (ButtonBase key : placeInMore)
				moreMenu.getItems().addAll( (MenuItem) ((ButtonBase)key).getUserData() );
			logger.trace("placeInMore = "+toSet);
		}
	}

}

class MaterialDesignMenu extends Menu {

    private Pane stackPane = new Pane();
    private Label label = new Label();

    private Circle circleRipple;
    private Rectangle rippleClip = new Rectangle();
    private Duration rippleDuration =  Duration.millis(250);
    private double lastRippleHeight = 0;
    private double lastRippleWidth = 0;
    private Color rippleColor = new Color(1, 0, 0, 0.3);

    public MaterialDesignMenu() {
        init("");
    }

    public MaterialDesignMenu(String text) {
        init(text);
    }

    public MaterialDesignMenu(String text, Node graphic) {
        init(text);
    }

    private void init(String text){
        label.setText(text);
        createRippleEffect();

        stackPane.getChildren().addAll(circleRipple, label);
        setGraphic(stackPane);
    }

    private void createRippleEffect() {
        circleRipple = new Circle(0.1, rippleColor);
        circleRipple.setOpacity(0.0);
        // Optional box blur on ripple - smoother ripple effect
        //circleRipple.setEffect(new BoxBlur(3, 3, 2));
        // Fade effect bit longer to show edges on the end of animation
        final FadeTransition fadeTransition = new FadeTransition(rippleDuration, circleRipple);
        fadeTransition.setInterpolator(Interpolator.EASE_OUT);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        final Timeline scaleRippleTimeline = new Timeline();
        final SequentialTransition parallelTransition = new SequentialTransition();
        parallelTransition.getChildren().addAll(
                scaleRippleTimeline,
                fadeTransition
        );
        // When ripple transition is finished then reset circleRipple to starting point
        parallelTransition.setOnFinished(event -> {
            circleRipple.setOpacity(0.0);
            circleRipple.setRadius(0.1);
        });

        stackPane.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            parallelTransition.stop();
            // Manually fire finish event
            parallelTransition.getOnFinished().handle(null);
            circleRipple.setCenterX(event.getX());
            circleRipple.setCenterY(event.getY());

            // Recalculate ripple size if size of button from last time was changed
            if (stackPane.getWidth() != lastRippleWidth || stackPane.getHeight() != lastRippleHeight) {
                lastRippleWidth = stackPane.getWidth();
                lastRippleHeight = stackPane.getHeight();
                rippleClip.setWidth(lastRippleWidth);
                rippleClip.setHeight(lastRippleHeight);
                /*
                // try block because of possible null of Background, fills ...
                try {
                    rippleClip.setArcHeight(stackPane.getBackground().getFills().get(0).getRadii().getTopLeftHorizontalRadius());
                    rippleClip.setArcWidth(stackPane.getBackground().getFills().get(0).getRadii().getTopLeftHorizontalRadius());
                    circleRipple.setClip(rippleClip);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                circleRipple.setClip(rippleClip);
                // Getting 45% of longest button's length, because we want edge of ripple effect always visible
                double circleRippleRadius = Math.max(stackPane.getHeight(), stackPane.getWidth()) * 0.45;
                final KeyValue keyValue = new KeyValue(circleRipple.radiusProperty(), circleRippleRadius, Interpolator.EASE_OUT);
                final KeyFrame keyFrame = new KeyFrame(rippleDuration, keyValue);
                scaleRippleTimeline.getKeyFrames().clear();
                scaleRippleTimeline.getKeyFrames().add(keyFrame);
            }
            parallelTransition.playFromStart();
        });
    }

    public void setRippleColor(Color color) {
        circleRipple.setFill(color);
    }
}