/**
 * 
 */
package org.prelle.javafx.fluent;

import org.prelle.javafx.CloseType;

import javafx.collections.ObservableList;
import javafx.scene.control.MenuItem;

/**
 * @author prelle
 *
 */
public interface NavigableContentNode extends NodeWithTitle {

	//-------------------------------------------------------------------
	public ObservableList<MenuItem> getStaticButtons();

	//-------------------------------------------------------------------
	public String[] getStyleSheets();

	//-------------------------------------------------------------------
	/**
	 * The user clicked on the BACK button. Let the page determine if this
	 * means it is a CANCEL or APPLY action
	 * 
	 * @return Type of closing or NULL, if closing is not allowed
	 */
	public CloseType backSelected();

}
