/**
 * 
 */
package org.prelle.javafx.fluent;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;

/**
 * 
 * @author prelle
 * @see https://docs.microsoft.com/en-us/windows/uwp/design/controls-and-patterns/app-bars
 */
public class CommandBar extends Control implements ResponsiveControl {

	private static final String DEFAULT_STYLE_CLASS = "command-bar";
	
	private ObjectProperty<ObservableList<MenuItem>> itemsProperty = new SimpleObjectProperty<ObservableList<MenuItem>>(FXCollections.observableArrayList());
	private ObjectProperty<WindowMode> modeProperty = new SimpleObjectProperty<WindowMode>(WindowMode.COMPACT);

	//-------------------------------------------------------------------
	public CommandBar() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		setSkin(new CommandBarSkin(this));
//		behaviour = new NavigationViewBehaviour(this);
	}

	/******************************************
	 * Property ITEMS
	 ******************************************/
	public final ObservableList<MenuItem> getItems() { return itemsProperty.get(); }
	public final ObjectProperty<ObservableList<MenuItem>> itemsProperty() { return itemsProperty; }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		modeProperty.set(value);
	}

}
