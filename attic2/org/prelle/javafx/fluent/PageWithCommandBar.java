/**
 * 
 */
package org.prelle.javafx.fluent;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Control;

/**
 * @author prelle
 *
 */
public class PageWithCommandBar extends Control implements NodeWithTitle {
	
	private StringProperty titleProperty = new SimpleStringProperty();
	private ObjectProperty<Node> contentProperty = new SimpleObjectProperty<Node>();

	//-------------------------------------------------------------------
	/**
	 */
	public PageWithCommandBar() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.fluent.NodeWithTitle#getTitle()
	 */
	@Override
	public String getTitle() { return titleProperty.get(); }
	@Override
	public ReadOnlyStringProperty titleProperty() { return titleProperty(); }
	protected void setTitle(String value) { titleProperty.setValue(value); }

	//-------------------------------------------------------------------
	@Override
	public Node getContent() { return contentProperty.get(); }
	@Override
	public ReadOnlyObjectProperty<Node> contentProperty() { return contentProperty; }

}
