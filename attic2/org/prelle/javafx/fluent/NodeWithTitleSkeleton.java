/**
 * 
 */
package org.prelle.javafx.fluent;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;

/**
 * @author prelle
 *
 */
public abstract class NodeWithTitleSkeleton implements NodeWithTitle {
	
	protected StringProperty titleProperty = new SimpleStringProperty();
	protected SimpleObjectProperty<Node> contentProperty = new SimpleObjectProperty<Node>();

	//-------------------------------------------------------------------
	public Node getContent() { return contentProperty.get(); }
	public void setContent(Node value) { contentProperty.setValue(value); }
	public ReadOnlyObjectProperty<Node> contentProperty() { return contentProperty(); }

	//-------------------------------------------------------------------
	public String getTitle() { return titleProperty.get(); }
	public void setTitle(String value) { titleProperty.setValue(value); }
	public ReadOnlyStringProperty titleProperty() { return titleProperty(); }
	
}
