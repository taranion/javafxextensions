/**
 *
 */
package org.prelle.javafx;

/**
 * @author Stefan
 *
 */
public class SlideInBorderPaneBehaviour {

//	private SlideInBorderPane model;

	//--------------------------------------------------------------------
	public SlideInBorderPaneBehaviour(SlideInBorderPane model) {

		model.setOnActionLeft(ev -> {
			model.visibleLeftProperty().set((SlideInPanel)ev.getSource());
		});
		model.setOnActionRight(ev -> {
			model.visibleRightProperty().set((SlideInPanel)ev.getSource());
		});
		model.setOnActionTop(ev -> {
			model.visibleTopProperty().set((SlideInPanel)ev.getSource());
		});
		model.setOnActionBottom(ev -> {
			model.visibleBottomProperty().set((SlideInPanel)ev.getSource());
		});
	}

}
