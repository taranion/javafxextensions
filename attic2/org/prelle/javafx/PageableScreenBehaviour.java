/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.ManagedScreenDialogSkin;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ListChangeListener;
import javafx.scene.layout.StackPane;

/**
 * @author Stefan
 *
 */
public class PageableScreenBehaviour extends ManagedScreenBehaviour {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	private final static int UNDEFINED = -1;

	private PageableScreen wizard;

	private StackPane contentStack;
	
	private int curPageIdx = UNDEFINED;
	
	//--------------------------------------------------------------------
	public PageableScreenBehaviour(PageableScreen control) {
		super(control);
		wizard = control;
		
		/*
		 * Content
		 * Make a stackpane, add content nodes of all pages to
		 * this stackpane and ensure that only one is visible
		 */
		contentStack = new StackPane();
		wizard.setTitle("Undefined");
		wizard.setContent(contentStack);
		for (Page page : wizard.getPages()) {
			contentStack.getChildren().add(page.getContent());
			page.getContent().setVisible(false);
		}
		
		wizard.getPages().addListener(new InvalidationListener() {
			public void invalidated(Observable observable) {
				logger.warn("TODO: invalidated");
			}
		});
		wizard.getPages().addListener(new ListChangeListener<Page>(){
			public void onChanged(ListChangeListener.Change<? extends Page> c) {
				while (c.next()) {
					if (c.wasAdded())  {
						for (Page page : c.getAddedSubList()) {
							page.setWizard(wizard);
							contentStack.getChildren().add(page.getContent());
							page.getContent().setVisible(false);
							initInteractivity(page);
							if (curPageIdx==UNDEFINED)
								navTo(0, null);
						}
					}
				}
			}});
	}
	
	//--------------------------------------------------------------------
	private void updateButton(Page page, CloseType button, boolean enabled) {
		if (page!=getCurrentPage()) {
			logger.debug("Ignore button update "+button+" to "+enabled); 
			return;
		}
		
		logger.debug("Update button "+button+" to "+enabled); 
		if (wizard.getSkin() instanceof ManagedScreenDialogSkin) {
			((ManagedScreenDialogSkin)wizard.getSkin()).setDisabled(button, !enabled);
		}
		
	}
	
	//--------------------------------------------------------------------
	private void initInteractivity(Page page) {
		page.titleProperty().addListener((ov,o,n)->{
			logger.warn("TODO: only set for active page");
			wizard.setTitle(n);
			});
		page.contentProperty()    .addListener((ov,o,n) -> {wizard.setContent(n);});

		// React on navig buttons
		wizard.setOnAction(CloseType.PREVIOUS, event -> priorPage());
		wizard.setOnAction(CloseType.CANCEL, event -> cancel());
		wizard.setOnAction(CloseType.FINISH, event -> finish());
		wizard.setOnAction(CloseType.NEXT, event -> nextPage());
		
		// Listen to button changes
		page.previousButton.addListener( (ov,o,n) -> updateButton(page, CloseType.PREVIOUS, n && hasPriorPage()));
		page.nextButton.addListener( (ov,o,n) -> updateButton(page, CloseType.NEXT, n && hasNextPage()));
		
		// Update next button from previous page
		if (curPageIdx!=UNDEFINED && wizard.getPages().get(curPageIdx)!=page && wizard.getPages().indexOf(page)==(curPageIdx+1)) {
			updateButton(wizard.getPages().get(curPageIdx), CloseType.NEXT, hasNextPage());
		}
	}
	
	//--------------------------------------------------------------------
	private void setDisabled(Page page, CloseType button, boolean disabled) {
		if (disabled) {
			logger.debug("Button "+button+" in page '"+page.getClass().getSimpleName()+"' is DISABLED");
		} else {
			logger.debug("Button "+button+" in page '"+page.getClass().getSimpleName()+"' is ENABLED");
		}
		
		if (curPageIdx==UNDEFINED)
			return;
		
		Page current = wizard.getPages().get(curPageIdx);
		if (page==current) {
			if (wizard.getSkin() instanceof ManagedScreenDialogSkin) {
				((ManagedScreenDialogSkin)wizard.getSkin()).setDisabled(button, disabled);
			}
		}
	}

	//-------------------------------------------------------------------
	private Page getCurrentPage() {
		if (curPageIdx==UNDEFINED || curPageIdx>=wizard.getPages().size())
			return null;
		return wizard.getPages().get(curPageIdx);
	}

	//-------------------------------------------------------------------
	void nextPage() {
		int newIndex = curPageIdx+1;
		if (newIndex>=wizard.getPages().size())
			newIndex = 0;

		navTo(newIndex, CloseType.NEXT);
	}

	//-------------------------------------------------------------------
	void priorPage() {
		int newIndex = curPageIdx-1;
		if (newIndex<0)
			newIndex = wizard.getPages().size()-1;

		navTo(newIndex, CloseType.PREVIOUS);
	}

	//-------------------------------------------------------------------
	boolean hasNextPage() {
		int newIndex = curPageIdx+1;
		if (newIndex>=wizard.getPages().size())
			newIndex = 0;
		return newIndex!=curPageIdx;
	}

	//-------------------------------------------------------------------
	boolean hasPriorPage() {
		int newIndex = curPageIdx-1;
		if (newIndex>=wizard.getPages().size())
			newIndex = 0;
		return newIndex!=curPageIdx;
	}

	//-------------------------------------------------------------------
	void navTo(int nextPageIdx, CloseType type) {
		logger.debug("navTo("+nextPageIdx+") of "+wizard.getPages().size()+" pages");
		if (nextPageIdx < 0 || nextPageIdx >= wizard.getPages().size()) {
			logger.error("Invalid page index "+nextPageIdx);
			return;
		}
		Page oldPage = null;
		if (curPageIdx != UNDEFINED) {
			oldPage = getCurrentPage();
			oldPage.getContent().setVisible(false);
			oldPage.pageLeft(type);
		}

		Page nextPage = wizard.getPages().get(nextPageIdx);
		logger.debug("make visible "+nextPage);
		nextPage.getContent().setVisible(true);
		wizard.setTitle(nextPage.getTitle());
		
		nextPage.pageVisited();
//		nextPage.setVisible(true);
		curPageIdx = nextPageIdx;
		
		manageButtons();
////		nextPage.requestLayout();
	}

//	//-------------------------------------------------------------------
//	void navTo(String id) {
//		Node page = lookup("#" + id);
//		if (page != null) {
//			int nextPageIdx = pages.indexOf(page);
//			if (nextPageIdx != UNDEFINED) {
//				navTo(nextPageIdx);
//			}
//		}
//	}

	//-------------------------------------------------------------------
	private void manageButtons() {
		Page current = wizard.getPages().get(curPageIdx);
		
		Boolean next = current.isNextButtonEnabled();
		Boolean prev = current.isPreviousButtonEnabled();
		logger.debug(String.format("manageButtons for %s = (prev=%s, next=%s", current.getClass().getSimpleName(), prev, next));

		
		setDisabled(current, CloseType.PREVIOUS, ! (hasPriorPage() && current.isPreviousButtonEnabled()) );
		setDisabled(current, CloseType.NEXT, ! (hasNextPage() && current.isNextButtonEnabled()) );
		
		/*
		 * Action buttons
		 */
		wizard.getStaticButtons().clear();
		logger.error("TODO: add staticButtons to Wizard");
//		wizard.getStaticButtons().addAll(current.getStaticButtons());
		wizard.getContextButtons().clear();
		wizard.getContextButtons().addAll(current.getContextButtons());
	}
	
	//-------------------------------------------------------------------
	public void finish() {
		getCurrentPage().pageLeft(CloseType.FINISH);
		wizard.getScreenManager().close(wizard, CloseType.FINISH);
	}

	//-------------------------------------------------------------------
	public void cancel() {
		getCurrentPage().pageLeft(CloseType.CANCEL);
		wizard.getScreenManager().close(wizard, CloseType.CANCEL);
	}
	
	//-------------------------------------------------------------------
	protected void close() {
		logger.warn("TODO: close");
	}

	//-------------------------------------------------------------------
	public void navigClicked(CloseType type) {
		logger.warn("TODO: navigation button "+type+" clicked");
		switch (type) {
		case PREVIOUS:
			priorPage();
			break;
		case NEXT:
			nextPage();
			break;
		case FINISH:
			finish();
			break;
		case CANCEL:
			cancel();
			break;
		default:
		}
	}

}
