/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.PageSkin;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author prelle
 *
 */
public class PageableScreen extends ManagedScreen {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private PageableScreenBehaviour behaviour;
	
	protected ObservableList<Page> pages;

	//-----------------------------------------------------------------
	/**
	 */
	public PageableScreen() {
		pages = FXCollections.observableArrayList();
		
		logger.debug("Add navigation buttons");
		getNavigButtons().addAll(
				CloseType.PREVIOUS,
				CloseType.NEXT
				);

		behaviour = new PageableScreenBehaviour(this);
		setSkin(new PageSkin(this, behaviour));
	}

	//--------------------------------------------------------------------
	public PageableScreen(Page...pages) {
		this();
		this.pages.addAll(pages);
		
		behaviour.navTo(0, null);
	}
	

	//--------------------------------------------------------------------
	public ObservableList<Page> getPages() {
		return pages;
	}
	
	//-----------------------------------------------------------------
	public void show(Page page) {
		int index = pages.indexOf(page);
		behaviour.navTo(index, CloseType.NEXT);
	}

	//--------------------------------------------------------------------
	public PageableScreenBehaviour impl_getBehaviour() {
		return behaviour;
	}
	
	//--------------------------------------------------------------------
	public boolean canBeFinished() {
		logger.warn("TODO: Wizard method canBeFinished() not overloaded yet");
		return true;
	}

}
