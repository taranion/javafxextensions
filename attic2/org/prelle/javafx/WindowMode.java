/**
 * 
 */
package org.prelle.javafx;

/**
 * @author prelle
 *
 */
public enum WindowMode {
	
	MINIMAL,
	COMPACT,
	EXPANDED

}
