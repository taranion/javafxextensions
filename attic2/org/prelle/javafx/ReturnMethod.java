/**
 * 
 */
package org.prelle.javafx;

/**
 * @author prelle
 *
 */
public interface ReturnMethod<T> {

	public void returned(T returnValue);
	
}
