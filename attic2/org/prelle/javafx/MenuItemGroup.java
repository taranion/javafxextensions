/**
 * 
 */
package org.prelle.javafx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.MenuItem;

/**
 * Grouped list of menu items that shall be displayed on same level as other items.
 * <pre>
 * - MenuItem 1
 * - MenuItem 2
 * 
 * - MenuGroup 1
 * - MenuItem 3
 * - MenuItem 4
 * 
 * - MenuItem 5
 * 
 * - MenuGroup 2
 * - MenuItem 6
 * </pre>
 * @author prelle
 *
 */
public class MenuItemGroup extends MenuItem {
	
	private ObservableList<MenuItem> items;

	//-------------------------------------------------------------------
	/**
	 */
	public MenuItemGroup() {
		items = FXCollections.observableArrayList();
	}

	//-------------------------------------------------------------------
	/**
	 * @param text
	 */
	public MenuItemGroup(String text) {
		super(text);
		items = FXCollections.observableArrayList();
	}

	//-------------------------------------------------------------------
	/**
	 * @param text
	 * @param graphic
	 */
	public MenuItemGroup(String text, Node graphic) {
		super(text, graphic);
		items = FXCollections.observableArrayList();
	}

	//-------------------------------------------------------------------
	public ObservableList<MenuItem> getItems() {
		return items;
	}

}
