/**
 * 
 */
package org.prelle.javafx;

/**
 * @author spr
 *
 */
public enum AlertType {

	CONFIRMATION,
	NOTIFICATION,
	QUESTION,
	ERROR,
	
}
