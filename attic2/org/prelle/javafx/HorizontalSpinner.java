/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.HorizontalSpinnerSkin;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class HorizontalSpinner<T> extends Control {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	
	private final static String DEFAULT_STYLE_CLASS = "spinner";

	// --- Items
	private ObjectProperty<ObservableList<T>> itemsProperty = new SimpleObjectProperty<ObservableList<T>>(FXCollections.observableArrayList());
	// --- Current selected value
	private ObjectProperty<T> valueProperty = new SimpleObjectProperty<T>();
    // --- Cell Factory
    private ObjectProperty<Callback<T,String>> converterProperty 
    	= new SimpleObjectProperty<Callback<T,String>>(new Callback<T, String>() {
			@Override
			public String call(T value) {
				return String.valueOf(value);
			}
		});
    // --- Cyclic
	private BooleanProperty cyclic = new SimpleBooleanProperty(true);

	//-------------------------------------------------------------------
	public HorizontalSpinner() {
		setSkin(new HorizontalSpinnerSkin<>(this));
		getStyleClass().add(DEFAULT_STYLE_CLASS);
	}

	/******************************************
	 * Property ITEMS
	 ******************************************/
  
    //-----------------------------------------------------------------
    public final void setItems(ObservableList<T> value) {
    	logger.debug("setItems("+value+")");
        itemsProperty.set(value);
        if (!value.isEmpty())
        	valueProperty.set(value.get(0));
    }

    //-----------------------------------------------------------------
    public final ObservableList<T> getItems() {
        return itemsProperty.get();
    }

    //-----------------------------------------------------------------
    public final ObjectProperty<ObservableList<T>> itemsProperty() {
        return itemsProperty;
    }

	/******************************************
	 * Property VALUE
	 ******************************************/
  
    //-----------------------------------------------------------------
    public final void setValue(T value) {
        valueProperty.set(value);
    }

    //-----------------------------------------------------------------
    public final T getValue() {
        return valueProperty.get();
    }

    //-----------------------------------------------------------------
    public final ObjectProperty<T> valueProperty() {
        return valueProperty;
    }
    
	/******************************************
	 * Property CONVERTER
	 ******************************************/

    //-----------------------------------------------------------------
    public final void setConverter(Callback<T, String> value) {
    	converterProperty().set(value);
    }

    //-----------------------------------------------------------------
    public final Callback<T, String> getConverter() {
        return converterProperty == null ? null : converterProperty.get();
    }

    //-----------------------------------------------------------------
    public final ObjectProperty<Callback<T,String>> converterProperty() {
        if (converterProperty == null) {
        	converterProperty = new SimpleObjectProperty<Callback<T, String>>(this, "converterFactory");
        }
        return converterProperty;
    }

    
	/******************************************
	 * Property CYCLIC
	 ******************************************/
   public void setCyclic(boolean value) { cyclic.set(value); }
   public boolean isCyclic() { return cyclic.get(); }
   public BooleanProperty cyclicProperty() { return cyclic; }
   
}
