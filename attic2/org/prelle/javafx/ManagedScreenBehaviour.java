/**
 * 
 */
package org.prelle.javafx;


/**
 * @author Stefan
 *
 */
public class ManagedScreenBehaviour {

	private ManagedScreen control;
	
	//--------------------------------------------------------------------
	public ManagedScreenBehaviour(ManagedScreen control) {
		this.control = control;
		
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the control
	 */
	public ManagedScreen getControl() {
		return control;
	}

}
