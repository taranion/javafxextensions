/**
 * 
 */
package org.prelle.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.fluent.NavigableContentNode;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;

/**
 * @author prelle
 *
 */
public abstract class ManagedScreenSkeleton extends Control implements NavigableContentNode {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");

	private StringProperty title = new SimpleStringProperty();
	private ObjectProperty<Node> content = new SimpleObjectProperty<Node>();
	private ObjectProperty<ObservableList<MenuItem>> menuItems = new SimpleObjectProperty<>(FXCollections.observableArrayList());
	protected transient ScreenManager manager;

	//-------------------------------------------------------------------
	public ManagedScreenSkeleton() {
		super();
	}

	//-------------------------------------------------------------------
	public StringProperty titleProperty() { return title; }
	public void setTitle(String value) {title.setValue(value); }
	public String getTitle() { return title.get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<Node> contentProperty() { return content; }
	public void setContent(Node value) {content.setValue(value); }
	public Node getContent() { return content.get(); }

	//-------------------------------------------------------------------
	/*
	 * Property STATIC BUTTONS
	 */
	public ObjectProperty<ObservableList<MenuItem>> staticButtonsProperty() { return menuItems; }
//	public void setStaticButtons(ObservableList<Button> value) {staticButtons.setValue(value); }
	public ObservableList<MenuItem> getStaticButtons() { return menuItems.get(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	public ScreenManager getScreenManager() {
		return manager;
	}

	//-------------------------------------------------------------------
	public void setScreenManager(ScreenManager manager) {
		this.manager = manager;
	}

}