/**
 *
 */
package org.prelle.javafx;

import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.skin.ManagedScreenDialogSkin;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ListChangeListener;
import javafx.scene.layout.StackPane;

/**
 * @author Stefan
 *
 */
public class WizardBehaviour extends ManagedScreenBehaviour {

	protected final static Logger logger = LogManager.getLogger("prelle.jfx");
	private final static int UNDEFINED = -1;

	private Wizard wizard;

	private StackPane contentStack;

	private Stack<Integer> history = new Stack<Integer>();
	private int curPageIdx = UNDEFINED;

	//--------------------------------------------------------------------
	public WizardBehaviour(Wizard control) {
		super(control);
		wizard = control;

		/*
		 * Content
		 * Make a stackpane, add content nodes of all pages to
		 * this stackpane and ensure that only one is visible
		 */
		contentStack = new StackPane();
		wizard.setTitle("Undefined");
		wizard.setContent(contentStack);
		for (WizardPage page : wizard.getPages()) {
			contentStack.getChildren().add(page.getContent());
			page.getContent().setVisible(false);
		}

		wizard.getPages().addListener(new InvalidationListener() {
			public void invalidated(Observable observable) {
				logger.debug("TODO: pages invalidated");
			}
		});
		wizard.getPages().addListener(new ListChangeListener<WizardPage>(){
			public void onChanged(ListChangeListener.Change<? extends WizardPage> c) {
				while (c.next()) {
					if (c.wasAdded())  {
						for (WizardPage page : c.getAddedSubList()) {
							page.setWizard(wizard);
							contentStack.getChildren().add(page.getContent());
							page.getContent().setVisible(false);
							initInteractivity(page);
							if (curPageIdx==UNDEFINED)
								navTo(0, true, null);
						}
					}
				}
			}});
	}

	//--------------------------------------------------------------------
	private void updateButton(WizardPage page, CloseType button, boolean enabled) {
		if (page!=getCurrentPage()) {
			logger.debug("Ignore button update "+button+" to "+enabled);
			return;
		}

		logger.debug("Update button "+button+" on "+page.getClass()+" to "+enabled);
		if (wizard.getSkin() instanceof ManagedScreenDialogSkin) {
			((ManagedScreenDialogSkin)wizard.getSkin()).setDisabled(button, !enabled);
		}

	}

	//--------------------------------------------------------------------
	private void initInteractivity(WizardPage page) {
		page.titleProperty().addListener((ov,o,n)->{
			logger.warn("TODO: only set for active page");
			wizard.setTitle(n);
			});
		page.contentProperty()    .addListener((ov,o,n) -> {wizard.setContent(n);});
		page.sideProperty()       .addListener((ov,o,n) -> {wizard.setSide(n);});
		page.imageProperty()      .addListener((ov,o,n) -> {wizard.setImage(n);});
		page.imageInsetsProperty().addListener((ov,o,n) -> {wizard.setImageInsets(n);});

		// React on navig buttons
		wizard.setOnAction(CloseType.PREVIOUS, event -> priorPage());
		wizard.setOnAction(CloseType.CANCEL, event -> cancel());
		wizard.setOnAction(CloseType.FINISH, event -> finish());
		wizard.setOnAction(CloseType.NEXT, event -> nextPage());

		// Listen to button changes
		page.previousButton.addListener( (ov,o,n) -> updateButton(page, CloseType.PREVIOUS, n && hasPriorPage()));
		page.finishButton.addListener( (ov,o,n) -> updateButton(page, CloseType.FINISH, n && wizard.canBeFinished()));
		page.nextButton.addListener( (ov,o,n) -> updateButton(page, CloseType.NEXT, n && hasNextPage()));

		// Update next button from previous page
		if (curPageIdx!=UNDEFINED && wizard.getPages().get(curPageIdx)!=page && wizard.getPages().indexOf(page)==(curPageIdx+1)) {
			updateButton(wizard.getPages().get(curPageIdx), CloseType.NEXT, hasNextPage() && wizard.getPages().get(curPageIdx).isNextButtonEnabled());
		}

		wizard.canBeFinishedProperty.addListener( (ov,o,n) -> updateButton(page, CloseType.FINISH, n));
	}

	//--------------------------------------------------------------------
	private void setDisabled(WizardPage page, CloseType button, boolean disabled) {
		if (disabled) {
			logger.debug("Button "+button+" in page '"+page.getClass().getSimpleName()+"' is DISABLED");
		} else {
			logger.debug("Button "+button+" in page '"+page.getClass().getSimpleName()+"' is ENABLED");
		}

		if (curPageIdx==UNDEFINED)
			return;

		WizardPage current = wizard.getPages().get(curPageIdx);
		if (page==current) {
			if (wizard.getSkin() instanceof ManagedScreenDialogSkin) {
				((ManagedScreenDialogSkin)wizard.getSkin()).setDisabled(button, disabled);
			}
		}
	}

	//-------------------------------------------------------------------
	private WizardPage getCurrentPage() {
		if (curPageIdx==UNDEFINED || curPageIdx>=wizard.getPages().size())
			return null;
		return wizard.getPages().get(curPageIdx);
	}

	//-------------------------------------------------------------------
	void nextPage() {
		if (hasNextPage()) {
			int next = curPageIdx+1;
			while (!wizard.getPages().get(next).isActive()) {
				next++;
			}
			navTo(next, true, CloseType.NEXT);
		}
	}

	//-------------------------------------------------------------------
	void priorPage() {
		if (hasPriorPage()) {
			navTo(history.pop(), false, CloseType.PREVIOUS);
		}
	}

	//-------------------------------------------------------------------
	boolean hasNextPage() {
		int next = curPageIdx;
		while (next<(wizard.getPages().size()-1)) {
			next++;
			if (wizard.getPages().get(next).isActive())
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	boolean hasPriorPage() {
		return !history.isEmpty();
	}

	//-------------------------------------------------------------------
	void navTo(int nextPageIdx, boolean pushHistory, CloseType type) {
		logger.debug("navTo("+nextPageIdx+") of "+wizard.getPages().size()+" pages");
		if (nextPageIdx < 0 || nextPageIdx >= wizard.getPages().size()) {
			logger.error("Invalid page index");
			return;
		}
		WizardPage oldPage = null;
		if (curPageIdx != UNDEFINED) {
			oldPage = getCurrentPage();
			oldPage.getContent().setVisible(false);
			logger.debug("  leave page "+oldPage+" with "+type);
			oldPage.pageLeft(type);
			if (pushHistory) {
				history.push(curPageIdx);
			}
		}

		WizardPage nextPage = wizard.getPages().get(nextPageIdx);
		logger.debug("make visible "+nextPage);
		nextPage.getContent().setVisible(true);
		wizard.setTitle(nextPage.getTitle());
		wizard.setImage(nextPage.getImage());
		wizard.setImageInsets(nextPage.getImageInsets());
		wizard.setSide(nextPage.getSide());

		nextPage.pageVisited();
//		nextPage.setVisible(true);
		curPageIdx = nextPageIdx;

		manageButtons();
////		nextPage.requestLayout();
	}

//	//-------------------------------------------------------------------
//	void navTo(String id) {
//		Node page = lookup("#" + id);
//		if (page != null) {
//			int nextPageIdx = pages.indexOf(page);
//			if (nextPageIdx != UNDEFINED) {
//				navTo(nextPageIdx);
//			}
//		}
//	}

	//-------------------------------------------------------------------
	private void manageButtons() {
		WizardPage current = wizard.getPages().get(curPageIdx);

		Boolean next = current.isNextButtonEnabled();
		Boolean prev = current.isPreviousButtonEnabled();
		Boolean fini = current.isFinishButtonEnabled();
		logger.debug(String.format("manageButtons for %s = (prev=%s, finish=%s, next=%s", current.getClass().getSimpleName(), prev,fini, next));


		setDisabled(current, CloseType.PREVIOUS, ! (hasPriorPage() && current.isPreviousButtonEnabled()) );
		setDisabled(current, CloseType.NEXT, ! (hasNextPage() && current.isNextButtonEnabled()) );
		setDisabled(current, CloseType.FINISH, !(wizard.canBeFinished() && current.isFinishButtonEnabled()));
	}

	//-------------------------------------------------------------------
	public void finish() {
		logger.info("Finish");
		if (!getCurrentPage().isCloseTypePermittedByUser(CloseType.FINISH)) {
			logger.debug("Finishing is not permitted by user");
			return;
		}

		getCurrentPage().pageLeft(CloseType.FINISH);
		wizard.getScreenManager().close(wizard, CloseType.FINISH);
	}

	//-------------------------------------------------------------------
	public void cancel() {
		if (!getCurrentPage().isCloseTypePermittedByUser(CloseType.CANCEL)) {
			logger.debug("Cancelling is not permitted by user");
			return;
		}

		getCurrentPage().pageLeft(CloseType.CANCEL);
		wizard.getScreenManager().close(wizard, CloseType.CANCEL);
	}

	//-------------------------------------------------------------------
	protected void close() {
		logger.warn("TODO: close");
	}

	//-------------------------------------------------------------------
	public void navigClicked(CloseType type) {
		logger.warn("TODO: navigation button "+type+" clicked");

		if (!getCurrentPage().isCloseTypePermittedByUser(type)) {
			logger.debug("Executing "+type+" is not permitted by user");
			return;
		}

		switch (type) {
		case PREVIOUS:
			priorPage();
			break;
		case NEXT:
			nextPage();
			break;
		case FINISH:
			finish();
			break;
		case CANCEL:
			cancel();
			break;
		default:
		}
	}

}
