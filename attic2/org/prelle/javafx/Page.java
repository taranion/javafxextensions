package org.prelle.javafx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;

public class Page {

	protected PageableScreen wizard;
	protected ObjectProperty<String> title;
	protected ObjectProperty<Node> content;
	protected BooleanProperty previousButton;
	protected BooleanProperty nextButton;
	private ObjectProperty<ObservableList<Button>> staticButtons = new SimpleObjectProperty<>(FXCollections.observableArrayList());
	private ObjectProperty<ObservableList<Button>> contextButtons = new SimpleObjectProperty<>(FXCollections.observableArrayList());

	public Page(PageableScreen wizard) {
		this.wizard = wizard;
		content = new SimpleObjectProperty<Node>();
		title   = new SimpleObjectProperty<String>();

		previousButton = new SimpleBooleanProperty(true);
		nextButton     = new SimpleBooleanProperty(true);
	}

	public Node getContent() { return content.get(); }

	public void setContent(Node value) { content.set(value);}

	public ObjectProperty<Node> contentProperty() { return content; }

	public String getTitle() { return title.get(); }

	public void setTitle(String value) { title.set(value);}

	public ObjectProperty<String> titleProperty() { return title; }

	public BooleanProperty getPreviousButtonProperty() {return previousButton;}

	public BooleanProperty getNextButtonProperty() {return nextButton;}

	public boolean isPreviousButtonEnabled() {return previousButton.get();}

	public boolean isNextButtonEnabled() {return nextButton.get();}

	/**
	 * Called from Wizard when page is shown to user
	 */
	public void pageVisited() {
		
	}

	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
	}

	//-------------------------------------------------------------------
	void setWizard(PageableScreen wizard) {
		this.wizard = wizard;
	}

	//-------------------------------------------------------------------
	public PageableScreen getPageable() {
		return wizard;
	}

	//-------------------------------------------------------------------
	/*
	 * Property STATIC BUTTONS
	 */
	public ObjectProperty<ObservableList<Button>> staticButtonsProperty() { return staticButtons; }
	public ObservableList<Button> getStaticButtons() { return staticButtons.get(); }

	//-------------------------------------------------------------------
	/*
	 * Property CONTEXT BUTTONS
	 */
	public ObjectProperty<ObservableList<Button>> contextButtonsProperty() { return contextButtons; }
	public ObservableList<Button> getContextButtons() { return contextButtons.get(); }

}