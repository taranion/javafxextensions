/**
 *
 */
package org.prelle.javafx;

import javafx.scene.Node;

/**
 * @author Stefan
 *
 */
public interface SlideInPanel {

	//--------------------------------------------------------------------
	public Node getContent();

	//--------------------------------------------------------------------
	public Node getIcon();

	//--------------------------------------------------------------------
	public String getTooltip();

}
