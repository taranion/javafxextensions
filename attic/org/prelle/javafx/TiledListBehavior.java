/**
 * 
 */
package org.prelle.javafx;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.prelle.javafx.skin.TiledListViewSkin;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * @author prelle
 *
 */
public class TiledListBehavior<T,S> {

	private final static Logger logger = Logger.getLogger("prelle.jfx");
	
	private Map<S, List<T>> listsPerSection;
	private Map<S, Label> sectionLabels;
	private TiledListViewSkin<T,S> skin;
	private Comparator<T> itemComparator;
	private Comparator<S> sectionComparator;

	private TiledListView<T, S> control;

    //-----------------------------------------------------------------
    @SuppressWarnings("unchecked")
	private final ListChangeListener<T> itemsListListener = c -> {
    	logger.trace("list changed: "+c);
        while (c.next()) {
        	if (c.wasAdded()) {
        		logger.trace("was added");
        		for (T value : c.getAddedSubList()) {
        			logger.trace("-------------------------------------------------------------");
        			TiledListBehavior.this.addInternal(value);
//        			TiledCell<T,S> cell = createCell(value);
//            		getControl().impl_getChildren().add(cell);
        		}
        	}
        	if (c.wasRemoved()) {
         		for (T value : c.getRemoved()) {
        			TiledListBehavior.this.removeInternal(value);
        		}
        	}
        	if (c.wasUpdated()) {
        		for (int index=c.getFrom(); index<=c.getTo(); index++) {
        			T value = getControl().getItems().get(index);
        			TiledCell<T,S> cell = TiledListBehavior.this.getCell(value);
        			logger.trace("Updated "+value+" with cell "+cell);
        			
        			if (cell!=null) {
            			TiledListBehavior.this.cellUpdated(cell, value);
        			}
        		}
        	}
        	if (c.wasReplaced()) {
        		logger.warn("TODO: implement replacing");
        	}
//            if (c.wasAdded() && c.getFrom() <= getAnchor()) {
//                setAnchor(getAnchor() + c.getAddedSize());
//            } else if (c.wasRemoved() && c.getFrom() <= getAnchor()) {
//                setAnchor(getAnchor() - c.getRemovedSize());
//            }
        }
        ((TiledSelectionModel<T,S>)TiledListBehavior.this.getControl().getSelectionModel()).updateItemCount();
    };

    //-----------------------------------------------------------------
    private final ChangeListener<ObservableList<T>> itemsListener = new ChangeListener<ObservableList<T>>() {
        @Override
        public void changed(
                ObservableValue<? extends ObservableList<T>> observable,
                ObservableList<T> oldValue, ObservableList<T> newValue) {
        	logger.debug("#####################changed: "+newValue);
        	if (selectionModel!=null)
        		selectionModel.updateItemCount();
//            if (oldValue != null) {
//                 oldValue.removeListener(weakItemsListListener);
//             } if (newValue != null) {
//                 newValue.addListener(weakItemsListListener);
//             }
        }
    };
    
    // Listen to changes when the whole item list is replaced
    private final WeakChangeListener<ObservableList<T>> weakItemsListener = 
            new WeakChangeListener<ObservableList<T>>(itemsListener);
    // Listen to changes within the items list
    private final WeakListChangeListener<T> weakItemsListListener = 
            new WeakListChangeListener<>(itemsListListener);
    
  
    private TiledSelectionModel<T,S> selectionModel;
    private Map<T, TiledCell<T,S>> cellsByItem;
    
	//-------------------------------------------------------------------
	public TiledListBehavior(TiledListView<T,S> control, TiledListViewSkin<T,S> skin) {
		this.control    = control;
		this.skin       = skin;
		listsPerSection = new HashMap<>();
		sectionLabels   = new HashMap<S, Label>();
		cellsByItem     = new HashMap<T, TiledCell<T,S>>();
		
        control.itemsProperty().addListener(weakItemsListener);
        if (control.getItems() != null) {
            control.getItems().addListener(weakItemsListListener);
        }
        
        selectionModel = new TiledSelectionModel<>(control);
        control.selectionModelProperty().set(selectionModel);
        control.setOnMouseClicked(event -> clicked(event));
        
        selectionModel.selectedItemProperty().addListener((ov,o,n) -> {changed(ov,o,n);});
        selectionModel.updateItemCount();
	}
    
	//-------------------------------------------------------------------
	private TiledListView<T, S> getControl() { return control; }

    //-----------------------------------------------------------------
	private TiledCell<T,S> createCell(T value) {
		logger.trace("create cell for "+value+" using factory "+getControl().getCellFactory());
		
		TiledCell<T,S> cell = getControl().getCellFactory().call(getControl());
		cell.setItem(value);
		
		return cell;
	}

	//-------------------------------------------------------------------
	public ObservableList<Node> impl_getChildren() {
		return getControl().impl_getChildren();
	}

	//-------------------------------------------------------------------
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void addSection(S section) {
		logger.trace("  createSectionLabel("+section+")");
		
		if (sectionLabels.containsKey(section))
			return;

		// Section does not exist yet
		
		// Create a list to store all sections contents
		listsPerSection.put(section, new ArrayList<>());
		
		// Create a label for this section
		Label sectHead = new Label(String.valueOf(section));
		sectHead.setMaxWidth(Double.MAX_VALUE);
		sectHead.getStyleClass().add("list-section-head");
		sectHead.setUserData(section);
		
		sectionLabels.put(section, sectHead);
		
		/*
		 * Inform skin
		 */
		String nameToAdd = String.valueOf(section);
		for (Node node : skin.getItems()) {
			if (node instanceof Label) {
				String otherName = ((Label) node).getText();
				int cmp = 0;
				if (sectionComparator!=null)
					cmp = sectionComparator.compare(section, (S)node.getUserData());
				else if (section instanceof Comparable) {
					cmp = ((Comparable) section).compareTo(node.getUserData());
				} else 
					cmp = nameToAdd.compareTo(otherName);
				if (cmp<0) {
					int index = skin.getItems().indexOf(node);
					logger.debug("Add section '"+nameToAdd+"' at index "+index);
					skin.getItems().add(index, sectHead);
					return;
				}
			}
		}
		logger.debug("Add section '"+nameToAdd+"' at end");
		skin.getItems().add(sectHead);
	}

    //-----------------------------------------------------------------
	private void clicked(MouseEvent event) {
		logger.debug("clicked "+event.getSource());
		event.consume();
	};

    //-----------------------------------------------------------------
	private void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
		logger.debug("changed "+observable.getClass()+" from "+oldValue+" to "+newValue+"  // "+selectionModel.getSelectedItems());
		
		if (oldValue!=null) {
			cellsByItem.get(oldValue).setSelected(false);
		}	
		if (newValue!=null) {
			if (cellsByItem.containsKey(newValue))
				cellsByItem.get(newValue).setSelected(true);
			else
				logger.error("Expected cell for "+newValue+" in "+cellsByItem);
		}	
	};

//	//-------------------------------------------------------------------
//	private void addSection(S section) {
//		logger.trace("  -----------------------------------------------------------");
//		logger.trace("  addSection("+section+")");
//		if (sectionLabels.containsKey(section)) {
//			logger.warn("Trying to add already existing section");
//			return;
//		}
//		
//		
//		// Create and add the new section
//		Label sectHead = addSection(section);
//		/*
//		 * Find to position within the controls children. To do so
//		 * sort sections and put it in the position the successor 
//		 * previously had
//		 */
//		logger.trace("  Existing sections are "+sectionLabels.keySet());
//		List<S> sections = new ArrayList<S>(sectionLabels.keySet());
//		if (section instanceof Comparable) {
//			Collections.sort(sections, new Comparator<S>(){
//				@SuppressWarnings("unchecked")
//				public int compare(S o1, S o2) {
//					return ((Comparable<S>)o1).compareTo(o2);
//				}});
//		} else {
//			Collections.sort(sections, new Comparator<S>(){
//				public int compare(S o1, S o2) {
//					return String.valueOf(o1).compareTo(String.valueOf(o2));
//				}});
//		}
//		
//		logger.trace("  Index = "+(sections.indexOf(section)+1)+" , size="+sections.size()+" == "+sections);
//		if ( (sections.indexOf(section)+1)==sections.size()) {
//			// New section is at the end
//			getControl().impl_getChildren().add(sectHead);
//			logger.debug("  Appended section '"+section+"'");
//		} else {
//			S successor = sections.get(sections.indexOf(section)+1);
//			logger.trace("Successor of "+section+" in "+sections+" is "+successor);
//			Label successorLabel = sectionLabels.get(successor);
//			int newLabelPos = getControl().impl_getChildren().indexOf(successorLabel);
//			if (newLabelPos==-1) {
//				logger.warn("Could not find position of "+successor);
//				System.exit(0);
//			}
//			logger.info("Add section '"+section+"' at "+newLabelPos);
//			getControl().impl_getChildren().add(newLabelPos,sectHead);
//		}
//	}

	//-------------------------------------------------------------------
	private void addInternal(T player) {
		logger.trace("addInternal: "+player);
		
		S section = null;
		if (getControl().getSectionFactory()!=null) {
			section = getControl().getSectionFactory().call(player);
			logger.trace("  Section factory found and returned "+section);
			if (!sectionLabels.containsKey(section))
				addSection(section);
		}

		TiledCell<T,S> toAdd = createCell(player);
		toAdd.setUserData(player);
		if (section!=null) 
			toAdd.setSection(section);
		
		cellsByItem.put(player, toAdd);
		addInternal(player, toAdd);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private synchronized void addInternal(T player, TiledCell<T,S> toAdd) {
		logger.trace("addInternal: "+player+", cell="+toAdd);
		
		logger.trace("Add "+player+" under section "+toAdd.getSection());
		
		/*
		 * Insert node at matching position
		 * Start after Label for the section, search to end or next section label
		 */
		
		int index = skin.getItems().indexOf(sectionLabels.get(toAdd.getSection()));
		if (index<0) {
			logger.error("Did not find section index for "+sectionLabels.get(toAdd.getSection()));
//			return;
//			throw new IllegalArgumentException("Did not find section index for "+sectionLabels.get(toAdd.getSection()));
		}
		index++; // 
		
		logger.debug("********Find position for "+toAdd+" in "+skin.getItems());
		for (int i=index; i<skin.getItems().size(); i++) {
			Node node = skin.getItems().get(i);
			/*
			 * If node is a label, than the end of this section's list has
			 * been reached. Insert new cell before label
			 */
			if (node instanceof Label) {
				logger.debug("Append "+toAdd+" at end of section "+toAdd.getSection()+" at pos "+i);
				skin.getItems().add(i, toAdd);
				return;
			}
			
			/*
			 * Within sections list, compare alphabetically
			 */			
			T existing = (T)node.getUserData();
			int cmp = 0;
			if (itemComparator!=null)
				cmp = itemComparator.compare(player, existing);
			else if (player instanceof Comparable)
				cmp = ((Comparable) player).compareTo(existing);
			else
				cmp = String.valueOf(player).compareToIgnoreCase(String.valueOf(existing));
//			logger.debug("Comparing "+String.valueOf(player)+" with "+String.valueOf(existing)+" returns "+cmp);
			if (cmp<0) {
				// Prepend
				logger.debug("Prepend "+toAdd+" before "+existing+" in section "+toAdd.getSection()+" at pos "+i);
				skin.getItems().add(i, toAdd);
				return;
			}
		}

		logger.debug("Append "+toAdd+" at end of whole list");
		skin.getItems().add(toAdd);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private TiledCell<T, S> getCell(T item) {
		for (Node node : skin.getItems()) {
			if (node instanceof TiledCell) {
				if ( ((TiledCell<T,S>)node).getItem()==item )
				return (TiledCell<T, S>) node;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void cellUpdated(TiledCell<T, S> cell, T newValue) {
		// Check sorting
		S oldSection = cell.getSection();
		S newSection = null;
		if (getControl().getSectionFactory()!=null) {
			newSection = getControl().getSectionFactory().call(newValue);
			cell.setSection(newSection);
		}
		// If section changed, remove and re-add item
		// Do this by replacing the item at its current position
		if (oldSection!=newSection) {
			logger.trace("Section changed");
			getControl().getItems().remove(cell.getItem());
			checkNecessity(oldSection);
			getControl().getItems().add(newValue);
			cell.setSection(newSection);
		}
		
		
		// Update displayed cell content
		cell.updateItem(newValue, false);

	}

	//-------------------------------------------------------------------
	private void checkNecessity(S section) {
		// Now check if label can be removed too
		Label sortLabel = null;
		for (Node tmp : skin.getItems()) {
			if (tmp instanceof Label) {
				if (tmp.getUserData()==section) {
					sortLabel = (Label) tmp;
				}
			} else if (tmp instanceof TiledCell) {
				@SuppressWarnings("unchecked")
				TiledCell<T,S> tmpCell = (TiledCell<T,S>)tmp;
				if (tmpCell.getSection()==section) {
					// Found another item for the section, so the section must remain. Done here
					logger.debug("Section "+section+" is still necessary");
					return;
				}
			}
		}
		
		if (sortLabel!=null) {
			// Label can be removed
			logger.trace("Section "+section+" can be removed");
			skin.getItems().remove(sortLabel);
		} else
			logger.trace("No label for section "+section+" found");
	}

	//-------------------------------------------------------------------
	private void removeInternal(T item) {
		logger.trace("removeInternal: "+item);
		cellsByItem.remove(item);
		
		TiledCell<T,S> cell = getCell(item);
		if (cell==null)
			return;
		
		if (Platform.isFxApplicationThread())
			skin.getItems().remove(cell);
		else {
			Platform.runLater(new Runnable(){
				public void run() {
					skin.getItems().remove(cell);
				}});
		}
	
		checkNecessity(cell.getSection());
	}

	//-----------------------------------------------------------------
	/**
	 * @param itemComparator the itemComparator to set
	 */
	public void setItemComparator(Comparator<T> itemComparator) {
		this.itemComparator = itemComparator;
	}

	//-----------------------------------------------------------------
	/**
	 * @param sectionComparator the sectionComparator to set
	 */
	public void setSectionComparator(Comparator<S> sectionComparator) {
		this.sectionComparator = sectionComparator;
	}
}
