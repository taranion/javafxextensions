/**
 * 
 */
package org.prelle.javafx.skin;

import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.TilePane;

import org.prelle.javafx.TiledListView;

/**
 * @author spr
 *
 */
public class TiledListViewSkin<T,S> extends SkinBase<TiledListView<T,S>> {

	private TilePane pane;
	
	
	//-----------------------------------------------------------------
	public TiledListViewSkin(TiledListView<T,S> control) {
		super(control);

		initComponents();

//		registerListener();
	}

	//-----------------------------------------------------------------
	private void initComponents() {
		pane = new TilePane(getSkinnable().getOrientation());
		
		pane.setVgap(10);
		pane.setHgap(10);
//		pane.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		pane.setMaxHeight(Double.MAX_VALUE);
		pane.setPrefColumns(1);
	}

	//-----------------------------------------------------------------
	public void initLayout(ObservableList<Node> realChildren) {
		ScrollPane scroll = new ScrollPane(pane);
		
//		if (getSkinnable().getOrientation()==Orientation.VERTICAL) {
//			scroll.setFitToWidth(true);
//		} else
			scroll.setFitToHeight(true);
			
		pane.prefHeightProperty().bind(scroll.heightProperty());
		
		realChildren.add(scroll);
		
//		Callback<TiledListView<T,S>, TiledCell<T,S>> factory = getSkinnable().getCellFactory();
//		if (factory==null) {
//			logger.warn("No CellFactory set for "+getSkinnable());
//			System.exit(0);
//			return;
//		}
//
//		for (T item : getSkinnable().getItems()) {
//			logger.debug("  call cell factory "+factory.getClass());
//			TiledCell<T,S> cell = factory.call(getSkinnable());
//			cell.setMaxWidth(Double.MAX_VALUE);
//			logger.debug("  call setItem("+item+") on "+cell.getClass());
//			cell.setItem(item);
//		}
	}

	//-----------------------------------------------------------------
	public List<Node> getItems() {
		return pane.getChildren();
	}
//	
//	//-----------------------------------------------------------------
//	private void registerListener() {
//		getSkinnable().itemsProperty().addListener(new ChangeListener<ObservableList<T>>() {
//
//			@Override
//			public void changed(ObservableValue<? extends ObservableList<T>> observable,
//							ObservableList<T> oldValue, ObservableList<T> newValue) {
//				// TODO Auto-generated method stub
//				logger.debug("Changed: "+newValue);
//			}
//		});
//	}
//
//	//-----------------------------------------------------------------
//	@Override
//	protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset) {
////		logger.debug("computePrefWidth("+height+", "+topInset+", "+rightInset+", "+bottomInset+", "+leftInset);
//		return super.computePrefWidth(height, topInset, rightInset, bottomInset, leftInset);
//	}

}
