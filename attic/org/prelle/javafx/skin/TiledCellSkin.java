/**
 * 
 */
package org.prelle.javafx.skin;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;

import org.prelle.javafx.TiledCell;

/**
 * @author spr
 *
 */
public class TiledCellSkin<T,S> extends SkinBase<TiledCell<T,S>> {
	
	protected HBox layout;
	protected Label label;

	//-----------------------------------------------------------------
	/**
	 * @param control
	 */
	public TiledCellSkin(TiledCell<T,S> control) {
		super(control);
		
		layout = new HBox();
		layout.setAlignment(Pos.TOP_LEFT);
		getChildren().add(layout);
		
		label = new Label();
		label.setWrapText(true);
//		label.setPrefWidth(200);
//		label.setAlignment(Pos.TOP_LEFT);
		label.setContentDisplay(ContentDisplay.LEFT);
		label.setGraphicTextGap(5);
//		label.setTextAlignment(TextAlignment.LEFT);
		
		layout.setMinWidth(200);
		layout.setMaxWidth(Double.MAX_VALUE);
		layout.getChildren().add(label);
		control.setMaxWidth(Double.MAX_VALUE);
		
		control.graphicProperty().addListener((ov,o,n) -> updateGraphic(o,n)); 
		control.textProperty().addListener((ov,o,n) -> updateText(n)); 
	}

	//-------------------------------------------------------------------
	private void updateGraphic(Node oldGraphic, Node graphic) {
		layout.getChildren().remove(oldGraphic);
		layout.getChildren().add(0, graphic);
	}

	//-------------------------------------------------------------------
	private void updateText(String value) {
		label.setText(value);
	}

	//-------------------------------------------------------------------
	public Labeled getLabeled() {
		return label;
	}

}
