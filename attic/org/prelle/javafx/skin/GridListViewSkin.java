/**
 * 
 */
package org.prelle.javafx.skin;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.prelle.javafx.GridListView;

import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.MyTilePane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class GridListViewSkin<T> extends SkinBase<ListView<T>> {

	private final static Logger logger = Logger.getLogger("prelle.jfx");

	private MyTilePane pane;
	private double cellWidth, cellHeight;
	private double vgap, hgap;

	private List<ListCell<T>> cells;
	private int currentColumns;
	private ScrollBar scrollbar;
	private int viewStart;

	private ListCell<T> referenceCell;

	//-------------------------------------------------------------------
	public GridListViewSkin(ListView<T> control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();

		recalculateCellsOnScreen();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		pane = new MyTilePane();
		cells= new ArrayList<>();
		scrollbar = new ScrollBar();
		scrollbar.setUnitIncrement(1.0);
		scrollbar.setBlockIncrement(2.0);

		if (getSkinnable() instanceof GridListView) {
			GridListView<T> grid = (GridListView<T>)getSkinnable();
			cellWidth = grid.getCellWidth();
			cellHeight = grid.getCellHeight();
		} else {
			cellWidth = 162;
			cellHeight = 162;			
		}
		vgap = 2;
		hgap = 2;
//		pane.setPrefTileWidth(160);
//		pane.setPrefTileHeight(160);

		pane.setVgap(vgap);
		pane.setHgap(hgap);
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		//		scrollbar.setRotate(90);
		//		Group rotated = new Group(scrollbar);
		scrollbar.setOrientation(Orientation.VERTICAL);

		//		pane.setPrefWidth(Double.MAX_VALUE);
		HBox.setHgrow(pane, Priority.ALWAYS);
		HBox content = new HBox(pane, scrollbar);
//		content.setStyle("-fx-background-color: pink");
		getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().widthProperty().addListener( (ov,o,n) -> recalculateCellsOnScreen());
		getSkinnable().heightProperty().addListener( (ov,o,n) -> recalculateCellsOnScreen());
//		pane.actualColumnsProperty().addListener( (ov,o,n) -> {
//			logger.info("+++++Columns = "+n);
//			recalculateCellsOnScreen();
//		});
//		pane.actualRowsProperty().addListener( (ov,o,n) -> {
//			logger.info("+++++Rows = "+n);
//			recalculateCellsOnScreenOld();
//		});
		getSkinnable().cellFactoryProperty().addListener( (ov,o,n) -> {
			cells.clear();
			pane.getChildren().clear();
			recalculateCellsOnScreen();
		});
		scrollbar.valueProperty().addListener( (ov,o,n) -> scrollbarUpdated(n));
		pane.setOnScroll(ev -> scrollbar.fireEvent(ev));
	}

	//-------------------------------------------------------------------
	private static <T> ListCell<T> createDefaultCellImpl() {
		logger.debug("createDefaultCellImpl");
		return new ListCell<T>() {
			@Override public void updateItem(T item, boolean empty) {
				super.updateItem(item, empty);

				if (empty) {
					setText(null);
					setGraphic(null);
				} else if (item instanceof Node) {
					setText(null);
					Node currentNode = getGraphic();
					Node newNode = (Node) item;
					if (currentNode == null || ! currentNode.equals(newNode)) {
						setGraphic(newNode);
					}
				} else {
					/**
					 * This label is used if the item associated with this cell is to be
					 * represented as a String. While we will lazily instantiate it
					 * we never clear it, being more afraid of object churn than a minor
					 * "leak" (which will not become a "major" leak).
					 */
					setText(item == null ? "null" : item.toString());
					setGraphic(null);
				}
			}
		};
	}

	//-------------------------------------------------------------------
	private ListCell<T> createCell() {
		ListCell<T> cell;
		if (getSkinnable().getCellFactory() != null) {
			cell = getSkinnable().getCellFactory().call(getSkinnable());
		} else {
			cell = createDefaultCellImpl();
		}

		if (referenceCell==null) {
			referenceCell = cell;
			referenceCell.widthProperty().addListener( (ov,o,n) -> { 
				cellWidth=Math.max(cellWidth,(int)Math.round((Double)n));
//				logger.debug("----------updated cell width to "+cellWidth);
				});
		}
		cell.updateListView(getSkinnable());

		return cell;
	}

	//-------------------------------------------------------------------
	private void updateCellItems() {
		//    	int viewStart=0;
		int i=0;
		for (ListCell<T> cell : cells) {
			int pos = viewStart+i;
			if (pos<getSkinnable().getItems().size()) {
				//    			logger.debug("Update "+pos);
				cell.updateIndex(pos);
			} else {
				//    			logger.debug("Empty "+pos);
				cell.updateIndex(-1);
			}
			i++;	
		}
	}

	//-------------------------------------------------------------------
	public void recalculateCellsOnScreen() {
		// COLUMNS Either calculate columns by window width or obtain from pane
		if (pane.actualColumnsProperty().get()!=0)
			currentColumns = pane.actualColumnsProperty().get();
		else {
			//		logger.debug("-----------------------recalculateCellsOnScreen for width "+getSkinnable().getWidth()+"x"+getSkinnable().getHeight()+" / "+cellWidth);
			double width = getSkinnable().getWidth() * 0.985;
			currentColumns = (int) (width/cellWidth);
			// Is there enough space for gaps - if not, reduce cells per row
			//		logger.debug("Is "+cellsPerRow+"*"+cellWidth+" ("+cellsPerRow+"-1)*"+hgap+"  > "+width+" == "+( (cellsPerRow*cellWidth + (cellsPerRow-1)*hgap)>width ));
			if ( (currentColumns*cellWidth + (currentColumns-1)*hgap)>width )
				currentColumns--;
			currentColumns = Math.max(currentColumns, 1);
		}

		// ROWS
		int currentRows = Math.max((int) (getSkinnable().getHeight()/(cellHeight+vgap)),1);
//		if (pane.actualRowsProperty().get()!=0)
//			currentRows = pane.actualRowsProperty().get();

		
		int requestedCells = Math.min(getSkinnable().getItems().size(), currentRows * currentColumns);
//		logger.debug("Cells = "+currentColumns+"x"+currentRows+" = "+requestedCells+"    for "+getSkinnable().getItems().size()+" items");

		// Create missing cells until cells.size() is at least the number of requested cells
		while (cells.size()<requestedCells) {
			// Create a cell or use a dummy label
			ListCell<T> cell = createCell();
			cell.setPrefWidth(cellWidth);
			cell.setPrefHeight(cellHeight);
			cells.add(cell);
		}

		// Adjust cells on pane to requested
//		logger.debug("Requested cells = "+requestedCells);
		while (pane.getChildren().size()<requestedCells) {
//			logger.debug("add cell");
			pane.getChildren().add(cells.get(pane.getChildren().size()));
		}
		while (pane.getChildren().size()>requestedCells) {
//			logger.debug("remove "+(pane.getChildren().size()-1));
			pane.getChildren().remove(pane.getChildren().size()-1);
		}

		if (requestedCells>getSkinnable().getItems().size())
			scrollbar.setValue(0);
		updateCellItems();
		updateScrollBar(currentColumns, currentRows);
	}

	//-------------------------------------------------------------------
	private void updateScrollBar(int cellsPerRow, int cellsPerColumn) {
		int totalRows = ((getSkinnable().getItems().size()+1) / cellsPerRow)+1;
		int rowsOnPane= cellsPerColumn;
//		logger.debug("updateScrollBar: "+(getSkinnable().getItems().size()+1)+" / "+cellsPerRow+" = "+totalRows+" rows that can be displayed");

		scrollbar.setMax(totalRows-rowsOnPane);
		scrollbar.setVisibleAmount(1);
		scrollbar.setBlockIncrement(1);
		scrollbar.setUnitIncrement(1);
//		logger.debug("maxScroll = "+totalRows+"   rowsOnPane="+rowsOnPane);

	}

	//-------------------------------------------------------------------
	private void scrollbarUpdated(Number n) {

		viewStart = (int) Math.round((Double)n) * currentColumns;
		logger.debug("scrollbar updated to "+n+"   viewStart = "+viewStart);
		updateCellItems();
	}

}
