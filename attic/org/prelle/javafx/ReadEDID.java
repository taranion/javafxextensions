/**
 * 
 */
package org.prelle.javafx;

import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javafx.application.Application;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class ReadEDID extends Application {
	
	private final static Logger logger = Logger.getLogger("prelle.jfx");

	//-----------------------------------------------------------------
	/**
	 */
	public ReadEDID() {
		// TODO Auto-generated cocnstructor stub
	}

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		PropertyConfigurator.configure("log4j.properties");
		// TODO Auto-generated method stub
//		
//		Path parentDir = FileSystems.getDefault().getPath("/sys/class/drm");
//		for (Path cardDir : Files.newDirectoryStream(parentDir)) {
//			if (!Files.isDirectory(cardDir) || !cardDir.getFileName().toString().startsWith("card"))
//				continue;
//			System.out.println("Graphic Card "+cardDir);
			Path edidPath = FileSystems.getDefault().getPath("/home/prelle/edid_acerT272HUL.bin");
			if (Files.exists(edidPath)) {
				byte[] data = new byte[EDIDConstants.EDID1_LEN];
				InputStream in = new FileInputStream(edidPath.toFile());
				in.read(data, 0, data.length);
				in.close();
				
				EDID edid = new EDID(data);
				
				System.out.println("* EDID:\n"+edid);
			}
//		}
		
//		File file = new File("/sys/class/drm/card0/card0-VGA-1/edid");
//		byte[] data = new byte[EDIDConstants.EDID1_LEN];
//		InputStream in = new FileInputStream(file);
//		in.read(data, 0, data.length);
//		in.close();
//		
//		EDID edid = new EDID(data);
//		
//		System.out.println("EDID: "+edid);
			
			Application.launch(args);
	}

	//-----------------------------------------------------------------
	public static List<byte[]> getAllEDIDs() {
		String osName = System.getProperty("os.name");
		logger.debug(osName);
		
		if ("Linux".equals(osName)) {
			// Linux code
			return getLinuxEDIDs();
		} else if (osName.startsWith("Windows")) {
			// Windows code
			return getWindowsEDIDs();
		} else {

			logger.warn("Cannot read EDID infos on this operating system: "+osName);
			return new ArrayList<>();
		}
	}

	//-----------------------------------------------------------------
	private static List<byte[]> getLinuxEDIDs() {
		List<byte[]> ret = new ArrayList<>();
		
		try {
			Path parentDir = FileSystems.getDefault().getPath("/sys/class/drm");
			for (Path cardDir : Files.newDirectoryStream(parentDir)) {
				if (!Files.isDirectory(cardDir) || !cardDir.getFileName().toString().startsWith("card"))
					continue;
				logger.debug("Graphic Card "+cardDir);
				Path edidPath = cardDir.resolve("edid");
				if (Files.exists(edidPath)) {
					byte[] data = new byte[EDIDConstants.EDID1_LEN];
					InputStream in = new FileInputStream(edidPath.toFile());
					in.read(data, 0, data.length);
					in.close();
					
					ret.add(data);
				}
			}
		} catch (Exception e) {
			logger.error("Error reading /sys/class/drm filesystem",e);
		}
		
		return ret;
	}

	//-----------------------------------------------------------------
	private static List<byte[]> getWindowsEDIDs() {
		List<byte[]> ret = new ArrayList<>();
		
		// REGISTRY URL = "SYSTEM\CurrentControlSet\Enum\DISPLAY"
		
		try {
			List<String> values = WinRegistry.readStringSubKeys (
				    WinRegistry.HKEY_LOCAL_MACHINE,                             //HKEY
				   "SYSTEM\\CurrentControlSet\\Enum\\DISPLAY"          //Key
				   );                                              //ValueName
			for (String device : values) {
				logger.debug("Card "+device);
				List<String> screens = WinRegistry.readStringSubKeys (
					    WinRegistry.HKEY_LOCAL_MACHINE,                             //HKEY
					   "SYSTEM\\CurrentControlSet\\Enum\\DISPLAY\\"+device    
					   );                                           
				for (String screen : screens) {
					logger.debug("  Screen "+screen);
					
					String newKey = "SYSTEM\\CurrentControlSet\\Enum\\DISPLAY\\"+device+"\\"+screen+"\\Device Parameters";
					String edid = WinRegistry.readRegistry("HKEY_LOCAL_MACHINE\\"+newKey, "EDID");

					logger.debug("    = "+edid);
					ret.add(convertHexStringToBytes(edid));
				}
			}
		} catch (Exception e) {
			logger.error("Error reading registry key",e);
		}
		return ret;
	}
		
	private static byte[] convertHexStringToBytes(String val) {
		byte[] ret = new byte[val.length()/2];
		for (int i=0; i<ret.length; i++) {
			String part = val.substring(2*i, 2*i+2);
			ret[i] = (byte)Integer.parseInt(part, 16);
		}
		return ret;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		List<byte[]> edids = getAllEDIDs();
		logger.debug("Found "+edids.size()+" EDID information");
		
		double width = 1;
		double height= 1;
		for (byte[] raw : edids) {
			EDID edid = new EDID(raw);
			logger.debug("* "+edid.getManufacturer()+" "+edid.getProductID()+"   "+edid.getHSize()+"x"+edid.getVSize()+"cm");
			width = edid.getHSize()*0.393; // To inch 
			height = edid.getVSize()*0.393; // To inch 
		}
		
		for (Screen screen : Screen.getScreens()) {
			double resX = screen.getBounds().getWidth();
			double resY = screen.getBounds().getHeight();
			logger.debug("Screen "+resX+"x"+resY);
			logger.debug("  DPI: calc="+Math.abs(resX/width)+"  reported="+screen.getDpi());
			logger.debug("  DPI: calc="+Math.abs(resY/height)+"  reported="+screen.getDpi());
			logger.debug("  "+Toolkit.getDefaultToolkit().getScreenResolution());
		}

	}
}
