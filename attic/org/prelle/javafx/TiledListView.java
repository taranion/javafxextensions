/**
 * 
 */
package org.prelle.javafx;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Cell;
import javafx.scene.control.Control;
import javafx.scene.control.ListCell;
import javafx.scene.control.MultipleSelectionModel;
import javafx.util.Callback;

import org.prelle.javafx.skin.TiledListViewSkin;

import com.sun.javafx.collections.ChangeHelper;
import com.sun.javafx.collections.ObservableListWrapper;

/**
 * @author prelle
 *
 */
public class TiledListView<T,S> extends Control {

	private static final String DEFAULT_STYLE_CLASS = "tiled-list-view";
    
    
    // --- Items
//    private ObjectProperty<ObservableList<T>> itemsProperty = new SimpleObjectProperty<ObservableList<T>>(FXCollections.observableArrayList());
    private ObjectProperty<ObservableList<T>> itemsProperty = new SimpleObjectProperty<ObservableList<T>>(new MyOberservableList<T>(new ArrayList<T>()));
    // --- Selection Model
    private ObjectProperty<Orientation> orientationProperty;
    // --- Selection Model
    private ObjectProperty<MultipleSelectionModel<T>> selectionModel = new SimpleObjectProperty<MultipleSelectionModel<T>>(this, "selectionModel");
    // --- Cell Factory
    private ObjectProperty<Callback<TiledListView<T,S>, TiledCell<T,S>>> cellFactory 
    	= new SimpleObjectProperty<Callback<TiledListView<T,S>,TiledCell<T,S>>>(new Callback<TiledListView<T,S>, TiledCell<T,S>>() {
			@Override
			public TiledCell<T,S> call(TiledListView<T,S> view) {
				return new TiledCell<T,S>(view);
			}
		});
	private ObjectProperty<Callback<T, S>> sectionFactory;
	
	private TiledListBehavior<T, S> behavior;
   
	//-------------------------------------------------------------------
	public TiledListView() {
        this(Orientation.VERTICAL);
	}

	//-------------------------------------------------------------------
	public TiledListView(Orientation orientation) {
		orientationProperty = new SimpleObjectProperty<Orientation>(orientation);
		
        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
        
        TiledListViewSkin<T,S> skin = new TiledListViewSkin<T,S>(this); 
        setSkin(skin);
        selectionModel.set(new TiledSelectionModel<>(this));
        behavior = new TiledListBehavior<T,S>(this, skin);
		
		skin.initLayout(getChildren());
 	}

	/******************************************
	 * Property ITEMS
	 ******************************************/
  
    //-----------------------------------------------------------------
    public final void setItems(ObservableList<T> value) {
        itemsProperty.set(value);
    }

    //-----------------------------------------------------------------
    public final ObservableList<T> getItems() {
        return itemsProperty.get();
    }

    //-----------------------------------------------------------------
    public final ObjectProperty<ObservableList<T>> itemsProperty() {
        return itemsProperty;
    }

	/******************************************
	 * Property SELECTION MODEL
	 ******************************************/
    
    //-----------------------------------------------------------------
    public final void setSelectionModel(MultipleSelectionModel<T> value) {
        selectionModelProperty().set(value);
    }

    //-----------------------------------------------------------------
    public final MultipleSelectionModel<T> getSelectionModel() {
        return selectionModel == null ? null : selectionModel.get();
    }

    //-----------------------------------------------------------------
    public final ObjectProperty<MultipleSelectionModel<T>> selectionModelProperty() {
        return selectionModel;
    }

    
	/******************************************
	 * Property ORIENTATION
	 ******************************************/
    
    //-----------------------------------------------------------------
    public final void setOrientiation(Orientation value) {
    	orientationProperty.set(value);
    }

    //-----------------------------------------------------------------
    public final Orientation getOrientation() {
        return orientationProperty.get();
    }

    //-----------------------------------------------------------------
    public final ObjectProperty<Orientation> orientationProperty() {
        return orientationProperty();
    }
   
    
	/******************************************
	 * Property CELL FACTORY
	 ******************************************/

    //-----------------------------------------------------------------
    /**
     * Sets a new cell factory to use in the ListView. This forces all old 
     * {@link ListCell}'s to be thrown away, and new ListCell's created with 
     * the new cell factory.
     */
    public final void setCellFactory(Callback<TiledListView<T,S>, TiledCell<T,S>> value) {
        cellFactoryProperty().set(value);
    }

    //-----------------------------------------------------------------
   /**
     * Returns the current cell factory.
     */
    public final Callback<TiledListView<T,S>, TiledCell<T,S>> getCellFactory() {
        return cellFactory == null ? null : cellFactory.get();
    }

    //-----------------------------------------------------------------
   /**
     * <p>Setting a custom cell factory has the effect of deferring all cell
     * creation, allowing for total customization of the cell. Internally, the
     * ListView is responsible for reusing ListCells - all that is necessary
     * is for the custom cell factory to return from this function a ListCell
     * which might be usable for representing any item in the ListView.
     *
     * <p>Refer to the {@link Cell} class documentation for more detail.
     */
    public final ObjectProperty<Callback<TiledListView<T,S>, TiledCell<T,S>>> cellFactoryProperty() {
        if (cellFactory == null) {
            cellFactory = new SimpleObjectProperty<Callback<TiledListView<T,S>, TiledCell<T,S>>>(this, "cellFactory");
        }
        return cellFactory;
    }

	//-------------------------------------------------------------------
    ObservableList<Node> impl_getChildren() {
    	return super.getChildren();
    }
   
    
 	/******************************************
 	 * Property SECTION FACTORY
 	 ******************************************/

     //-----------------------------------------------------------------
     public final void setSectionFactory(Callback<T,S> value) {
         sectionFactoryProperty().set(value);
     }

     //-----------------------------------------------------------------
    /**
      * Returns the current cell factory.
      */
     public final Callback<T,S> getSectionFactory() {
         return sectionFactory == null ? null : sectionFactory.get();
     }

    //-----------------------------------------------------------------
   /**
     * <p>Setting a custom cell factory has the effect of deferring all cell
     * creation, allowing for total customization of the cell. Internally, the
     * ListView is responsible for reusing ListCells - all that is necessary
     * is for the custom cell factory to return from this function a ListCell
     * which might be usable for representing any item in the ListView.
     *
     * <p>Refer to the {@link Cell} class documentation for more detail.
     */
    public final ObjectProperty<Callback<T,S>> sectionFactoryProperty() {
        if (sectionFactory == null) {
        	sectionFactory = new SimpleObjectProperty<Callback<T,S>>(this, "sectionFactory");
        }
        return sectionFactory;
    }

	//-------------------------------------------------------------------
	/**
	 * Inform the view that the contents of the given item have been updated
	 */
	public void updateItem(T item) {
		((MyOberservableList<T>)itemsProperty.get()).fireUpdate(item);
	}

	//-----------------------------------------------------------------
	public void setSectionComparator(Comparator<S> comp) {
		behavior.setSectionComparator(comp);
	}

	//-----------------------------------------------------------------
	public void setItemComparator(Comparator<T> comp) {
		behavior.setItemComparator(comp);
	}

}

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class MyOberservableList<E> extends ObservableListWrapper<E> {

    private static class SubChange<E> {

        int from, to;
        List<E> removed;
        int[] perm;
        boolean updated;

        public SubChange(int from, int to, List<E> removed, int[] perm, boolean updated) {
            this.from = from;
            this.to = to;
            this.removed = removed;
            this.perm = perm;
            this.updated = updated;
        }
    }
	
    private static class SingleChange<E> extends Change<E> {
        private final SubChange<E> change;
        private boolean onChange;

        public SingleChange(SubChange<E> change, ObservableListBase<E> list) {
            super(list);
            this.change = change;
        }

        @Override
        public boolean next() {
            if (onChange) {
                return false;
            }
            onChange = true;
            return true;
        }

        @Override
        public void reset() {
            onChange = false;
        }

        @Override
        public int getFrom() {
            checkState();
            return change.from;
        }

        @Override
        public int getTo() {
            checkState();
            return change.to;
        }

        @Override
        public List<E> getRemoved() {
            checkState();
            return change.removed;
        }

        @Override
        protected int[] getPermutation() {
            checkState();
            return change.perm;
        }

        @Override
        public boolean wasUpdated() {
            checkState();
            return change.updated;
        }

        private void checkState() {
            if (!onChange) {
                throw new IllegalStateException("Invalid Change state: next() must be called before inspecting the Change.");
            }
        }

        @Override
        public String toString() {
            String ret;
            if (change.perm.length != 0) {
                ret = ChangeHelper.permChangeToString(change.perm);
            } else if (change.updated) {
                ret = ChangeHelper.updateChangeToString(change.from, change.to);
            } else {
                ret = ChangeHelper.addRemoveChangeToString(change.from, change.to, getList(), change.removed);
            }
            return "{ " + ret + " }";
        }

    }

	//-------------------------------------------------------------------
	public MyOberservableList(List<E> list) {
		super(list);
	}
	
	//-------------------------------------------------------------------
	public void fireUpdate(E item) {
		int index = this.indexOf(item);
		if (index<0)
			return;
		int from = index;
		int to   = index;
		Change<E> change = new SingleChange<E>(new SubChange<E>(from, to, new ArrayList<E>(), new int[0], true), this);
		super.fireChange(change);
	}
	
}