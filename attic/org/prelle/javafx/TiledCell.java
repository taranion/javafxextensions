/**
 * 
 */
package org.prelle.javafx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.input.MouseEvent;

import org.apache.log4j.Logger;
import org.prelle.javafx.skin.TiledCellSkin;

/**
 * @author prelle
 *
 */
public class TiledCell<T,S> extends Control {
    
	private final static Logger logger = Logger.getLogger("prelle.jfx");
	
	private final static String DEFAULT_STYLE_CLASS = "tiled-list-cell";
	   
    /***************************************************************************
     *                                                                         *
     * Stylesheet Handling                                                     *
     *                                                                         *
     **************************************************************************/

    private static final PseudoClass PSEUDO_CLASS_SELECTED =
            PseudoClass.getPseudoClass("selected");
//    private static final PseudoClass PSEUDO_CLASS_FOCUSED = 
//            PseudoClass.getPseudoClass("focused");

    
	private ReadOnlyObjectWrapper<TiledListView<T,S>> listView = new ReadOnlyObjectWrapper<TiledListView<T,S>>(this, "listView");
	private ObjectProperty<T> itemProperty = new SimpleObjectProperty<>();
	private ObjectProperty<S> sectionProperty = new SimpleObjectProperty<>();
    private ReadOnlyBooleanWrapper selected = new ReadOnlyBooleanWrapper() {
        @Override protected void invalidated() {
        	logger.debug("Call pseudoClassStateChanged");
            pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, get());
        }

        @Override
        public Object getBean() {
            return TiledCell.this;
        }
        
        @Override
        public String getName() {
            return "selected";
        }
    };
    private ObjectProperty<Node> graphicProperty = new SimpleObjectProperty<>();
    private StringProperty       textProperty    = new SimpleStringProperty();
	
    private EventHandler<ActionEvent> actionHandler;
    
	//-------------------------------------------------------------------
	public TiledCell(TiledListView<T,S> listView) {
		this.listView.set(listView);
		setSkin(new TiledCellSkin<>(this));
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		
		setHover(false);
		
//		updateItem();
		itemProperty().addListener((ov,o,n)->updateItem(n, n==null));
		this.setOnMouseClicked(event -> clicked(event));
	}
	
	//-------------------------------------------------------------------
	public ObjectProperty<T> itemProperty() { return itemProperty; }
	public T getItem() { return itemProperty.get(); }
	public void setItem(T value) { itemProperty.set(value); }
	//-------------------------------------------------------------------
	public ObjectProperty<S> sectionProperty() { return sectionProperty; }
	public S getSection() { return sectionProperty.get(); }
	public void setSection(S value) { sectionProperty.set(value); }
	//-------------------------------------------------------------------
	public ReadOnlyBooleanProperty selectedProperty() { return selected; }
	public boolean isSelected() { return selected.get(); }
	public void setSelected(boolean value) { selected.set(value); }

	//-------------------------------------------------------------------
    protected void updateItem(T item, boolean empty) {
    	logger.debug("***********updateItem("+item+")");
    }

//    //-------------------------------------------------------------------
//   /**
//     * Indicates whether or not this cell has been selected. For example, a
//     * ListView defines zero or more cells as being the "selected" cells.
//     */
//    public final ReadOnlyBooleanProperty selectedProperty() { return selected.getReadOnlyProperty(); }
//
//	//-------------------------------------------------------------------
//    void setSelected(boolean value) { selected.set(value); }
//    
//	//-------------------------------------------------------------------
//    /**
//     * Returns whether this cell is currently selected or not.
//     * @return True if the cell is selected, false otherwise.
//     */
//    public final boolean isSelected() { return selected.get(); }

    //-----------------------------------------------------------------
	private void clicked(MouseEvent event) {
		int index = listView.get().getItems().indexOf(getItem());

//		logger.debug("----> clicked: "+event.getClickCount());
		if (event.getClickCount()==1) {
			// Single click for selection
			boolean selectState = !isSelected();

			// Select
			logger.debug("Set selection state in model to "+selectState);
			if (selectState) {
				listView.get().getSelectionModel().clearSelection();;
				listView.get().getSelectionModel().select(index);
			} else
				listView.get().getSelectionModel().clearSelection(index);
			event.consume();
		} else {
			listView.get().fireEvent(event);
			if (actionHandler!=null) {
				ActionEvent actEv = new ActionEvent(this, event.getTarget());
				actionHandler.handle(actEv);
			}
		}
	};

    //-----------------------------------------------------------------
	public StringProperty textProperty() { return textProperty; }
	public void setText(String value) { textProperty.setValue(value);; }
	public String  getText() { return textProperty.get(); }

    //-----------------------------------------------------------------
	public ObjectProperty<Node> graphicProperty() { return graphicProperty; }
	public void setGraphic(Node value) { graphicProperty.set(value); }
	public Node getGraphic() { return graphicProperty.get(); }

	//-----------------------------------------------------------------
	public void setOnAction(EventHandler<ActionEvent> handler) {
		actionHandler = handler;
	}

}
