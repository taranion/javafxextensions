/**
 * 
 */
package org.prelle.javafx;

import org.prelle.javafx.skin.GridListViewSkin;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

/**
 * @author prelle
 *
 */
public class GridListView<T> extends ListView<T> {


    /***************************************************************************
     * Properties                                                              *
     **************************************************************************/

    // --- CellWidth
    private ObjectProperty<Double> cellWidth = new SimpleObjectProperty<Double>(150.0);

    public final void setCellWidth(double value) { cellWidthProperty().set(value); }
    public final Double getCellWidth() { return cellWidth.get(); }
    public final ObjectProperty<Double> cellWidthProperty() { return cellWidth; }

    // --- CellHeight
    private ObjectProperty<Double> cellHeight = new SimpleObjectProperty<Double>(150.0);

    public final void setCellHeight(double value) { cellHeightProperty().set(value); }
    public final Double getCellHeight() { return cellHeight.get(); }
    public final ObjectProperty<Double> cellHeightProperty() { return cellHeight; }

	//-------------------------------------------------------------------
	/**
	 */
	public GridListView() {
		// TODO Auto-generated constructor stub
		setSkin(new GridListViewSkin<>(this));
	}

	//-------------------------------------------------------------------
	/**
	 * @param items
	 */
	public GridListView(ObservableList<T> items) {
		super(items);
		setSkin(new GridListViewSkin<>(this));
	}

}
