package javafx.scene.layout;

import com.sun.javafx.binding.ExpressionHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.css.CssMetaData;
import javafx.css.StyleableDoubleProperty;
import javafx.css.StyleableIntegerProperty;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import com.sun.javafx.css.converters.EnumConverter;
import com.sun.javafx.css.converters.SizeConverter;
import com.sun.javafx.geom.Vec2d;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.css.Styleable;

import static javafx.geometry.Orientation.*;
import javafx.util.Callback;


/**
 * TilePane lays out its children in a grid of uniformly sized "tiles".
 * <p>
 * A horizontal tilepane (the default) will tile nodes in rows, wrapping at the
 * tilepane's width.  A vertical tilepane will tile nodes in columns,
 * wrapping at the tilepane's height.
 * <p>
 * The size of each "tile" defaults to the size needed to encompass the largest
 * preferred width and height of the tilepane's children and the tilepane
 * will recompute the size of the tiles as needed to accommodate the largest preferred
 * size of its children as it changes.   The application may also control the size
 * of the tiles directly by setting prefTileWidth/prefTileHeight
 * properties to a value other than USE_COMPUTED_SIZE (the default).
 * <p>
 * Applications should initialize either <code>prefColumns</code> (for horizontal)
 * or <code>prefRows</code> (for vertical) to establish the tilepane's preferred
 * size (the arbitrary default is 5).  Note that prefColumns/prefRows
 * is used only for calculating the preferred size and may not reflect the actual
 * number of rows or columns, which may change as the tilepane is resized and
 * the tiles are wrapped at its actual boundaries.
 * <p>
 * The alignment property controls how the rows and columns are aligned
 * within the bounds of the tilepane and defaults to Pos.TOP_LEFT.  It is also possible
 * to control the alignment of nodes within the individual tiles by setting
 * {@link #tileAlignmentProperty() tileAlignment}, which defaults to Pos.CENTER.
 * <p>
 * A horizontal tilepane example:
 * <pre><code>
 *    TilePane tile = new TilePane();
 *    tile.setHgap(8);
 *    tile.setPrefColumns(4);
 *    for (int i = 0; i < 20; i++) {
 *        tile.getChildren().add(new ImageView(...));
 *    }
 * </code></pre>
 * <p>
 * A vertical TilePane example:
 * <pre><code>
 *    TilePane tile = new TilePane(Orientation.VERTICAL);
 *    tile.setTileAlignment(Pos.CENTER_LEFT);
 *    tile.setPrefRows(10);
 *    for (int i = 0; i < 50; i++) {
 *        tile.getChildren().add(new ImageView(...));
 *    }
 * </code></pre>
 *
 * The TilePane will attempt to resize each child to fill its tile.
 * If the child could not be sized to fill the tile (either because it was not
 * resizable or its size limits prevented it) then it will be aligned within the
 * tile using tileAlignment.
 *
 * <h4>Resizable Range</h4>
 *
 * A tilepane's parent will resize the tilepane within the tilepane's resizable range
 * during layout.   By default the tilepane computes this range based on its content
 * as outlined in the tables below.
 * <p>
 * Horizontal:
 * <table border="1">
 * <tr><td></td><th>width</th><th>height</th></tr>
 * <tr><th>minimum</th>
 * <td>left/right insets plus the tile width.</td>
 * <td>top/bottom insets plus height required to display all tiles when wrapped at a specified width with a vgap between each row.</td></tr>
 * <tr><th>preferred</th>
 * <td>left/right insets plus prefColumns multiplied by the tile width.</td>
 * <td>top/bottom insets plus height required to display all tiles when wrapped at a specified width with a vgap between each row.</td></tr>
 * <tr><th>maximum</th>
 * <td>Double.MAX_VALUE</td><td>Double.MAX_VALUE</td></tr>
 * </table>
 * <p>
 * Vertical:
 * <table border="1">
 * <tr><td></td><th>width</th><th>height</th></tr>
 * <tr><th>minimum</th>
 * <td>left/right insets plus width required to display all tiles when wrapped at a specified height with an hgap between each column.</td>
 * <td>top/bottom insets plus the tile height.</td><tr>
 * <tr><th>preferred</th>
 * <td>left/right insets plus width required to display all tiles when wrapped at the specified height with an hgap between each column.</td>
 * <td>top/bottom insets plus prefRows multiplied by the tile height.</td><tr>
 * <tr><th>maximum</th>
 * <td>Double.MAX_VALUE</td><td>Double.MAX_VALUE</td></tr>
 * </table>
 * <p>
 * A tilepane's unbounded maximum width and height are an indication to the parent that
 * it may be resized beyond its preferred size to fill whatever space is assigned to it.
 * <p>
 * TilePane provides properties for setting the size range directly.  These
 * properties default to the sentinel value Region.USE_COMPUTED_SIZE, however the
 * application may set them to other values as needed:
 * <pre><code>
 *     <b>tilepane.setMaxWidth(500);</b>
 * </code></pre>
 * Applications may restore the computed values by setting these properties back
 * to Region.USE_COMPUTED_SIZE.
 * <p>
 * TilePane does not clip its content by default, so it is possible that childrens'
 * bounds may extend outside the tiles (and possibly the tilepane bounds) if a
 * child's pref size prevents it from being fit within its tile. Also, if the tilepane
 * is resized smaller than its preferred size, it may not be able to fit all the
 * tiles within its bounds and the content will extend outside.
 *
 * <h4>Optional Layout Constraints</h4>
 *
 * An application may set constraints on individual children to customize TilePane's layout.
 * For each constraint, TilePane provides a static method for setting it on the child.
 * <p>
 * <table border="1">
 * <tr><th>Constraint</th><th>Type</th><th>Description</th></tr>
 * <tr><td>alignment</td><td>javafx.geometry.Pos</td><td>The alignment of the child within its tile.</td></tr>
 * <tr><td>margin</td><td>javafx.geometry.Insets</td><td>Margin space around the outside of the child.</td></tr>
 * </table>
 * <p>
 * Example:
 * <pre><code>
 *     TilePane tilepane = new TilePane();
 *     for (int i = 0; i < 20; i++) {
 *        Label title = new Label(imageTitle[i]):
 *        Imageview imageview = new ImageView(new Image(imageName[i]));
 *        TilePane.setAlignment(label, Pos.BOTTOM_RIGHT);
 *        tilepane.getChildren().addAll(title, imageview);
 *     }
 * </code></pre>
 * @since JavaFX 2.0
 */
@SuppressWarnings("restriction")
public class MyTilePane extends Pane {

    /********************************************************************
     *  BEGIN static methods
     ********************************************************************/

    private static final String MARGIN_CONSTRAINT = "tilepane-margin";
    private static final String ALIGNMENT_CONSTRAINT = "tilepane-alignment";

    /**
     * Sets the alignment for the child when contained by a tilepane.
     * If set, will override the tilepane's default alignment for children
     * within their 'tiles'.
     * Setting the value to null will remove the constraint.
     * @param node the child node of a tilepane
     * @param value the alignment position for the child
     */
    public static void setAlignment(Node node, Pos value) {
        setConstraint(node, ALIGNMENT_CONSTRAINT, value);
    }

    /**
     * Returns the child's alignment constraint if set.
     * @param node the child node of a tilepane
     * @return the alignment position for the child or null if no alignment was set
     */
    public static Pos getAlignment(Node node) {
        return (Pos)getConstraint(node, ALIGNMENT_CONSTRAINT);
    }

    /**
     * Sets the margin for the child when contained by a tilepane.
     * If set, the tilepane will layout the child with the margin space around it.
     * Setting the value to null will remove the constraint.
     * @param node the child node of a tilepane
     * @param value the margin of space around the child
     */
    public static void setMargin(Node node, Insets value) {
        setConstraint(node, MARGIN_CONSTRAINT, value);
    }

    /**
     * Returns the child's margin constraint if set.
     * @param node the child node of a tilepane
     * @return the margin for the child or null if no margin was set
     */
    public static Insets getMargin(Node node) {
        return (Insets)getConstraint(node, MARGIN_CONSTRAINT);
    }

    private static final Callback<Node, Insets> marginAccessor = n -> getMargin(n);

    /**
     * Removes all tilepane constraints from the child node.
     * @param child the child node
     */
    public static void clearConstraints(Node child) {
        setAlignment(child, null);
        setMargin(child, null);
    }

    /********************************************************************
     *  END static methods
     ********************************************************************/

    private double _tileWidth = -1;
    private double _tileHeight = -1;

    /**
     * Creates a horizontal TilePane layout with prefColumn = 5 and hgap/vgap = 0.
     */
    public MyTilePane() {
        super();
    }

    /**
     * Creates a TilePane layout with the specified orientation,
     * prefColumn/prefRows = 5 and hgap/vgap = 0.
     * @param orientation the direction the tiles should flow & wrap
     */
    public MyTilePane(Orientation orientation) {
        super();
        setOrientation(orientation);
    }

    /**
     * Creates a horizontal TilePane layout with prefColumn = 5 and the specified
     * hgap/vgap.
     * @param hgap the amount of horizontal space between each tile
     * @param vgap the amount of vertical space between each tile
     */
    public MyTilePane(double hgap, double vgap) {
        super();
        setHgap(hgap);
        setVgap(vgap);
    }

    /**
     * Creates a TilePane layout with the specified orientation, hgap/vgap,
     * and prefRows/prefColumns = 5.
     * @param orientation the direction the tiles should flow & wrap
     * @param hgap the amount of horizontal space between each tile
     * @param vgap the amount of vertical space between each tile
     */
    public MyTilePane(Orientation orientation, double hgap, double vgap) {
        this();
        setOrientation(orientation);
        setHgap(hgap);
        setVgap(vgap);
    }

    /**
     * Creates a horizontal TilePane layout with prefColumn = 5 and hgap/vgap = 0.
     * @param children The initial set of children for this pane.
     * @since JavaFX 8.0
     */
    public MyTilePane(Node... children) {
        super();
        getChildren().addAll(children);
    }

    /**
     * Creates a TilePane layout with the specified orientation,
     * prefColumn/prefRows = 5 and hgap/vgap = 0.
     * @param orientation the direction the tiles should flow & wrap
     * @param children The initial set of children for this pane.
     * @since JavaFX 8.0
     */
    public MyTilePane(Orientation orientation, Node... children) {
        super();
        setOrientation(orientation);
        getChildren().addAll(children);
    }

    /**
     * Creates a horizontal TilePane layout with prefColumn = 5 and the specified
     * hgap/vgap.
     * @param hgap the amount of horizontal space between each tile
     * @param vgap the amount of vertical space between each tile
     * @param children The initial set of children for this pane.
     * @since JavaFX 8.0
     */
    public MyTilePane(double hgap, double vgap, Node... children) {
        super();
        setHgap(hgap);
        setVgap(vgap);
        getChildren().addAll(children);
    }

    /**
     * Creates a TilePane layout with the specified orientation, hgap/vgap,
     * and prefRows/prefColumns = 5.
     * @param orientation the direction the tiles should flow & wrap
     * @param hgap the amount of horizontal space between each tile
     * @param vgap the amount of vertical space between each tile
     * @param children The initial set of children for this pane.
     * @since JavaFX 8.0
     */
    public MyTilePane(Orientation orientation, double hgap, double vgap, Node... children) {
        this();
        setOrientation(orientation);
        setHgap(hgap);
        setVgap(vgap);
        getChildren().addAll(children);
    }

    /**
     * The orientation of this tilepane.
     * A horizontal tilepane lays out children in tiles, left to right, wrapping
     * tiles at the tilepane's width boundary.   A vertical tilepane lays out
     * children in tiles, top to bottom, wrapping at the tilepane's height.
     * The default is horizontal.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public final ObjectProperty<Orientation> orientationProperty() {
        if (orientation == null) {
            orientation = new StyleableObjectProperty(HORIZONTAL) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Orientation> getCssMetaData() {
                    return StyleableProperties.ORIENTATION;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "orientation";
                }
            };
        }
        return orientation;
    }

    private ObjectProperty<Orientation> orientation;
    public final void setOrientation(Orientation value) { orientationProperty().set(value); }
    public final Orientation getOrientation() { return orientation == null ? HORIZONTAL : orientation.get();  }


    /**
     * The preferred number of rows for a vertical tilepane.
     * This value is used only to compute the preferred size of the tilepane
     * and may not reflect the actual number of rows, which may change
     * if the tilepane is resized to something other than its preferred height.
     * This property is ignored for a horizontal tilepane.
     * <p>
     * It is recommended that the application initialize this value for a
     * vertical tilepane.
     */
    public final IntegerProperty prefRowsProperty() {
        if (prefRows == null) {
            prefRows = new StyleableIntegerProperty(5) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Number> getCssMetaData() {
                    return StyleableProperties.PREF_ROWS;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "prefRows";
                }
            };
        }
        return prefRows;
    }

    private IntegerProperty prefRows;
    public final void setPrefRows(int value) { prefRowsProperty().set(value); }
    public final int getPrefRows() { return prefRows == null ? 5 : prefRows.get(); }

    /**
     * The preferred number of columns for a horizontal tilepane.
     * This value is used only to compute the preferred size of the tilepane
     * and may not reflect the actual number of rows, which may change if the
     * tilepane is resized to something other than its preferred height.
     * This property is ignored for a vertical tilepane.
     * <p>
     * It is recommended that the application initialize this value for a
     * horizontal tilepane.
     */
    public final IntegerProperty prefColumnsProperty() {
        if (prefColumns == null) {
            prefColumns = new StyleableIntegerProperty(5) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Number> getCssMetaData() {
                    return StyleableProperties.PREF_COLUMNS;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "prefColumns";
                }
            };
        }
        return prefColumns;
    }

    private IntegerProperty prefColumns;
    public final void setPrefColumns(int value) { prefColumnsProperty().set(value); }
    public final int getPrefColumns() { return prefColumns == null ? 5 : prefColumns.get(); }

    /**
     * The preferred width of each tile.
     * If equal to USE_COMPUTED_SIZE (the default) the tile width wlll be
     * automatically recomputed by the tilepane when the preferred size of children
     * changes to accommodate the widest child.  If the application sets this property
     * to value greater than 0, then tiles will be set to that width and the tilepane
     * will attempt to resize children to fit within that width (if they are resizable and
     * their min-max width range allows it).
     */
    public final DoubleProperty prefTileWidthProperty() {
        if (prefTileWidth == null) {
            prefTileWidth = new StyleableDoubleProperty(USE_COMPUTED_SIZE) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Number> getCssMetaData() {
                    return StyleableProperties.PREF_TILE_WIDTH;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "prefTileWidth";
                }
            };
        }
        return prefTileWidth;
    }

    private DoubleProperty prefTileWidth;
    public final void setPrefTileWidth(double value) { prefTileWidthProperty().set(value); }
    public final double getPrefTileWidth() { return prefTileWidth == null ? USE_COMPUTED_SIZE : prefTileWidth.get(); }

    /**
     * The preferred height of each tile.
     * If equal to USE_COMPUTED_SIZE (the default) the tile height wlll be
     * automatically recomputed by the tilepane when the preferred size of children
     * changes to accommodate the tallest child.  If the application sets this property
     * to value greater than 0, then tiles will be set to that height and the tilepane
     * will attempt to resize children to fit within that height (if they are resizable and
     * their min-max height range allows it).
     */
    public final DoubleProperty prefTileHeightProperty() {
        if (prefTileHeight == null) {
            prefTileHeight = new StyleableDoubleProperty(USE_COMPUTED_SIZE) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Number> getCssMetaData() {
                    return StyleableProperties.PREF_TILE_HEIGHT;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "prefTileHeight";
                }
            };
        }
        return prefTileHeight;
    }

    private DoubleProperty prefTileHeight;
    public final void setPrefTileHeight(double value) { prefTileHeightProperty().set(value); }
    public final double getPrefTileHeight() { return prefTileHeight == null ? USE_COMPUTED_SIZE : prefTileHeight.get(); }

    /**
     * The actual width of each tile.  This property is read-only.
     */
    public final ReadOnlyDoubleProperty tileWidthProperty() {
        if (tileWidth == null) {
            tileWidth = new TileSizeProperty("tileWidth", _tileWidth) {

                @Override
                public double compute() {
                    return computeTileWidth();
                }

            };
        }
        return tileWidth;
    }
    private TileSizeProperty tileWidth;
    private void invalidateTileWidth() {
        if (tileWidth != null) {
            tileWidth.invalidate();
        } else {
            _tileWidth = -1;
        }
    }

    public final double getTileWidth() {
        if (tileWidth != null) {
            return tileWidth.get();
        }
        if (_tileWidth == -1) {
            _tileWidth = computeTileWidth();
        }
        return _tileWidth;
    }

    /**
     * The actual height of each tile.  This property is read-only.
     */
    public final ReadOnlyDoubleProperty tileHeightProperty() {
        if (tileHeight == null) {
            tileHeight = new TileSizeProperty("tileHeight", _tileHeight) {

                @Override
                public double compute() {
                    return computeTileHeight();
                }

            };
        }
        return tileHeight;
    }
    private TileSizeProperty tileHeight;
    private void invalidateTileHeight() {
        if (tileHeight != null) {
            tileHeight.invalidate();
        } else {
            _tileHeight = -1;
        }
    }

    public final double getTileHeight() {
        if (tileHeight != null) {
            return tileHeight.get();
        }
        if (_tileHeight == -1) {
            _tileHeight = computeTileHeight();
        }
        return _tileHeight;
    }

    /**
     * The amount of horizontal space between each tile in a row.
     */
    public final DoubleProperty hgapProperty() {
        if (hgap == null) {
            hgap = new StyleableDoubleProperty() {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Number> getCssMetaData() {
                    return StyleableProperties.HGAP;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "hgap";
                }
            };
        }
        return hgap;
    }

    private DoubleProperty hgap;
    public final void setHgap(double value) { hgapProperty().set(value); }
    public final double getHgap() { return hgap == null ? 0 : hgap.get(); }

    /**
     * The amount of vertical space between each tile in a column.
     */
    public final DoubleProperty vgapProperty() {
        if (vgap == null) {
            vgap = new StyleableDoubleProperty() {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Number> getCssMetaData() {
                    return StyleableProperties.VGAP;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "vgap";
                }
            };
        }
        return vgap;
    }

    private DoubleProperty vgap;
    public final void setVgap(double value) { vgapProperty().set(value); }
    public final double getVgap() { return vgap == null ? 0 : vgap.get(); }

    /**
     * The overall alignment of the tilepane's content within its width and height.
     * <p>For a horizontal tilepane, each row will be aligned within the tilepane's width
     * using the alignment's hpos value, and the rows will be aligned within the
     * tilepane's height using the alignment's vpos value.
     * <p>For a vertical tilepane, each column will be aligned within the tilepane's height
     * using the alignment's vpos value, and the columns will be aligned within the
     * tilepane's width using the alignment's hpos value.
     *
     */
    public final ObjectProperty<Pos> alignmentProperty() {
        if (alignment == null) {
            alignment = new StyleableObjectProperty<Pos>(Pos.TOP_LEFT) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Pos> getCssMetaData() {
                    return StyleableProperties.ALIGNMENT;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "alignment";
                }
            };
        }
        return alignment;
    }

    private ObjectProperty<Pos> alignment;
    public final void setAlignment(Pos value) { alignmentProperty().set(value); }
    public final Pos getAlignment() { return alignment == null ? Pos.TOP_LEFT : alignment.get(); }
    private Pos getAlignmentInternal() {
        Pos localPos = getAlignment();
        return localPos == null ? Pos.TOP_LEFT : localPos;
    }

    /**
     * The default alignment of each child within its tile.
     * This may be overridden on individual children by setting the child's
     * alignment constraint.
     */
    public final ObjectProperty<Pos> tileAlignmentProperty() {
        if (tileAlignment == null) {
            tileAlignment = new StyleableObjectProperty<Pos>(Pos.CENTER) {
                @Override
                public void invalidated() {
                    requestLayout();
                }

                @Override
                public CssMetaData<MyTilePane, Pos> getCssMetaData() {
                    return StyleableProperties.TILE_ALIGNMENT;
                }

                @Override
                public Object getBean() {
                    return MyTilePane.this;
                }

                @Override
                public String getName() {
                    return "tileAlignment";
                }
            };
        }
        return tileAlignment;
    }

    private ObjectProperty<Pos> tileAlignment;
    public final void setTileAlignment(Pos value) { tileAlignmentProperty().set(value); }
    public final Pos getTileAlignment() { return tileAlignment == null ? Pos.CENTER : tileAlignment.get(); }
    private Pos getTileAlignmentInternal() {
        Pos localPos = getTileAlignment();
        return localPos == null ? Pos.CENTER : localPos;
    }

    @Override public Orientation getContentBias() {
        return getOrientation();
    }

    @Override public void requestLayout() {
        invalidateTileWidth();
        invalidateTileHeight();
        super.requestLayout();
    }

    @Override protected double computeMinWidth(double height) {
        if (getContentBias() == Orientation.HORIZONTAL) {
            return getInsets().getLeft() + getTileWidth() + getInsets().getRight();
        }
        return computePrefWidth(height);
    }

    @Override protected double computeMinHeight(double width) {
        if (getContentBias() == Orientation.VERTICAL) {
            return getInsets().getTop() + getTileHeight() + getInsets().getBottom();
        }
        return computePrefHeight(width);
    }

    @Override protected double computePrefWidth(double forHeight) {
        List<Node> managed = getManagedChildren();
        final Insets insets = getInsets();
        int prefCols = 0;
        if (forHeight != -1) {
            // first compute number of rows that will fit in given height and
            // compute pref columns from that
            int prefRows = computeRows(forHeight - snapSpace(insets.getTop()) - snapSpace(insets.getBottom()), getTileHeight());
            prefCols = computeOther(managed.size(), prefRows);
        } else {
            prefCols = getOrientation() == HORIZONTAL? getPrefColumns() : computeOther(managed.size(), getPrefRows());
        }
        return snapSpace(insets.getLeft()) +
               computeContentWidth(prefCols, getTileWidth()) +
               snapSpace(insets.getRight());
    }

    @Override protected double computePrefHeight(double forWidth) {
        List<Node> managed = getManagedChildren();
        final Insets insets = getInsets();
        int prefRows = 0;
        if (forWidth != -1) {
            // first compute number of columns that will fit in given width and
            // compute pref rows from that
            int prefCols = computeColumns(forWidth - snapSpace(insets.getLeft()) - snapSpace(insets.getRight()), getTileWidth());
            prefRows = computeOther(managed.size(), prefCols);
        } else {
            prefRows = getOrientation() == HORIZONTAL? computeOther(managed.size(), getPrefColumns()) : getPrefRows();
        }
        return snapSpace(insets.getTop()) +
               computeContentHeight(prefRows, getTileHeight()) +
               snapSpace(insets.getBottom());
    }

    private double computeTileWidth() {
        List<Node> managed = getManagedChildren();
        double preftilewidth = getPrefTileWidth();
        if (preftilewidth == USE_COMPUTED_SIZE) {
            double h = -1;
            boolean vertBias = false;
            for (int i = 0, size = managed.size(); i < size; i++) {
                Node child = managed.get(i);
                if (child.getContentBias() == VERTICAL) {
                    vertBias = true;
                    break;
                }
            }
            if (vertBias) {
                // widest may depend on height of tile
                h = computeMaxPrefAreaHeight(managed, marginAccessor, -1, getTileAlignmentInternal().getVpos());
            }
            return snapSize(computeMaxPrefAreaWidth(managed, marginAccessor, h, true));
        }
        return snapSize(preftilewidth);
    }

    private double computeTileHeight() {
        List<Node> managed = getManagedChildren();
        double preftileheight = getPrefTileHeight();
        if (preftileheight == USE_COMPUTED_SIZE) {
            double w = -1;
            boolean horizBias = false;
            for (int i = 0, size = managed.size(); i < size; i++) {
                Node child = managed.get(i);
                if (child.getContentBias() == Orientation.HORIZONTAL) {
                    horizBias = true;
                    break;
                }
            }
            if (horizBias) {
                // tallest may depend on width of tile
                w = computeMaxPrefAreaWidth(managed, marginAccessor);
            }
            return snapSize(computeMaxPrefAreaHeight(managed, marginAccessor, w, getTileAlignmentInternal().getVpos()));
        }
        return snapSize(preftileheight);
    }

    private int computeOther(int numNodes, int numCells) {
        double other = (double)numNodes/(double)Math.max(1, numCells);
        return (int)Math.ceil(other);
    }

    private int computeColumns(double width, double tilewidth) {
        return Math.max(1,(int)((width + snapSpace(getHgap())) / (tilewidth + snapSpace(getHgap()))));
    }

    private int computeRows(double height, double tileheight) {
        return Math.max(1, (int)((height + snapSpace(getVgap())) / (tileheight + snapSpace(getVgap()))));
    }

    private double computeContentWidth(int columns, double tilewidth) {
        if (columns == 0) return 0;
        return columns * tilewidth + (columns - 1) * snapSpace(getHgap());
    }

    private double computeContentHeight(int rows, double tileheight) {
        if (rows == 0) return 0;
        return rows * tileheight + (rows - 1) * snapSpace(getVgap());
    }

    @Override protected void layoutChildren() {
        List<Node> managed = getManagedChildren();
        HPos hpos = getAlignmentInternal().getHpos();
        VPos vpos = getAlignmentInternal().getVpos();
        double width = getWidth();
        double height = getHeight();
        double top = snapSpace(getInsets().getTop());
        double left = snapSpace(getInsets().getLeft());
        double bottom = snapSpace(getInsets().getBottom());
        double right = snapSpace(getInsets().getRight());
        double vgap = snapSpace(getVgap());
        double hgap = snapSpace(getHgap());
        double insideWidth = width - left - right;
        double insideHeight = height - top - bottom;

        double tileWidth = getTileWidth() > insideWidth ? insideWidth : getTileWidth();
        double tileHeight = getTileHeight() > insideHeight ? insideHeight : getTileHeight();

        int lastRowRemainder = 0;
        int lastColumnRemainder = 0;
        if (getOrientation() == HORIZONTAL) {
            actualColumns.set(computeColumns(insideWidth, tileWidth));
            actualRows.set( computeOther(managed.size(), actualColumns.get()) );
            // remainder will be 0 if last row is filled
            lastRowRemainder = hpos != HPos.LEFT?
                 actualColumns.get() - (actualColumns.get()*actualRows.get() - managed.size()) : 0;
        } else {
            // vertical
            actualRows.set( computeRows(insideHeight, tileHeight) );
            actualColumns.set( computeOther(managed.size(), actualRows.get()));
            // remainder will be 0 if last column is filled
            lastColumnRemainder = vpos != VPos.TOP?
                actualRows.get() - (actualColumns.get()*actualRows.get() - managed.size()) : 0;
        }
        double rowX = left + computeXOffset(insideWidth,
                                            computeContentWidth(actualColumns.get(), tileWidth),
                                            hpos);
        double columnY = top + computeYOffset(insideHeight,
                                            computeContentHeight(actualRows.get(), tileHeight),
                                            vpos);

        double lastRowX = lastRowRemainder > 0?
                          left + computeXOffset(insideWidth,
                                            computeContentWidth(lastRowRemainder, tileWidth),
                                            hpos) :  rowX;
        double lastColumnY = lastColumnRemainder > 0?
                          top + computeYOffset(insideHeight,
                                            computeContentHeight(lastColumnRemainder, tileHeight),
                                            vpos) : columnY;
        double baselineOffset = getTileAlignmentInternal().getVpos() == VPos.BASELINE ?
                getAreaBaselineOffset(managed, marginAccessor, i -> tileWidth, tileHeight, false) : -1;

        int r = 0;
        int c = 0;
        for (int i = 0, size = managed.size(); i < size; i++) {
            Node child = managed.get(i);
            double xoffset = r == (actualRows.get() - 1)? lastRowX : rowX;
            double yoffset = c == (actualColumns.get() - 1)? lastColumnY : columnY;

            double tileX = xoffset + (c * (tileWidth + hgap));
            double tileY = yoffset + (r * (tileHeight + vgap));

            Pos childAlignment = getAlignment(child);

            layoutInArea(child, tileX, tileY, tileWidth, tileHeight, baselineOffset,
                    getMargin(child),
                    childAlignment != null? childAlignment.getHpos() : getTileAlignmentInternal().getHpos(),
                    childAlignment != null? childAlignment.getVpos() : getTileAlignmentInternal().getVpos());

            if (getOrientation() == HORIZONTAL) {
                if (++c == actualColumns.get()) {
                    c = 0;
                    r++;
                }
            } else {
                // vertical
                if (++r == actualRows.get()) {
                    r = 0;
                    c++;
                }
            }
        }
    }

    private IntegerProperty actualRows = new SimpleIntegerProperty(0);
    private IntegerProperty actualColumns = new SimpleIntegerProperty(0);
    
    public ReadOnlyIntegerProperty actualRowsProperty() { return actualRows; }
    public ReadOnlyIntegerProperty actualColumnsProperty() { return actualColumns; }

    /***************************************************************************
     *                                                                         *
     *                         Stylesheet Handling                             *
     *                                                                         *
     **************************************************************************/


     /**
      * Super-lazy instantiation pattern from Bill Pugh.
      * @treatAsPrivate implementation detail
      */
     private static class StyleableProperties {

         private static final CssMetaData<MyTilePane,Pos> ALIGNMENT =
             new CssMetaData<MyTilePane,Pos>("-fx-alignment",
                 new EnumConverter<Pos>(Pos.class),
                 Pos.TOP_LEFT) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.alignment == null || !node.alignment.isBound();
            }

            @Override
            public StyleableProperty<Pos> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Pos>)node.alignmentProperty();
            }
        };

         private static final CssMetaData<MyTilePane,Number> PREF_COLUMNS =
             new CssMetaData<MyTilePane,Number>("-fx-pref-columns",
                 SizeConverter.getInstance(), 5.0) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.prefColumns == null ||
                        !node.prefColumns.isBound();
            }

            @Override
            public StyleableProperty<Number> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Number>)node.prefColumnsProperty();
            }
        };

         private static final CssMetaData<MyTilePane,Number> HGAP =
             new CssMetaData<MyTilePane,Number>("-fx-hgap",
                 SizeConverter.getInstance(), 0.0) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.hgap == null ||
                        !node.hgap.isBound();
            }

            @Override
            public StyleableProperty<Number> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Number>)node.hgapProperty();
            }
        };

 		private static final CssMetaData<MyTilePane,Number> PREF_ROWS =
             new CssMetaData<MyTilePane,Number>("-fx-pref-rows",
                 SizeConverter.getInstance(), 5.0) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.prefRows == null ||
                        !node.prefRows.isBound();
            }

            @Override
            public StyleableProperty<Number> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Number>)node.prefRowsProperty();
            }
        };

         private static final CssMetaData<MyTilePane,Pos> TILE_ALIGNMENT =
             new CssMetaData<MyTilePane,Pos>("-fx-tile-alignment",
                 new EnumConverter<Pos>(Pos.class),
                 Pos.CENTER) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.tileAlignment == null ||
                        !node.tileAlignment.isBound();
            }

            @Override
            public StyleableProperty<Pos> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Pos>)node.tileAlignmentProperty();
            }
         };

         private static final CssMetaData<MyTilePane,Number> PREF_TILE_WIDTH =
             new CssMetaData<MyTilePane,Number>("-fx-pref-tile-width",
                 SizeConverter.getInstance(), USE_COMPUTED_SIZE) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.prefTileWidth == null ||
                        !node.prefTileWidth.isBound();
            }

            @Override
            public StyleableProperty<Number> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Number>)node.prefTileWidthProperty();
            }
        };

         private static final CssMetaData<MyTilePane,Number> PREF_TILE_HEIGHT =
             new CssMetaData<MyTilePane,Number>("-fx-pref-tile-height",
                 SizeConverter.getInstance(), USE_COMPUTED_SIZE) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.prefTileHeight == null ||
                        !node.prefTileHeight.isBound();
            }

            @Override
            public StyleableProperty<Number> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Number>)node.prefTileHeightProperty();
            }
         };

         private static final CssMetaData<MyTilePane,Orientation> ORIENTATION =
             new CssMetaData<MyTilePane,Orientation>("-fx-orientation",
                 new EnumConverter<Orientation>(Orientation.class),
                 Orientation.HORIZONTAL) {

                @Override
                public Orientation getInitialValue(MyTilePane node) {
                    // A vertical TilePane should remain vertical
                    return node.getOrientation();
                }

                @Override
                public boolean isSettable(MyTilePane node) {
                    return node.orientation == null ||
                            !node.orientation.isBound();
                }

                @Override
                public StyleableProperty<Orientation> getStyleableProperty(MyTilePane node) {
                    return (StyleableProperty<Orientation>)node.orientationProperty();
                }
         };

         private static final CssMetaData<MyTilePane,Number> VGAP =
             new CssMetaData<MyTilePane,Number>("-fx-vgap",
                 SizeConverter.getInstance(), 0.0) {

            @Override
            public boolean isSettable(MyTilePane node) {
                return node.vgap == null ||
                        !node.vgap.isBound();
            }

            @Override
            public StyleableProperty<Number> getStyleableProperty(MyTilePane node) {
                return (StyleableProperty<Number>)node.vgapProperty();
            }
        };

         private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;
         static {
            final List<CssMetaData<? extends Styleable, ?>> styleables =
                new ArrayList<CssMetaData<? extends Styleable, ?>>(Region.getClassCssMetaData());
            styleables.add(ALIGNMENT);
            styleables.add(HGAP);
            styleables.add(ORIENTATION);
            styleables.add(PREF_COLUMNS);
            styleables.add(PREF_ROWS);
            styleables.add(PREF_TILE_WIDTH);
            styleables.add(PREF_TILE_HEIGHT);
            styleables.add(TILE_ALIGNMENT);
            styleables.add(VGAP);
            STYLEABLES = Collections.unmodifiableList(styleables);
         }
    }

    /**
     * @return The CssMetaData associated with this class, which may include the
     * CssMetaData of its super classes.
     * @since JavaFX 8.0
     */
    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return StyleableProperties.STYLEABLES;
    }

    /**
     * {@inheritDoc}
     *
     * @since JavaFX 8.0
     */


    @Override
    public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
        return getClassCssMetaData();
    }

    private abstract class TileSizeProperty extends ReadOnlyDoubleProperty {
        private final String name;
        private ExpressionHelper<Number> helper;
        private double value;
        private boolean valid;

        TileSizeProperty(String name, double initSize) {
            this.name = name;
            this.value = initSize;
            this.valid = initSize != -1;
        }


        @Override
        public Object getBean() {
            return MyTilePane.this;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void addListener(InvalidationListener listener) {
            helper = ExpressionHelper.addListener(helper, this, listener);
        }

        @Override
        public void removeListener(InvalidationListener listener) {
            helper = ExpressionHelper.removeListener(helper, listener);
        }

        @Override
        public void addListener(ChangeListener<? super Number> listener) {
            helper = ExpressionHelper.addListener(helper, this, listener);
        }

        @Override
        public void removeListener(ChangeListener<? super Number> listener) {
            helper = ExpressionHelper.removeListener(helper, listener);
        }

        @Override
        public double get() {
            if (!valid) {
                value = compute();
                valid = true;
            }

            return value;
        }

        public void invalidate() {
            if (valid) {
                valid = false;
                ExpressionHelper.fireValueChangedEvent(helper);
            }
        }

        public abstract double compute();
    }

    //---------------------------------------------------------------
    //---------------------------------------------------------------
    //---------------------------------------------------------------


    double computeChildMinAreaWidth(Node child, Insets margin) {
        return computeChildMinAreaWidth(child, -1, margin, -1, false);
    }

    double computeChildMinAreaWidth(Node child, double baselineComplement, Insets margin, double height, boolean fillHeight) {
        final boolean snap = isSnapToPixel();
        double left = margin != null? snapSpace(margin.getLeft(), snap) : 0;
        double right = margin != null? snapSpace(margin.getRight(), snap) : 0;
        double alt = -1;
        if (height != -1 && child.isResizable() && child.getContentBias() == Orientation.VERTICAL) { // width depends on height
            double top = margin != null? snapSpace(margin.getTop(), snap) : 0;
            double bottom = (margin != null? snapSpace(margin.getBottom(), snap) : 0);
            double bo = child.getBaselineOffset();
            final double contentHeight = bo == BASELINE_OFFSET_SAME_AS_HEIGHT && baselineComplement != -1 ?
                    height - top - bottom - baselineComplement :
                     height - top - bottom;
            if (fillHeight) {
                alt = snapSize(boundedSize(
                        child.minHeight(-1), contentHeight,
                        child.maxHeight(-1)));
            } else {
                alt = snapSize(boundedSize(
                        child.minHeight(-1),
                        child.prefHeight(-1),
                        Math.min(child.maxHeight(-1), contentHeight)));
            }
        }
        return left + snapSize(child.minWidth(alt)) + right;
    }

    double computeChildMinAreaHeight(Node child, Insets margin) {
        return computeChildMinAreaHeight(child, -1, margin, -1);
    }

    double computeChildMinAreaHeight(Node child, double minBaselineComplement, Insets margin, double width) {
        final boolean snap = isSnapToPixel();
        double top =margin != null? snapSpace(margin.getTop(), snap) : 0;
        double bottom = margin != null? snapSpace(margin.getBottom(), snap) : 0;

        double alt = -1;
        if (child.isResizable() && child.getContentBias() == Orientation.HORIZONTAL) { // height depends on width
            double left = margin != null? snapSpace(margin.getLeft(), snap) : 0;
            double right = margin != null? snapSpace(margin.getRight(), snap) : 0;
            alt = snapSize(width != -1? boundedSize(child.minWidth(-1), width - left - right, child.maxWidth(-1)) :
                    child.maxWidth(-1));
        }

        // For explanation, see computeChildPrefAreaHeight
        if (minBaselineComplement != -1) {
            double baseline = child.getBaselineOffset();
            if (child.isResizable() && baseline == BASELINE_OFFSET_SAME_AS_HEIGHT) {
                return top + snapSize(child.minHeight(alt)) + bottom
                        + minBaselineComplement;
            } else {
                return baseline + minBaselineComplement;
            }
        } else {
            return top + snapSize(child.minHeight(alt)) + bottom;
        }
    }

    double computeChildPrefAreaWidth(Node child, Insets margin) {
        return computeChildPrefAreaWidth(child, -1, margin, -1, false);
    }

    double computeChildPrefAreaWidth(Node child, double baselineComplement, Insets margin, double height, boolean fillHeight) {
        final boolean snap = isSnapToPixel();
        double left = margin != null? snapSpace(margin.getLeft(), snap) : 0;
        double right = margin != null? snapSpace(margin.getRight(), snap) : 0;
        double alt = -1;
        if (height != -1 && child.isResizable() && child.getContentBias() == Orientation.VERTICAL) { // width depends on height
            double top = margin != null? snapSpace(margin.getTop(), snap) : 0;
            double bottom = margin != null? snapSpace(margin.getBottom(), snap) : 0;
            double bo = child.getBaselineOffset();
            final double contentHeight = bo == BASELINE_OFFSET_SAME_AS_HEIGHT && baselineComplement != -1 ?
                    height - top - bottom - baselineComplement :
                     height - top - bottom;
            if (fillHeight) {
                alt = snapSize(boundedSize(
                        child.minHeight(-1), contentHeight,
                        child.maxHeight(-1)));
            } else {
                alt = snapSize(boundedSize(
                        child.minHeight(-1),
                        child.prefHeight(-1),
                        Math.min(child.maxHeight(-1), contentHeight)));
            }
        }
        return left + snapSize(boundedSize(child.minWidth(alt), child.prefWidth(alt), child.maxWidth(alt))) + right;
    }

    double computeChildPrefAreaHeight(Node child, Insets margin) {
        return computeChildPrefAreaHeight(child, -1, margin, -1);
    }

    double computeChildPrefAreaHeight(Node child, double prefBaselineComplement, Insets margin, double width) {
        final boolean snap = isSnapToPixel();
        double top = margin != null? snapSpace(margin.getTop(), snap) : 0;
        double bottom = margin != null? snapSpace(margin.getBottom(), snap) : 0;

        double alt = -1;
        if (child.isResizable() && child.getContentBias() == Orientation.HORIZONTAL) { // height depends on width
            double left = margin != null ? snapSpace(margin.getLeft(), snap) : 0;
            double right = margin != null ? snapSpace(margin.getRight(), snap) : 0;
            alt = snapSize(boundedSize(
                    child.minWidth(-1), width != -1 ? width - left - right
                    : child.prefWidth(-1), child.maxWidth(-1)));
        }

        if (prefBaselineComplement != -1) {
            double baseline = child.getBaselineOffset();
            if (child.isResizable() && baseline == BASELINE_OFFSET_SAME_AS_HEIGHT) {
                // When baseline is same as height, the preferred height of the node will be above the baseline, so we need to add
                // the preferred complement to it
                return top + snapSize(boundedSize(child.minHeight(alt), child.prefHeight(alt), child.maxHeight(alt))) + bottom
                        + prefBaselineComplement;
            } else {
                // For all other Nodes, it's just their baseline and the complement.
                // Note that the complement already contain the Node's preferred (or fixed) height
                return top + baseline + prefBaselineComplement + bottom;
            }
        } else {
            return top + snapSize(boundedSize(child.minHeight(alt), child.prefHeight(alt), child.maxHeight(alt))) + bottom;
        }
    }

    double computeChildMaxAreaWidth(Node child, double baselineComplement, Insets margin, double height, boolean fillHeight) {
        double max = child.maxWidth(-1);
        if (max == Double.MAX_VALUE) {
            return max;
        }
        final boolean snap = isSnapToPixel();
        double left = margin != null? snapSpace(margin.getLeft(), snap) : 0;
        double right = margin != null? snapSpace(margin.getRight(), snap) : 0;
        double alt = -1;
        if (height != -1 && child.isResizable() && child.getContentBias() == Orientation.VERTICAL) { // width depends on height
            double top = margin != null? snapSpace(margin.getTop(), snap) : 0;
            double bottom = (margin != null? snapSpace(margin.getBottom(), snap) : 0);
            double bo = child.getBaselineOffset();
            final double contentHeight = bo == BASELINE_OFFSET_SAME_AS_HEIGHT && baselineComplement != -1 ?
                    height - top - bottom - baselineComplement :
                     height - top - bottom;
            if (fillHeight) {
                alt = snapSize(boundedSize(
                        child.minHeight(-1), contentHeight,
                        child.maxHeight(-1)));
            } else {
                alt = snapSize(boundedSize(
                        child.minHeight(-1),
                        child.prefHeight(-1),
                        Math.min(child.maxHeight(-1), contentHeight)));
            }
            max = child.maxWidth(alt);
        }
        // if min > max, min wins, so still need to call boundedSize()
        return left + snapSize(boundedSize(child.minWidth(alt), max, Double.MAX_VALUE)) + right;
    }

    double computeChildMaxAreaHeight(Node child, double maxBaselineComplement, Insets margin, double width) {
        double max = child.maxHeight(-1);
        if (max == Double.MAX_VALUE) {
            return max;
        }

        final boolean snap = isSnapToPixel();
        double top = margin != null? snapSpace(margin.getTop(), snap) : 0;
        double bottom = margin != null? snapSpace(margin.getBottom(), snap) : 0;
        double alt = -1;
        if (child.isResizable() && child.getContentBias() == Orientation.HORIZONTAL) { // height depends on width
            double left = margin != null? snapSpace(margin.getLeft(), snap) : 0;
            double right = margin != null? snapSpace(margin.getRight(), snap) : 0;
            alt = snapSize(width != -1? boundedSize(child.minWidth(-1), width - left - right, child.maxWidth(-1)) :
                child.minWidth(-1));
            max = child.maxHeight(alt);
        }
        // For explanation, see computeChildPrefAreaHeight
        if (maxBaselineComplement != -1) {
            double baseline = child.getBaselineOffset();
            if (child.isResizable() && baseline == BASELINE_OFFSET_SAME_AS_HEIGHT) {
                return top + snapSize(boundedSize(child.minHeight(alt), child.maxHeight(alt), Double.MAX_VALUE)) + bottom
                        + maxBaselineComplement;
            } else {
                return top + baseline + maxBaselineComplement + bottom;
            }
        } else {
            // if min > max, min wins, so still need to call boundedSize()
            return top + snapSize(boundedSize(child.minHeight(alt), max, Double.MAX_VALUE)) + bottom;
        }
    }

    /* Max of children's minimum area widths */

    double computeMaxMinAreaWidth(List<Node> children, Callback<Node, Insets> margins) {
        return getMaxAreaWidth(children, margins, new double[] { -1 }, false, true);
    }

    double computeMaxMinAreaWidth(List<Node> children, Callback<Node, Insets> margins, double height, boolean fillHeight) {
        return getMaxAreaWidth(children, margins, new double[] { height }, fillHeight, true);
    }

    double computeMaxMinAreaWidth(List<Node> children, Callback<Node, Insets> childMargins, double childHeights[], boolean fillHeight) {
        return getMaxAreaWidth(children, childMargins, childHeights, fillHeight, true);
    }

    /* Max of children's minimum area heights */

    double computeMaxMinAreaHeight(List<Node>children, Callback<Node, Insets> margins, VPos valignment) {
        return getMaxAreaHeight(children, margins, null, valignment, true);
    }

    double computeMaxMinAreaHeight(List<Node>children, Callback<Node, Insets> margins, VPos valignment, double width) {
        return getMaxAreaHeight(children, margins, new double[] { width }, valignment, true);
    }

    double computeMaxMinAreaHeight(List<Node>children, Callback<Node, Insets> childMargins, double childWidths[], VPos valignment) {
        return getMaxAreaHeight(children, childMargins, childWidths, valignment, true);
    }

    /* Max of children's pref area widths */

    double computeMaxPrefAreaWidth(List<Node>children, Callback<Node, Insets> margins) {
        return getMaxAreaWidth(children, margins, new double[] { -1 }, false, false);
    }

    double computeMaxPrefAreaWidth(List<Node>children, Callback<Node, Insets> margins, double height,
            boolean fillHeight) {
        return getMaxAreaWidth(children, margins, new double[] { height }, fillHeight, false);
    }

    double computeMaxPrefAreaWidth(List<Node>children, Callback<Node, Insets> childMargins,
            double childHeights[], boolean fillHeight) {
        return getMaxAreaWidth(children, childMargins, childHeights, fillHeight, false);
    }

    /* Max of children's pref area heights */

    double computeMaxPrefAreaHeight(List<Node>children, Callback<Node, Insets> margins, VPos valignment) {
        return getMaxAreaHeight(children, margins, null, valignment, false);
    }

    double computeMaxPrefAreaHeight(List<Node>children, Callback<Node, Insets> margins, double width, VPos valignment) {
        return getMaxAreaHeight(children, margins, new double[] { width }, valignment, false);
    }

    double computeMaxPrefAreaHeight(List<Node>children, Callback<Node, Insets> childMargins, double childWidths[], VPos valignment) {
        return getMaxAreaHeight(children, childMargins, childWidths, valignment, false);
    }

    /**
     * Returns the size of a Node that should be placed in an area of the specified size,
     * bounded in it's min/max size, respecting bias.
     *
     * @param node the node
     * @param areaWidth the width of the bounding area where the node is going to be placed
     * @param areaHeight the height of the bounding area where the node is going to be placed
     * @param fillWidth if Node should try to fill the area width
     * @param fillHeight if Node should try to fill the area height
     * @param result Vec2d object for the result or null if new one should be created
     * @return Vec2d object with width(x parameter) and height (y parameter)
     */
    static Vec2d boundedNodeSizeWithBias(Node node, double areaWidth, double areaHeight,
            boolean fillWidth, boolean fillHeight, Vec2d result) {
        if (result == null) {
            result = new Vec2d();
        }

        Orientation bias = node.getContentBias();

        double childWidth = 0;
        double childHeight = 0;

        if (bias == null) {
            childWidth = boundedSize(
                    node.minWidth(-1), fillWidth ? areaWidth
                    : Math.min(areaWidth, node.prefWidth(-1)),
                    node.maxWidth(-1));
            childHeight = boundedSize(
                    node.minHeight(-1), fillHeight ? areaHeight
                    : Math.min(areaHeight, node.prefHeight(-1)),
                    node.maxHeight(-1));

        } else if (bias == Orientation.HORIZONTAL) {
            childWidth = boundedSize(
                    node.minWidth(-1), fillWidth ? areaWidth
                    : Math.min(areaWidth, node.prefWidth(-1)),
                    node.maxWidth(-1));
            childHeight = boundedSize(
                    node.minHeight(childWidth), fillHeight ? areaHeight
                    : Math.min(areaHeight, node.prefHeight(childWidth)),
                    node.maxHeight(childWidth));

        } else { // bias == VERTICAL
            childHeight = boundedSize(
                    node.minHeight(-1), fillHeight ? areaHeight
                    : Math.min(areaHeight, node.prefHeight(-1)),
                    node.maxHeight(-1));
            childWidth = boundedSize(
                    node.minWidth(childHeight), fillWidth ? areaWidth
                    : Math.min(areaWidth, node.prefWidth(childHeight)),
                    node.maxWidth(childHeight));
        }

        result.set(childWidth, childHeight);
        return result;
    }

    /* utility method for computing the max of children's min or pref heights, taking into account baseline alignment */
    private double getMaxAreaHeight(List<Node> children, Callback<Node,Insets> childMargins,  double childWidths[], VPos valignment, boolean minimum) {
        final double singleChildWidth = childWidths == null ? -1 : childWidths.length == 1 ? childWidths[0] : Double.NaN;
        if (valignment == VPos.BASELINE) {
            double maxAbove = 0;
            double maxBelow = 0;
            for (int i = 0, maxPos = children.size(); i < maxPos; i++) {
                final Node child = children.get(i);
                final double childWidth = Double.isNaN(singleChildWidth) ? childWidths[i] : singleChildWidth;
                Insets margin = childMargins.call(child);
                final double top = margin != null? snapSpace(margin.getTop()) : 0;
                final double bottom = margin != null? snapSpace(margin.getBottom()) : 0;
                final double baseline = child.getBaselineOffset();

                final double childHeight = minimum? snapSize(child.minHeight(childWidth)) : snapSize(child.prefHeight(childWidth));
                if (baseline == BASELINE_OFFSET_SAME_AS_HEIGHT) {
                    maxAbove = Math.max(maxAbove, childHeight + top);
                } else {
                    maxAbove = Math.max(maxAbove, baseline + top);
                    maxBelow = Math.max(maxBelow,
                            snapSpace(minimum?snapSize(child.minHeight(childWidth)) : snapSize(child.prefHeight(childWidth))) -
                            baseline + bottom);
                }
            }
            return maxAbove + maxBelow; //remind(aim): ceil this value?
        } else {
            double max = 0;
            for (int i = 0, maxPos = children.size(); i < maxPos; i++) {
                final Node child = children.get(i);
                Insets margin = childMargins.call(child);
                final double childWidth = Double.isNaN(singleChildWidth) ? childWidths[i] : singleChildWidth;
                max = Math.max(max, minimum?
                    computeChildMinAreaHeight(child, -1, margin, childWidth) :
                        computeChildPrefAreaHeight(child, -1, margin, childWidth));
            }
            return max;
        }
    }

    /* utility method for computing the max of children's min or pref width, horizontal alignment is ignored for now */
    private double getMaxAreaWidth(List<javafx.scene.Node> children,
            Callback<Node, Insets> childMargins, double childHeights[], boolean fillHeight, boolean minimum) {
        final double singleChildHeight = childHeights == null ? -1 : childHeights.length == 1 ? childHeights[0] : Double.NaN;

        double max = 0;
        for (int i = 0, maxPos = children.size(); i < maxPos; i++) {
            final Node child = children.get(i);
            final Insets margin = childMargins.call(child);
            final double childHeight = Double.isNaN(singleChildHeight) ? childHeights[i] : singleChildHeight;
            max = Math.max(max, minimum?
                computeChildMinAreaWidth(children.get(i), -1, margin, childHeight, fillHeight) :
                    computeChildPrefAreaWidth(child, -1, margin, childHeight, fillHeight));
        }
        return max;
    }

    /**
     * If snapToPixel is true, then the value is rounded using Math.round. Otherwise,
     * the value is simply returned. This method will surely be JIT'd under normal
     * circumstances, however on an interpreter it would be better to inline this
     * method. However the use of Math.round here, and Math.ceil in snapSize is
     * not obvious, and so for code maintenance this logic is pulled out into
     * a separate method.
     *
     * @param value The value that needs to be snapped
     * @param snapToPixel Whether to snap to pixel
     * @return value either as passed in or rounded based on snapToPixel
     */
    private static double snapSpace(double value, boolean snapToPixel) {
        return snapToPixel ? Math.round(value) : value;
    }

    /**
     * If snapToPixel is true, then the value is ceil'd using Math.ceil. Otherwise,
     * the value is simply returned.
     *
     * @param value The value that needs to be snapped
     * @param snapToPixel Whether to snap to pixel
     * @return value either as passed in or ceil'd based on snapToPixel
     */
    private static double snapSize(double value, boolean snapToPixel) {
        return snapToPixel ? Math.ceil(value) : value;
    }

    /***************************************************************************
     *                                                                         *
     * Static convenience methods for layout                                   *
     *                                                                         *
     **************************************************************************/

    /**
     * Computes the value based on the given min and max values. We encode in this
     * method the logic surrounding various edge cases, such as when the min is
     * specified as greater than the max, or the max less than the min, or a pref
     * value that exceeds either the max or min in their extremes.
     * <p/>
     * If the min is greater than the max, then we want to make sure the returned
     * value is the min. In other words, in such a case, the min becomes the only
     * acceptable return value.
     * <p/>
     * If the min and max values are well ordered, and the pref is less than the min
     * then the min is returned. Likewise, if the values are well ordered and the
     * pref is greater than the max, then the max is returned. If the pref lies
     * between the min and the max, then the pref is returned.
     *
     *
     * @param min The minimum bound
     * @param pref The value to be clamped between the min and max
     * @param max the maximum bound
     * @return the size bounded by min, pref, and max.
     */
    static double boundedSize(double min, double pref, double max) {
        double a = pref >= min ? pref : min;
        double b = min >= max ? min : max;
        return a <= b ? a : b;
    }


    static double computeXOffset(double width, double contentWidth, HPos hpos) {
        switch(hpos) {
            case LEFT:
                return 0;
            case CENTER:
                return (width - contentWidth) / 2;
            case RIGHT:
                return width - contentWidth;
            default:
                throw new AssertionError("Unhandled hPos");
        }
    }

    static double computeYOffset(double height, double contentHeight, VPos vpos) {
        switch(vpos) {
            case BASELINE:
            case TOP:
                return 0;
            case CENTER:
                return (height - contentHeight) / 2;
            case BOTTOM:
                return height - contentHeight;
            default:
                throw new AssertionError("Unhandled vPos");
        }
    }

    static double[] createDoubleArray(int length, double value) {
        double[] array = new double[length];
        for (int i = 0; i < length; i++) {
            array[i] = value;
        }
        return array;
    }

    static Object getConstraint(Node node, Object key) {
        if (node.hasProperties()) {
            Object value = node.getProperties().get(key);
            if (value != null) {
                return value;
            }
        }
        return null;
    }

}
